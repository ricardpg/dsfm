#include"mex.h"
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<assert.h>

#define greater(a,b) ((a) > (b))
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define PAD_BY_CONTINUITY



void
mexFunction(int nout, mxArray *out[], 
            int nin, const mxArray *in[])
{
  int feat1items, feat2items, dimensions, ic1, ic2, dd;
  double* feat1array;
  double* feat2array;
  double* variance;
  double* distarray;
  double dst;
  enum {feat1=0,feat2,var} ;
  enum {dist=0} ;

  /* ------------------------------------------------------------------
  **                                                Check the arguments
  ** --------------------------------------------------------------- */ 
  if (nin != 2) {
    mexErrMsgTxt("Exactly two input arguments required.");
  } else if (nout > 1) {
    mexErrMsgTxt("Too many output arguments.");
  }
  
  dimensions = mxGetM(in[feat1]);
  feat1items = mxGetN(in[feat1]);
  feat1array = mxGetPr(in[feat1]);
  feat2items = mxGetN(in[feat2]);
  feat2array = mxGetPr(in[feat2]);
  out[dist] = mxCreateDoubleMatrix(feat1items, feat2items, mxREAL) ;
  distarray = mxGetPr(out[dist]);  
  
  for(ic1 = 0; ic1 < feat1items; ic1++){
      for(ic2 = 0; ic2 < feat2items; ic2++){
          dst = 0;
          for(dd=0; dd<dimensions; dd++){
             dst += ((feat1array[ic1*dimensions+dd] - feat2array[ic2*dimensions+dd]) * (feat1array[ic1*dimensions+dd] - feat2array[ic2*dimensions+dd]));
          }
          distarray[ic2*feat1items+ic1] = sqrt(dst);
      }
  //  printf("\n");

  }
  
}
