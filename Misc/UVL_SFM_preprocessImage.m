function [I H W D] = UVL_SFM_preprocessImage(I, CurFrame, Processing, CachePath )

if ~exist('CachePath', 'var'), CachePath = './Cache'; end

    I = im2double(I);
    [H W D] = size(I);
    if D ~= 1,
        switch Processing.SingleChannel,
            case 0, %None
                error('Conversion to SingleChannel needed.');
            case 1, %Red
                I = I(:,:,1);
            case 2, %Green
                I = I(:,:,2);
            case 3, %Bleu
                I = I(:,:,3);
            case 4, %Grayscale
                I = rgb2gray(I);
            case 5, %PCA
                [GrayImgs, Vt, Dt, mk] = pca(I, []);
                I = GrayImgs(:,:,3);
            case 6, %Y chanel of YIQ
                YIQ = rgb2ntsc(I);
                I = YIQ(:,:,1);
            case 7, %I chanel of YIQ
                YIQ = rgb2ntsc(I);
                I = YIQ(:,:,2);
            case 8, %Q chanel of YIQ
                YIQ = rgb2ntsc(I);
                I = YIQ(:,:,3);
            case 9, %H chanel of HSV
                HSV = rgb2hsv(I);
                I = HSV(:,:,1);
            case 10, %S chanel of HSV
                HSV = rgb2hsv(I);
                I = HSV(:,:,2);
            case 11, %V chanel of HSV
                HSV = rgb2hsv(I);
                I = HSV(:,:,3);
            otherwise,
                error('Incorrect SingleChannel Parameter.');
        end
    end

    if Processing.NonUniformLightCorrection
        filter = imfilter(I, fspecial('gaussian', W/2, W/12), 'replicate');
        I = I ./ filter;
        I = I -  min(I(:));
        I = I ./ max(I(:)); %normalized between 0 and 1
    end
    
    if Processing.GaussianSize > 1,
        if Processing.GaussianSize > 9,
            error('Too big gaussian.');
        else
            Gaussian = fspecial('gaussian', Processing.GaussianSize, Processing.GaussianSize/6);
            I = conv2(I, Gaussian, 'same');
        end
    end

    switch Processing.Equalization,
        case 0, % None
        case 1, % Normalization
            I = I - min(I(:));
            I = I ./ max(I(:));
        case 2, % Equalization
            %Histogram = imhist(I);
            I = histeq(I, 255); 
        case 3, % Clahe
            I = adapthisteq(I);
        case 4, % CLAHS
            Nr = 16;     % number of CLAHS subregions along X and Y axis, def=8
            Clim = 2;     % Normalized clip limit, (higher values = more contrast); value <= 1 = standard AHE
            ImProc = uint16(65535 * imcrop(I, [0, 0, Nr*fix(W/Nr), Nr*fix(H/Nr)]));
            I = mat2gray(double(clahs(ImProc, Nr, Nr, 'cliplimit', Clim)));
        otherwise,
            error('Incorrect Equalization Parameter.');
    end 
    
    I = imresize(I, Processing.ResizeCoeff, 'bilinear');
    
    FileName = sprintf('%s/I%06dP%d%d%d%d%d.png', CachePath, CurFrame, Processing.SingleChannel, Processing.NonUniformLightCorrection, Processing.GaussianSize, Processing.Equalization, Processing.ResizeCoeff);
    imwrite(I, FileName);
end