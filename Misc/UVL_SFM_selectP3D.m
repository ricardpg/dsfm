function P3D = UVL_SFM_selectP3D(P3D, MinViews)

ErasedPoints = 0;

for Group = 1 : numel(P3D)
    for pp = find((P3D(Group).Status == 3) & P3D(Group).TotalFrames < MinViews)
        LocalIndices = P3D(Group).DescIndex{pp};
        P3D(Group).Status(:,pp) = uint8(0);                     % Free up the point
        P3D(Group).ViewIndex(LocalIndices) = uint16(0);  % free up the slot
        P3D(Group).DescIndex{pp} = [];        % Reset indices
        P3D(Group).TotalFrames(pp) = 0;
        ErasedPoints = ErasedPoints + 1;
    end
end

UVL_SFM_printOut(sprintf(' %d removed', ErasedPoints));