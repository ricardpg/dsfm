#include"mex.h"
// #include<stdlib.h>
// #include<string.h>
#include<math.h>
// #include<assert.h>
// 
// #define greater(a,b) ((a) > (b))
// #define min(a,b) (((a)<(b))?(a):(b))
// #define max(a,b) (((a)>(b))?(a):(b))
// 
// #define PAD_BY_CONTINUITY


/* convolve and transopose */

void mexFunction(int nout, mxArray *out[], int nin, const mxArray *in[])
{
  int feat1items, feat2items, dimensions, ic1, ic2, dd;
  double *variance, *distarray, *feat1array, *feat2array, ff, dst;
  enum {feat1=0,feat2,var} ;
  enum {dist=0} ;

  /* ------------------------------------------------------------------
  **                                                Check the arguments
  ** --------------------------------------------------------------- */ 
  if (nin != 3) {
    mexErrMsgTxt("Exactly three input arguments required.");
  } else if (nout > 1) {
    mexErrMsgTxt("Too many output arguments.");
  }
  
//  if (!mxIsDouble(in[feat1]) || !mxIsDouble(in[var1]) ||!mxIsDouble(in[feat2]) || !mxIsDouble(in[var2])) {
//    mexErrMsgTxt("All arguments must be real.") ;
//  }
  dimensions = mxGetM(in[feat1]);
  feat1items = mxGetN(in[feat1]);
  feat1array = mxGetPr(in[feat1]);
  feat2items = mxGetN(in[feat2]);
  feat2array = mxGetPr(in[feat2]);
  variance = mxGetPr(in[var]);
  out[dist] = mxCreateDoubleMatrix(feat1items, feat2items, mxREAL) ;
  distarray = mxGetPr(out[dist]);  
  
    for(ic2 = 0; ic2 < feat2items; ++ic2, feat2array += dimensions){
      for(ic1 = 0; ic1 < feat1items; ++ic1, feat1array += dimensions){
          dst = 0;
          for(dd = 0; dd < dimensions; ++dd){
              ff = feat1array[dd] - feat2array[dd];
              dst += ff * ff / variance[dd];
          }
          *distarray++ = sqrt(dst);
      }
      feat1array -= feat1items*dimensions;
  }
  
  
  
  
  
  
  
  
  
  
//   for(ic2 = 0; ic2 < feat2items; ++ic2){
//       for(ic1 = 0; ic1 < feat1items; ++ic1){
//           dst = 0;
//           for(dd=0; dd<dimensions; dd++){
//               dst += (*f1 - *f2) * (*f1 - *f2) / variance[dd];
//               f1++;
//               f2++;
//           }
//           *distarray = sqrt(dst);
//           distarray++;
//           f2 -= dimensions;
//       }
//       f1 = feat1array;
//       f2 += dimensions;
//   }
        
        
  
  
  
//   for(ic1 = 0; ic1 < feat1items; ic1++){
//       for(ic2 = 0; ic2 < feat2items; ic2++){
//           dst = 0;
//           for(dd=0; dd<dimensions; dd++){
// //             dst += ((feat1array[ic1*dimensions+dd] - feat2array[ic2*dimensions+dd]) * (feat1array[ic1*dimensions+dd] - feat2array[ic2*dimensions+dd])) / (var1array[ic1*dimensions+dd] + var2array[ic2*dimensions+dd]);
//              dst += ((feat1array[ic1*dimensions+dd] - feat2array[ic2*dimensions+dd]) * (feat1array[ic1*dimensions+dd] - feat2array[ic2*dimensions+dd])) / variance[dd];
//           }
//           distarray[ic2*feat1items+ic1] = sqrt(dst);
//       }
//   //  printf("\n");
// 
//   }
             
          
  
//  for (i=0;i<feat1items;i++)
//      for (j=0;j<dimensions;j++){
//          printf("%f\n",feat1array[i+j*dimensions]);
//          printf("dim: %d, feat %d\n ",dimensions,feat1items);
          
//      }
 
//              euclidian_norm = sqrt(sum((clusters(jj).centroid - clusters(ii).centroid).^2 ./ (clusters(jj).var + clusters(ii).var)));

  
/*  if(max(mxGetM(in[S]),mxGetN(in[S])) > 1) {
    mexErrMsgTxt("S must be a scalar.\n") ;
  }
*/
//  M = mxGetM(in[I]) ;
//  N = mxGetN(in[I]) ;
/*
  out[J] = mxCreateDoubleMatrix(M, N, mxREAL) ;
  
  I_pt   = mxGetPr(in[I]) ;
  J_pt   = mxGetPr(out[J]) ;
  s      = *mxGetPr(in[S]) ;

  if(s > 0.01) {
    
    int W = (int) ceil(4*s) ;
    int j ;
    double* g0 = (double*) mxMalloc( (2*W+1)*sizeof(double) ) ;
    double* buffer = (double*) mxMalloc( M*N*sizeof(double) ) ;
    double acc=0.0 ;
    
    for(j = 0 ; j < 2*W+1 ; ++j) {
      g0[j] = exp(-0.5 * (j - W)*(j - W)/(s*s)) ;
      acc += g0[j] ;
    }
    for(j = 0 ; j < 2*W+1 ; ++j) {
      g0[j] /= acc ;
    }

#if defined PAD_BY_CONTINUITY

    econvolve(buffer, I_pt, M, N, g0, W) ;
    econvolve(J_pt, buffer, N, M, g0, W) ;

#else
    
    convolve(buffer, I_pt, M, N, g0, W) ;
    convolve(J_pt, buffer, N, M, g0, W) ;

#endif
  
    mxFree(buffer) ;
    mxFree(g0) ;
  } else {
    memcpy(J_pt, I_pt, sizeof(double)*M*N) ;   
  }
*/  
}
