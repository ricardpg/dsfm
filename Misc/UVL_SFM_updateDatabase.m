function [P3D Updated] = UVL_SFM_updateDatabase(P3D, Features, CurFrame, GeneralParams)
Updated = [];

% for all feature groups
for FeatureType = 1:numel(Features)
    Group = find([P3D(:).PointType] == Features(FeatureType).PointType & [P3D(:).DescriptorType] == Features(FeatureType).DescriptorType);
    FeaturesGroup = Features(FeatureType).Points;
    DescriptorsGroup = Features(FeatureType).Descriptors;
    PairsGroup = Features(FeatureType).Pairs;
    
    %% Index same features (assumed as adjacent)
    index = 1;
    IndexTable = zeros(1, size(FeaturesGroup, 2));
    IndexTable(1) = 1;
    for count = 2 : size(FeaturesGroup, 2)
        if norm(FeaturesGroup(1:2,count-1)-FeaturesGroup(1:2,count)) % if points are different
            index = index + 1;
        end
        IndexTable(count) = index;
    end
    FreeIndivPointPos = find(P3D(Group).ViewIndex == 0, size(FeaturesGroup, 2));
    if numel(FreeIndivPointPos) < size(FeaturesGroup, 2),
        P3D(Group).ViewIndex(end + 1 : end + GeneralParams.PreAllocateDescriptors) = 0;
        P3D(Group).ViewPos(:, end + 1 : end + GeneralParams.PreAllocateDescriptors) = 0;
        P3D(Group).ViewDesc(:, end + 1 : end + GeneralParams.PreAllocateDescriptors) = 0;
        P3D(Group).ViewSaliency(:, end + 1 : end + GeneralParams.PreAllocateDescriptors) = 0;
        P3D(Group).ViewP3DIndex(:, end + 1 : end + GeneralParams.PreAllocateDescriptors) = 0;        
        FreeIndivPointPos = find(P3D(Group).ViewIndex == 0, size(FeaturesGroup, 2));
    end

    if numel(PairsGroup)
        %% remove duplicate Pairs
        [B,IX] = sort(PairsGroup(3,:), 2); % is it usefull ? unique is doing a sort also
        PairsGroup = PairsGroup(:,IX);
        [A m] = unique(PairsGroup(2,:), 'first');
        PairsGroup = PairsGroup(:, m);

        %% Remove pairs representing the same feature
        [B,IX] = sort(PairsGroup(3,:), 2);
        PairsGroup = PairsGroup(:,IX);
        [A m] = unique(IndexTable(PairsGroup(1, :)), 'first');
        PairsGroup = PairsGroup(:, m);
        

        %% Update the database with the matched points and remove them
        NotTaken = true(1, size(FeaturesGroup, 2));
        for count = 1 : size(PairsGroup, 2)
            DBItem = PairsGroup(2,count);
            LocalItem = PairsGroup(1,count);
            NewViewIndex = FreeIndivPointPos(count);
            LocalIndexes = P3D(Group).DescIndex{DBItem};
            if any(P3D(Group).ViewIndex(LocalIndexes) == CurFrame)
                warning('MATLAB:UVL_SFM_updateDatabase', 'Duplicate Database entries!');
            end
            
            P3D(Group).ViewIndex(NewViewIndex) = CurFrame;
            P3D(Group).ViewPos(:, NewViewIndex) = FeaturesGroup(1:2, LocalItem);
            P3D(Group).ViewSaliency(:, NewViewIndex) = FeaturesGroup(end, LocalItem);            
            P3D(Group).ViewDesc(:, NewViewIndex) = DescriptorsGroup(:, LocalItem) * 2 ^ 15;
            P3D(Group).ViewP3DIndex(NewViewIndex) = DBItem;
            LocalIndices = [P3D(Group).DescIndex{DBItem} NewViewIndex];
            LocalViews = P3D(Group).ViewIndex(LocalIndices);
            [Dmy, Idx] = sort(LocalViews);
            P3D(Group).DescIndex{DBItem} = LocalIndices(Idx);
            P3D(Group).Desc(:, DBItem) = (P3D(Group).Desc(:, DBItem) * single(P3D(Group).TotalFrames(DBItem)) + single(DescriptorsGroup(:, LocalItem))) / ( single(P3D(Group).TotalFrames(:, DBItem)) + 1 );
            P3D(Group).TotalFrames(:, DBItem) = numel(LocalIndices);
            CurIndex = IndexTable(LocalItem);
            Taken = IndexTable == CurIndex;
            NotTaken(Taken) = false;
            Updated = [Updated [Group DBItem]'];
        end
        FreeIndivPointPos = FreeIndivPointPos(size(PairsGroup, 2)+1 : end);
        FeaturesGroup = FeaturesGroup(:, NotTaken);
        DescriptorsGroup = DescriptorsGroup(:, NotTaken);
        IndexTable = IndexTable(NotTaken);
    end


    %% Remove the duplicate features
    [A m] = unique(IndexTable, 'first');
    FeaturesGroup = FeaturesGroup(:, m);
    DescriptorsGroup = DescriptorsGroup(:, m);

    if size(FeaturesGroup, 2), %if there is still feature not matched
        Free3DPointPos = find(P3D(Group).Status == 0, size(FeaturesGroup, 2));
        if numel(Free3DPointPos) < size(FeaturesGroup, 2)
            P3D(Group).Status(end + 1 : end + GeneralParams.PreAllocatePoints) = 0;
            P3D(Group).IsGeometrical(end + 1 : end + GeneralParams.PreAllocatePoints) = false;
            P3D(Group).IsInitial(end + 1 : end + GeneralParams.PreAllocatePoints) = false;
            P3D(Group).Cov(end + 1 : end + GeneralParams.PreAllocatePoints) = 0;
            P3D(Group).Pos(:, end + 1 : end + GeneralParams.PreAllocatePoints) = 0;
            P3D(Group).DescIndex(end + 1 : end + GeneralParams.PreAllocatePoints) = cell(1, GeneralParams.PreAllocatePoints);
            P3D(Group).TotalFrames(end + 1 : end + GeneralParams.PreAllocatePoints) = 0;
            P3D(Group).Desc(:, end + 1 : end + GeneralParams.PreAllocatePoints) = 0;
            Free3DPointPos = find(P3D(Group).Status == 0, size(FeaturesGroup, 2));
        end

        %% Update the database with the remaining features
        for count = 1 : size(FeaturesGroup, 2)
            NewIndex = Free3DPointPos(count);
            NewViewIndex = FreeIndivPointPos(count);

            P3D(Group).Status(NewIndex) = 2;
            P3D(Group).IsGeometrical(NewIndex) = false;
            P3D(Group).Cov(NewIndex) = 0;
            P3D(Group).Pos(:, NewIndex) = [0; 0; 0];
            P3D(Group).TotalFrames(NewIndex) = 1;
            P3D(Group).Desc(:, NewIndex) = DescriptorsGroup(:,count);
            P3D(Group).ViewIndex(NewViewIndex) = CurFrame;
            P3D(Group).ViewPos(:, NewViewIndex) = FeaturesGroup(1:2, count);
            P3D(Group).ViewSaliency(:, NewViewIndex) = FeaturesGroup(end, count);
            P3D(Group).ViewDesc(:, NewViewIndex) = DescriptorsGroup(:, count) * 2 ^ 15;
            P3D(Group).ViewP3DIndex(NewViewIndex) = NewIndex;
            P3D(Group).DescIndex{NewIndex} = NewViewIndex;
        end
    end
end

