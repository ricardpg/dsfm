function P3D = UVL_SFM_filterVerticies(P3D, MinimumDisparity)

    for PType = 1 : numel(P3D)
        Sequence = find(P3D(PType).Status == 3);
        for p = Sequence,
            LocalIndices = P3D(PType).DescIndex{p};
            Points = double(P3D(PType).ViewPos(:, LocalIndices));
            Dist = UVL_SFM_EuclidianDistances(Points, Points, [1 1]');
            if max(Dist(:)) < MinimumDisparity,
                P3D(PType).Status(p) = 2;
            end
        end
    end
end