function [Mout,T] = UVL_SFM_normalize3D(M)

    cx1=sum(M(1,:))/size(M,2);
    cy1=sum(M(2,:))/size(M,2);
    cz1=sum(M(3,:))/size(M,2);
    
    dx1=M(1,:)-cx1;
    dy1=M(2,:)-cy1;
    dz1=M(3,:)-cz1;
    
    d1=sum((dx1.^2+dy1.^2+dz1.^2).^(1/2))/size(M,2);
    
    Mout(1,:)=sqrt(3)./d1.*(M(1,:)-cx1);
    Mout(2,:)=sqrt(3)./d1.*(M(2,:)-cy1);
    Mout(3,:)=sqrt(3)./d1.*(M(3,:)-cz1);
    
    
    T=[sqrt(3)/d1 0 0 -(sqrt(3)/d1*cx1); 
        0 sqrt(3)/d1 0 -(sqrt(3)/d1*cy1); 
        0 0 sqrt(3)/d1  -(sqrt(3)/d1*cz1); 
        0 0 0 1];
end
