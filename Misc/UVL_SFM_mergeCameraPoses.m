function [P3D Features] = UVL_SFM_mergeCameraPoses(P3D, CameraRegistration, MatchingParams)
for PType = 1 : numel(CameraRegistration(1).Features)
    PairTable = zeros(numel(CameraRegistration), size(CameraRegistration(1).Features(PType).Points, 2));
    Dists = zeros(1, size(CameraRegistration(1).Features(PType).Points, 2));
    for Range = 1 : numel(CameraRegistration)
        PairTable(Range, CameraRegistration(Range).Features(PType).Pairs(1, :)) = CameraRegistration(Range).Features(PType).Pairs(2, :);
        Dists(CameraRegistration(Range).Features(PType).Pairs(1, :)) = CameraRegistration(Range).Features(PType).Pairs(3, :);
    end
    Features(PType) = CameraRegistration(1).Features(PType); %% copy the rest of the structure;
    Features(PType).Pairs = [];  %% cleaar the pairs
    DBItem = find([P3D(:).PointType] == Features(PType).PointType & [P3D(:).DescriptorType] == Features(PType).DescriptorType);
    for Cursor = 1 : size(PairTable, 2)
        MatchIdx = find(PairTable(:, Cursor));
        Matches = PairTable(MatchIdx, Cursor);
        Matches = unique(Matches);

        if numel(Matches) == 1
            Dist = Dists(Cursor);
            Features(PType).Pairs = [Features(PType).Pairs [Cursor Matches Dist]'];
        elseif numel(Matches) > 1
            Matches = sort(Matches);
            Have3D = P3D(DBItem).Status(Matches) == 3;
            if ~sum(Have3D)
                Features(PType).Pairs = [Features(PType).Pairs [Cursor Matches(1) Dists(Cursor)]'];
                continue;
            end
            Matches = Matches(Have3D);
            Pos = P3D(DBItem).Pos(:, Matches);
            if ~isempty(Pos)
                P3D(DBItem).Pos(:, Matches(1)) = Pos(:, 1);
                P3D(DBItem).Status(Matches(1)) = 3;
            end
            P3D(DBItem).TotalFrames(Matches(1)) = sum(P3D(DBItem).TotalFrames(Matches));
            P3D(DBItem).DescIndex{Matches(1)} = [P3D(DBItem).DescIndex{Matches}];
            P3D(DBItem).Status(Matches(2:end)) = 0;
            LocalIndices = P3D(DBItem).DescIndex{Matches(1)};
            P3D(DBItem).Desc(:, Matches(1)) = mean(double(P3D(DBItem).ViewDesc(:, LocalIndices)) / 2 ^15, 2);
            FeatDesc = Features(PType).Descriptors(:, Cursor);
            DescriptorDist = std_euclidian(double(P3D(DBItem).Desc(:, Matches(1))), double(FeatDesc), ones(size(FeatDesc)));
            Features(PType).Pairs = [Features(PType).Pairs [Cursor Matches(1) DescriptorDist]'];
        end
    end
end

                

            
            
