function P3D = UVL_SFM_P3DPrecision(P3D, Precision)
for pp = 1 : numel(P3D)
    switch Precision.GlobalDescriptorPrecision
        case 8
            P3D(pp).Desc = double(P3D(pp).Desc);
            P3D(pp).GlobalDescriptorPrecision = 8;
        case 4
            P3D(pp).Desc = single(P3D(pp).Desc);
            P3D(pp).GlobalDescriptorPrecision = 4;
        otherwise
            error ('MATLAB:UVL_SFM_P3DPrecision', 'UVL_SFM_P3DPrecision: Invalid global descriptor precision (8 or 4)');
    end
    Desc1st = P3D(pp).View(1).Desc; %#ok<NASGU>
    DescInfo = whos('Desc1st');
    InputDescType = DescInfo.class; %% check input desc type
    for vv = 1 :  numel(P3D(pp).View)
        switch Precision.LocalDescriptorPrecision
            case 8
                if strcmpi(InputDescType, 'int16')
                    P3D(pp).View(vv).Desc = double(P3D(pp).View(vv).Desc) / 2 ^ 15;
                else
                    P3D(pp).View(vv).Desc = double(P3D(pp).View(vv).Desc);
                end
                P3D(pp).LocalDescriptorPrecision = 8;
            case 4
                if strcmpi(InputDescType, 'int16')
                    P3D(pp).View(vv).Desc = single(P3D(pp).View(vv).Desc) / 2 ^ 15;
                else
                    P3D(pp).View(vv).Desc = single(P3D(pp).View(vv).Desc);
                end
                P3D(pp).LocalDescriptorPrecision = 4;
            case 2
                if ~strcmpi(InputDescType, 'int16')
                    P3D(pp).View(vv).Desc = int16(P3D(pp).View(vv).Desc * 2 ^ 15);
                end
                P3D(pp).LocalDescriptorPrecision = 2;
            otherwise
                error ('MATLAB:UVL_SFM_P3DPrecision', 'UVL_SFM_P3DPrecision: Invalid local descriptor precision (8 4 2)');
        end
    end 
end

            