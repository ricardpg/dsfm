function P3D = UVL_SFM_eraseForgottenFeatures(P3D, current, EraseFeatures)

ErasedPoints = 0;
for Group = 1 : numel(P3D)
    no3D = find(P3D(Group).Status == 2);
    for pp = no3D,
        LocalIndices = P3D(Group).DescIndex{pp};
        if P3D(Group).ViewIndex(LocalIndices(end)) < current - EraseFeatures.LastSeen;
            P3D(Group).Status(pp) = uint8(0);
            P3D(Group).ViewIndex(LocalIndices) = uint16(0);
            ErasedPoints = ErasedPoints + 1;
        end
    end
end

UVL_SFM_printOut(sprintf(' removed %d feat.', ErasedPoints));