function db_statistics = UVL_SFM_frontEndDBStatistics(P3D, db_statistics, figh)
db_statistics.DBItems(end + 1) = 0;
db_statistics.P3DItems(end + 1) = 0;
for PType = 1:numel(P3D)
    db_statistics.DBItems(end) = db_statistics.DBItems(end) + sum(P3D(PType).Status == 2);
    db_statistics.P3DItems(end) = db_statistics.DBItems(end) + sum(P3D(PType).Status == 3);
end
db_statistics.DBItems(end) = db_statistics.DBItems(end) + db_statistics.P3DItems(end);
if exist('figh', 'var')
    figure(figh), hold off, bar(db_statistics.DBItems, 'grouped'), hold on, bar(db_statistics.P3DItems, 'grouped', 'g'), title('DB size / 3D points')
    set(figh, 'Toolbar', 'none')
    set(figh, 'MenuBar', 'none')
    drawnow;
end
