function UVL_SFM_printOut(printString)
global LOGFILE_
% if file handle is declared, print to file
if ~isempty(LOGFILE_)
    try
        fprintf(LOGFILE_, '%s', printString);
    catch
%        warning('UVL_SFM_printOut:LogFileErr', 'Error attempting to stream out to log')
    end
end
fprintf('%s', printString);
