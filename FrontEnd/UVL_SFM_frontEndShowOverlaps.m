clear FFrames;
CurrMovieFrm = 1;
LoopPause = 3;
BlankImg = uint8(255 * ones(size(UVL_SFM_getInputStream(InputDataInfo, 1))));
FigH = figure('Color', [1 1 1], 'position', [100 230 800 600]);
set(FigH, 'Toolbar', 'none')
set(FigH, 'MenuBar', 'none')
TotalFrames = 1000;
%for i = 1 : size(Loops, 1)
for CurFrame = 400 : 5 : TotalFrames
    PairPos = find(Loops(:,1) == CurFrame);
    figure(FigH), axis tight
    hold off
    subplot(4, 2, [1 3 5]); 
    TitleQuery = sprintf('Query frame %d', CurFrame);
    imshow(UVL_SFM_getInputStream(InputDataInfo, CurFrame)); title(TitleQuery)
    hold on
    subplot(4, 2,[7 8]), 
    plot(FrameSimilaritySmoothed(1:CurFrame-2, CurFrame), 'g-'), title('FrameSimilarity'), axis([1 TotalFrames 0 1]), xlabel('[frames]'), ylabel('[similarity]')
    hold on;
    plot(FrameSimilaritySmoothed_kmeans(1:CurFrame-1, CurFrame)*.7+.1, 'b-'), title('FrameSimilarity'), axis([1 TotalFrames 0 1]), xlabel('[frames]'), ylabel('[similarity]')
    %hold on;
    %plot(Overlap(1:CurFrame-2, CurFrame)*.95, 'r-'), title('FrameSimilarity'), axis([1 TotalFrames 0 1]), xlabel('[frames]'), ylabel('[similarity]')
    hold off;
    if numel(PairPos)
        TitleCross = sprintf('Cross-over frame %d', Loops(PairPos,2));
        subplot(4, 2,[2 4 6]); imshow(UVL_SFM_getInputStream(InputDataInfo, Loops(PairPos,2))); title(TitleCross)
        for i = 1 : LoopPause
            figure(FigH), axis tight
            FFrames(CurrMovieFrm) = getframe(FigH);
            CurrMovieFrm = CurrMovieFrm + 1;
        end
    else      
        subplot(4, 2,[2 4 6]); imshow(BlankImg); title('No cross-over');
        figure(FigH), axis tight
        FFrames(CurrMovieFrm) = getframe(FigH);
        CurrMovieFrm = CurrMovieFrm + 1;        
    end
end
movie2avi(FFrames, 'test11.avi');


    