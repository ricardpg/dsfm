function P3D = UVL_SFM_frontEndEditWorld(P3D, View, K, InputDataInfo)
global VertexList P3DEdit ViewEdit KEdit InputDataInfoEdit
P3DEdit = P3D;
ViewEdit = View;
KEdit = K;
InputDataInfoEdit = InputDataInfo;
%R = FixedXYZ2RotMat(pi/2, pi, 0);
R = FixedXYZ2RotMat(0, pi/2, 0);
R = FixedXYZ2RotMat(pi, 0, 0);
T = [R zeros(3,1);
     0 0 0 1];
has3D = P3D.Status == 3;
labels = find(has3D);

P = P3D.Pos(:, has3D);
Mask = P(1, :)> -5 & P(1, :)< 10 & P(2, :)> -4 & P(2, :)< 4 & P(3, :)> 0 & P(3, :)< 20;
%P = P(:, Mask);
%labels = labels(Mask);
VertexList = labels;
P = UVL_SFM_coordinateTransform(P, T);
figh = figure; hold on
plot3(P(1, :), P(2, :), P(3, :), 'r.', 'MarkerSize', 3)
select3dtool(figh);
hold on
axis equal
