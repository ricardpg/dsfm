function figh = UVL_SFM_frontEndShowMatchingDB(Features, P3D, figh)
if ~exist('figh', 'var')
    figh = figure;
else
    figure(figh);
    clf;
end
figure(figh); title('DB matching'), %axis([1 size(pairs,2) 1 numel(view)]); 
hold on
xlabel ('Pair index'), ylabel('Initiating Frame Index')
InitiatingViews = [];
for FeatureType = 1 : numel(Features)
    InitiatingViews = [InitiatingViews P3D(Features(FeatureType).Pairs(2,:)).View.Index];
end
plot(1:numel(InitiatingViews), InitiatingViews, 'gx');
set(figh, 'Toolbar', 'none')
set(figh, 'MenuBar', 'none')
drawnow;
