function UVL_SFM_frontEndFigurePropreties(action)

    switch action
        case 'write'
            fig_pos = [];
            figs = get(0, 'children');
            for i = 1:numel(figs)
                curr_pos = get(figs(i), 'position');
                fig_pos = [fig_pos; curr_pos];
            end
            save UVL_SFM_figureLayout fig_pos
        case 'read'
            if ~exist('UVL_SFM_figureLayout.mat', 'file')
                error('Figure layout file not found')
            end
            load UVL_SFM_figureLayout.mat
            figs = get(0, 'children');
            if numel(figs) ~= size(fig_pos,1)
                error('Incorrect number of windows in layout');
            end
            for i = 1:numel(figs)
                set(figs(i), 'position', fig_pos(i,:));
            end
        otherwise
            error('Unknown action')
    end
end
        