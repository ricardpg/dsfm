 function figh = UVL_SFM_frontEndShowMatchingFrame(Features, Img, figh, ResizeCoeddicient)
if ~exist('figh', 'var')
    figh = figure;
else
    figure(figh);
    clf
end
figure(figh), imagesc(Img), 
if size(Img,3)==1
    colormap('gray');
end
hold on

if nargin < 4,
  ResizeCoeddicient = 1 ;
end

for FeatureType = 1:numel(Features)
    plot(Features(FeatureType).Points(1, :) * ResizeCoeddicient, Features(FeatureType).Points(2, :) * ResizeCoeddicient, 'bx')
    plot(Features(FeatureType).Points(1, Features(FeatureType).Pairs(1,:)) * ResizeCoeddicient, Features(FeatureType).Points(2, Features(FeatureType).Pairs(1,:)) * ResizeCoeddicient, 'gx')
end
set(figh, 'Toolbar', 'none')
set(figh, 'MenuBar', 'none')
axis off
drawnow;
