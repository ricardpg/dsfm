function UVL_SFM_frontEndPlayReconstruction(P3D, View, K, AviName, FPS, Param)
aviobj = avifile(AviName,'fps',FPS,'COMPRESSION','None', 'quality', 100);

P = [];
for PType = 1:numel(P3D)
    GoodPoints = P3D(PType).Status == 3;
    P = [P P3D(PType).Pos(:, GoodPoints)];
end
V = [View(:).Pose];

xrange = 1*[min([P(1,:) V(1,:)]) max([P(1,:) V(1,:)])];
yrange = 1*[min([P(2,:) V(2,:)]) max([P(2,:) V(2,:)])];
zrange = 1.6*[min([-P(3,:) V(3,:)]) max(-[P(3,:) V(3,:)])];
figh = figure; axis([xrange yrange zrange]);% axis equal
zrangep = [min([-P(3,:)]) max(-[P(3,:)])];
set(figh,'Position', [10 10 1524 968]);
count = 1;
colormap(autumn);
cmp = colormap;
traj = [];
K(1,1) = K(1,1)/2;
K(2,2) = K(2,2)/2;
K(1,3) = K(1,3)/2;
K(2,3) = K(2,3)/2;
Width = Param.Frame.Width/2;
Height = Param.Frame.Height/2;

for VV = numel(View) : 1 : numel(View)
    RegularPoints = [];
    CurrentPoints = [];
    NewPoints = [];
    AllPoints = [];
    for PType = 1:numel(P3D)
        RegularIndices = P3D(PType).ViewIndex < VV;
        RegularIndices = P3D(PType).ViewP3DIndex(RegularIndices);
        RegularIndices = RegularIndices(RegularIndices > 0);
        RegularIndices = unique(RegularIndices);
        RegularIndices = RegularIndices(P3D(PType).Status(RegularIndices) == 3);
        CurrentIndices = P3D(PType).ViewIndex == VV;
        CurrentIndices = P3D(PType).ViewP3DIndex(CurrentIndices);
        CurrentIndices = CurrentIndices(CurrentIndices > 0);
        CurrentIndices = unique(CurrentIndices);
        CurrentIndices = CurrentIndices(P3D(PType).Status(CurrentIndices) == 3);
        NewIndices = setdiff(CurrentIndices, RegularIndices);
        RegularPoints = [RegularPoints P3D(PType).Pos(:, RegularIndices)];
        CurrentPoints = [CurrentPoints P3D(PType).Pos(:, CurrentIndices)];
        NewPoints = [NewPoints P3D(PType).Pos(:, NewIndices)];
        AllIndices = unique([RegularIndices CurrentIndices]);
        AllPoints = [AllPoints P3D(PType).Pos(:, AllIndices)];
    end
    figure(figh), hold off,
    triangles = delaunay(AllPoints(1,:),AllPoints(2,:));
    set(figh, 'color', 'w')
    clf
    axis([xrange yrange zrange]); hold on
    axis('equal')
    view(-40,24)
    axis off
    trisurf(triangles,AllPoints(1,:),AllPoints(2,:),-AllPoints(3,:), 'FaceColor', 'none', 'EdgeColor', 'interp' ), hold on;
    cc = round((size(cmp,1)-1) * (-P(3,:)-zrangep(1))/(zrangep(2)-zrangep(1)))+1;
    colormap(cmp(min(cc):max(cc),:));
    
    
    
    hold on,
    plot3(CurrentPoints(1, :), CurrentPoints(2, :), -CurrentPoints(3, :), 'g.'),
    hold on,
    plot3(NewPoints(1, :), NewPoints(2, :), -NewPoints(3, :), 'b.'),

    cam_pos_m = inv(View(VV).Pose);
    cam_pos(1:3,1) = cam_pos_m(1:3,4);
    cam_pos(4:6,1) = rodrigues(cam_pos_m(1:3,1:3));
    cam_pos(4) = -cam_pos(4);
    cam_pos(5) = -cam_pos(5);
    cam_pos(3) = -cam_pos(3)+1;
    show_camera(figh, K, cam_pos, Width, Height)
    drawnow;
    frm=getframe(figh);
    aviobj = addframe(aviobj,frm);
end
aviobj = close(aviobj);
