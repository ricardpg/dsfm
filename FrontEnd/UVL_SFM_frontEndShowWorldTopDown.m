function UVL_SFM_frontEndShowWorldTopDown(P3D, cam_view, figh)
X = [];
for PType = 1 : numel(P3D)
    have3D = P3D(PType).Status == 3; %% have 3D
    X = [X P3D(PType).Pos(:,have3D)];
end
X(3, :) = - X(3, :);
%triangles = delaunay(X(1, :), X(2, :));
figure(figh), clf, title('World model/camera pos'), hold on
%trisurf(triangles, X(1, :), X(2, :), X(3, :)), hold on;
plot3(X(1, :), X(2, :), X(3, :), 'rx')
for i = 1:(numel(cam_view)-1)
    cpos = inv(cam_view(i).Pose);
    cpos1 = inv(cam_view(i+1).Pose);
    cpos = cpos(1:3, 4);
    cpos1 = cpos1(1:3, 4);
    cpos(3) = -cpos(3);
    cpos1(3) = -cpos1(3);
    plot3(cpos(1), cpos(2), cpos(3), 'rd');
    plot3([cpos(1) cpos1(1)], [cpos(2) cpos1(2)], [cpos(3) cpos1(3)], 'g-');
    if i == 1
        text(cpos(1), cpos(2), cpos(3), sprintf('%d', i))    
    end
end
plot3(cpos1(1), cpos1(2), cpos1(3), 'rd');
text(cpos1(1), cpos1(2), cpos1(3), sprintf('%d', i+1))    
axis off
%axis ij;
%axis tight
%axis vis3d;
view(-43,28)
rotate3d
set(figh, 'Toolbar', 'none')
set(figh, 'MenuBar', 'none')
drawnow;