CCDSize = 0.05;
CamColor = [0 1 0];
%CamColor = []; %%% DONT PLOT CAMERA !!!!!!!!!!!!!!!
CamStep = 1;
IVFile = oiCreate ( 'house.iv' );
IVFig = figure;
figure(IVFig); hold on;
%FID = [IVFile IVFig];
FID = [IVFile nan];
CamInds = 1:CamStep:numel(View);
PoseComp = [View(:).PoseComputed];
CamMask = PoseComp(CamInds)==1;
CamInds = CamInds(CamMask);
oifPlotmsbaTrajectoryDSFM (FID, K, View(CamInds), P3D, CCDSize, [Param.Frame.Height Param.Frame.Width], CamInds, CamColor);
oiClose ( IVFile )