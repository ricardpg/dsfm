% UVL_SFM_frontEndShowWorld(P3D, View, ShowTriangles, ShowCamera, ShowP3DLabels, ShowCameraLabels)
function UVL_SFM_frontEndShowWorld(P3D, View, ShowTriangles, ShowCamera, ShowP3DLabels, ShowCameraLabels)
VertexStep = 1;
CameraStep = 1;
GeometricalOnly = 0;
R = FixedXYZ2RotMat(pi, 0, pi/2);
%R = FixedXYZ2RotMat(0, pi/2, 0);
R = FixedXYZ2RotMat(0, pi, 0);
R = FixedXYZ2RotMat(0, 0, 0);
Unit = 1;

HasInitial = isfield(P3D, 'IsInitial');
   
T = [R zeros(3,1);
     0 0 0 1];
if ~exist('ShowCamera', 'var'); ShowCamera = 0; end
if ~exist('ShowP3DLabels', 'var'); ShowP3DLabels = 0; end
if ~exist('ShowTriangles', 'var'); ShowTriangles = 0; end
P = [];

IsInitial = [];
for PType = 1:numel(P3D)
    has3D = P3D(PType).Status == 3;
    isGeom = P3D(PType).IsGeometrical == 1;
    if GeometricalOnly
        has3D = has3D & isGeom;
    end
    P = [P P3D(PType).Pos(:, has3D)];
    if HasInitial
        IsInitial = [IsInitial P3D(PType).IsInitial(has3D)];
    end
end
%Mask = P(1, :)> -5 & P(1, :)< 10 & P(2, :)> -4 & P(2, :)< 4 & P(3, :)> 0 & P(3, :)< 20;
%Mask = P(1, :)> -100 & P(1, :)< 100 & P(2, :)> -100 & P(2, :)< 100 & P(3, :)> -100 & P(3, :)< 100;
Mask = true(1, size(P,2));
%Mask = P(1, :)> -2 & P(1, :)< 4 & P(2, :)> -2 & P(2, :)< 2 & P(3, :)> 2 & P(3, :)< 6;
%Mask = P(1, :)> -10 & P(1, :)< 10 & P(2, :)> -10 & P(2, :)< 10 & P(3, :)> -10 & P(3, :)< 10;
has3D = find(has3D);
%has3D = has3D(Mask);
%P = P(:, Mask);
fprintf('Total verteces: %d\n', size(P,2));

labels = has3D;
%P = [P3D3.Pos];
P = UVL_SFM_coordinateTransform(P, T) * Unit;
P_ini = P(:, IsInitial==1);
%P(3,:) = 0;
if ShowTriangles
    figh=draw3D_direct(P(:, 1:VertexStep:end));
else
    figh = figure;
    plot3(P(1, 1:VertexStep:end), P(2, 1:VertexStep:end), P(3, 1:VertexStep:end), 'r.', 'MarkerSize', 4)
    hold on,
    plot3(P_ini(1, 1:VertexStep:end), P_ini(2, 1:VertexStep:end), P_ini(3, 1:VertexStep:end), 'r.', 'MarkerSize', 4)    
    xlabel('X [m]'), ylabel('Y [m]'), zlabel('Z [m]')
    
   % plot3(P(1, 1:VertexStep:end), P(2, 1:VertexStep:end), P(3, 1:VertexStep:end), 'r.')
end
figure(figh)
IsInitial = [];
hold on
if ShowCamera
    PoseComputed = [View(:).PoseComputed];
    if HasInitial
        IsInitial =  [View(PoseComputed).IsInitial];
    end
    View = View(PoseComputed);
    labels = find(PoseComputed);
    cpos = [];
    for i = 1:CameraStep:numel(View)
        Pose = inv(View(i).Pose);
        cpos = [cpos Pose(1:3,4)];
    end
    cpos = UVL_SFM_coordinateTransform(cpos, T) * Unit;
    cpos_ini = cpos(:, IsInitial==1);
    plot3(cpos(1, :), cpos(2, :), cpos(3, :), '-db', 'MarkerSize', 5);
    plot3(cpos_ini(1, :), cpos_ini(2, :), cpos_ini(3, :), '-dr');
    Range = [1 : 50 : size(cpos, 2)];
    Labels = [1 : 50 : numel(View)];
%    text(cpos(1, Range) + .3, cpos(2, Range), cpos(3, Range), int2str(Range'));
    %text(cpos(1, Range)+.2, cpos(2, Range), cpos(3, Range), int2str(Labels'), 'FontSize', 7);
end
%axis equal

if ShowP3DLabels
    text(P(1,:), P(2,:), P(3,:), num2str(labels'));
end
%cpos = motion_inverse(view(end).Pos) .* axis_tf;

if ShowCameraLabels
  PoseComputed = [View(:).PoseComputed];
  View = View(PoseComputed);
  labels = find(PoseComputed);
  cpos = [];
  for i = 1:CameraStep:numel(View)
      Pose = inv(View(i).Pose);
      cpos = [cpos Pose(1:3,4)];
  end
  
  text( cpos( 1,: ), cpos( 2, : ), cpos( 3, : ), num2str(labels') ) ;
  
end