% Usage: UVL_SFM_frontEndShowSigleFeatureTracking(P3D, View, K, InputDataInfo, P3DIndex)
function UVL_SFM_frontEndShowSigleFeatureTracking(P3D, View, K, InputDataInfo, P3DIndex)
figh = figure;

for Group = 1 : numel(P3D)
    Num3D = numel(P3D(Group).Status);
    if P3DIndex > Num3D,
        P3DIndex = P3DIndex - Num3D;
        continue;
    else
        for vv = P3D(Group).DescIndex{P3DIndex}
            FrameIndex = P3D(Group).ViewIndex(vv);
            Frame = UVL_SFM_getInputStream(InputDataInfo, FrameIndex);
            hold off, imshow(Frame), hold on;
            P3DProj = (K * View(FrameIndex).Pose(1:3, :)) * [P3D(Group).Pos(1:3, P3DIndex); 1];
            P3DProj = P3DProj ./ P3DProj(3);
%            plot(P3D(P3DIndex).View(vv).Pos(1), P3D(P3DIndex).View(vv).Pos(2), 'gx')
            plot(P3D(Group).ViewPos(1, vv), P3D(Group).ViewPos(2, vv), 'bo')
            plot(P3DProj(1), P3DProj(2), 'rx')
            pause
        end
    end
end

close(figh);