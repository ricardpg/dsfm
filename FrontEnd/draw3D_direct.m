function figH = draw3D_direct(X, figH)
%X = 10*X;
X = X';
if nargin<2
    if nargout<1
        figure; axis equal, axis ij
    else
        figH = figure; axis equal, axis ij
    end
else
    figure(figH);
end
triangles = delaunay(X(:,1),X(:,2));
%trisurf(triangles,X(:,1),X(:,2),X(:,3), 'LineStyle', 'None'), hold on;
%trisurf(triangles,X(:,1),X(:,2),X(:,3), 'FaceColor', 'none', 'EdgeColor', [1 0 0]), hold on;
trisurf(triangles,X(:,1),X(:,2),X(:,3)), hold on;
%axis ij; axis equal
%for i = 1:size(X,1)
%text(X(i,1),X(i,2),X(i,3), num2str(i));
%end