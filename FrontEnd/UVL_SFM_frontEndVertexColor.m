function P3D = UVL_SFM_frontEndVertexColor(P3D, InputDataInfo)
P3D.Color = uint8(zeros(4, size(P3D.Pos, 2)));
for CurrFrame = 1 : InputDataInfo.TotalFrames
    fprintf('Frame %d/%d...\n', CurrFrame, InputDataInfo.TotalFrames)
    PtsInFrame = find(P3D.ViewIndex == CurrFrame);
    Has3D = P3D.Status(P3D.ViewP3DIndex(PtsInFrame)) == 3;
    PtsInFrame = PtsInFrame(Has3D);
    PtsInFramePos = P3D.ViewPos(:, PtsInFrame);
    P3DInFrame = P3D.ViewP3DIndex(PtsInFrame);
    Image = UVL_SFM_getInputStream(InputDataInfo, CurrFrame);
    for CurrPt = 1 : numel(P3DInFrame)
        PtPos = round(PtsInFramePos(:, CurrPt));
        CurrP3D = P3DInFrame(CurrPt);
        Luminance = uint8((double(Image(PtPos(2), PtPos(1), 1)) + double(Image(PtPos(2), PtPos(1), 2)) + double(Image(PtPos(2), PtPos(1), 3))) / 3);
        if P3D.Color(4, CurrP3D) < Luminance
            P3D.Color(1, CurrP3D) = Image(PtPos(2), PtPos(1), 1);
            P3D.Color(2, CurrP3D) = Image(PtPos(2), PtPos(1), 2);
            P3D.Color(3, CurrP3D) = Image(PtPos(2), PtPos(1), 3);
            P3D.Color(4, CurrP3D) = uint8(Luminance);
        end
    end
end



% if P3D.Color(4, CurrP3D) < 1
%     P3D.Color(1, CurrP3D) = uint16((double(P3D.Color(1, CurrP3D)) * double(P3D.Color(4, CurrP3D)) + double(Image(PtPos(2), PtPos(1), 1))) / (double(P3D.Color(4, CurrP3D))+1));
%     P3D.Color(2, CurrP3D) = uint16((double(P3D.Color(2, CurrP3D)) * double(P3D.Color(4, CurrP3D)) + double(Image(PtPos(2), PtPos(1), 2))) / (double(P3D.Color(4, CurrP3D))+1));
%     P3D.Color(3, CurrP3D) = uint16((double(P3D.Color(3, CurrP3D)) * double(P3D.Color(4, CurrP3D)) + double(Image(PtPos(2), PtPos(1), 3))) / (double(P3D.Color(4, CurrP3D))+1));
%     P3D.Color(4, CurrP3D) = P3D.Color(4, CurrP3D) + 1;
% end
