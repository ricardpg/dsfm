% Usage: UVL_SFM_frontEndShowSigleFeatureTracking(P3D, View, K, InputDataInfo, P3DIndex)
function UVL_SFM_frontEndShowFeatureMatch(P3D, InputDataInfo, P3DIndex1, P3DIndex2)
figh = figure;

for Group = 1 : numel(P3D)
    Num3D = numel(P3D(Group).Status);
    if P3DIndex1 > Num3D,
        P3DIndex = P3DIndex - Num3D;
        continue;
    else
        vv1 = P3D(Group).DescIndex{P3DIndex1};
        vv1 = vv1(1);
        vv2 = P3D(Group).DescIndex{P3DIndex2};
        vv2 = vv2(1);
        FrameIndex1 = P3D(Group).ViewIndex(vv1);
        FrameIndex2 = P3D(Group).ViewIndex(vv2);      
        Frame1 = UVL_SFM_getInputStream(InputDataInfo, FrameIndex1);
        Frame2 = UVL_SFM_getInputStream(InputDataInfo, FrameIndex2);
        subplot(1,2,1), imshow(Frame1), hold on, plot(P3D(Group).ViewPos(1, vv1), P3D(Group).ViewPos(2, vv1), 'rx');
        subplot(1,2,2), imshow(Frame2), hold on, plot(P3D(Group).ViewPos(1, vv2), P3D(Group).ViewPos(2, vv2), 'rx');
    end
end
pause;
close(figh);