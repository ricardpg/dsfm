function P3D = UVL_SFM_getBpError(P3D, View, K)

if ~exist('range', 'var')
    range = 1:numel(View);
end

ErasedPoints = 0;

for Group = 1 : numel(P3D)
    P3D(Group).Errs = zeros(2, numel(P3D(Group).Status));
    for pp = find(P3D(Group).Status == 3)
        LocalIndices = P3D(Group).DescIndex{pp};
        DistArray = [];
        if any(ismembc2(range, double(P3D(Group).ViewIndex(LocalIndices))));
            for vv = LocalIndices;
                if isempty(View(P3D(Group).ViewIndex(vv)).PoseComputed) | (View(P3D(Group).ViewIndex(vv)).PoseComputed == false)
                    DistArray  = [DistArray 0];
                    continue
                end
                Pose = View(P3D(Group).ViewIndex(vv)).Pose(1:3, :);
                p_hat = K * Pose * [P3D(Group).Pos(:,pp); 1];
                Dist = norm(p_hat(1:2)./p_hat(3) - P3D(Group).ViewPos(1:2, vv));
                DistArray = [DistArray Dist];
            end
            P3D(Group).Errs(1, pp) = max(DistArray);
            P3D(Group).Errs(2, pp) = mean(DistArray);
        end
    end
end
