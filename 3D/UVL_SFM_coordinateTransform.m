function X = UVL_SFM_coordinateTransform(X, T)
IsHomogeneous = size(X,1) == 4;
if ~IsHomogeneous
    X = [X; ones(1, size(X,2))];
end
X = T * X;
if ~IsHomogeneous
    X = X(1:3, :);
end