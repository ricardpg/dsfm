function P3D = UVL_SFM_P3D_removeOutliers(P3D, Radius)
if ~exist('Radius', 'var')
    Radius = 6;
end
Outliers = 0;
for PType = 1:numel(P3D)
    OutliersStep = 1;
    while(numel(OutliersStep))
        Has3D = find(P3D(PType).Status == 3);
        PP = P3D(PType).Pos(:, Has3D);
        PP = PP.^2;
        PP = sum(PP);
        PP_mean = mean(PP);
        PP_std = std(PP);
        Mask = PP > (PP_mean + Radius * PP_std);
        OutliersStep = Has3D(Mask);
        P3D(PType).Status(OutliersStep) = 0;
        Outliers = Outliers + numel(OutliersStep);
    end
end
fprintf(' Removed %d vertices...', Outliers);
