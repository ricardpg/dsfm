function [P3D Updated] = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, VertexPositionParams)

Updated = [];

for PType = 1 : numel(P3D)
    total_views = false(1, numel(View));
    Indices = [];
    No3D = find([P3D(PType).Status] == 2);
    EnoughViews = P3D(PType).TotalFrames(No3D)  >= VertexPositionParams.MinimumViews;
    Sequence = No3D(EnoughViews);
    for i = Sequence
        LocalIndices = P3D(PType).DescIndex{i};
        p = double(P3D(PType).ViewPos(:, LocalIndices));
        dist = UVL_SFM_EuclidianDistances(p, p, [1 1]');
        if max(dist(:)) > VertexPositionParams.MinimumDisparity
            LocalIndices = P3D(PType).DescIndex{i};
            pts = numel(LocalIndices);
            p = [p; ones(1, size(p,2))]; 
            v = cell(1, pts);
            for pp = 1 : pts
                v{pp} = View(P3D(PType).ViewIndex(LocalIndices(pp))).Pose;
            end
            P = UVL_SFM_singlePointFact(p, v, K);
            bp_err = zeros(1, P3D(PType).TotalFrames(i));
            for pp = 1 : P3D(PType).TotalFrames(i)
                Index = P3D(PType).ViewIndex(LocalIndices(pp));
                p_hat = UVL_SFM_3DPointResection(P, K, View(Index).Pose);
                pm = P3D(PType).ViewPos(:, LocalIndices(pp));
                err = norm(p_hat - pm);
                bp_err(pp) = err;
            end
            inliers = bp_err < VertexPositionParams.BackProjectionMaximumError;
            outliers = ~inliers;
            if  sum(inliers) >= VertexPositionParams.MinimumInliers
                if numel(inliers) ~= sum(inliers)
                    P3D(PType).ViewIndex(LocalIndices(outliers)) = 0; %% free up slots
                    P3D(PType).DescIndex{i} = P3D(PType).DescIndex{i}(inliers); %% keep indexes to inlier views
                    P3D(PType).TotalFrames(i) = uint8(sum(inliers));
                    LocalIndices = P3D(PType).DescIndex{i};
                    Descriptors = double(P3D(PType).ViewDesc(:, LocalIndices)) / 2^15;
                    P3D(PType).Desc(:, i) = single(mean( Descriptors, 2));
                end
                total_views([P3D(PType).ViewIndex(LocalIndices)]) = true;
                P3D(PType).Pos(:, i) = P;
                P3D(PType).Status(i) = uint8(3);
                P3D(PType).IsGeometrical(i) = false;
                Indices = [Indices i];
                Updated = [Updated [PType i]'];
                LocalIndices = P3D(PType).DescIndex{i};
                if any(P3D(PType).ViewIndex(LocalIndices)==0)
                    fprintf('Group %d Point %d\n ', PType, i);
                end
                
            end
        end
    end
    if numel(Indices)
        total_views = find(total_views);
        P3D(PType) = UVL_SFM_adjust3DPointsSBA(P3D(PType), View, total_views, Indices, K);
    end
end


UVL_SFM_printOut(sprintf(' %d new verteces', numel(Indices)));
