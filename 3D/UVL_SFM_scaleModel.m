function [P3D View] = UVL_SFM_scaleModel(P3D, View, Scale)
for VV = 1 : numel(View)
    View(VV).Pose(1:3, 4) = View(VV).Pose(1:3, 4) * Scale;
end
for PType = 1 : numel(P3D)
    P3D(PType).Pos(:, P3D(PType).Status == 3) = P3D(PType).Pos(:, P3D(PType).Status == 3) * Scale;
end