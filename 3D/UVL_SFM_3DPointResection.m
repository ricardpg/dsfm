function [p, p3] = UVL_SFM_3DPointResection(P, K, Pose)

if size(P,1)>3,
    error ('MATLAB:UVL_SFM_3DPointResection', 'points should be in (3 by M) format!');
end

P(4,:) = 1;
P = Pose * P;
p3 = K * P(1:3, :);
p=[
    p3(1, :) ./ p3(3, :)
    p3(2, :) ./ p3(3, :)
];