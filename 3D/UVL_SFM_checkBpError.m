function P3D = UVL_SFM_checkBpError(P3D, View, K, mean_th, max_th, range)

if ~exist('range', 'var')
    range = 1:numel(View);
end

ErasedPoints = 0;

for Group = 1 : numel(P3D)
    for pp = find(P3D(Group).Status == 3)
        LocalIndices = P3D(Group).DescIndex{pp};
        DistArray = [];
        if any(ismembc2(range, double(P3D(Group).ViewIndex(LocalIndices))));
            for vv = LocalIndices;
                if isempty(View(P3D(Group).ViewIndex(vv)).PoseComputed) | (View(P3D(Group).ViewIndex(vv)).PoseComputed == false)
                    DistArray  = [DistArray 0];
                    continue
                end
                Pose = View(P3D(Group).ViewIndex(vv)).Pose(1:3, :);
                p_hat = K * Pose * [P3D(Group).Pos(:,pp); 1];
                Dist = norm(p_hat(1:2)./p_hat(3) - P3D(Group).ViewPos(1:2, vv));
                DistArray = [DistArray Dist];
            end
            MaxDist = max(DistArray);
            MeanDist = mean(DistArray);
            if (MaxDist > max_th) || (MeanDist > mean_th),
%                fprintf('Group: %d No: %d Max err: %f, mean err %f\n', Group, pp, MaxDist, SumDist/numel(LocalIndices));
                P3D(Group).Status(:,pp) = uint8(2);                     % Set it back to 2D
                P3D(Group).ViewIndex(LocalIndices(DistArray == MaxDist)) = uint16(0);  % free up the slot
                P3D(Group).DescIndex{pp} = LocalIndices(DistArray ~= MaxDist);        % Reset indices
                P3D(Group).TotalFrames(pp) = P3D(Group).TotalFrames(pp) - sum(DistArray == MaxDist);
                LocalIndices = P3D(Group).DescIndex{pp};
                Descriptors = double(P3D(Group).ViewDesc(:, LocalIndices)) / 2^15;
                P3D(Group).Desc(:, pp) = single(mean( Descriptors, 2));
                ErasedPoints = ErasedPoints + 1;
            end
        end
    end
end

UVL_SFM_printOut(sprintf(' %d removed', ErasedPoints));