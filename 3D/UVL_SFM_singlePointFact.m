function P = UVL_SFM_singlePointFact(p, v, K)

vf = UVL_SFM_pose2FirstView(v);

p = inv(K) * p;
numer = 0;
denom = 0;
for i = 2 : size(p, 2)
    Pose = vf{i};
    T = Pose(1:3, 4);
    R = Pose(1:3, 1:3);
    Skewp = [0 -p(3, i) p(2, i); p(3, i) 0 -p(1, i); -p(2, i) p(1, i) 0];
    numer = numer + (Skewp*T)'*Skewp*R*p(:,1);
    denom = denom + norm(Skewp*T)^2;
end
alfa = numer / denom;
P = - 1/alfa * p(:,1);
P = inv(v{1}) * [P;1];
P = P(1:3);