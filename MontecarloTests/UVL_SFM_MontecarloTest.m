% function UVL_SFM_MontecarloTest( NumRuns, GaussianNoiseStd ) 
% 
% Description: Function to perform a Montecarlo test to the results of the DSFM software
% 

function [ RunsResults3DPts, RunResultsStd ] = UVL_SFM_MontecarloTest( ResultsFolder, NumRuns, GaussianNoiseStd ) 

%% turn off anoying warnings
warning off MATLAB:divideByZero
warning off MATLAB:singularMatrix
warning off MATLAB:nearlySingularMatrix
warning off MATLAB:illConditionedMatrix

%% load default parameters
UVL_SFM_defaultParameters

%% try load specific parameters for sequence
if exist('UVL_SFM_sequenceParameters.m', 'file')
    UVL_SFM_sequenceParameters
else
    warning('MATLAB:UVL_SFM_run', 'No sequence specific parameters found.');
end

%% load results data
if ~exist('last.mat', 'file'),
  error( 'MATLAB:UVL_SFM_MontecarloTest', '''last.mat'' not found on the current folder!' ) ;
else
  load last
end

%% Create a folder with the intermediate results
if ~exist( ResultsFolder, 'dir' ),
  mkdir( ResultsFolder ) ;
end

%% Run the Montecarlo test
P3D_noisy = P3D ;
OriginalViewPos = P3D.ViewPos ; % Retrieve the original 2D projections
vpCols = size( OriginalViewPos, 2 ) ;
numP3D = sum( P3D.Status == 3 ) ;
RunsResults3DPts = zeros( 3, numP3D, NumRuns ) ;
for i = 1 : NumRuns,
  fprintf( ['- Montecarlo run ' num2str( i ) ': '] ) ;
  % Add Noise to the 2D projections of each point
  Noise = GaussianNoiseStd * randn( 2, vpCols ) ;
  P3D_noisy.ViewPos = OriginalViewPos + Noise ;

  % Compute the Bundle Adjustment with the noisy measures
  [ P3D_noisyBA View_noisyBA ] = UVL_SFM_runSBA( P3D_noisy, View, K, 0 ) ;
  
  % Store the computed 3D points
  RunsResults3DPts( :, :, i ) = P3D_noisyBA.Pos( :, P3D_noisyBA.Status == 3 ) ;
  
  % Save intermediate step
  save( [ ResultsFolder '/MontecarloRun_' num2str(i) ], 'P3D_noisyBA', 'View_noisyBA' ) ;
end
fprintf( '\n' ) ;

save( [ ResultsFolder '/Results.mat' ] ) ;

%% Extract some results

% Compute standard deviation
RunResultsStd = zeros( 3, numP3D ) ;
for i = 1 : numP3D,
  RunResultsStd( 1, i ) = std( RunsResults3DPts( 1, i, : ) ) ; % Std in X for point i
  RunResultsStd( 2, i ) = std( RunsResults3DPts( 2, i, : ) ) ; % Std in Y for point i
  RunResultsStd( 3, i ) = std( RunsResults3DPts( 3, i, : ) ) ; % Std in Z for point i
end

% Plot variances as colors
minStdX = min( RunResultsStd( 1, : ) ) ;
minStdY = min( RunResultsStd( 2, : ) ) ;
minStdZ = min( RunResultsStd( 3, : ) ) ;
maxStdX = max( RunResultsStd( 1, : ) ) ;
maxStdY = max( RunResultsStd( 2, : ) ) ;
maxStdZ = max( RunResultsStd( 3, : ) ) ;

figure ;
scatter3( P3D.Pos( 1, P3D.Status == 3 ), P3D.Pos( 2, P3D.Status == 3 ), P3D.Pos( 3, P3D.Status == 3 ), [], RunResultsStd( 1, : ), 'Filled' ) ;
axis equal ;
set( gcf, 'Name', 'Std in X' ) ;
figure ;
scatter3( P3D.Pos( 1, P3D.Status == 3 ), P3D.Pos( 2, P3D.Status == 3 ), P3D.Pos( 3, P3D.Status == 3 ), [], RunResultsStd( 2, : ), 'Filled' ) ;
axis equal ;
set( gcf, 'Name', 'Std in Y' ) ;
figure ;
scatter3( P3D.Pos( 1, P3D.Status == 3 ), P3D.Pos( 2, P3D.Status == 3 ), P3D.Pos( 3, P3D.Status == 3 ), [], RunResultsStd( 3, : ), 'Filled' ) ;
axis equal ;
set( gcf, 'Name', 'Std in Z' ) ;

