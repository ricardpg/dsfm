function [OrthoMosaic ValidMap Dense3D] = UVL_SFM_orthoMosaic2(P3D, View, K, ParamOrthoMosaic, InputDataInfo)
warning off Images:initSize:adjustingMag
Itmp = UVL_SFM_getInputStream(InputDataInfo, 1); %%% get frame size
[height, width, depth] = size(Itmp);
fprintf('----Generating Orthomosaic----\n')
fprintf('\tVertex Triangulation...')
X = [];
for PType = 1 : numel(P3D)
    Have3D = P3D(PType).Status == 3;
    X = [X P3D(PType).Pos(:, Have3D)];
end
Gaussian = fspecial('gaussian', 15, 2.5);
triangles = delaunay(X(1,:), X(2,:));
fprintf('Done.\n')

%% get the transformation that would provide the maximum projection area
fprintf('\tComputing Ortho-projection Transformation...')
XTmesh = linspace(min(X(1,:)), max(X(1,:)), 100); %% generate a grid to compute the transformation
YTmesh = linspace(min(X(2,:)), max(X(2,:)), 100);
[xmesh ymesh] = meshgrid(XTmesh, YTmesh);
zmesh = griddata(X(1,:), X(2,:), X(3,:), xmesh, ymesh, ParamOrthoMosaic.Interpolation);
zmeshv = reshape(zmesh,1,numel(zmesh));
pin = ~isnan(zmeshv);  %%% take out invalid points
zmeshv = zmeshv(pin);
xmeshv = reshape(xmesh, 1, numel(xmesh));
xmeshv = xmeshv(pin);
ymeshv = reshape(ymesh, 1, numel(ymesh));
ymeshv = ymeshv(pin);
Xi = [xmeshv; ymeshv; zmeshv];
global planeParms
planeParms = [0 0 -1 0];
transform = find_ortho_plane(Xi);  %%% finds the transformation that provides maximum resolution
transform = [transform; 0];
wRo = rodrigues(transform(4:6));
wRo = eye(3);
wTo = [wRo [0 0 0]';
       0 0 0 1];
oTw = [wRo' [0 0 0]';
        0 0 0 1];
clear xmesh ymesh zmesh xmeshv ymeshv zmeshv pin Xi XTmesh YTmesh
fprintf('Done.\n')

%% generate the ortho_mosaic
% transformed 3D points
fprintf('\tGenerating dense grid...')
Xt = UVL_SFM_coordinateTransform(X, wTo);  %% transforming verteces in mosaic 3D coordinates
% ortho-mosaic metric step
ortho_step = sqrt(((max(Xt(1,:))-min(Xt(1,:)))*(max(Xt(2,:))-min(Xt(2,:)))) / ParamOrthoMosaic.Resolution);
% transformed 3D mesh
[xmesh_ortho ymesh_ortho] = meshgrid(min(Xt(1,:)):ortho_step:max(Xt(1,:)),min(Xt(2,:)):ortho_step:max(Xt(2,:)));
zmesh_ortho = griddata(Xt(1,:), Xt(2,:), Xt(3,:), xmesh_ortho, ymesh_ortho, ParamOrthoMosaic.Interpolation);
NanMask = isnan(zmesh_ortho);
zmesh_ortho = conv2(zmesh_ortho, Gaussian, 'same');
zmesh_ortho(NanMask) = nan;
% transformed 3D points in pixel coordinates (ortho-mosaic)
Xtpx = Xt(1,:) - min(Xt(1,:));
Xtpx = Xtpx / max(Xtpx);
Xtpx = Xtpx * (size(zmesh_ortho,2) - 1) + 1;
Xtpy = Xt(2,:) - min(Xt(2,:));
Xtpy = Xtpy / max(Xtpy);
Xtpy = Xtpy * (size(zmesh_ortho,1) - 1) + 1;
Xppx = [Xtpx; Xtpy; Xt(3,:)];
fprintf('Done.\n')
%% compute the triangles map
fprintf('\tComputing patch mapping...')
triangle_map = zeros(size(zmesh_ortho));
pXppx = Xppx(:,reshape(triangles',1,numel(triangles)));
if sum(pXppx(3,:)) < 0
    pXppx(3,:) = -pXppx(3,:);
end
triangle_map = index_triangles(triangle_map, pXppx, -1, uint32(1:length(triangles)));
clear pXppx
fprintf('Done.\n')

%% initialize ortho-mosaic
fprintf('\tComputing patch normals...')
OrthoMosaic = uint8(255 * ones(size(triangle_map,1), size(triangle_map,2), depth));
ortho_mosaic_distance = inf(size(triangle_map,1), size(triangle_map,2));
ortho_mosaic_angles = ParamOrthoMosaic.InitialAngle * ones(size(triangle_map,1),  size(triangle_map,2));
% compute triangle normals
%XTriangle = Xt(:,triangles');
%PointsPos = 1 : 3 : size(XTriangle, 2);
%u = (XTriangle(:, PointsPos) - XTriangle(:, PointsPos + 1));
%v = (XTriangle(:, PointsPos + 2) - XTriangle(:, PointsPos + 1));
%TriangleNormals = cross(v,u);
% compute triangle gravity centers
%A = XTriangle(:, PointsPos);
%B = XTriangle(:, PointsPos + 1);
%C = XTriangle(:, PointsPos + 2);
%Ap = (B + C)/2;
%Bp = (C + A)/2;
%AAp = A - Ap;
%BBp = B - Bp;
%t = (A(1, : ) - B(1, :)) ./ (BBp(1, :) - AAp(1, :));
%TriangleGravityCenters = A + AAp .* repmat(t, [3 1]);

%ortho_mosaic_normals1 = griddata(TriangleGravityCenters(1,:), TriangleGravityCenters(2,:), TriangleNormals(1,:), xmesh_ortho, ymesh_ortho, ParamOrthoMosaic.Interpolation);
%NanMask = isnan(ortho_mosaic_normals1);
%ortho_mosaic_normals1 = conv2(ortho_mosaic_normals1, Gaussian, 'same');
%ortho_mosaic_normals1(NanMask) = nan;

%ortho_mosaic_normals2 = griddata(TriangleGravityCenters(1,:), TriangleGravityCenters(2,:), TriangleNormals(2,:), xmesh_ortho, ymesh_ortho, ParamOrthoMosaic.Interpolation);
%NanMask = isnan(ortho_mosaic_normals2);
%ortho_mosaic_normals2 = conv2(ortho_mosaic_normals2, Gaussian, 'same');
%ortho_mosaic_normals2(NanMask) = nan;

%ortho_mosaic_normals3 = griddata(TriangleGravityCenters(1,:), TriangleGravityCenters(2,:), TriangleNormals(3,:), xmesh_ortho, ymesh_ortho, ParamOrthoMosaic.Interpolation);
%NanMask = isnan(ortho_mosaic_normals3);
%ortho_mosaic_normals3 = conv2(ortho_mosaic_normals3, Gaussian, 'same');
%ortho_mosaic_normals3(NanMask) = nan;
%ortho_mosaic_normalsv = [reshape(ortho_mosaic_normals1, 1, numel(ortho_mosaic_normals1));
%                         reshape(ortho_mosaic_normals2, 1, numel(ortho_mosaic_normals2));
%                         reshape(ortho_mosaic_normals3, 1, numel(ortho_mosaic_normals3))];
%clear ortho_mosaic_normals1 ortho_mosaic_normals2 ortho_mosaic_normals3
%ortho_norms = magn(ortho_mosaic_normalsv);
%ortho_mosaic_normalsv = [ortho_mosaic_normalsv(1,:) ./ ortho_norms;
%                         ortho_mosaic_normalsv(2,:) ./ ortho_norms;
%                         ortho_mosaic_normalsv(3,:) ./ ortho_norms];
%fprintf('Done.\n')

%% trasnform back ortho_coordinates
X_mesh_t = [reshape(xmesh_ortho, 1, numel(xmesh_ortho)); reshape(ymesh_ortho, 1, numel(ymesh_ortho)); reshape(zmesh_ortho, 1, numel(zmesh_ortho))];
good_points = find(~isnan(reshape(zmesh_ortho, 1, numel(zmesh_ortho))));
X_mesh_t = X_mesh_t(:, good_points);
X_mesh = oTw * [X_mesh_t; ones(1,length(X_mesh_t))];
X_mesh = [X_mesh(1,:)./X_mesh(4,:); X_mesh(2,:)./X_mesh(4,:); X_mesh(3,:)./X_mesh(4,:)];
clear X_mesh_t

%% ortho_mosaic pixel coordinates
[xmeshp ymeshp] = meshgrid(1:size(OrthoMosaic,2),1:size(OrthoMosaic,1));
X_mesh_p = [reshape(xmeshp, 1, numel(xmeshp)); reshape(ymeshp, 1, numel(ymeshp))]; 
X_mesh_p = X_mesh_p(:, good_points);
clear xmeshp ymeshp

%% get camera projection rays & misc
%RayVectorsCamera = UVL_SFM_rayVectorsFast(width, height, K);
ChannelShiftOrtho = size(OrthoMosaic, 1) * size(OrthoMosaic, 2);

%% compute rendering for each view
fprintf('\tRendering orthomosaic... ')
if ParamOrthoMosaic.DisplayProgress
    figh = figure;
end
txt = [];
ValidMap = false(size(OrthoMosaic, 1), size(OrthoMosaic, 2));
for image_index = 1 : ParamOrthoMosaic.FrameUpdateStep : numel(View)
    tic
    erase_string = sprintf('fprintf(''%s'')', repmat('\b',[1 numel(txt)]));
    eval(erase_string)
    txt = sprintf('Frame: %d/%d', image_index,numel(View));
    fprintf('%s', txt);
    p_mesh = UVL_SFM_3DPointResection(X_mesh, K, View(image_index).Pose);
    p_inImage = p_mesh(1,:) >= ParamOrthoMosaic.FrameBorder & p_mesh(1,:) <= (width - ParamOrthoMosaic.FrameBorder) & p_mesh(2,:) >= ParamOrthoMosaic.FrameBorder & p_mesh(2,:) <= (height - ParamOrthoMosaic.FrameBorder);
    p_mesh = p_mesh(:,p_inImage);
    X_mesh_p_image = X_mesh_p(:, p_inImage);
    p_coords = ((round(p_mesh(1,:))-1) * height + round(p_mesh(2,:)));
    X_coords = ((round(X_mesh_p_image(1,:))-1) * size(OrthoMosaic,1) + round(X_mesh_p_image(2,:)));
    
%    SqrtDistFromFocal = sum((p_mesh(1:2,:) - K(1:2,3)).^2);
    SqrtDistFromFocal = (p_mesh(1,:) - K(1,3)).^2 + (p_mesh(2,:) - K(2,3)).^2;
    improved_points = ortho_mosaic_distance(X_coords) > SqrtDistFromFocal;
    p_coords = p_coords(improved_points);
    X_coords = X_coords(improved_points);    
    SqrtDistFromFocal = SqrtDistFromFocal(improved_points);
    ortho_mosaic_distance(X_coords) = SqrtDistFromFocal; %%%%%%%
    ValidMap(X_coords) = true;
    
    ImTmp = uint8(im2double(UVL_SFM_getInputStream(InputDataInfo, image_index + ParamOrthoMosaic.FrameOffset))*255);
    for Channel = 1:depth
        ImageChannel = ImTmp(:, :, Channel);
        OrthoMosaic((Channel - 1) * ChannelShiftOrtho + X_coords) = ImageChannel(p_coords);
    end
    fprintf('Elapsed time %f\n                             ', toc);
    if ParamOrthoMosaic.DisplayProgress
        figure(figh), imshow(OrthoMosaic(:, :, 1:depth));
        pause(.001)
    end
end
Mesh3DStep = sqrt(((max(Xt(1,:))-min(Xt(1,:)))*(max(Xt(2,:))-min(Xt(2,:)))) / ParamOrthoMosaic.MeshResolution);

% transformed 3D mesh
[XMesh3D YMesh3D] = meshgrid(min(Xt(1,:)):Mesh3DStep:max(Xt(1,:)), min(Xt(2,:)) : Mesh3DStep : max(Xt(2,:)));
ZMesh3D = griddata(Xt(1,:), Xt(2,:), Xt(3,:), XMesh3D, YMesh3D, ParamOrthoMosaic.Interpolation);

Dense3D = zeros(size(XMesh3D, 1), size(XMesh3D, 2), 3);

Dense3D(:, :, 1) = XMesh3D;
Dense3D(:, :, 2) = YMesh3D;
Dense3D(:, :, 3) = ZMesh3D;
fprintf('\nAll Done.\n')
