function UVL_SFM_export2PLY(P3D, FileName, GeometricOnly)
if ~exist('GeometricOnly', 'var')
    GeometricOnly = 0;
end

fprintf('-----PLY Format exporter-----\n')
%% setting files and textures
PLYFileName = sprintf('%s.ply', FileName);

%% saving obj file
fprintf('\tExporting to PLY File [%s]...\n', PLYFileName);
PLYFile = fopen(PLYFileName,'wt');
if PLYFile == -1
    error ('MATLAB:UVL_SFM_export2PLY', 'File IO Error saving PLY File');
end

%% Variable Initialization 
fprintf('\tGenerating Vertices...\n');
P = [];
for PType = 1:numel(P3D)
    has3D = P3D(PType).Status == 3;
    if GeometricOnly
        has3D = has3D & P3D(PType).IsGeometrical;
    end
    P = [P P3D(PType).Pos(:, has3D)];
end
fprintf('\tGenerating facets...\n');
Facets = delaunay(P(1,:),P(2,:));

%% Writing header
fprintf('\tWriting Header...\n');
fprintf(PLYFile, 'ply\n');
fprintf(PLYFile, 'format ascii 1.0\n');
fprintf(PLYFile, 'element vertex %d\n', size(P, 2));
fprintf(PLYFile, 'property float32 x\n');
fprintf(PLYFile, 'property float32 y\n');
fprintf(PLYFile, 'property float32 z\n');
fprintf(PLYFile, 'element face %d\n', size(Facets, 1));
fprintf(PLYFile, 'property list uint8 int32 vertex_indices\n');
fprintf(PLYFile, 'end_header\n');

%% Writing vertices
fprintf('\tWriting Verteces...')
for Count = 1 : size(P, 2)
    fprintf(PLYFile, '%f %f %f\n', P(1, Count), P(2, Count), -P(3, Count));
end
fprintf(' %d valid vertices\n', Count);
%% Generating Facets
fprintf('\tWriting Facets...')
for Count = 1 : size(Facets, 1)
    fprintf(PLYFile, '3 %d %d %d\n', Facets(Count,3)-1, Facets(Count,2)-1, Facets(Count,1)-1);
end
fprintf(' %d valid facets\n', Count);
%% Close file
fclose(PLYFile);
fprintf('\tAll Done...\n')