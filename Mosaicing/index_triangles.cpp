#include "mex.h"
#include <memory.h>

// J = index_triangles(I, P, zbuff, V)

// Ported to Matlab by M. Everingham

template<class T> static void FillTriangle(const T *xyv, T *img, unsigned int *ind, unsigned int ind_val, int ih, int iw, int zbuff);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	int				iw, ih, ntri, i;
	int 			zbuff;

	if (nrhs != 4)
		mexErrMsgTxt("4 input arguments expected.");

	if (nlhs < 1 || nlhs > 2)
		mexErrMsgTxt("1 or 2 output arguments expected.");

	if (mxIsComplex(prhs[2]) || mxGetNumberOfElements(prhs[2]) != 1)
		mexErrMsgTxt("input 3 (zbuff) must be scalar");

	zbuff = (int)mxGetScalar(prhs[2]);

	if (zbuff != 0 && zbuff != -1 && zbuff != 1)
		mexErrMsgTxt("input 3 (zbuff) must have value -1/0/+1");

	int len = 3 * (2 + zbuff * zbuff);

	if ((!mxIsDouble(prhs[0]) && !mxIsSingle(prhs[0])) || mxGetClassID(prhs[0]) != mxGetClassID(prhs[1]))
		mexErrMsgTxt("inpus 1 (I) and 2 (P) must be the same type, either single or double arrays");

	if (mxIsComplex(prhs[0]) || mxGetNumberOfDimensions(prhs[0]) != 2)
		mexErrMsgTxt("input 1 (I) must be a real matrix");

	ih = mxGetM(prhs[0]);
	iw = mxGetN(prhs[0]);

	if (mxIsComplex(prhs[1]) || mxGetNumberOfElements(prhs[1]) % len)
		mexErrMsgTxt("input 2 (P) must be a real array with a multiple of 3*(2+abs(zbuff)) elements");

	ntri = mxGetNumberOfElements(prhs[1]) / len;

	if (mxIsComplex(prhs[3]) || mxGetNumberOfElements(prhs[3]) != ntri || mxGetClassID(prhs[3]) != mxUINT32_CLASS)
		mexErrMsgTxt("input 4 (V) must be a real UINT32 array with numel(P)/(3*(2+abs(zbuff))) elements");
	
	unsigned int *V = (unsigned int *)mxGetPr(prhs[3]);
	plhs[0] = mxCreateNumericMatrix(ih, iw, mxUINT32_CLASS, mxREAL);
	unsigned int *out = (unsigned int *)mxGetPr(plhs[0]);
	mxArray *Z = NULL;
	void *Zp = NULL;
	if (zbuff) {
		Z = mxDuplicateArray(prhs[0]);
		Zp = mxGetPr(Z);
	}
	const void *xyv = mxGetPr(prhs[1]);

	if (mxIsSingle(prhs[0])) {
		for (i = 0; i < ntri; i++)		
			FillTriangle((const float *)(xyv) + i * len, (float *)Zp, out, V[i], iw, ih, zbuff);
	} else {
		for (i = 0; i < ntri; i++)		
			FillTriangle((const double *)(xyv) + i * len, (double *)Zp, out, V[i], iw, ih, zbuff);
	}
	if (zbuff) {
		if (nlhs > 1)
			plhs[1] = Z;
		else
			mxDestroyArray(Z);
	}
}

template<class T> static void FillTriangle(const T *xyv, T *img, unsigned int *ind, unsigned int ind_val, int ih, int iw, int zbuff)
{
	// note x, y etc. transposed

	const T	*v1, *v2, *v3, *vt;

	T		x_top, x_bottom, x_middle, ex;
	T		y_top, y_bottom, y_middle, ey, emy;
	T		xb_minus_xt, yb_minus_yt, ym_minus_yt, xm_minus_xt;
	T		left, left_delta, right, right_delta, width;
	T		t, t_to_b_inv_slope, t_to_m_inv_slope, m_to_b_inv_slope;
	T		recip_yb_minus_yt, recip_yb_minus_ym, recip_ym_minus_yt;
	int		iy, iy_end, iy_mid_end, ix, ix_end;
	T		v, v_start, v_middle, dvdx, dvds;
	unsigned int *ip, *ip_end, *ip_y;
	T       *zp, *zp_y;

	// sort into top, middle and bottom vertices

	v1 = &xyv[0 * ((zbuff != 0) + 2)];
	v2 = &xyv[1 * ((zbuff != 0) + 2)];
	v3 = &xyv[2 * ((zbuff != 0) + 2)];

	if (v1[0] > v2[0]) { vt = v1; v1 = v2; v2 = vt; }
	if (v1[0] > v3[0]) { vt = v1; v1 = v3; v3 = vt; }
	if (v2[0] > v3[0]) { vt = v2; v2 = v3; v3 = vt; }

	x_top = v1[1];
	y_top = v1[0];
	x_middle = v2[1];
	y_middle = v2[0];
	x_bottom = v3[1];
	y_bottom = v3[0];
	xb_minus_xt = x_bottom - x_top;
	yb_minus_yt = y_bottom - y_top;
	recip_yb_minus_yt = 1.0f / yb_minus_yt;
	recip_yb_minus_ym = 1.0f / (y_bottom - y_middle);
	ym_minus_yt = y_middle - y_top;
	recip_ym_minus_yt = 1.0f / ym_minus_yt;
	xm_minus_xt = x_middle - x_top;

	iy = (int) (y_top + 0.5);	// first scanline inside
	if (iy < ih)
	{
		if (iy < 1)
			iy = 1;

		ey = static_cast<T>(iy) + 0.5 - y_top;	// vertical error from top to pixel center

		iy_end = (int) (y_bottom + 0.5);	// first scanline outside
		if (iy_end > ih + 1)
			iy_end = ih + 1;

		iy_mid_end = (int) (y_middle + 0.5);	// first scanline outside top "half"
		if (iy_mid_end > ih + 1)
			iy_mid_end = ih + 1;

		emy = static_cast<T>(iy_mid_end) + 0.5 - y_middle;	// vertical error from middle to pixel center in bottom "half"

		ip_y = ind + (iy - 1) * iw;
		zp_y = img + (iy - 1) * iw;
		t_to_b_inv_slope = recip_yb_minus_yt * xb_minus_xt;
		m_to_b_inv_slope = recip_yb_minus_ym * (x_bottom - x_middle);

		if (y_top != y_middle)
		{
			// no horizontal top

			t_to_m_inv_slope = recip_ym_minus_yt * xm_minus_xt;
			t = ym_minus_yt * recip_yb_minus_yt;
			width = xm_minus_xt - xb_minus_xt * t;

			if (t_to_m_inv_slope < t_to_b_inv_slope)
			{
				// middle on left

				left_delta = t_to_m_inv_slope;
				right_delta = t_to_b_inv_slope;
				
				if (zbuff)
					dvds = (v2[2] - v1[2]) * recip_ym_minus_yt;
			}
			else
			{
				right_delta = t_to_m_inv_slope;
				left_delta = t_to_b_inv_slope;

				if (zbuff)
					dvds = (v3[2] - v1[2]) * recip_yb_minus_yt;
			}

			if (zbuff) {
				v_start = v1[2] + ey * dvds;
				v_middle = (v3[2] - v1[2]) * t + v1[2];
				dvdx = (v2[2] - v_middle) / width;
			}

			left = left_delta * ey + x_top; 
			right = right_delta * ey + x_top;

			while (iy < iy_mid_end)
			{
				ix = (int) (left + 0.5);
				if (ix <= iw)
				{
					if (ix < 1)
						ix = 1;

					ex = static_cast<T>(ix) + 0.5 - left;	// horizontal error from edge to pixel center

					ix_end = (int) (right + 0.5);
					if (ix_end > iw + 1)
						ix_end = iw + 1;

					ip = ip_y + (ix - 1);
					ip_end = ip_y + (ix_end - 1);

					if (zbuff == 0)
					{
						while (ip < ip_end) 
						{
							*(ip++) = ind_val;
						}
					}
					else
					{
						zp = zp_y + (ix - 1);
						v = v_start + dvdx * ex;
						if (zbuff < 0)
						{
							while (ip < ip_end) 
							{
								if (v > *zp) {
									*zp = v;
									*ip = ind_val;
								}
								ip++;
								zp++;
								v += dvdx;
							}
						}
						else
						{
							while (ip < ip_end) 
							{
								if (v < *zp) {
									*zp = v;
									*ip = ind_val;
								}
								ip++;
								zp++;
								v += dvdx;
							}
						}
						v_start += dvds;
					}
				}

				left += left_delta;
				right += right_delta;
				ip_y += iw;
				zp_y += iw;
				iy++;
			} 

			// set up deltas for botthom "half" and reset x

			if (t_to_m_inv_slope < t_to_b_inv_slope)
			{
				// middle on left

				left_delta = m_to_b_inv_slope;
				left = x_middle + emy * left_delta;

				if (zbuff)
				{
					dvds = (v3[2] - v2[2]) * recip_yb_minus_ym;
					v_start = v2[2] + emy * dvds; 
				}
			} 
			else
			{
				right_delta = m_to_b_inv_slope; 
				right = x_middle + emy * right_delta;
			}
		}
		else 
		{
			// horizontal top

			if (x_middle < x_top)
			{
				// middle on left
				left_delta = m_to_b_inv_slope; 
				right_delta = t_to_b_inv_slope;  
				left = x_middle + emy * left_delta;
				right = x_top + emy * right_delta;  

				if (zbuff)
				{
					dvds = (v3[2] - v2[2]) * recip_yb_minus_ym;
					v_start = v2[2] + emy * dvds;
					dvdx = (v2[2] - v1[2]) / xm_minus_xt;
				}
			} 
			else
			{
				right_delta = m_to_b_inv_slope; 
				left_delta = t_to_b_inv_slope;
				right = x_middle + emy * right_delta;
				left = x_top + emy * left_delta;

				if (zbuff)
				{
					dvds = (v3[2] - v1[2]) * recip_yb_minus_yt;
					v_start = v1[2] + emy * dvds;
					dvdx = (v2[2] - v1[2]) / xm_minus_xt;
				}
			}
		} 

		while (iy < iy_end)
		{
			ix = (int) (left + 0.5);
			if (ix <= iw)
			{
				if (ix < 1)
					ix = 1;

				ex = static_cast<T>(ix) + 0.5f - left; // horizontal error from edge to pixel center

				ix_end = (int) (right + 0.5);
				if (ix_end > iw + 1)
					ix_end = iw + 1;

				ip = ip_y + (ix - 1); 
				ip_end = ip_y + (ix_end - 1);

				if (zbuff == 0)
				{
					while (ip < ip_end) 
					{
						*(ip++) = ind_val;
					}
				}
				else
				{
					zp = zp_y + (ix - 1);
					v = v_start + dvdx * ex;
					if (zbuff < 0)
					{
						while (ip < ip_end) 
						{
							if (v > *zp) {
								*zp = v;
								*ip = ind_val;
							}
							ip++;
							zp++;
							v += dvdx;
						}
					}
					else
					{
						while (ip < ip_end) 
						{
							if (v < *zp) {
								*zp = v;
								*ip = ind_val;
							}
							ip++;
							zp++;
							v += dvdx;
						}
					}
					v_start += dvds;
				}
			}

			left += left_delta;
			right += right_delta;
			ip_y += iw;
			zp_y += iw;
			iy++;
		}
	}
} 
