function [planeParms distances]= find_ortho_plane(Xi)
global X
X = Xi;
%options = optimset('LevenbergMarquardt', 'on', 'MaxIter', 5000, 'MaxFunEvals', 8000, 'TolFun', 1e-20, 'TolX', 1e-20);
options = optimset( 'Algorithm', 'levenberg-marquardt', 'Display', 'off');
planeParms = lsqnonlin(@points2planeDistances, zeros(5,1), [], [], options);
%planeParms = lsqnonlin(@points2planeDistances, [0 0 0 0 0 3]', [], [], options);
distances = points2planeDistances(planeParms);