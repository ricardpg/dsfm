function UVL_SFM_export2VRML_speheres(P3D, FileName, Radius)
Step = 1;

fprintf('-----VRML Format exporter-----\n')
if ~isfield(P3D, 'Color')
    error ('MATLAB:UVL_SFM_export2VRML', 'No vertex color information. Call "UVL_SFM_frontEndVertexColor"');
end
%% setting file
WRLFileName = sprintf('%s.wrl', FileName);

%% saving WRL file
fprintf('\tExporting to VRML File [%s]...\n', WRLFileName);
WRLFile = fopen(WRLFileName,'wt');
if WRLFile == -1
    error ('MATLAB:UVL_SFM_export2VRML', 'File IO Error saving VRML File');
end

%% Variable Initialization 
fprintf('\tGenerating Vertices...\n');
P = [];
Colors = [];
for PType = 1:numel(P3D)
    has3D = P3D(PType).Status == 3;
    P = [P P3D(PType).Pos(:, has3D)];
    Colors = [Colors P3D(PType).Color(1:3, has3D)]; 
end
Colors = double(Colors) / 255;

%% Writing header
fprintf('\tWriting Header...\n');
fprintf(WRLFile, '#VRML V2.0 utf8\n');
%fprintf(WRLFile, '#VRML V1.0 ascii\n');

%% Writing vertices
fprintf('\tWriting vertices and colors...')
for Count = 1 : Step : size(P, 2)
    fprintf(WRLFile, 'Transform {\n');
    fprintf(WRLFile, '\ttranslation %f %f %f\n', P(1, Count), P(2, Count), P(3, Count));
    fprintf(WRLFile, '\tchildren\n');
    fprintf(WRLFile, '\t\tShape{  appearance Appearance {\n');
    fprintf(WRLFile, '\t\t\tmaterial Material { diffuseColor %.2f %.2f %.2f}}\n', Colors(1, Count), Colors(2, Count), Colors(3, Count));
    fprintf(WRLFile, '\t\tgeometry Sphere{radius %f}\n', Radius);
    fprintf(WRLFile, '\t\t}\n');
    fprintf(WRLFile, '}\n');
end
fprintf(' %d valid vertices\n', Count);

%% Close file
fclose(WRLFile);
fprintf('\tAll Done...\n')