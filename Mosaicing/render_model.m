border =1;
figure, hold on, %title('Rendered G+F model')
surface(-zmesh_ortho(border:end-border,border:end-border)*50,ortho_mosaic(border:end-border,border:end-border,:)/255,...
        'FaceColor','texturemap',...
        'EdgeColor','none',...
        'CDataMapping','direct')
%    axis ij
%    axis vis3d
    axis equal
    axis off
fig_view(1,50), axis vis3d