function [P3D P3Db] = boundary_vertexes(P3D)
good_vert = find([P3D(:).status] == 1);
P = [P3D(good_vert).pos];
P2 = P(1:2,:)';
[v,c]=voronoin(P2); 
boundary = [];
for i = 1:length(c) 
    if any(c{i}==1)
        boundary = [boundary i];
    end
end
for i = 1:numel(boundary)
    P3D(good_vert(boundary(i))).is_geometrical = 1;
end
P3Db = P3D(good_vert(boundary));
