function UVL_SFM_export2OBJ(OrthoMosaic, Dense3D, ValidMap, FileName)

fprintf('-----OBJ Format exporter-----\n')
%% setting files and textures
OBJFileName = sprintf('%s.obj', FileName);
TextureFileName = sprintf('%s.png', FileName);
LIBFileName = sprintf('%s.mtl', FileName);
MaterialName = sprintf('%s_Texture', FileName);
ObjectName = sprintf('%s_World', FileName);

%% saving texture
fprintf('\tSaving Texture [%s]...\n', TextureFileName);
imwrite(OrthoMosaic, TextureFileName)

%% saving library file
fprintf('\tExoprting Library File [%s]...\n', LIBFileName);
LibFile = fopen(LIBFileName,'wt');
if LibFile == -1
    error ('MATLAB:UVL_SFM_export2OBJ', 'File IO Error saving Library File');
end
fprintf(LibFile, 'newmtl %s\n', MaterialName);
fprintf(LibFile, 'Ka  0.6 0.6 0.6\n');
fprintf(LibFile, 'Kd  0.6 0.6 0.6\n');
fprintf(LibFile, 'Ks  0.9 0.9 0.9\n');
fprintf(LibFile, 'd  1.0\n');
fprintf(LibFile, 'Ns  0.0\n');
fprintf(LibFile, 'illum 2\n');
fprintf(LibFile, 'map_Kd %s\n', TextureFileName);
fclose(LibFile);

%% saving obj file
fprintf('\tExoprting OBJ File [%s]...\n', OBJFileName);
OBJFile = fopen(OBJFileName,'wt');
if OBJFile == -1
    error ('MATLAB:UVL_SFM_export2OBJ', 'File IO Error saving OBJ File');
end

%% Variable Initialization 
MosaicWidth = size(OrthoMosaic, 2);
MosaicHeight = size(OrthoMosaic, 1);
MeshWidth = size(Dense3D, 2);
MeshHeight = size(Dense3D, 1);
ScaleX = (MosaicWidth - 1) / (MeshWidth - 1);
ScaleY = (MosaicHeight - 1) / (MeshHeight - 1);

%% Writing header
fprintf('\tWriting Header...\n');
fprintf(OBJFile, 'mtllib ./%s\n', LIBFileName);

%% Generating verteces
fprintf('\tGenerating Verteces...')
VertexCounter = 0;
for xc = 1 : MeshWidth %% reading by columns to match matlab matrix ref.
    xm = floor((xc - 1) * ScaleX) + 1;
    for yc = 1 : MeshHeight
        ym = floor((yc - 1) * ScaleY) + 1;
        if isnan(Dense3D(yc, xc, 3))
            fprintf(OBJFile, 'v 0 0 0\n');  %% invalid verteces
            fprintf(OBJFile, 'vt 0 0 0\n');  %% invalid verteces
            continue
        end
        fprintf(OBJFile, 'v %f %f %f\n', Dense3D(yc, xc, 1), Dense3D(yc, xc, 2), -Dense3D(yc, xc, 3));
        fprintf(OBJFile, 'vt %f %f 0\n', xm / MosaicWidth, 1 - ym / MosaicHeight);
        VertexCounter = VertexCounter + 1;
    end
end
fprintf('%d valid verteces\n', VertexCounter);
%% Generating Object
fprintf('\tGenerating Facets...')
fprintf(OBJFile, 'g %s\n', ObjectName);
fprintf(OBJFile, 'usemtl %s\n', MaterialName);
fprintf(OBJFile, 's 1\n');

IdxXTri1 = [0 1 0];
IdxYTri1 = [0 1 1];
IdxXTri2 = [0 1 1];
IdxYTri2 = [0 0 1];
FacetCounter = 0;
Dense3DZ = Dense3D(:, :, 3);
for yc = 1 : (MeshHeight - 1)
    for xc = 1 : (MeshWidth - 1)
        % 1st triangle    
        IdxTr1 = (xc - 1 + IdxXTri1) * MeshHeight + yc + IdxYTri1;
        IdxTr1M = (floor((xc - 1 + IdxXTri1) * ScaleX)) * MosaicHeight + floor((yc + IdxYTri1 - 1) * ScaleY) + 1;
        if all(ValidMap(IdxTr1M)) & all(~isnan(Dense3DZ(IdxTr1)))
            fprintf(OBJFile, 'f %d/%d %d/%d %d/%d\n', IdxTr1(1), IdxTr1(1), IdxTr1(2), IdxTr1(2), IdxTr1(3), IdxTr1(3));
            FacetCounter = FacetCounter + 1;
        end
        % 2nd triangle
        IdxTr2 = (xc - 1 + IdxXTri2) * MeshHeight + yc + IdxYTri2;
        IdxTr2M = (floor((xc - 1 + IdxXTri2) * ScaleX)) * MosaicHeight + floor((yc + IdxYTri2 - 1) * ScaleY) + 1;
        if all(ValidMap(IdxTr2M)) & all(~isnan(Dense3DZ(IdxTr2)))
            fprintf(OBJFile, 'f %d/%d %d/%d %d/%d\n', IdxTr2(1), IdxTr2(1), IdxTr2(2), IdxTr2(2), IdxTr2(3), IdxTr2(3));
            FacetCounter = FacetCounter + 1;
        end
    end
end
fprintf('%d valid facets\n', FacetCounter);
fclose(OBJFile);
fprintf('\tAll Done...\n')