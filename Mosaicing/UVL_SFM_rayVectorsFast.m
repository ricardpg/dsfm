function vectors = UVL_SFM_rayVectorsFast(width, height, K)
    [xmesh ymesh] = meshgrid(1:width, 1:height);        
    xmesh = (xmesh(:)-K(1,3)) / K(1,1);
    ymesh = (ymesh(:)-K(2,3)) / K(2,2);
    vectors = [xmesh ymesh ones(width*height, 1)];
    vectors_norms = magn(vectors, 2);
    vectors(:,1) = vectors(:,1) ./ vectors_norms;
    vectors(:,2) = vectors(:,2) ./ vectors_norms;
    vectors(:,3) = vectors(:,3) ./ vectors_norms;
    vectors = vectors';
end