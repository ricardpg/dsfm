function UVL_SFM_export2VRML(P3D, FileName)

fprintf('-----VRML Format exporter-----\n')
if ~isfield(P3D, 'Color')
    error ('MATLAB:UVL_SFM_export2VRML', 'No vertex color information. Call "UVL_SFM_frontEndVertexColor"');
end
%% setting file
WRLFileName = sprintf('%s.wrl', FileName);

%% saving WRL file
fprintf('\tExporting to VRML File [%s]...\n', WRLFileName);
WRLFile = fopen(WRLFileName,'wt');
if WRLFile == -1
    error ('MATLAB:UVL_SFM_export2VRML', 'File IO Error saving VRML File');
end

%% Variable Initialization 
fprintf('\tGenerating Vertices...\n');
P = [];
Colors = [];
for PType = 1:numel(P3D)
    has3D = P3D(PType).Status == 3;
    P = [P P3D(PType).Pos(:, has3D)];
    Colors = [Colors P3D(PType).Color(1:3, has3D)]; 
end
Colors = double(Colors) / 255;

%% Writing header
fprintf('\tWriting Header...\n');
%fprintf(WRLFile, '#VRML V2.0 utf8\n');
fprintf(WRLFile, '#VRML V1.0 ascii\n');
fprintf(WRLFile, 'Shape{  appearance Appearance { material Material {emissiveColor 1 1 1 }}\n');


%% Writing vertices
fprintf('\tWriting Verteces...')
fprintf(WRLFile, 'geometry PointSet {\n');
fprintf(WRLFile, 'coord Coordinate {point [\n');
for Count = 1 : 100 : size(P, 2)
    fprintf(WRLFile, '%f %f %f\n', P(1, Count), P(2, Count), P(3, Count));
end
fprintf(WRLFile, ']\n');
fprintf(WRLFile, '}\n');
fprintf(' %d valid vertices\n', Count);

%% Writing Colors
fprintf('\tWriting Colors...')
fprintf(WRLFile, 'color Color { color [\n');
for Count = 1 : 100 : size(Colors, 2)
    fprintf(WRLFile, '%.2f %.2f %.2f\n', Colors(1, Count), Colors(2, Count), Colors(3, Count));
end
fprintf(WRLFile, ']\n');
fprintf(WRLFile, '}\n');
fprintf(WRLFile, '}\n');
fprintf(WRLFile, '}\n');
fprintf(' %d valid colors\n', Count);
%% Close file
fclose(WRLFile);
fprintf('\tAll Done...\n')