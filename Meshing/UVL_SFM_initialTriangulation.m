close all
MaxAngle = 1.0;
VectorStep = 100;

Meshing = [];
InitMeshing = [];
RayVectorsCamera = UVL_SFM_rayVectorsFast(Param.Frame.Width, Param.Frame.Height, K);
for CurrView = 1 : 10 %numel(View)
    PtsInView = find(P3D.ViewIndex == CurrView);
    Have3D = P3D.Status(P3D.ViewP3DIndex(PtsInView)) == 3;
    PtsInView = PtsInView(Have3D);
    P3DInView = P3D.ViewP3DIndex(PtsInView);
    PosPts = double(P3D.ViewPos(:, PtsInView));
    Triangles = delaunay(PosPts(1, :), PosPts(2, :));
    TrianglesP3D = [P3DInView(Triangles(:,1))' P3DInView(Triangles(:,2))' P3DInView(Triangles(:,3))'];
    %% check patch orientation
    % compute patch normals
    PTriangle = P3D.Pos(:, TrianglesP3D');
    PointsPos = 1 : 3 : size(PTriangle, 2);
    u = (PTriangle(:, PointsPos) - PTriangle(:, PointsPos + 1));
    v = (PTriangle(:, PointsPos + 2) - PTriangle(:, PointsPos + 1));
    w = (PTriangle(:, PointsPos) - PTriangle(:, PointsPos + 2));
    TriangleNormals = cross(v,u);
    TriangleAreas = sqrt(TriangleNormals(1, :).^2 + TriangleNormals(2, :).^2 + TriangleNormals(3, :).^2) / 2;
    TrianglePerimeters = sqrt(sum(u.^2)) + sqrt(sum(v.^2)) + sqrt(sum(w.^2));
    
    % compute patch gravity centers
    A = PTriangle(:, PointsPos);
    B = PTriangle(:, PointsPos + 1);
    C = PTriangle(:, PointsPos + 2);
    Ap = (B + C)/2;
    Bp = (C + A)/2;
    AAp = A - Ap;
    BBp = B - Bp;
    t = (A(1, : ) - B(1, :)) ./ (BBp(1, :) - AAp(1, :));
    TriangleGravityCenters = A + AAp .* repmat(t, [3 1]);
    
    % compute angle between camera rays and patch normals
    PoseInv = inv(View(CurrView).Pose);
    RInv = PoseInv(1:3, 1:3);
    vectors = RInv * RayVectorsCamera; %%% in world coordinates
    Pose = View(CurrView).Pose(1:3, :);
    TrGravCentersProj = K * Pose * [TriangleGravityCenters; ones(1, size(TriangleGravityCenters, 2))];
    TrGravCentersProj(1, :) = round(TrGravCentersProj(1, :) ./ TrGravCentersProj(3, :));
    TrGravCentersProj(2, :) = round(TrGravCentersProj(2, :) ./ TrGravCentersProj(3, :));   
    TrGravCentersProj = ((TrGravCentersProj(1, :)) - 1) * Param.Frame.Height + TrGravCentersProj(2, :);
    ValidProjections = (TrGravCentersProj > 0) & (TrGravCentersProj <= Param.Frame.Height * Param.Frame.Width);
    TrGravCentersProj = TrGravCentersProj(ValidProjections);
    TriangleNormals = TriangleNormals(:, ValidProjections);
    TrianglesP3D = TrianglesP3D(ValidProjections, :);
    
    ProjectionRays = vectors(:, TrGravCentersProj);
    ProjectionRaysNorm = sqrt(ProjectionRays(1, :).^2 + ProjectionRays(2, :).^2 + ProjectionRays(3, :).^2);
    ProjectionRays(1, :) = ProjectionRays(1, :) ./ ProjectionRaysNorm;
    ProjectionRays(2, :) = ProjectionRays(2, :) ./ ProjectionRaysNorm;
    ProjectionRays(3, :) = ProjectionRays(3, :) ./ ProjectionRaysNorm;
    TriangleNormalsNorm = sqrt(TriangleNormals(1, :).^2 + TriangleNormals(2, :).^2 + TriangleNormals(3, :).^2);
    TriangleNormals(1, :) = TriangleNormals(1, :) ./ TriangleNormalsNorm;
    TriangleNormals(2, :) = TriangleNormals(2, :) ./ TriangleNormalsNorm;
    TriangleNormals(3, :) = TriangleNormals(3, :) ./ TriangleNormalsNorm;
    
    
    Angles = acos(dot(ProjectionRays, TriangleNormals));
    %figure; trisurf(TrianglesP3D(:, :), P3D.Pos(1,:), P3D.Pos(2,:), P3D.Pos(3,:));
%    figure; trisurf(TrianglesP3D(Angles < .8, :), P3D.Pos(1,:), P3D.Pos(2,:), P3D.Pos(3,:));
    
%    hold on, quiver3(TriangleGravityCenters(1, 1:VectorStep:end), TriangleGravityCenters(2, 1:VectorStep:end), TriangleGravityCenters(3, 1:VectorStep:end), VectorScale * TriangleNormals(1, 1:VectorStep:end), VectorScale * TriangleNormals(2, 1:VectorStep:end), VectorScale * TriangleNormals(3, 1:VectorStep:end));
%    hold on, quiver3(TriangleGravityCenters(1, 1:VectorStep:end), TriangleGravityCenters(2, 1:VectorStep:end), TriangleGravityCenters(3, 1:VectorStep:end), ...
%        vectors(1, TrGravCentersProj(1:VectorStep:end)), vectors(2, TrGravCentersProj(1:VectorStep:end)), vectors(3, TrGravCentersProj(1:VectorStep:end)));
    % vectors(:, TrGravCentersProj)
    %hold on, plot3(TriangleGravityCenters(1, 1:VectorStep:end), TriangleGravityCenters(2, 1:VectorStep:end), TriangleGravityCenters(3, 1:VectorStep:end), 'rx')
%    axis equal;
    InitMeshing = [InitMeshing; TrianglesP3D];

    TrianglesP3D = TrianglesP3D(Angles < MaxAngle, :);
    Meshing = [Meshing; TrianglesP3D];
    
    
end
InitMeshing = unique(InitMeshing, 'rows');
Meshing = unique(Meshing, 'rows');


figure, trisurf(InitMeshing(:, :), P3D.Pos(1,:), P3D.Pos(2,:), P3D.Pos(3,:)), title('Initial Meshing'), axis equal
figure, trisurf(Meshing(:, :), P3D.Pos(1,:), P3D.Pos(2,:), P3D.Pos(3,:)), title('Filtered Meshing'), axis equal

%% filter 1: Area Threshold
% TriangleArea = zeros(size(Meshing, 1), 1);
% TriangleAngles = zeros(size(Meshing, 1), 3);
% for Count = 1 : size(Meshing, 1)
%     TriangleArea(Count) = triangle_area(P3D.Pos(:, Meshing(Count, :)));
%     TriangleAngles(Count, :) = triangle_angles(P3D.Pos(:, Meshing(Count, :)));    
% end
% MinAngles = min(TriangleAngles, [], 2);
% SmallTriangles = TriangleArea < 1 & MinAngles > 0.010;
% figure, trisurf(Meshing(SmallTriangles, :), P3D.Pos(1,:), P3D.Pos(2,:), P3D.Pos(3,:));
