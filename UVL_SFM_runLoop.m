%% cleanup
close all
clear

%% turn off anoying warnings
warning off MATLAB:divideByZero
warning off MATLAB:singularMatrix
warning off MATLAB:nearlySingularMatrix
warning off MATLAB:illConditionedMatrix

%% load default parameters
UVL_SFM_defaultParameters

%% try load specific parameters for sequence
if exist('UVL_SFM_sequenceParameters.m', 'file')
    UVL_SFM_sequenceParameters
else
    error ('MATLAB:UVL_SFM_runSFM', 'No sequence specific parameters found.');
end

%% open logfile if needed
if exist('LOGFILE_', 'var')
    LOGFILE_ = fopen('runSFM.log', 'a');
end
Now = clock();
UVL_SFM_printOut(sprintf('++++++++++++++++++++++ START_SESSION ++++++++++++++++++++++++++\n'));
UVL_SFM_printOut(sprintf('at: %d-%d-%d, %d:%02d:%02.0f\n', Now(1), Now(2), Now(3), Now(4), Now(5),  Now(6)));

%% check and create cache dir
if ~exist('Cache', 'dir')
    mkdir('Cache')
end

%% load initialization data or resume
if exist('last.mat', 'file') && RESUME_ && ~RESTART_
    load last
elseif exist('initial_model.mat', 'file') && ~RESTART_
    load initial_model
end

%% initialize variables
UVL_SFM_printOut(sprintf('Total number of frames: %d\n', NbrFrames));
if ~exist('P3D', 'var')
    P3D = UVL_SFM_allocateDB(Param.General, Param.Detection, Param.Description);
    View = [];
    View(1).Pose = eye(4);
    View(1).PoseComputed = true;
    DBStatistics.DBItems = [];
    DBStatistics.P3DItems = [];    
end

%% Front End Initialization
if FRONTEND_
    FigImage = figure;
    FigWorld = figure;
    FigMatch = figure;
    FigStats = figure;
    UVL_SFM_frontEndFigurePropreties('read');
end

%% model initialization
% check if there is 3D information available
No3DInfo = numel(View) < 2;
if No3DInfo
    UVL_SFM_printOut(sprintf('\n[Initializing model]\n'));
end
CurFrame = 1;
JustGot3DInfo = 0;
while No3DInfo
    tic
    UVL_SFM_printOut(sprintf('--------Frame %d--------\n',CurFrame));
    
    UVL_SFM_printOut(sprintf('\tExtracting features  >>'));
    [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
    if isempty(ICur),
        ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
        ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
    end
    Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
    Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc));
    PrevTime = toc;  
    
    UVL_SFM_printOut(sprintf('\tMatching Features    >>'));
    Features = UVL_SFM_match(P3D, Features, Param.Matching);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc;  
    
    % if not the 1st view, try to compute the motion and check for outliers
    if CurFrame > 5
        % attempts to compute camera motion
        UVL_SFM_printOut(sprintf('\tCamera Motion        >> '));
        [CurrentCameraMotion P3D Features] = UVL_SFM_computeCameraMotion(P3D, Features, K, Param.Motion);
        % if motion was successfuly computed
        if numel(CurrentCameraMotion)
            No3DInfo = false;
            View(CurFrame).Pose = CurrentCameraMotion;
            View(CurFrame).PoseComputed = true;
            % now that we have a starting 3D model, let's find the previous camera positions
            for BaseIndex = 2:(CurFrame-1)
                UVL_SFM_printOut(sprintf('\tRegistering camera   >>'));
                [View(BaseIndex).Pose P3D] = UVL_SFM_getCameraPoseRecursive(P3D, BaseIndex, K, Param.CameraPose);
                if isempty(View(BaseIndex).Pose),
                    View(BaseIndex).PoseComputed = false;
                    error('camera pose not found (should erase the points of this view)');
                else
                    View(BaseIndex).PoseComputed = true;
                end
                UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
                PrevTime = toc;
            end
            DBStatistics = UVL_SFM_frontEndDBStatistics(P3D, DBStatistics);
        end
    end
    % update database
    UVL_SFM_printOut(sprintf('\tUpdating Database    >>'));
    P3D = UVL_SFM_updateDatabase(P3D, Features, CurFrame, Param.General);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    CurFrame = CurFrame + 1;
    JustGot3DInfo = 1;
    UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc));  
end

if JustGot3DInfo
    tic
    UVL_SFM_printOut(sprintf('--------SBA--------\n'));
    [P3D View] = UVL_SFM_runSBA(P3D, View, K, 0);
    UVL_SFM_printOut(sprintf('\tCheck bad points     >>')); 
    P3D = UVL_SFM_checkBpError(P3D, View, K, 1, 1.5);
    UVL_SFM_printOut(sprintf('\n'));
    UVL_SFM_printOut(sprintf('\tNew 3D verteces      >>')); 
    P3D = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, Param.VertexPosition);
    UVL_SFM_printOut(sprintf('\n'));
    UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc));  
    save initial_model P3D View DBStatistics
end


%% direct camera registration
UVL_SFM_printOut(sprintf('\n[Switching to direct registration]\n'));
for CurFrame = numel(View) + 1 : NbrFrames
    tic
    UVL_SFM_printOut(sprintf('--------Frame %d--------\n',CurFrame));
    
    UVL_SFM_printOut(sprintf('\tExtracting features  >>'));
    [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
    if isempty(ICur),
        ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
        ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
    end
    Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
    Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc));
    PrevTime = toc; 
%%%%
    if CurFrame > 2000 
        FrameRanges{1} = 1 : 100;
        FrameRanges{2} = CurFrame - 40 : CurFrame;
    else
        FrameRanges{1} = max([CurFrame - 40 1]) : CurFrame;
    end
%    FrameRanges{1} = 1 : 30;
%    FrameRanges{2} = CurFrame - 20 : CurFrame;
    CameraRegistration = [];
    Cursor = 1;
    for Group = 1 : numel(FrameRanges)
        UVL_SFM_printOut(sprintf('\tMatching Features    >>'));
        FeaturesMatched = UVL_SFM_match(P3D, Features, Param.Matching, FrameRanges{Group});
        UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
        PrevTime = toc;  
        MatchRatio = [];
        for i = 1 : numel(FeaturesMatched)
            MatchRatio = [MatchRatio size(FeaturesMatched(i).Pairs, 2) / size(FeaturesMatched(i).Points, 2)];
        end
        if mean(MatchRatio) > .4
            % get camera position
            UVL_SFM_printOut(sprintf('\tRegistering camera   >>'));
        
            [ViewPose FeaturesMatched] = UVL_SFM_getCameraPoseDirect(P3D, FeaturesMatched, K, Param.CameraPose);
            UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
            PrevTime = toc;
            if ~isempty(ViewPose),
                UVL_SFM_printOut(sprintf('\tRefining matches     >>'));                
                [ViewPose FeaturesMatched] = UVL_SFM_refineMatches(P3D, FeaturesMatched, ViewPose, K, Param.Frame, Param.Matching);
                CameraRegistration(Cursor).Pose = ViewPose;
                CameraRegistration(Cursor).Features = FeaturesMatched;
                Cursor = Cursor + 1;
                UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
                PrevTime = toc; 
                
            end
        end
    end
%%%%
    switch numel(CameraRegistration)
        case 0
            View(CurFrame).Pose = eye(4);
            View(CurFrame).PoseComputed = false;
            Features = [];
        case 1
            View(CurFrame).Pose = CameraRegistration(1).Pose;
            View(CurFrame).PoseComputed = true;
            Features = CameraRegistration(1).Features;
        otherwise
            [P3D Features] = UVL_SFM_mergeCameraPoses(P3D, CameraRegistration);
            View(CurFrame).Pose = CameraRegistration(end).Pose;
            View(CurFrame).PoseComputed = true;
            [P3Db Viewb] = UVL_SFM_runSBA(P3D, View, K, 0);
            ttt = 4;
    end
    
    % update database
    UVL_SFM_printOut(sprintf('\tUpdating Database    >>'));
    P3D = UVL_SFM_updateDatabase(P3D, Features, CurFrame, Param.General);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc; 
    
    % get position of new vertexes
    UVL_SFM_printOut(sprintf('\tNew 3D verteces      >>'));
    P3D = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, Param.VertexPosition);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc;
    
    % cleanup forgotten features
    UVL_SFM_printOut(sprintf('\tCleaning up Database >>'));
    P3D = UVL_SFM_eraseForgottenFeatures(P3D, CurFrame, Param.EraseFeatures);
%    P3D = UVL_SFM_reMatchForgottenFeatures(P3D, View, K,  CurFrame, Param.Frame.Width, Param.Frame.Height, Param.Matching, Param.EraseFeatures);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    
    % some more front end
    if FRONTEND_
        UVL_SFM_frontEndShowMatchingFrame(Features, ICur, FigImage);
        DBStatistics=UVL_SFM_frontEndDBStatistics(P3D, DBStatistics, FigStats);
        UVL_SFM_frontEndShowWorldTopDown(P3D, View, FigWorld)
    else
        DBStatistics=UVL_SFM_frontEndDBStatistics(P3D, DBStatistics);
    end
    UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc));       

    % global alignment
    if ~mod(CurFrame, Param.General.BundleStep)
        tic
        UVL_SFM_printOut(sprintf('--------SBA--------\n'));
        range_start = max([1 CurFrame-Param.General.BundleRange]);
        [P3D View] = UVL_SFM_runSBA(P3D, View, K, 0, range_start:CurFrame);  %%% bundle the whole sequence
        UVL_SFM_printOut(sprintf('\tCheck bad points     >>')); 
        P3D = UVL_SFM_checkBpError(P3D, View, K, 2, 2.5, range_start:CurFrame);
        UVL_SFM_printOut(sprintf('\n'));
        UVL_SFM_printOut(sprintf('\tNew 3D verteces      >>'));
        P3D = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, Param.VertexPosition);
        UVL_SFM_printOut(sprintf('\n'));
%        [P3D View]=UVL_SFM_runSBA(P3D, View, K, 0, range_start:CurFrame);  %%% bundle the whole sequence
        UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc)); 
    end
    pause(0.001);
end
%[P3Db Viewb] = UVL_SFM_runSBA(P3D, View, K, 0);
%save last P3Db P3D Viewb View DBStatistics
save last P3D View DBStatistics
% close the log file
if exist('LOGFILE_', 'var')
    if ~isempty(LOGFILE_)
        fclose(LOGFILE_);
        LOGFILE_ = [];
    end
end