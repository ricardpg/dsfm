function [P3D Merged] = UVL_SFM_mergeViewsDirect(P3D, Pair, Param, K, InputDataInfo)
Merged = 0;
BaseFrame = min(Pair);
CurrFrame = max(Pair);
BaseRange = max([1 BaseFrame - 10]) : (BaseFrame + 10);

[ICur Features] = UVL_SFM_loadCache(CurrFrame, Param);
if isempty(ICur),
    ICur = UVL_SFM_getInputStream(InputDataInfo, CurrFrame);
    ICur = UVL_SFM_preprocessImage(ICur, CurrFrame, Param.Processing);
end
Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
tic;
Features = UVL_SFM_match(P3D, Features, Param.Matching, BaseRange);
if isempty(Features(1).Pairs)
    return;
end
[Pose Features] = UVL_SFM_getCameraPoseDirect(P3D, Features, K, Param.CameraPose);
if isempty(Pose)
    return;
end


ValidP3D = P3D(1).Status(Features(1).Pairs(2, :)) == 3;
ValidPairs = Features(1).Pairs(:, ValidP3D);

PointsImCurr = find(P3D.ViewIndex == CurrFrame);
Points3DImCurr = P3D.Status(P3D.ViewP3DIndex(PointsImCurr)) == 3;
PointsImCurr = PointsImCurr(Points3DImCurr);
PointsImCurrPos = P3D.ViewPos(:, PointsImCurr);

MatchingPairs = UVL_SFM_nearestNeighbour(double(Features(1).Points(1:2, ValidPairs(1,:))), double(PointsImCurrPos), 3, 0.001);

P3DIndBase = ValidPairs(2, MatchingPairs(1,:));
P3DIndCurr = P3D.ViewP3DIndex(:, PointsImCurr(MatchingPairs(2,:)));
for CurrPair = 1 : numel(P3DIndCurr)
    Vert1 = P3DIndBase(CurrPair);
    Vert2 = P3DIndCurr(CurrPair);
    if Vert1 ~= Vert2
        %UVL_SFM_frontEndShowFeatureMatch(P3D, InputDataInfo, Vert1, Vert2)
        Verts = [Vert1 Vert2];
        VertNew = min(Verts);
        VertOld = max(Verts);
        P3D.Desc(:, VertNew) = (P3D.Desc(:, VertNew) * single(P3D.TotalFrames(VertNew)) + P3D.Desc(:, VertOld) * single(P3D.TotalFrames(VertOld))) / single(P3D.TotalFrames(VertNew) + P3D.TotalFrames(VertOld));
        P3D.DescIndex{VertNew} = [P3D.DescIndex{VertNew} P3D.DescIndex{VertOld}];
        P3D.IsGeometrical(VertNew) = P3D.IsGeometrical(VertNew) | P3D.IsGeometrical(VertOld);
        P3D.IsInitial(VertNew) = P3D.IsInitial(VertNew) | P3D.IsInitial(VertOld);
        P3D.TotalFrames(VertNew) = P3D.TotalFrames(VertNew) + P3D.TotalFrames(VertOld);
        LocalIndicesOld = P3D.DescIndex{VertOld};
        P3D.ViewP3DIndex(LocalIndicesOld) = VertNew;
        P3D.Status(VertOld) = 0;
        Merged = Merged + 1;
    end
end

