function [P3D Merged] = UVL_SFM_mergeViews(P3D, Pair, Param, InputDataInfo)
Merged = 0;
Thresh = Param.Matching.(Param.Description.Descriptors{P3D.DescriptorType}).DistanceThreshold;
PointsIm1 = find(P3D.ViewIndex == Pair(1));
Points3DIm1 = P3D.Status(P3D.ViewP3DIndex(PointsIm1)) == 3;
PointsIm1 = PointsIm1(Points3DIm1);
PointsIm1Desc = P3D.Desc(:, P3D.ViewP3DIndex(PointsIm1));

PointsIm2 = find(P3D.ViewIndex == Pair(2));
Points3DIm2 = P3D.Status(P3D.ViewP3DIndex(PointsIm2)) == 3;
PointsIm2 = PointsIm2(Points3DIm2);
PointsIm2Desc = P3D.Desc(:, P3D.ViewP3DIndex(PointsIm2));

Matches = UVL_SFM_approximateNearestNeighbour(double(PointsIm1Desc), double(PointsIm2Desc), 1, Thresh+.05, 1);
PointsIm1 = PointsIm1(Matches(1,:));
PointsIm2 = PointsIm2(Matches(2,:));

PointsIm1Pos = P3D.ViewPos(:, PointsIm1);
PointsIm2Pos = P3D.ViewPos(:, PointsIm2);

[F, Inliers] = UVL_SFM_ransacFundamentalMatrix(PointsIm1Pos, PointsIm2Pos, .001);

PointsIm1 = PointsIm1(Inliers);
PointsIm2 = PointsIm2(Inliers);

for CurrPair = 1 : numel(PointsIm1)
    Vert1 = P3D.ViewP3DIndex(PointsIm1(CurrPair));
    Vert2 = P3D.ViewP3DIndex(PointsIm2(CurrPair));
    if Vert1 ~= Vert2
        %UVL_SFM_frontEndShowFeatureMatch(P3D, InputDataInfo, Vert1, Vert2)
        Verts = [Vert1 Vert2];
        VertNew = min(Verts);
        VertOld = max(Verts);
        P3D.Desc(:, VertNew) = (P3D.Desc(:, VertNew) * single(P3D.TotalFrames(VertNew)) + P3D.Desc(:, VertOld) * single(P3D.TotalFrames(VertOld))) / single(P3D.TotalFrames(VertNew) + P3D.TotalFrames(VertOld));
        P3D.DescIndex{VertNew} = [P3D.DescIndex{VertNew} P3D.DescIndex{VertOld}];
        P3D.IsGeometrical(VertNew) = P3D.IsGeometrical(VertNew) | P3D.IsGeometrical(VertOld);
        P3D.IsInitial(VertNew) = P3D.IsInitial(VertNew) | P3D.IsInitial(VertOld);
        P3D.TotalFrames(VertNew) = P3D.TotalFrames(VertNew) + P3D.TotalFrames(VertOld);
        LocalIndicesOld = P3D.DescIndex{VertOld};
        P3D.ViewP3DIndex(LocalIndicesOld) = VertNew;
        P3D.Status(VertOld) = 0;
        Merged = Merged + 1;
    end
end

