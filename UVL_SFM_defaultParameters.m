%global LOGFILE_
RESTART_ = 1;
RESUME_ = 0;
FRONTEND_ = 0;

%load K
%[InputDataInfo NbrFrames] = UVL_SFM_initializeInputStream('sequence', 'Images', 'jpg');
%[InputDataInfo NbrFrames] = UVL_SFM_initializeInputStream('datfile', 'images.dat');



% [General Parameters]
Param.General.FrameSimilaritySmoothWin = 31;
Param.General.FrameSimilarityMinOverlap = 0.3;
Param.General.FrameSimilarityFrmRange = 20;
Param.General.FrameSimilaritMinFrameDist = 50;
Param.General.PreAllocatePoints = 10000;
Param.General.PreAllocateDescriptors = 20000;
Param.General.MinimumViewsForInitialization = 7;
Param.General.BundleStep = 20;
Param.General.BundleRange = 50;

% [Image preprocessing]
Param.Processing.SingleChannel = 4;                           % 0-no, 1-red, 2-green, 3-blue, 4-grayscale, 5-PCA, 6-YIQ
Param.Processing.NonUniformLightCorrection = false;           % 
Param.Processing.GaussianSize = 1;                            % 1 = No Gaussian filtering
Param.Processing.Equalization = 1;                            % 0-no, 1-Normalization, 2-Equalization, 3-Clahe, 4-CLAHS
Param.Processing.ResizeCoeff = 1;                             % image decimation (resize)

% [Detector]
% 1:Harris, 2:Laplacian, 3:Hessian, 4:SIFT, 5:SURF, 6:MSER, ...
% 7:Harris-Laplace, 8:Hessian-Laplace, 9:Harris-Affine, ...
% 10:Hessian-Affine, 11:Harris-Hessian-Laplace
Param.Detection.Detectors = {'Harris', 'Laplacian', 'Hessian', 'SIFT', ... % 1-4
                             'SURF', 'MSER', 'HarLap', 'HesLap', ...       % 5-8
                             'HarAff', 'HesAff', 'HarHesLap', 'SIFTGPU'};  % 9-12
                              
Param.Detection.Type = [5];   %#ok<NBRAK>

Param.Detection.Harris.MaxNumber = 1000;                      % max number of features to detect;  
Param.Detection.Harris.RadiusNonMaximal = 4;                  % radius for non-maximal suppression for detector
Param.Detection.Harris.Border = 10; % outer border

Param.Detection.Laplacian.MaxNumber = 1000;                   % max number of features to detect;  
Param.Detection.Laplacian.RadiusNonMaximal = 4;               % radius for non-maximal suppression for detector
Param.Detection.Laplacian.Border = 10;                        % outer border

Param.Detection.Hessian.MaxNumber = 1000;                     % max number of features to detect;  
Param.Detection.Hessian.RadiusNonMaximal = 4;                 % radius for non-maximal suppression for detector
Param.Detection.Hessian.Border = 10;                          % outer border

Param.Detection.SIFT.MaxNumber = 1000;                        % max number of features to detect;  
Param.Detection.SIFT.RadiusNonMaximal = 5;                    % radius for non-maximal suppression for detector
Param.Detection.SIFT.Border = 10;                             % outer border

Param.Detection.SURF.MaxNumber = 1000;                        % max number of features to detect;  
Param.Detection.SURF.RadiusNonMaximal = 4;                    % radius for non-maximal suppression for detector
Param.Detection.SURF.Border = 10;                             % outer border

Param.Detection.MSER.MaxNumber = 1000;                        % max number of features to detect;  
Param.Detection.MSER.RadiusNonMaximal = 4;                    % radius for non-maximal suppression for detector
Param.Detection.MSER.Border = 10;                             % outer border
Param.Detection.MSER.MinimumRegionSize = 6;
Param.Detection.MSER.MinimumMargin = 3; % not used now

Param.Detection.HarLap.MaxNumber = 1000;                         % max number of features to detect;  
Param.Detection.HarLap.RadiusNonMaximal = 4;                     % radius for non-maximal suppression for detector
Param.Detection.HarLap.Border = 10;                              % outer border

Param.Detection.HesLap.MaxNumber = 1000;                         % max number of features to detect;  
Param.Detection.HesLap.RadiusNonMaximal = 4;                     % radius for non-maximal suppression for detector
Param.Detection.HesLap.Border = 10;                              % outer border

Param.Detection.HarAff.MaxNumber = 1000;                         % max number of features to detect;  
Param.Detection.HarAff.RadiusNonMaximal = 4;                     % radius for non-maximal suppression for detector
Param.Detection.HarAff.Border = 10;                              % outer border

Param.Detection.HesAff.MaxNumber = 1000;                         % max number of features to detect;  
Param.Detection.HesAff.RadiusNonMaximal = 4;                     % radius for non-maximal suppression for detector
Param.Detection.HesAff.Border = 10;                              % outer border

Param.Detection.HarHesLap.MaxNumber = 1000;                         % max number of features to detect;  
Param.Detection.HarHesLap.RadiusNonMaximal = 4;                     % radius for non-maximal suppression for detector
Param.Detection.HarHesLap.Border = 10;                              % outer border

% NOTES: Siftgpu is experimental, Siftgpu features can only be described
%        with Siftgpu
Param.Detection.SIFTGPU.MaxNumber = 1000;                        % max number of features to detect;  
Param.Detection.SIFTGPU.RadiusNonMaximal = 5;                    % radius for non-maximal suppression for detector
Param.Detection.SIFTGPU.Border = 10;                             % outer border

% [Descriptors]
% 1:SIFT, 2:SURF, 3:U-SURF, 4:SURF-128, 5:U-SURF-128, ...
% 6:SURF-36, 7:U-SURF-36, 8:ImagePatch, 9:RotatedImagePatch
Param.Description.Descriptors = {'SIFT', 'SURF', 'USURF', 'SURF128', ...         % 1-4
                                 'USURF128', 'SURF36', 'USURF36', ...            % 5-7
                                 'ImagePatch', 'RotatedImagePatch', 'SIFTGPU' }; % 8-10
Param.Description.Dimensions = [128 64 64 128 128 36 36 nan nan 128]; %% last 2 yet to be implemented
Param.Description.Type = [2]; %#ok<NBRAK>

% [Matchers]
% 1:NearestNeighbour, 2:SecondNearestNeighbour,
% 3:SecondNearestNeighbourBidirectionnal
Param.Matching.Type = 2;
Param.Matching.ErrorBounds = 2;                               % Approximation for the nearest neighbor (1 = none)
Param.Matching.MaxMatches = 1;                                % Percentage of matches comparing to the number of points
Param.Matching.SIFT.DistanceThreshold = 0.5;
Param.Matching.SIFTGPU.DistanceThreshold = 0.5;
Param.Matching.SURF.DistanceThreshold = 0.25;
Param.Matching.USURF.DistanceThreshold = 0.25;
Param.Matching.SURF128.DistanceThreshold = 0.5;
Param.Matching.USURF128.DistanceThreshold = 0.5;
Param.Matching.SURF36.DistanceThreshold = 0.15;
Param.Matching.USURF36.DistanceThreshold = 0.15;
Param.Matching.ImagePatch.DistanceThreshold = 0.2;
Param.Matching.RotatedImagePatch.DistanceThreshold = 0.2;
Param.Matching.Descriptors = Param.Description.Descriptors;
Param.Matching.Descriptors = {'SIFT', 'SURF', 'USURF', 'SURF128', ...         % 1-4
                              'USURF128', 'SURF36', 'USURF36', ...            % 5-7
                              'ImagePatch', 'RotatedImagePatch', 'SIFTGPU' }; % 8-10

% [Motion Parmeters]
Param.Motion.MinimumMotion = 30;
Param.Motion.EssentialOutlierRejectionTh = 3;
Param.Motion.EssentialBuckets = 5;
Param.Motion.EssentialSamples = 10;
Param.Motion.EssentialFirstIterationSteps = 2000;
Param.Motion.EssentialSecondIterationSteps = 2000;
Param.Motion.EssentialMaximalRotation = pi/2;
Param.Motion.EssentialMaximalOutlierRatio = .1;
Param.Motion.HomographyInlierThreshold = 2.5;
Param.Motion.HomographyMaximalRotation = pi/2;
Param.Motion.HomographyMaximalOutlierRatio = 0.1;
Param.Motion.HomographyMinimumMotion = 30;
Param.Motion.HomographyMinimumMotionVotes = 10;
Param.Motion.MaximumRetries = 10;
Param.Motion.MaximumScaleError = 0.9;

% [Camera Pose]
Param.CameraPose.RansacSampleSetSize = 6;
Param.CameraPose.OutlierRejectionThRansac = 4; %in pixels
Param.CameraPose.OutlierRejectionThNL = 3; %in pixels
Param.CameraPose.CheckAttSmothness = true; %check for big diffs in camera attitude
Param.CameraPose.AttSmothnessDelta = 1.2; %diff th



% [Vertex Position]
Param.VertexPosition.MinimumViews = 3;
Param.VertexPosition.MinimumInliers = 3;
Param.VertexPosition.MinimumDisparity = 12;
Param.VertexPosition.BackProjectionMaximumError = 1;

% [Database maintenance]
Param.EraseFeatures.LastSeen = 4;
Param.EraseFeatures.Rematch = false;

% [Ortho-Mosaic]
Param.OrthoMosaic.Resolution = 800 * 600;
Param.OrthoMosaic.MeshResolution = 200 * 200;
Param.OrthoMosaic.Interpolation = 'cubic';   %% interpolation can be either set to 'linear' or 'cubic'
Param.OrthoMosaic.FrameUpdateStep = 1;
Param.OrthoMosaic.FrameOffset = 0;  %% offset in frame index
Param.OrthoMosaic.InitialAngle = 30;  %% Projection ray angle higher than this is not updated
Param.OrthoMosaic.MinAngleInprovement = .2; %% Projection ray angle improvement below this is not updated
Param.OrthoMosaic.DisplayProgress = true;
Param.OrthoMosaic.FrameBorder = 10;

% [Geometrical]
Param.Geometrical.Enable = 0;
Param.Geometrical.FrameDec = 2;
Param.Geometrical.Border = 10;
Param.Geometrical.Subsampling = 1;
Param.Geometrical.SigmaDepthMap = 2;
Param.Geometrical.FlowDecLevels = 3;
Param.Geometrical.SegmentationObjectElements = 200;
Param.Geometrical.SegmentationWindowSize = 35;
Param.Geometrical.SegmentationThreshold = 0.5;
Param.Geometrical.MinimumEdgeElements = 35;
Param.Geometrical.InitialDerivativeSigma = 1;
Param.Geometrical.SigmaSteps = 5;
Param.Geometrical.MinimalBrenchElements = 7;
Param.Geometrical.CurvatureSmoothing = 3;
Param.Geometrical.RegionSuppresion = 35;
Param.Geometrical.MaximumDistance = 40;
Param.Geometrical.NonExploredThreshold = 0.1; %%% of the image surface
% Optical flow related parameters
Param.Geometrical.WindowSize = 7 ;
Param.Geometrical.NumLevels = 5 ;
Param.Geometrical.MField = 3 ;
Param.Geometrical.Weight = 1 ;
Param.Geometrical.Method = 'color_3' ;
