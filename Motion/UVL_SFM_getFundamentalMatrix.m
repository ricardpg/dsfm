function [F, A, R] = UVL_SFM_getFundamentalMatrix(PointsImg1, PointsImg2)

% Determina��o da Matriz Fundamental usando m�nimos quadrados.
% N�o se for�a det(F) = 0.
% S�o necess�rias pelo menos 8 pontos em cada lista de coordenadas.
%
% [F,CONDS] = lsfund(COOR1,COOR2) devolve tambem o n� de condi��o do sistema (A'*A).
% [F,CONDS,A,R] = lsfund(COOR1,COOR2) devolve a matriz do sistema A e o vector de res�duos R.

PointsImg1(3,:) = 1;
PointsImg2(3,:) = 1;

A = zeros(size(PointsImg1, 2), 9);

for l = 1 : size(PointsImg1, 2)
    ML = PointsImg2(:,l) * PointsImg1(:,l)';
    A(l,:) = ML(:)';
end

% get the unit eigvector of A'*A corresponding to the smallest eigenvalue of A'*A.
[V,D] = eig(A'*A);
[Y,I] = min(diag(D)); 
N = V(:,I);
N = N / sqrt(N' * N);

% compute the residuals
R = A * N;
F = reshape(N, 3, 3)';

% % Get Singular Aproximation Using Frobenius Distance
% [U,D,V] = svd(F);
% D(1,1) = (D(1,1)+D(2,2))/2;
% D(2,2) = D(1,1);
% D(end) = 0;
% F = U*D*V'
