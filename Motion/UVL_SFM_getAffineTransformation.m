function  H = getAffineTransformation(corr, bySvd)

% H = getAffineTransformation(corr, bySvd)
%
%     corr  - Array of points and matches
%                 row 1: point x
%                 row 2: point y
%                 row 3: match x
%                 row 4: match y
%     bySvd - false = Least-Squares; true = svd (default = true)
%
%     H     - Resulting affine transformation matrix
%
% Compute a affine transformation from points and matches
%
% Developed by Tudor Nicosevici, Rafael Garcia and Ricard Prados
% Dep. of Electronics, Informatics and Automation, University of Girona



    UMat = zeros(size(corr, 2)*2,6);
    X    = zeros(size(corr, 2)*2,1);

    for Cnt_Punts = 1 : size(corr, 2),
       UMat(Cnt_Punts*2-1,1) = corr(1, Cnt_Punts); %a
       UMat(Cnt_Punts*2-1,2) = corr(2, Cnt_Punts); %b
       UMat(Cnt_Punts*2-1,3) = 1;                 %c
       UMat(Cnt_Punts*2,4)   = corr(1, Cnt_Punts);
       UMat(Cnt_Punts*2,5)   = corr(2, Cnt_Punts);
       UMat(Cnt_Punts*2,6)   = 1;

       X(Cnt_Punts*2-1) = corr(3, Cnt_Punts);
       X(Cnt_Punts*2)   = corr(4, Cnt_Punts);
    end
    
    if exist('bySvd', 'var') && bySvd == true,
        [U, S, V] = svd(UMat, 'econ');
        A = V * diag ( 1 ./ ( diag ( S ) + eps ) ) * U' * X;
    else
        A = inv(UMat'*UMat) * UMat' * X;
    end

    H = [A(1) A(2) A(3);
         A(4) A(5) A(6);
            0    0    1];
end