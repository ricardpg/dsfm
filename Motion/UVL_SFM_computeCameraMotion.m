function [CurrentCameraMotion P3D Features] = UVL_SFM_computeCameraMotion(P3D, Features, K, MotionParameters)
%% extract correspondences between current view and base view
PairIndices = [];
Correspondences = [];
CurrentCameraMotion = [];


for FeatureType = 1 : numel(Features)
    PairsNo = size(Features(FeatureType).Pairs, 2);
    PairIndicesGroup = zeros(1, PairsNo);
    p = [];
    DBItem = find([P3D(:).PointType] == Features(FeatureType).PointType & [P3D(:).DescriptorType] == Features(FeatureType).DescriptorType);
    for CurrPair = 1 : PairsNo
        LocalIndex = P3D(DBItem).DescIndex{Features(FeatureType).Pairs(2,CurrPair)}(1);
        ViewIndex = P3D(DBItem).ViewIndex(LocalIndex);
        if ViewIndex == 1
            PairIndicesGroup(CurrPair) = 2;
            p = [p double(P3D(DBItem).ViewPos(:, LocalIndex))];
        end
    end
    PairIndices = [PairIndices PairIndicesGroup];
    Correspondences = [Correspondences [p; Features(FeatureType).Points(1:2, Features(FeatureType).Pairs(1, logical(PairIndicesGroup)))]];
end
InlierMask = false(1, size(Correspondences, 2));

[Motion inliers] = UVL_SFM_cameraMotionEH(Correspondences, K, MotionParameters);
if isempty(Motion)
    CurrentCameraMotion = [];
    return
end
Correspondences = Correspondences(:, inliers);
P = UVL_SFM_LSQIntersection(Correspondences(1:2,:), Correspondences(3:4,:), eye(4), inv(Motion), K);
GoodPoints = P(3,:) > 0;  %% check point depth
inliers(inliers) = GoodPoints;

CorrespPosition = find(PairIndices);
PairIndices(CorrespPosition(inliers)) = 1;
Correspondences = Correspondences(:, GoodPoints);

P = P(:, GoodPoints);
ViewPoses{1} = eye(4); % everything is computed in the coordinates of the 1st camera
ViewPoses{2} = Motion;
[P_SBA Views_SBA SBAerror] = UVL_SFM_adjustTwoViewsSBA(P, Correspondences, ViewPoses, K);
if isempty(P_SBA) || SBAerror > 1 % could not find a consistent solution
    return
end
CurrentCameraMotion = Views_SBA{2};

% % get rid of outliers
% InlierMask(inliers) = true;
% Outliers = ~InlierMask;
% ValidPairIndices = find(PairIndices);
% InlierMask = true(1, numel(PairIndices));
% InlierMask(ValidPairIndices(Outliers)) = false; % get inlier mask

PCursor = 1;

for FeatureType = 1 : numel(Features)
    DBItem = find([P3D(:).PointType] == Features(FeatureType).PointType & [P3D(:).DescriptorType] == Features(FeatureType).DescriptorType);
    PairsNo = size(Features(FeatureType).Pairs,2);
    PairIndicesGroup = PairIndices(1:PairsNo); % cut out mask for specific feature group
    PairIndices = PairIndices(PairsNo+1:end);
    for PairCount = 1 : PairsNo
        if PairIndicesGroup(PairCount) == 1
            P3D(DBItem).Pos(:, Features(FeatureType).Pairs(2, PairCount)) = P_SBA(:, PCursor);
            P3D(DBItem).Status(Features(FeatureType).Pairs(2, PairCount)) = uint8(3);
            P3D(DBItem).IsInitial(Features(FeatureType).Pairs(2, PairCount)) = true;
            PCursor = PCursor + 1;
        end
    end
    InlierMask = PairIndicesGroup ~= 2;  %%% outliers
    Features(FeatureType).Pairs = Features(FeatureType).Pairs(:, InlierMask);
end

