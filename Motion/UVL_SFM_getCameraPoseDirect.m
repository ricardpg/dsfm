function [CameraPose Features] = UVL_SFM_getCameraPoseDirect(P3D, Features, K, CameraPoseParams)

% concatenate data and pickup the pairs (2D -> 3D)
PairIndices = [];
P = [];
p = [];

for FeatureType = 1 : numel(Features)
    PairsNo = size(Features(FeatureType).Pairs, 2);
    PairIndicesGroup = zeros(1, PairsNo);
    DBItem = find([P3D(:).PointType] == Features(FeatureType).PointType & [P3D(:).DescriptorType] == Features(FeatureType).DescriptorType);
    ValidP3D = P3D(DBItem).Status(Features(FeatureType).Pairs(2, :)) == 3; % have 3D position
    PairIndicesGroup(ValidP3D) = 2;
    ValidPairs = Features(FeatureType).Pairs(:, ValidP3D);
    P = [P P3D(DBItem).Pos(:, ValidPairs(2, :))];
    p = [p double(Features(FeatureType).Points(1:2, ValidPairs(1, :)))];
    PairIndices = [PairIndices PairIndicesGroup];
end
UVL_SFM_printOut(sprintf(' INIT: %d c,', size(P, 2)));

[CameraPoseRANSAC, InliersRANSAC, Method, Transformations] = UVL_SFM_ransacCameraPoseMult(P, p, K, CameraPoseParams.OutlierRejectionThRansac);
if isempty(CameraPoseRANSAC)
    UVL_SFM_printOut(sprintf(' RANSAC: giving up'));
    CameraPose = [];
    Features = []; %remove the outliers ... all !
    return
end
NoInliers = sum(InliersRANSAC);
UVL_SFM_printOut(sprintf(' RANSAC[%d]: %d c,', Method, NoInliers)); drawnow('update') ;
InliersSBA = InliersRANSAC;
CameraPoseSBA = CameraPoseRANSAC;
SBARuns = 1;
while true  % run until number of inliers stabilizes
    CameraPoseSBA = UVL_SFM_adjustCameraDirectSBA(P(:,InliersSBA), p(:,InliersSBA), CameraPoseSBA, K); % NL adjust of camera position
    BPDistances = UVL_SFM_singlePoseBackProjectionError(P, p, CameraPoseSBA, K); % compute back-projection
    InliersSBA = BPDistances <= CameraPoseParams.OutlierRejectionThNL; % get inliers within distance threshold
    NewNoInliers = sum(InliersSBA);
    if NewNoInliers <= NoInliers
        break
    end
    NoInliers = NewNoInliers;
    SBARuns = SBARuns + 1;
end
UVL_SFM_printOut(sprintf(' NL[%d runs]: %d c.', SBARuns, sum(InliersSBA)));
if(sum(InliersSBA)) < max(size(P,2) / 10, 12)  %% at least 10% inliers
    UVL_SFM_printOut(sprintf(' NL: giving up'));
    CameraPose = [];
    Features = []; %remove the outliers ... all !
    return
end



CorrespPosition = find(PairIndices);
PairIndices(CorrespPosition(InliersSBA)) = 1;

% get rid of outliers
for FeatureType = 1 : numel(Features)
    FTypeElements = size(Features(FeatureType).Pairs, 2);
    PairIndicesGroup = PairIndices(1:FTypeElements);
    PairIndices = PairIndices(FTypeElements+1:end);
    ValidIndicesGroup = PairIndicesGroup ~= 2; %% remove outliers
    Features(FeatureType).Pairs = Features(FeatureType).Pairs(:, ValidIndicesGroup);
end

CameraPose = CameraPoseSBA;
