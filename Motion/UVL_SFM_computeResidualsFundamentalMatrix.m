function Residuals = UVL_SFM_computeResidualsFundamentalMatrix(F, PointsImg1, PointsImg2)

% Compute the distance of a points to its corresponding epipolar line


PointsImg1(3,:) = 1;
PointsImg2(3,:) = 1;

EpiLinesInImg2 = F' * PointsImg1;
EpiLinesInImg1 = F  * PointsImg2;

% Condition : match' * F * point = 0
DistInImg1 = abs(sum(PointsImg1 .* EpiLinesInImg1));
DistInImg2 = abs(sum(PointsImg2 .* EpiLinesInImg2)); % should be equal to 0 why divide by norm ?

NormEpiLinesInImg2 = sqrt(sum(EpiLinesInImg2.^2)); %why divide by norm ?
NormEpiLinesInImg1 = sqrt(sum(EpiLinesInImg1.^2));

Residuals = ((DistInImg2 ./ NormEpiLinesInImg2) + (DistInImg1 ./ NormEpiLinesInImg1)) / 2;

