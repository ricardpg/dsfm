
	
#include <math.h>
#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double *H, det, *points, distx, disty, dist1, dist2, *out;
    int nbrCorr, nbrLine;
    register int i;
    
	/* parameter checks */
	if ((nrhs != 2) || (nlhs < 1)) {
		mexErrMsgTxt("Usage: [residuals] = UVL_SFM_computeResidualsHomography(corr, H)\n");
		return;
	}
	
	/* reading the parameters */
	points  = static_cast<double*>(mxGetPr(prhs [0]));
	nbrCorr = mxGetN(prhs [0]);
    nbrLine = mxGetM(prhs [0]);
	H = mxGetPr(prhs [1]);
	
    plhs [0] = mxCreateDoubleMatrix(1, nbrCorr, mxREAL);
    out = mxGetPr(plhs [0]);
    

    double invH[9];
    //invH = static_cast<double*>(mxMalloc(9*sizeof(double)));
    det = (H[0]*H[4] - H[0]*H[7]*H[5] - H[3]*H[1] + H[3]*H[7]*H[2] + H[6]*H[1]*H[5] - H[6]*H[4]*H[2]);
    invH[8] = ((H[0] * H[4] - H[3] * H[1]) / det);
    invH[7] = ((H[6] * H[1] - H[0] * H[7]) / det) / invH[8];
    invH[6] = ((H[3] * H[7] - H[6] * H[4]) / det) / invH[8];
    invH[5] = ((H[3] * H[2] - H[0] * H[5]) / det) / invH[8];
    invH[4] = ((H[0]        - H[2] * H[6]) / det) / invH[8];
    invH[3] = ((H[6] * H[5] - H[3]       ) / det) / invH[8];
	invH[2] = ((H[1] * H[5] - H[4] * H[2]) / det) / invH[8];
    invH[1] = ((H[2] * H[7] - H[1]       ) / det) / invH[8];
    invH[0] = ((H[4]        - H[5] * H[7]) / det) / invH[8];
    invH[8] = 1;
		
    for(i=0; i<nbrCorr; ++i)
    {
        det   =            (H[2] * points[0] + H[5] * points[1] + H[8]); 
        distx = points[2]-((H[0] * points[0] + H[3] * points[1] + H[6]) / det);
        disty = points[3]-((H[1] * points[0] + H[4] * points[1] + H[7]) / det);
        dist1 = sqrt(distx*distx + disty*disty);
        det   =            (invH[2] * points[2] + invH[5] * points[3] + invH[8]); 
        distx = points[0]-((invH[0] * points[2] + invH[3] * points[3] + invH[6]) / det);
        disty = points[1]-((invH[1] * points[2] + invH[4] * points[3] + invH[7]) / det);
        dist2 = sqrt(distx*distx + disty*disty);
        out[i] = (dist1 + dist2) / 2;
        points += nbrLine;
    }
    //mxFree(invH);
    return;
}



