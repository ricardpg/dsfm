function [BestT, BestInliers, BestMethod] = UVL_SFM_ransacCameraPose(P, p, K, Threshold, PreviousAtt, AttDelta)
    if ~exist('AttDelta', 'var')
        AttDelta = 0;
    end
    Threshold = Threshold.^2;
    PHomo = [P; ones(1, size(P,2))];

    MinPoints = 6;

    BestT = [];
    BestInliers = [];
    BestNumInliers = 6;
    BestMethod = 0; %% none so far
    NumPoints = size(P,2);
    Probability = 0.999; % Desired probability of choosing at least one sample free from outliers
    NumTrialsForProbability = 100000;
    
    Buckets = UVL_SFM_divideIntoBuckets(p, 10);
    for count = 0 : NumTrialsForProbability 
        RandomSamples = UVL_SFM_randomCorrespondences(Buckets, MinPoints, false);
%        [ignore RandomSamples] = sort(rand(1, NumPoints));
%        RandomSamples = RandomSamples(1:MinPoints);
        if numel(RandomSamples) == MinPoints,
%%      Method 1: Transformation by the Homography
            [t, R, Method] = UVL_SFM_computeExtrinsicParameters(p(:, RandomSamples), P(:, RandomSamples), K);
            if numel(t),
                % check only if AttDelta != 0
                if AttDelta
                    AttDiff = norm(PreviousAtt - R);
                else
                    AttDiff = -inf;
                end
                if AttDiff < AttDelta
                    Tr = [R t];
                    Pj = K * Tr;
                    pp = Pj * PHomo;
                    pp(1,:) = pp(1,:) ./ pp(3,:);
                    pp(2,:) = pp(2,:) ./ pp(3,:);
                    dist = (p(1,:)-pp(1,:)).^2 + (p(2,:)-pp(2,:)).^2; % dont do the sqrt() -> faster to square the threshold
                    Inliers = dist <= Threshold; % we can use logical indexing and sum instead of length
                    NumInliers = sum(Inliers);

                    if NumInliers > BestNumInliers     
                        BestNumInliers = NumInliers;  
                        BestInliers = Inliers;
                        BestT = Tr;
                        BestMethod = Method;

                        pNoOutliers = 1 - (NumInliers/NumPoints)^MinPoints;
                        pNoOutliers = min(1-eps, max(eps, pNoOutliers));% Avoid division by -Inf and division by 0.
                        NumTrialsForProbability = log(1-Probability)/log(pNoOutliers);
                    end
                end
            end
            if count > NumTrialsForProbability, return; end
        end
    end
    
    
end
