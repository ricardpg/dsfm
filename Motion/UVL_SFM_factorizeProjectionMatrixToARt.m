function [A,R,t] = UVL_SFM_factorizeProjectionMatrixToARt(P,fsign)
% ART  factorize the  PPM P as P=A*[R;t]
%
%         [A,R,t] = art(P,fsign)  factorize the projection matrix P 
%         as P=A*[R;t] and enforce the sign  fsign for the focal lenght. 
%         By defaukt fsign=1.

%         Andrea Fusiello, 1999 (fusiello@sci.univr.it)

%         29/01/2004: sign enforcing added to fix the QR ambiguity;
%         10/04/2204: changed default focal lenght to positive
%         27/04/2004; fixed sign of B(3,3)


% by default assume POSITIVE focal lenght
if nargin == 1
    fsign = 1;
end

Q = inv(P(1:3, 1:3));
[U,B] = qr(Q);

% fix the sign of B(3,3)
U=U*sign(B(3,3));
B=B*sign(B(3,3));


% if the sign of the focal lenght is not the required one, 
% change it, and change the rotation accordingly.

if fsign*B(1,1) < 0
     E= [-1     0     0
         0    1     0
         0     0     1];
     B = E*B;
     U = U*E;
 end
 
 if fsign*B(2,2) < 0
     E= [1     0     0
         0    -1     0
         0     0     1];
     B = E*B;
     U = U*E;
 end
 
% sanity check 
% if norm(Q-U*B)>1e-10 error('Something wrong with the factorization.'); end
% if det(U)<0 error('U is not a rotation matrix'); end
if det(U)<0 A=[];R=[];t=[]; return; end
if norm(Q-U*B)>1e-10 A=[];R=[];t=[]; return; end
R = inv(U);
t = B*P(1:3,4);
A = inv(B);
A = A ./A(3,3);


