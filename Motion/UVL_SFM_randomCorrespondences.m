function RandomSamples = UVL_SFM_randomCorrespondences(Buckets, NumPoints, Extend)

% RandomSamples = randomCorrespondences(Buckets, NumPoints, Extend)
%
%       Buckets       - Buckets containing the arrays of points and matches
%       NumPoints     - Number of points to get
%       Extend        - Boolean that says if the neighbooring buckets are
%                       also set to 0
%
%       RandomSamples - Array of random samples
%
% Randomly extracts indeces from the buckets 
% (see UVL_SFM_divideIntoBuckets())
%
% Developed by Olivier Delaunoy (delaunoy@eia.udg.es)
% Dep. of Electronics, Informatics and Automation, University of Girona

    if ~exist('Extend', 'var'), Extend = true; end


    RandomSamples= zeros(1, NumPoints);
    PickedPoints = 0;

    if Extend,
        [h w] = size(Buckets);
        [ignore ind] = sort(rand(1,h*w));
        for i=1:length(ind),
            in = ind(i);
            if Buckets(in).NumEl ,
                PickedPoints = PickedPoints + 1;
                RandomSamples(PickedPoints) = Buckets(in).Index(ceil(rand * Buckets(in).NumEl));          
                if PickedPoints == NumPoints, return; end
                Buckets(max(1,in-h-1)).NumEl = 0;
                Buckets(max(1,in-h)).NumEl = 0;
                Buckets(max(1,in-h+1)).NumEl = 0;
                Buckets(max(1,in-1)).NumEl = 0;
                Buckets(in).NumEl = 0;
                Buckets(min(h*w,in+1)).NumEl = 0;
                Buckets(min(h*w,in+h-1)).NumEl = 0;
                Buckets(min(h*w,in+h)).NumEl = 0;
                Buckets(min(h*w,in+h+1)).NumEl = 0;
            end
        end
    else
        [ignore ind] = sort(rand(1,numel(Buckets)));
        for i=1:numel(ind),
            if Buckets(ind(i)).NumEl, %faster to use the ind vector
                PickedPoints = PickedPoints + 1;
                RandomSamples(PickedPoints) = Buckets(ind(i)).Index(ceil(rand * Buckets(ind(i)).NumEl));          
                if PickedPoints == NumPoints, return; end
            end
        end
    end
    RandomSamples = RandomSamples(1:PickedPoints);
end