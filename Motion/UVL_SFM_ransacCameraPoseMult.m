function [BestT, BestInliers, BestMethod, Transformations] = UVL_SFM_ransacCameraPoseMult(P, p, K, Threshold)

Threshold = Threshold.^2;
PHomo = [P; ones(1, size(P,2))];
TotalPoints = size(P,2);
MinPoints = 6;

BestT = [];
BestInliers = [];
BestNumInliers = 6;
BestMethod = 0; %% none so far
NumPoints = size(P,2);
Probability = 0.99; % Desired probability of choosing at least one sample free from outliers
% Probability = 0.95
% NumTrialsForProbability = 300000;
NumTrialsForProbability = 10000;
Transformations = [];
Buckets = UVL_SFM_divideIntoBuckets(p, 6);
for count = 0 : NumTrialsForProbability,
%     RandomSamples = UVL_SFM_randomCorrespondences(Buckets, MinPoints, false);
    [ignore, RandomSamples] = sort(rand(1, NumPoints)) ;
    RandomSamples = RandomSamples(1:MinPoints) ;
    if numel(RandomSamples) == MinPoints,
        %%      Method 1: Transformation by the Homography
        [t, R, Method] = UVL_SFM_computeExtrinsicParameters(p(:, RandomSamples), P(:, RandomSamples), K);
        if numel(t),            
            Tr = [R t] ;
            % check the cheriality constrain
 %           P_cam = [Tr; [0 0 0 1]] * [P(:, RandomSamples); ones(1,MinPoints)];
 %           if any(P_cam(3,:) <= 0)
 %               continue
 %           end
            P_cam = [Tr; [0 0 0 1]] * PHomo;
            P_front = sum(P_cam(3, :) > 0);
            if P_front < (.5 * TotalPoints);
                continue
            end
            Pj = K * Tr;
            pp = Pj * PHomo;
            pp(1,:) = pp(1,:) ./ pp(3,:);
            pp(2,:) = pp(2,:) ./ pp(3,:);
            dist = (p(1,:)-pp(1,:)).^2 + (p(2,:)-pp(2,:)).^2; % dont do the sqrt() -> faster to square the threshold
            Inliers = dist <= Threshold; % we can use logical indexing and sum instead of length
            NumInliers = sum(Inliers);
            
            if NumInliers > BestNumInliers
                BestNumInliers = NumInliers;
                BestInliers = Inliers;
                BestT = Tr;
                BestMethod = Method;
                
                pNoOutliers = 1 - (NumInliers/NumPoints)^MinPoints;
                pNoOutliers = min(1-eps, max(eps, pNoOutliers));% Avoid division by -Inf and division by 0.
                NumTrialsForProbability = log(1-Probability)/log(pNoOutliers);
            end
        end
    end
    count
    BestNumInliers
    if count > NumTrialsForProbability, return; end
end


end
