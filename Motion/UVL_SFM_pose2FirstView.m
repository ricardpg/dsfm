function Poses1st = UVL_SFM_pose2FirstView(Poses)

invP1 = inv(Poses{1});
Poses1st = cell(1, numel(Poses));
Poses1st{1} = eye(4);
for i = 2 : numel(Poses)
    Poses1st{i} = Poses{i} * invP1;
end
