function dist = UVL_SFM_singlePoseBackProjectionError(P, p, Pose, K)
Pose = Pose(1:3, :);
Pj = K * Pose;
P = [P; ones(1, size(P,2))];
pp = Pj * P;
pp(1,:) = pp(1,:) ./ pp(3,:);
pp(2,:) = pp(2,:) ./ pp(3,:);
dist = sqrt((p(1,:)-pp(1,:)).^2 + (p(2,:)-pp(2,:)).^2);