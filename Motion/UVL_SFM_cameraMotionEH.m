function [motion Inliers] = UVL_SFM_cameraMotionEH(Features, K, motionParameters)

H_OK = 0;
E_OK = 0;
Inliers = [];
motion = [];

%% Compute homography

%                    1st Camera      2nd Camera
%H = CalcHomoPrj ( Features(1:2,:), Features(3:4,:), 1 );
% MotionEstimationPrs.HomographyModel = 'Projective';
% MotionEstimationPrs.SpatialStdev    = 2;        % stdev of spatial coord of features, in pixels
% [ H_RANSAC, inliers_H ] = EstimateMotion2D ( Features(1:2,:), Features(3:4,:), [1:size(Features,2); 1:size(Features,2)],...
%     MotionEstimationPrs);
[ H, inliers_H ] = UVL_SFM_ransacHomography(Features, 'nlp', 5);
% compute center distances
c1 = [K(1,3) K(2,3) 1]';
c2 = H * c1;
c2 = c2 / c2(3);
dist = norm(c1-c2);
if dist < motionParameters.MinimumMotion
    UVL_SFM_printOut(sprintf('Motion too small...\n'));
    motion = [];
    return;
end
% compute inliers
%inliers_H = inliersH(Features, H, motionParameters);
% if there are ouliers
% H_RANSAC = inv(H_RANSAC);
% H = getNLProjectiveTransformation(Features(:, inliers_H)', H_RANSAC);
% discompose homography
[R1H, t1H, n1, R2H, t2H] = UVL_SFM_homography2Rt(inv(K) * H * K);

if det(R1H) < 0 || det(R2H)<0
    [R1H, t1H, n1, R2H, t2H] = UVL_SFM_homography2Rt(-inv(K) * H * K);
end

t1H = -R1H * t1H;
t2H = -R2H * t2H;

% check for aberant rotations
mH = cell(0, 0);
% check for aberant rotations
if(~sum(abs(rodrigues(R1H)) > motionParameters.HomographyMaximalRotation))
    mH = [mH [R1H t1H; [0 0 0 1]]];
end
if(~sum(abs(rodrigues(R2H)) > motionParameters.HomographyMaximalRotation))
    mH = [mH [R2H t2H; [0 0 0 1]]];
%    load CurrentCameraMotion.mat
%    mH = [mH CurrentCameraMotion]; 
end
bestMotionH=[];
if numel(mH)
    % check for depth sign and residuals
    [residualsH ZsignH] = UVL_SFM_cameraMotionCheck(mH, Features, K);

    % check the ratio of "behind the camera" points
    ratios = ZsignH(:,1) ./ (ZsignH(:,2)+eps);
    minRatio = min(ratios);
    if minRatio < motionParameters.HomographyMaximalOutlierRatio
        candidateMotionH = find(ratios == minRatio);
        % if multiple candidates, choose the lowest residual
        bestCandidateMotionH = residualsH(candidateMotionH)==min(residualsH(candidateMotionH));
        bestMotionHpos = candidateMotionH(bestCandidateMotionH);
        bestMotionH = mH{bestMotionHpos};
        bestMotionHResidual = residualsH(bestMotionHpos);
        % check if for ouliers
%        if minRatio > 0
%            inliers = find(signDetails(bestMotionHpos,:) > 0);
%            Features = Features(:,inliers);
%            inliers_H = inliers_H(inliers);
%        end
        H_OK = 1;
    end
end
if ~H_OK
%    fprintf('    Degenerated H!\n')
end

%% compute E
% Normalize the points and the threshold in order to have the 
% essential matrix instead of the fundamental matrix.
invK = inv(K);
Tolerance = motionParameters.EssentialOutlierRejectionTh * ((invK(1,1) + invK(2,2)) / 2);
PointsImg1 = Features(1:2,:);
PointsImg1(3,:) = 1;
PointsImg2 = Features(3:4,:);
PointsImg2(3,:) = 1;
PointsImg1uv = invK * PointsImg1;
PointsImg2uv = invK * PointsImg2;

[E, inliers_E] = UVL_SFM_ransacFundamentalMatrix(PointsImg1uv, PointsImg2uv, Tolerance);

% Enforce 2 sigular values to be equal (condition for essential matrix)
[U,D,V] = svd(E);
D(1,1) = (D(1,1)+D(2,2))/2;
D(2,2) = D(1,1);
D(end) = 0;
E = U*D*V';

E = E';
E = E / norm(E);

% descompose E and get motion
[U,S,V] = svd(E);
W = [0 -1 0;
    1  0 0;
    0  0 1];
R1E = U * W * V';
R2E = U * W' * V';
t1E =  U(:,3);
t2E = -U(:,3);
if det(R1E) < 0
    R1E = -R1E;
end
if det(R2E) < 0
    R2E = -R2E;
end
mE = cell(0, 0);
% check for aberant rotations
if(~sum(abs(rodrigues(R1E))>motionParameters.EssentialMaximalRotation))
    mE = [mE [R1E t1E;[0 0 0 1]]];
    mE = [mE [R1E t2E;[0 0 0 1]]];
end
if(~sum(abs(rodrigues(R2E))>motionParameters.EssentialMaximalRotation))
    mE = [mE [R2E t1E;[0 0 0 1]]];
    mE = [mE [R2E t2E;[0 0 0 1]]];
end
bestMotionE=[];
if numel(mE)
    % check for depth sign and residuals
    [residualsE ZsignE] = UVL_SFM_cameraMotionCheck(mE, Features, K);
    % check the ratio of "behind the camera" points
    ratios = ZsignE(:,1) ./ (ZsignE(:,2)+eps);
    minRatio = min(ratios);
    if minRatio < motionParameters.EssentialMaximalOutlierRatio
        candidateMotionE = find(ratios == minRatio);
        % if multiple candidates, choose the lowest residual
        bestCandidateMotionE = residualsE(candidateMotionE)==min(residualsE(candidateMotionE));
        bestMotionEpos = candidateMotionE(bestCandidateMotionE);
        bestMotionE = mE{bestMotionEpos};
        bestMotionEResidual = residualsE(bestMotionEpos);
        % check if for ouliers
%        if minRatio > 0
%            inliers = find(signDetails(bestMotionEpos,:) > 0);
%        end
        E_OK = 1;
    end
end

%% decide the best motion model
if ~E_OK && ~H_OK
    UVL_SFM_printOut(sprintf('Both models failed\n'));
    Inliers = [];
    motion = [];
    return
end
if E_OK && ~H_OK
    motion = bestMotionE;
    UVL_SFM_printOut(sprintf('Using essential matrix\n'));
    Inliers = inliers_E;

end
if ~E_OK && H_OK
    motion = bestMotionH;
    UVL_SFM_printOut(sprintf('Using homography\n'));
    Inliers = inliers_H;

end
if E_OK && H_OK
    if bestMotionEResidual < bestMotionHResidual
        motion = bestMotionE;
        UVL_SFM_printOut(sprintf('Using essential matrix\n'));
        Inliers = inliers_E;
    else
        motion = bestMotionH;
        UVL_SFM_printOut(sprintf('Using homography\n'));
        Inliers = inliers_H;
    end
end