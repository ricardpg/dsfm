%  Author(s)     : Tudor Nicosevici, Olivier Delaunoy
%  e-mail        : { tudor, delaunoy } @ eia.udg.edu
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : UVL SFM
%  File          : UVL_SFM_computeExtrinsicParameters.m
%  Compiler      : MATLAB >= 7.0
%-----------UVL_SFM_computeExtrinsicParameters---------------
% Computes camera pose provided a set of 2D-3D correspondences and camera
% calibration. 
% Use: [t, R, Method] = UVL_SFM_computeExtrinsicParameters(p, P, K)
% Input:  p - [2xn] set of 2D image points 
%         P - [3xn] set of 3D verteces
%         K - [3x3] camera calibration matrix
% Output: t - [3x1] camera translation
%         R - [3x3] camera rotiation
%         Method - method used to compute the camera pose: 1 - using
%         homografies if the scene is close to planar, 2 - using projection
%         matrix otherwise

function [t, R, Method] = UVL_SFM_computeExtrinsicParameters(p, P, K)
    Method = 0;

    pn = [(p(1,:) - K(1,3))/K(1,1);(p(2,:) - K(2,3))/K(2,2)]; % Subtract principal point, and divide by the focal length
    NumPoints = size(pn,2);

%% Check for planarity of the structure:
    X_mean = mean(P, 2);
    Y = P - (X_mean*ones(1, NumPoints));
    YY = Y*Y';
    [U,S,V] = svd(YY);
    r = S(3,3)/S(2,2);

    if (r < 1e-3) || (NumPoints < 5),
%% If planar
        % Transform the plane to bring it in the Z=0 plane:
        R_transform = V';
        if norm(R_transform(1:2,3)) < 1e-6, R_transform = eye(3); end;
        if det(R_transform) < 0, R_transform = -R_transform; end;
        T_transform = -(R_transform) * X_mean;
        X_new = R_transform * P + T_transform * ones(1,NumPoints);

        % Compute the planar homography:
        H = UVL_SFM_computeHomography(pn, X_new(1:2,:));

        % De-embed the motion parameters from the homography:
        sc = mean([norm(H(:,1));norm(H(:,2))]);
        H = H/sc;

        u1 = H(:,1);
        u1 = u1 / norm(u1);
        u2 = H(:,2) - dot(u1,H(:,2)) * u1;
        u2 = u2 / norm(u2);
        u3 = cross(u1,u2);
        R = [u1 u2 u3];
        if any(isnan(R)),
            R = []; t = []; return;
        end

        t = H(:,3) + R * T_transform;
        R = R * R_transform;
        Method = 1;  %%% corresp to 
    else
%% If non planar
       % Computes an initial guess for extrinsic parameters (works for general 3d structure, not planar!!!):
       % The DLT method is applied here!!

        A = zeros(NumPoints*2, 12);
        for i = 1:NumPoints
            A(i*2-1, :) = [-P(1,i) 0       pn(1,i)*P(1,i) -P(2,i) 0       pn(1,i)*P(2,i) -P(3,i) 0      pn(1,i)*P(3,i)   -1 0  pn(1,i)];
            A(i*2, :) =   [0       P(1,i) -pn(2,i)*P(1,i)  0      P(2,i) -pn(2,i)*P(2,i)  0      P(3,i) -pn(2,i)*P(3,i)   0 1 -pn(2,i)];
        end
        [U, S, V] = svd(A'*A);
        RR = reshape(V(1:9, 12), 3, 3);
        if det(RR) < 0,
            V(:,12) = -V(:,12);
            RR = -RR;
        end

        [Ur,Sr,Vr] = svd(RR);
        R = Ur*Vr';

        sc = norm(V(1:9, 12)) / norm(R(:));
        t = V(10:12, 12)/sc;
        Method = 2;
    end
end
