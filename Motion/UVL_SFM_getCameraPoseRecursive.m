function [CameraPose P3D] = UVL_SFM_getCameraPoseRecursive(P3D, BaseIndex, K, CameraPoseParams)

% concatenate data and pickup the pairs (2D -> 3D)
PairIndices = [];
P = [];
p = [];
for PType = 1 : numel(P3D)
    PairIndicesGroup = zeros(1, numel(P3D(PType).Status));
    P3DSequence = find([P3D(PType).Status] == 3);
    for PIndex = P3DSequence
        LocalIndex = P3D(PType).DescIndex{PIndex};
        CorrespondingViewIndex = P3D(PType).ViewIndex(LocalIndex) == BaseIndex;
        if any(CorrespondingViewIndex)
            CorrespondingView = LocalIndex(CorrespondingViewIndex);
            PairIndicesGroup(PIndex) = 2;
            P = [P P3D(PType).Pos(:, PIndex)];
            p = [p double(P3D(PType).ViewPos(:, CorrespondingView))];
        end
    end
    PairIndices = [PairIndices PairIndicesGroup];
end
    
% 
% for P3Dindex = P3DSequence
%     CorrespondingViewIndex = [P3D(P3Dindex).View(:).Index] == BaseIndex;
%     if any(CorrespondingViewIndex)
%         PairIndices(P3Dindex) = true;
%         P = [P P3D(P3Dindex).Pos];
%         p = [p P3D(P3Dindex).View(CorrespondingViewIndex).Pos(1:2)];
%     end
% end
UVL_SFM_printOut(sprintf(' Init: %d c,', size(p, 2)));


[CameraPoseRANSAC, InliersRANSAC] = UVL_SFM_ransacCameraPose(P, p, K, CameraPoseParams.OutlierRejectionThRansac);
if isempty(CameraPoseRANSAC)
    UVL_SFM_printOut(sprintf('  RANSAC: giving up\n'));
%    should remove the outliers
    return;
    %error('Camera pose not found')
end
UVL_SFM_printOut(sprintf(' RANSAC: %d c,', sum(InliersRANSAC)));


 
% 1st pass - NL camera pose with RANSAC points
CameraPoseSBA = UVL_SFM_adjustCameraDirectSBA(P(:,InliersRANSAC), p(:,InliersRANSAC), CameraPoseRANSAC, K); % NL adjust of camera position
BPDistances = UVL_SFM_singlePoseBackProjectionError(P, p, CameraPoseSBA, K); % compute back-projection
InliersSBA = BPDistances <= CameraPoseParams.OutlierRejectionThNL; % get inliers within distance threshold
% 2nd pass - NL camera pose with more points (usually :)
CameraPoseSBA = UVL_SFM_adjustCameraDirectSBA(P(:,InliersSBA), p(:,InliersSBA), CameraPoseSBA, K); % NL adjust of camera position
BPDistances = UVL_SFM_singlePoseBackProjectionError(P, p, CameraPoseSBA, K); % compute back-projection
InliersSBA = BPDistances <= CameraPoseParams.OutlierRejectionThNL; % get inliers within distance threshold
UVL_SFM_printOut(sprintf(' NL: %d c.', sum(InliersSBA)));

CorrespPosition = find(PairIndices);
PairIndices(CorrespPosition(InliersSBA)) = 1;

% get rid of outliers
for PType = 1 : numel(P3D)
    PTypeElements = numel(P3D(PType).Status);
    PairIndicesGroup = PairIndices(1:PTypeElements);
    PairIndices = PairIndices(PTypeElements+1:end);
    PairIndicesGroup = find(PairIndicesGroup == 2); %% if it's outlier
    for PIndex = PairIndicesGroup
        LocalIndices = P3D(PType).DescIndex{PIndex};
        ViewIndices = P3D(PType).ViewIndex(LocalIndices);
        GoodViewIndex = ViewIndices ~= BaseIndex;
        BadViewIndex = ViewIndices == BaseIndex;
        P3D(PType).ViewIndex(LocalIndices(BadViewIndex)) = 0;
        P3D(PType).DescIndex{PIndex} = LocalIndices(GoodViewIndex); %% eliminate the outlier view
        P3D(PType).TotalFrames(PIndex) = P3D(PType).TotalFrames(PIndex) - 1; %% 
        if P3D(PType).TotalFrames(PIndex) %% if there are any views left
            Descriptors = double(P3D(PType).ViewDesc(:, P3D(PType).DescIndex{PIndex})) / 2^15;
            P3D(PType).Desc(:, PIndex) = single(mean( Descriptors, 2));
        else
            P3D(PType).Status(PIndex) = uint8(0); %% free up the slot
        end
    end
end
CameraPose = CameraPoseSBA;
