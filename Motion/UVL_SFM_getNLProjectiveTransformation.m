function  H = UVL_SFM_getNLProjectiveTransformation(corr, initH)

% H = getNLProjectiveTransformation(corr)
%
%     corr - Array of points and matches
%                row 1: point x
%                row 2: point y
%                row 3: match x
%                row 4: match y
%
%     H    - Resulting non-linear projective transformation matrix
%
% Compute a projective transformation from points and matches using 
% non-linear method which minimizes the reprojection error
%
% Developed by Olivier Delaunoy (delaunoy@eia.udg.es)
% Dep. of Electronics, Informatics and Automation, University of Girona

    warning off MATLAB:singularMatrix
    warning off MATLAB:nearlySingularMatrix
    warning off MATLAB:illConditionedMatrix

    if ~exist('initH', 'var')
        initH = UVL_SFM_getProjectiveTransformation(corr);
    end
    
    if size(corr,2)>4,
        points = corr(1:2,:);
        matches = corr(3:4,:);

        % Make conditioners to normalize
        m=mean(points,2);
        s=std(points,0,2);
        PConditioner = [diag(sqrt(2)./s) -diag(sqrt(2)./s)*m; 0 0 1];
        m=mean(matches,2);
        s=std(matches,0,2);
        MConditioner = [diag(sqrt(2)./s) -diag(sqrt(2)./s)*m; 0 0 1];
        invMConditioner = inv(MConditioner);

        H_cond = MConditioner * initH * inv(PConditioner);
        H_cond = H_cond(1:8) ./ H_cond(9); 

        %Faster than using optimset: optimset = 2s for 100 iter
        opt.Display = 'off';
        opt.MaxFunEvals = '100*numberofvariables';
        opt.MaxIter = 400;
        opt.TolFun = 1.0000e-006;
        opt.TolX = 1.0000e-006;
        opt.FunValCheck = 'off';
        opt.OutputFcn = [];
        opt.PlotFcns = [];
        opt.Algorithm = 'levenberg-marquardt';
        opt.ActiveConstrTol = [];
        opt.BranchStrategy = [];
        opt.DerivativeCheck = 'off';
        opt.Diagnostics = 'off';
        opt.DiffMaxChange = 0.1000;
        opt.DiffMinChange = 1.0000e-008;
        opt.GoalsExactAchieve = [];
        opt.GradConstr = [];
        opt.GradObj = [];
        opt.Hessian = [];
        opt.HessMult = [];
        opt.HessPattern = [];
        opt.HessUpdate = [];
        opt.InitialHessType = [];
        opt.InitialHessMatrix = [];
        opt.Jacobian = 'off';
        opt.JacobMult = [];
        opt.JacobPattern = 'sparse(ones(jrows,jcols))';
        opt.LargeScale = 'off';
        opt.LevenbergMarquardt = 'on';
        opt.LineSearchType = 'quadcubic';
        opt.MaxNodes = [];
        opt.MaxPCGIter = 'max(1,floor(numberofvariables/2))';
        opt.MaxRLPIter = [];
        opt.MaxSQPIter = [];
        opt.MaxTime = [];
        opt.MeritFunction = [];
        opt.MinAbsMax = [];
        opt.NodeDisplayInterval = [];
        opt.NodeSearchStrategy = [];
        opt.NonlEqnAlgorithm = [];
        opt.NoStopIfFlatInfeas = [];
        opt.PhaseOneTotalScaling = [];
        opt.Preconditioner = [];
        opt.PrecondBandWidth = 0;
        opt.RelLineSrchBnd = [];
        opt.RelLineSrchBndDuration = [];
        opt.ShowStatusWindow = [];
        opt.Simplex = [];
        opt.TolCon = [];
        opt.TolPCG = 0.1000;
        opt.TolRLPFun = [];
        opt.TolXInteger = [];
        opt.TypicalX = 'ones(numberofvariables,1)';
        
        H_cond = lsqnonlin(@lsq_func,H_cond,[],[],opt);

        H_cond = [H_cond(1),H_cond(4),H_cond(7);H_cond(2),H_cond(5),H_cond(8);H_cond(3),H_cond(6),1];
        
        H = invMConditioner * H_cond * PConditioner;
        H = H/H(3,3);
    else
        H = initH;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function r = lsq_func(H)
        H = [H(1),H(4),H(7);H(2),H(5),H(8);H(3),H(6),1];
        H_decond = invMConditioner * H * PConditioner;
        H_decond = H_decond ./ H_decond(3,3); 
        r = UVL_SFM_computeResidualsHomography(corr, H_decond);
    end
end