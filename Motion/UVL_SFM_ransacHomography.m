function [BestH, BestInliers, BestResidualMean] = UVL_SFM_ransacHomography(Corresp, TransfModel, Tolerance, MaxTrials)

% [BestInliers, BestH, BestResidualMean] = UVL_SFM_ransacHomography(Corresp, TransfModel, Tolerance, MaxTrials)
%
% IMPUT:
%     Corresp          - Array of points and matches
%                            row 1: point x
%                            row 2: point y
%                            row 3: match x
%                            row 4: match y
%                            row ...
%     TransfModel      - Model of the transformation matrix
%     Tolerance        - Stdev of spatial coord of points, in pixels (default = 5)
%     MaxTrials        - Maximum number of sample tested
%
% OUTPUT:
%     BestInliers      - Array of good points and matches
%                            row 1: point x
%                            row 2: point y
%                            row 3: match x
%                            row 4: match y
%                            row ...
%     BestH            - Transformation matrix
%     BestResidualMean - Mean of residuals
%
% Finds the inliers among a set of corresondences using Ransac.
%
% Developed by Delaunoy Olivier (delaunoy@eia.udg.es)
% Dep. of Electronics, Informatics and Automation, University of Girona



    if ~exist('Tolerance', 'var'), Tolerance = 5; end
    if ~exist('MaxTrials', 'var'), MaxTrials = 2000; end

    switch lower(TransfModel(1:3))
        case 'euc'
            MinPoints = 2;
            HomographyMethod = @UVL_SFM_getEuclideanTransformation;
        case 'sim'
            MinPoints = 2;
            HomographyMethod = @UVL_SFM_getSimilarityTransformation;
        case 'aff'
            MinPoints = 3;
            HomographyMethod = @UVL_SFM_getAffineTransformation;
        case 'pro'
            MinPoints = 4;
            HomographyMethod = @UVL_SFM_getProjectiveTransformation;
        case 'nlp'
            MinPoints = 4;
            HomographyMethod = @UVL_SFM_getNLProjectiveTransformation;
        otherwise
            error('Unknown model.')
    end

    BestInliers = [];
    BestNumInliers = MinPoints;
    BestH = [];
    BestResidualMean = NaN;
    NumPoints = size(Corresp, 2);
    Probability = 0.999; % Desired probability of choosing at least one sample free from outliers
    
%     Buckets = UVL_SFM_divideIntoBuckets(Corresp, 15);
    for count = 0 : MaxTrials 
%         RandomSamples = UVL_SFM_randomCorrespondences(Buckets, MinPoints, true);
        [ignore RandomSamples] = sort(rand(1, NumPoints));
        RandomSamples = RandomSamples(1:MinPoints);
        if numel(RandomSamples) == MinPoints,
            H = HomographyMethod(Corresp(:, RandomSamples));
            Inliers = UVL_SFM_computeResidualsHomography(Corresp, H) <= Tolerance;
            NumInliers = sum(Inliers);
            if NumInliers > BestNumInliers     
                BestNumInliers = NumInliers;  
                BestInliers = Inliers;

                pNoOutliers = 1 - (NumInliers/NumPoints)^MinPoints;
                pNoOutliers = min(1-eps, max(eps, pNoOutliers));% Avoid division by -Inf and division by 0.
                MaxTrials = log(1-Probability)/log(pNoOutliers);
            end
            if count > MaxTrials, break; end
        end
    end

    if BestNumInliers > MinPoints
        NumInliers = BestNumInliers;
        Continue = true;
        while Continue,
            BestH = HomographyMethod(Corresp(:, BestInliers));
            Residuals = UVL_SFM_computeResidualsHomography(Corresp, BestH);
            BestInliers = Residuals <= Tolerance;
            BestResidualMean = mean(Residuals(BestInliers));
            BestNumInliers = sum(BestInliers);
            if BestNumInliers > NumInliers     
                NumInliers = BestNumInliers;  
            else
                Continue = false;
            end
        end
    end
%     if BestNumInliers > MinPoints
%         BestH = HomographyMethod(Corresp(:, BestInliers));
%         BestInliers = UVL_SFM_computeResidualsHomography(Corresp, BestH) <= Tolerance;
%         BestH = HomographyMethod(Corresp(:, BestInliers));
% 		Residuals = UVL_SFM_computeResidualsHomography(Corresp, BestH);
% 		BestInliers = Residuals <= Tolerance;
%         BestResidualMean = mean(Residuals(BestInliers));
%     end
end