function  H = getSimilarityTransformation(corr)

% H = getSimilarityTransformation(corr)
%
%     corr - Array of points and matches
%                row 1: point x
%                row 2: point y
%                row 3: match x
%                row 4: match y
%
%     H    - Resulting similarity transformation matrix
%
% Compute a similarity transformation from points and matches using
% orthonormal matrices (1988 by Horn, Hilden, Negahdaripour)
%
% Developed by Olivier Delaunoy (delaunoy@eia.udg.es)
% Dep. of Electronics, Informatics and Automation, University of Girona



    % Find centroid of the two sets of points
    m = mean(corr, 2);

    % Center the two sets of source data
    Pc      = corr(1,:) - m(1);
    Pc(2,:) = corr(2,:) - m(2);
    Mc      = corr(3,:) - m(3);
    Mc(2,:) = corr(4,:) - m(4);

    Scale = sum(sum(Mc .* Mc)) / sum(sum(Pc .* Pc));

    % Compute Rotation
    M = Mc * Pc' / Scale;
    if any(isnan(M)) | any(isinf(M))
        H = zeros(3);
        return;
    end
    [U, S, V] = svd(M);
    R = U * V;
    if det(R) < 0, %Mirror Effect include by the euclidean transformation
        R = U * [ V(1,1)  V(1,2);
                 -V(2,1) -V(2,2)];
    end

    Phi = atan2(R(2, 1), R(2, 2));
    T = (m(3:4) - Scale * R * m(1:2));

    H = [Scale*cos(Phi) -Scale*sin(Phi) T(1);
         Scale*sin(Phi)  Scale*cos(Phi) T(2);
         0               0              1];
end