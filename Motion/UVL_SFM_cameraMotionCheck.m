function [residuals Zsign ZsignDetail] = UVL_SFM_cameraMotionCheck(Motion, corresp, K)

lpts=corresp(1:2,:);
rpts=corresp(3:4,:);
PointsNbr = size(lpts, 2);
PosesNbr = size(Motion, 2);
ZsignDetail = zeros(PosesNbr, PointsNbr);
Zsign = zeros(PosesNbr, 2);
residuals = zeros(PosesNbr, 1);
for count = 1 : PosesNbr
    res = [];
    P = UVL_SFM_LSQIntersection(lpts, rpts, eye(4), inv(Motion{count}), K);
    ZsignDetail(count,:) = sign(P(3,:));
    Zsign(count,1) = sum(P(3,:)<0);
    Zsign(count,2) = sum(P(3,:)>0);
    lpts_est = UVL_SFM_3DPointResection(P, K, eye(4));
    rpts_est = UVL_SFM_3DPointResection(P, K, Motion{count});
    res1 = sqrt((lpts(1,:) - lpts_est(1,:)).^2 + (lpts(2,:) - lpts_est(2,:)).^2);
    res2 = sqrt((rpts(1,:) - rpts_est(1,:)).^2 + (rpts(2,:) - rpts_est(2,:)).^2);
    res(1,:) = res1;
    res(2,:) = res2;
    res = mean(mean(res));
    residuals(count) = res;
end
