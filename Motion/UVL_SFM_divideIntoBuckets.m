function Buckets = UVL_SFM_divideIntoBuckets(Coordinates, NumBuckets)

% Buckets = UVL_SFM_divideIntoBuckets(Coordinates, NumBuckets)
% 
% INPUT:
%     Coordinates - Array of points and matches
%                       Row 1: x coordinates
%                       Row 2: y coordinates
%     NumBuckets  - number of buckets in x/y directions (default = 10)
%
% OUTPUT:
%     Buckets     - a structure containing the indices
%
% Divides the coordinates into buckets for uniform distribution sampling
%
% Developed by Delaunoy Olivier (delaunoy@eia.udg.es)
% Dep. of Electronics, Informatics and Automation, University of Girona

    if ~exist('NumBuckets', 'var'), NumBuckets = 10; end
    
    maxX = max(Coordinates(1,:));
    minX = min(Coordinates(1,:));
    maxY = max(Coordinates(2,:));
    minY = min(Coordinates(2,:));
    
    width = maxX - minX;
    height = maxY - minY;

    SizeBucketX = width/NumBuckets;
    SizeBucketY = height/NumBuckets;
    
    SizeBucketX = SizeBucketX + SizeBucketX/100; % increase the size by 1%
    SizeBucketY = SizeBucketY + SizeBucketY/100;

    Buckets(NumBuckets, NumBuckets).Index = [];
    Buckets(NumBuckets, NumBuckets).NumEl = 0;

    for co = 1 : size(Coordinates, 2)
        CurrBucketX = floor((Coordinates(1,co)-minX)/SizeBucketX)+1;          
        CurrBucketY = floor((Coordinates(2,co)-minY)/SizeBucketY)+1;
        try
        Buckets(CurrBucketX, CurrBucketY).Index = [Buckets(CurrBucketX, CurrBucketY).Index co];
        catch
          disp( 'here' ) ;
        end
    end  
    for co = 1 : (NumBuckets * NumBuckets)
        Buckets(co).NumEl = numel(Buckets(co).Index);
    end
end