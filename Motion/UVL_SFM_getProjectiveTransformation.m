function  H = UVL_SFM_getProjectiveTransformation(corr, bySvd)

% H = getProjectiveTransformation(corr, bySvd)
%
%     corr  - Array of points and matches
%                 row 1: point x
%                 row 2: point y
%                 row 3: match x
%                 row 4: match y
%     bySvd - false = Least-Squares; true = svd (default = true)
%
%     H     - Resulting projective transformation matrix
%
% Compute a projective transformation from points and matches
%
% Developed by Tudor Nicosevici, Rafael Garcia and Ricard Prados
% Dep. of Electronics, Informatics and Automation, University of Girona

    warning off MATLAB:singularMatrix
    warning off MATLAB:nearlySingularMatrix
    warning off MATLAB:illConditionedMatrix

    UMat = zeros(size(corr, 2)*2,8);
    X    = zeros(size(corr, 2)*2,1);

    for Cnt_Punts = 1 : size(corr, 2),
       UMat(Cnt_Punts*2-1,1) = corr(1, Cnt_Punts); %a
       UMat(Cnt_Punts*2-1,2) = corr(2, Cnt_Punts); %b
       UMat(Cnt_Punts*2-1,3) = 1;                    %c
       UMat(Cnt_Punts*2-1,7) = -(corr(1, Cnt_Punts)*corr(3, Cnt_Punts));   
       UMat(Cnt_Punts*2-1,8) = -(corr(2, Cnt_Punts)*corr(3, Cnt_Punts));   
       UMat(Cnt_Punts*2,4)   = corr(1, Cnt_Punts);
       UMat(Cnt_Punts*2,5)   = corr(2, Cnt_Punts);
       UMat(Cnt_Punts*2,6)   = 1;
       UMat(Cnt_Punts*2,7)   = -(corr(1, Cnt_Punts)*corr(4, Cnt_Punts));
       UMat(Cnt_Punts*2,8)   = -(corr(2, Cnt_Punts)*corr(4, Cnt_Punts));
       
       X(Cnt_Punts*2-1) = corr(3, Cnt_Punts);
       X(Cnt_Punts*2)   = corr(4, Cnt_Punts);
    end

    if exist('bySvd', 'var') && bySvd == true,
        [U, S, V] = svd(UMat, 'econ');
        A = V * diag ( 1 ./ ( diag ( S ) + eps ) ) * U' * X;
    else
        A = inv(UMat'*UMat) * UMat' * X;
    end

    H = [A(1) A(2) A(3);
         A(4) A(5) A(6);
         A(7) A(8)    1];
end
