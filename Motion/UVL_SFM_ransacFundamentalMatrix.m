function [BestE, BestInliers, BestResidualMean] = UVL_SFM_ransacFundamentalMatrix(PointsImg1, PointsImg2, Tolerance)

% !!! should normalize the points before this function !!!

    MinPoints = 8;

    BestInliers = [];
    BestNumInliers = MinPoints;
    BestE = [];
    BestResidualMean = NaN;
    NumPoints = size(PointsImg1, 2);
    Probability = 0.999; % Desired probability of choosing at least one sample free from outliers
    NumTrialsForProbability = 1000;
    
    Buckets = UVL_SFM_divideIntoBuckets(PointsImg1, 15);
    for count = 0 : NumTrialsForProbability,
         RandomSamples = UVL_SFM_randomCorrespondences(Buckets, MinPoints, true);
%        [ignore RandomSamples] = sort(rand(1, NumPoints));
%        RandomSamples = RandomSamples(1:MinPoints);
        if numel(RandomSamples) == MinPoints,
            E = UVL_SFM_getFundamentalMatrix(PointsImg1(:, RandomSamples), PointsImg2(:, RandomSamples));
            Residuals = UVL_SFM_computeResidualsFundamentalMatrix(E, PointsImg1, PointsImg2);
            Inliers = Residuals <= Tolerance;
            NumInliers = sum(Inliers);
            if NumInliers > BestNumInliers     
                BestNumInliers = NumInliers;  
                BestInliers = Inliers;

                pNoOutliers = 1 - (NumInliers/NumPoints)^MinPoints;
                pNoOutliers = min(1-eps, max(eps, pNoOutliers));% Avoid division by -Inf and division by 0.
                NumTrialsForProbability = log(1-Probability)/log(pNoOutliers) ;
            end
            if count > NumTrialsForProbability, break; end
        end
    end
        
    if BestNumInliers > MinPoints
        BestE = UVL_SFM_getFundamentalMatrix(PointsImg1(:, BestInliers), PointsImg2(:, BestInliers));
        BestInliers = UVL_SFM_computeResidualsFundamentalMatrix(BestE, PointsImg1, PointsImg2) <= Tolerance;
        BestE = UVL_SFM_getFundamentalMatrix(PointsImg1(:, BestInliers), PointsImg2(:, BestInliers));
        Residuals = UVL_SFM_computeResidualsFundamentalMatrix(BestE, PointsImg1, PointsImg2);
        BestInliers = Residuals <= Tolerance;
        BestResidualMean = mean(Residuals(BestInliers));
    end
