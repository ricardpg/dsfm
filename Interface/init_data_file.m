function fid = init_data_file(name, width, height, depth)
fid = fopen(name,'wb');
fwrite(fid, int32(width), 'int32');
fwrite(fid, int32(height), 'int32');
fwrite(fid, int32(depth), 'int32');
