% converts avi movies to sequence files
% Usage: UVL_SFM_importAVI(AviFile, ImageFileNameRoot, ImageFileNameType, FrameSequence, Deinterlace, Undistort)
%        AviFile - name of the input file
%        [start step stop] - frame reading 
%        Deinterlace - bool, true to deinterlace the frames
%        Undistort - bool, true to rectify the frames

function UVL_SFM_importAVI(AviFile, ImageFileNameRoot, ImageFileNameType, FrameSequence, Undistort, Deinterlace, FrameShift)

if nargin < 3
    fprintf('Not enough input parameters. Use: help UVL_SFM_importAVI')
    return
end

AviInfo = aviinfo(AviFile);

if ~exist('FrameSequence', 'var')
    Sequence = 1 : AviInfo.NumFrames;
elseif numel(FrameSequence) == 1   %% only 1 parameter - assuming frame step
    Sequence = 1 : FrameSequence : AviInfo.NumFrames;
elseif numel(FrameSequence) == 2
    Sequence = FrameSequence(1) : min([AviInfo.NumFrames FrameSequence(2)]);
elseif numel(FrameSequence) == 3
    Sequence = FrameSequence(1) : FrameSequence(1) : min([AviInfo.NumFrames FrameSequence(3)]);
else
    fprintf('Invalid format on FrameSequence. Use: help UVL_SFM_importAVI')
    return
end    

if ~exist('Deinterlace', 'var')
    Deinterlace = false;
end

if ~exist('Undistort', 'var')
    Undistort = false;
end

if ~exist('FrameShift', 'var')
    FrameShift = 0;
end

fprintf('Aditional options: ')
if Deinterlace
    fprintf('[deinterlacing] ')
end
if Undistort
    fprintf('[rectifying] ')
end
fprintf('\n');

FrameHeight = AviInfo.Height;
FrameWidth = AviInfo.Width;

if length(Sequence)<1
    error('0 frames extracted. Check the prameters')
end
if Undistort
    fprintf('Reading radial distortion params [radial_dist.mat]...\n')
    if ~exist('radial_dist.mat', 'file')
        error('Radial distortion parameter file not found.')
    end
    load radial_dist
    fprintf('Computing rectification LUT...\n');
    RectLUT = computeRectLUT(eye(3), fc, cc, kc, alpha_c, KK_new, FrameWidth, FrameHeight);
end

if Deinterlace
    fprintf('Reading radial camera calibration [K.mat]...\n')
    if ~exist('K.mat', 'file')
        error('Camera calibration parameter file not found.')
    end
    load K
end

for FrameIndex = Sequence
    fprintf('[%d/%d] > Reading ', FrameIndex, Sequence(end));
    Frame = aviread(AviFile, FrameIndex);
    Frame = double(Frame.cdata);
    if Undistort
        fprintf('> Rectifying ')
        Frame = applyRectLUT(Frame, RectLUT);       
    end
    if Deinterlace
        fprintf('> Deinterlacing ')
        Frame = deinterlace(Frame, K);
    end
    ImgFileName = sprintf('%s_frm%07d.%s', ImageFileNameRoot, FrameIndex + FrameShift, ImageFileNameType);
    imwrite(uint8(Frame), ImgFileName);
    fprintf('> Done.\n')
end

function img = deinterlace(img, K)
Ratio = K(2,2) / K(1,1);
Transform = maketform('affine', [Ratio 0 0; 0 1 0; 0 0 1]');
%img = imtransform(img(1:2:end, :, :), Transform, 'bilinear');
img = imtransform(uint8(img), Transform, 'bilinear', 'FillValues', [255 255 255]');
