function LUT = computeRectLUT(R,f,c,k,alpha,KK_new,width,height)

if ~exist('KK_new'),
   KK_new = [f(1) alpha*f(1) c(1);0 f(2) c(2);0 0 1];
end;

[mx,my] = meshgrid(1:width, 1:height);
px = reshape(mx',width*height,1);
py = reshape(my',width*height,1);

rays = inv(KK_new)*[(px - 1)';(py - 1)';ones(1,length(px))];


% Rotation: (or affine transformation):

rays2 = R'*rays;

x = [rays2(1,:)./rays2(3,:);rays2(2,:)./rays2(3,:)];


% Add distortion:
xd = apply_distortion(x,k);


% Reconvert in pixels:

px2 = f(1)*(xd(1,:)+alpha*xd(2,:))+c(1);
py2 = f(2)*xd(2,:)+c(2);


% Interpolate between the closest pixels:

px_0 = floor(px2);


py_0 = floor(py2);
%py_1 = py_0 + 1;


good_points = find((px_0 >= 0) & (px_0 <= (width-2)) & (py_0 >= 0) & (py_0 <= (height-2)));

px2 = px2(good_points);
py2 = py2(good_points);
px_0 = px_0(good_points);
py_0 = py_0(good_points);

alpha_x = px2 - px_0;
alpha_y = py2 - py_0;

a1 = (1 - alpha_y).*(1 - alpha_x);
a2 = (1 - alpha_y).*alpha_x;
a3 = alpha_y .* (1 - alpha_x);
a4 = alpha_y .* alpha_x;

ind_lu = px_0 * height + py_0 + 1;
ind_ru = (px_0 + 1) * height + py_0 + 1;
ind_ld = px_0 * height + (py_0 + 1) + 1;
ind_rd = (px_0 + 1) * height + (py_0 + 1) + 1;

ind_new = (px(good_points)-1)*height + py(good_points);

LUT.a1 = a1;
LUT.a2 = a2;
LUT.a3 = a3;
LUT.a4 = a4;
LUT.ind_lu = ind_lu;
LUT.ind_ld = ind_ld;
LUT.ind_ru = ind_ru;
LUT.ind_rd = ind_rd;
LUT.ind_new = ind_new';




return;


% Convert in indices:

% fact = 3;
% 
% [XX,YY]= meshgrid(1:width,1:height);
% [XXi,YYi]= meshgrid(1:1/fact:width,1:1/fact:height);
% 
% %tic;
% Iinterp = interp2(XX,YY,I,XXi,YYi); 
% %toc
% 
% [nri,nci] = size(Iinterp);
% 
% 
% ind_col = round(fact*(f(1)*xd(1,:)+c(1)))+1;
% ind_row = round(fact*(f(2)*xd(2,:)+c(2)))+1;
% 
% good_points = find((ind_col >=1)&(ind_col<=nci)&(ind_row >=1)& (ind_row <=nri));
