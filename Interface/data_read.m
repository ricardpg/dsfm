function data = data_read(fid, pos)
fseek(fid, 0, 'bof');
width = fread(fid, [1], 'int32');
height = fread(fid, [1], 'int32');
depth = fread(fid, [1], 'int32');
if exist('pos','var')
    fseek(fid, (pos-1)*height*width*depth+12, 'bof');
    for d = 1:depth
        data(:,:,d) = fread(fid, [height width], 'uint8');
    end
    if max(max(max(data))) <= 1
        data = uint8(255 * data);
    end
    data = uint8(data);
else
    fseek(fid, 0, 'eof');
    pos = ftell(fid);
    data = (pos-12)/(height*width*depth);
end
