%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Stanford ply format to iv (Open Inventor) converter.
%
%  File          : Ply2Iv.m
%  Date          : 27/01/2010 - 28/01/2010
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2010 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  Ply2Iv Simple Stanford ply to Iv Open Inventor converter.
%
%      Res = Ply2Iv ( SrcPlyFileName, DstIvFileName )
%
%     Input Parameters:
%      SrcPlyFileName: String containing the full path to the input sty file.
%      DstPlyFileName: String containing the full path to the output iv file.
%
%     Output Parameters:
%      Res: 0 for a successfull conversion, any other value otherwise.
%


function Res = Ply2Iv ( SrcPlyFileName, DstIvFileName )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 0, 1, nargout ) );

  SrcFID = fopen ( SrcPlyFileName, 'r' );

  if SrcFID ~= -1;
    DstFID = fopen ( DstIvFileName, 'w' );

    if DstFID ~= -1;
      % Write Header
      fprintf ( DstFID, '#Inventor V2.1 ascii\n' );
      fprintf ( DstFID, '#Created by Jordi Ferrer <jferrerp@eia.udg.edu> from %s (%s)\n\n', SrcPlyFileName, datestr ( now ) );

      % Read First Line
      Line = fgetl ( SrcFID );
      LineCnt = 1;
      IdxCoord = 0;
      IdxX = 0; IdxY = 0; IdxZ = 0;
      IdxR = 0; IdxG = 0; IdxB = 0;

      % Storage for points and colors
      VERTEX_BUFFER_SIZE = 0;
      Points = zeros ( 3, VERTEX_BUFFER_SIZE );
      Colors = zeros ( 3, VERTEX_BUFFER_SIZE );
      lv = 0;
      FACE_BUFFER_SIZE = 0;
      Faces = zeros ( 4, FACE_BUFFER_SIZE );
      lf = 0;

      DEFAULT_COLOR = [ 1.0; 1.0; 1.0 ];
      PointSize = 4;

      % State Constants
      STATE_HEADER = 0;
      STATE_POINT_LIST = 1;
      STATE_FACE_LIST = 2;
      STATE_EXIT = 3;

      State = STATE_HEADER;
      while State ~= STATE_EXIT && ischar ( Line );
        % Split input line
        SrcParts = regexp ( Line, ' ', 'split' );
        NumParts = length ( SrcParts );
        i = 1;
        j = 0;
        Parts = {};
        while i <= NumParts;
          if ~isempty ( strtrim ( SrcParts{i} ) );
            j = j + 1;
            Parts{j} = SrcParts{i};
          end
          i = i + 1;
        end
        NumParts = length ( Parts );

        % Check Format
        if LineCnt == 1 && ~strcmpi ( Line, 'ply' );
          error ( 'MATLAB:Ply2Iv:Io', 'Invalid ply file format (first line must contain ''ply'' instead of ''%s'')!', Line );
        end
        if LineCnt == 2 && ( NumParts ~= 3 || ( NumParts == 3 && ...
            ( ~strcmpi ( Parts{1}, 'format' ) || ~strcmpi ( Parts{2}, 'ascii' ) ) ) )
          error ( 'MATLAB:Ply2Iv:Io', 'Invalid ply file format (second line must contain ''format ascii [ver]'' instead of ''%s'')!', Line );
        end

        % Check State
        switch ( State );
          case STATE_HEADER;
            % Vertex Vector Size
            if NumParts == 3 && strcmpi ( Parts{1}, 'element' ) && strcmpi ( Parts{2}, 'vertex' );
              Num = fix ( str2double ( Parts{3} ) );
              if Num > 0;
                VERTEX_BUFFER_SIZE = Num;
                Points = zeros ( 3, VERTEX_BUFFER_SIZE );
                Colors = zeros ( 3, VERTEX_BUFFER_SIZE );
              end
            end
            
            % Face Vector Size
            if NumParts == 3 && strcmpi ( Parts{1}, 'element' ) && strcmpi ( Parts{2}, 'face' );
              Num = fix ( str2double ( Parts{3} ) );
              if Num > 0;
                FACE_BUFFER_SIZE = Num;
                Faces = zeros ( 3, FACE_BUFFER_SIZE );
              end
            end

            % X
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && ( strcmpi ( Parts{2}, 'float' ) || strcmpi ( Parts{2}, 'double' ) ) && strcmpi ( Parts{3}, 'x' );
              IdxCoord = IdxCoord + 1;
              IdxX = IdxCoord;
            end            
            % Y
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && ( strcmpi ( Parts{2}, 'float' ) || strcmpi ( Parts{2}, 'double' ) ) && strcmpi ( Parts{3}, 'y' );
              IdxCoord = IdxCoord + 1;
              IdxY = IdxCoord;
            end
            % Z
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && ( strcmpi ( Parts{2}, 'float' ) || strcmpi ( Parts{2}, 'double' ) ) && strcmpi ( Parts{3}, 'z' );
              IdxCoord = IdxCoord + 1;
              IdxZ = IdxCoord;
            end
            % nx
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && ( strcmpi ( Parts{2}, 'float' ) || strcmpi ( Parts{2}, 'double' ) ) && strcmpi ( Parts{3}, 'nx' );
              IdxCoord = IdxCoord + 1;
%              Idxnx = IdxCoord;
            end
            % ny
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && ( strcmpi ( Parts{2}, 'float' ) || strcmpi ( Parts{2}, 'double' ) ) && strcmpi ( Parts{3}, 'ny' );
              IdxCoord = IdxCoord + 1;
%              Idxny = IdxCoord;
            end
            % nz
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && ( strcmpi ( Parts{2}, 'float' ) || strcmpi ( Parts{2}, 'double' ) ) && strcmpi ( Parts{3}, 'nz' );
              IdxCoord = IdxCoord + 1;
%              Idxnz = IdxCoord;
            end            
            % Red
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && strcmpi ( Parts{2}, 'uchar' ) && strcmpi ( Parts{3}, 'diffuse_red' );
              IdxCoord = IdxCoord + 1;
              IdxR = IdxCoord;
            end
            % Green
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && strcmpi ( Parts{2}, 'uchar' ) && strcmpi ( Parts{3}, 'diffuse_green' );
              IdxCoord = IdxCoord + 1;
              IdxG = IdxCoord;
            end
            % Blue
            if NumParts == 3 && strcmpi ( Parts{1}, 'property' ) && strcmpi ( Parts{2}, 'uchar' ) && strcmpi ( Parts{3}, 'diffuse_blue' );
              IdxCoord = IdxCoord + 1;
              IdxB = IdxCoord;
            end

            % End of Header
            if strcmpi ( Line, 'end_header' );
              % 3D Coords are mandatory
              if IdxX == 0 || IdxY == 0 || IdxZ == 0;
                error ( 'MATLAB:Ply2Iv:Io', 'Either X (%d), Y (%d), Z (%d) indices not found in header!', IdxX, IdxY, IdxZ );
              end

              State = STATE_POINT_LIST;
            end

          % Read Points
          case STATE_POINT_LIST;
            if VERTEX_BUFFER_SIZE == 0;
              error ( 'MATLAB:Ply2Iv:Io', 'Number of vertices not found in header!' );
            end
%            if NumParts >= IdxCoord;
%              error ( 'MATLAB:Ply2Iv:Io', 'Size of vertex data (%d) does not match with header properties (%d)!', length ( Parts ), IdxCoord );
%            end
            
            if NumParts >= 3;
              lv = lv + 1;
              Points(:,lv) = [str2double(Parts{IdxX}); str2double(Parts{IdxY}); str2double(Parts{IdxZ})];
              if NumParts >= 6;
                if IdxR == 0 || IdxG == 0 || IdxB == 0;
                  error ( 'MATLAB:Ply2Iv:Io', 'Color R (%d), G (%d), B (%d) indices not found in header!', IdxR, IdxG, IdxB );
                end

                Colors(:,lv) = [str2double(Parts{IdxR}); str2double(Parts{IdxG}); str2double(Parts{IdxB})] / 255.0;
              else
                Colors(:,lv) = DEFAULT_COLOR;
              end
            else
              fprintf ( 2, 'Skipping line %d (''%s'') since does not match with 3 or 6 elements!', LineCnt, Line ); 
            end

            if lv == VERTEX_BUFFER_SIZE;
              State = STATE_FACE_LIST;
            end

          % Read Faces
          case STATE_FACE_LIST;
            if FACE_BUFFER_SIZE == 0;
              error ( 'MATLAB:Ply2Iv:Io', 'Number of faces not found in header!' );
            end
            if NumParts == 4;
              Idx1 = fix(str2double(Parts{1}));
              Idx2 = fix(str2double(Parts{2}));
              Idx3 = fix(str2double(Parts{3}));
              Idx4 = fix(str2double(Parts{4}));

              if Idx1 == 3 && ...
                 Idx2 >= 0 && Idx2 < VERTEX_BUFFER_SIZE && ...
                 Idx3 >= 0 && Idx3 < VERTEX_BUFFER_SIZE && ...
                 Idx4 >= 0 && Idx4 < VERTEX_BUFFER_SIZE;
                 lf = lf + 1;
                 Faces(:,lf) = [Idx2; Idx3; Idx4];
              else
                error ( 'MATLAB:Ply2Iv:Io', 'Some vertex index (%d, %d, %d) is out of range [0..%d] or face count (%d) is not 3!', Idx2, Idx3, Idx4, VERTEX_BUFFER_SIZE - 1, Idx1 );
              end
            else
              error ( 'MATLAB:Ply2Iv:Io', 'Only triangles are allowed not faces of %d vertices!', NumParts - 1 );
            end

            if lf == FACE_BUFFER_SIZE;
              State = STATE_EXIT;
            end

          otherwise
            error ( 'MATLAB:Ply2Iv:Io', 'Undefined state %d decoding line %d ''%s''!', State, LineCnt, Line );
        end
      
        % Read Next Line
        Line = fgetl ( SrcFID );
        LineCnt = LineCnt + 1;
      end

      % Export Vertices
      if lv > 0 && lv == VERTEX_BUFFER_SIZE;
        % Plot the points
        fprintf ( DstFID, 'Separator {\n' );
        fprintf ( DstFID, '\tMaterial { diffuseColor [ ' );
        for i = 1:(lv - 1)
          fprintf ( DstFID, '%.6f %.6f %.6f, ', Colors(1,i), Colors(2,i), Colors(3,i) );
        end
        fprintf ( DstFID, '%.6f %.6f %.6f ] }\n', Colors(1,lv), Colors(2,lv), Colors(3,lv) );

        fprintf ( DstFID, '\tMaterialBinding { value PER_PART }\n' );
        fprintf ( DstFID, '\tCoordinate3 { point [ ' );
        for i = 1:(lv - 1)
          fprintf ( DstFID, '%.6f %.6f %.6f, ', Points(1,i), Points(2,i), Points(3,i) );
        end
        fprintf ( DstFID, '%.6f %.6f %.6f ] }\n', Points(1,lv), Points(2,lv), Points(3,lv) );

        fprintf ( DstFID, '\tDrawStyle { pointSize %.6f }\n', PointSize );
        fprintf ( DstFID, '\tPointSet { }\n' );
        fprintf ( DstFID, '}\n' );
      end
      
      % Export Faces
      if lf > 0 && lf == FACE_BUFFER_SIZE;
        fprintf ( DstFID, 'Separator {\n' );
        fprintf ( DstFID, '      Coordinate3 {\n' );
        fprintf ( DstFID, '        point [ ' );
        for i = 1:(lv - 1)
          fprintf ( DstFID, '%.6f %.6f %.6f, ', Points(1,i), Points(2,i), Points(3,i) );
        end
        fprintf ( DstFID, '%.6f %.6f %.6f ]\n', Points(1,lv), Points(2,lv), Points(3,lv) );
        fprintf ( DstFID, '      }\n' );
        
        fprintf ( DstFID, '      IndexedFaceSet {\n' );
        fprintf ( DstFID, '          coordIndex [ ' );
        for i = 1 : (lf - 1);
          fprintf ( DstFID, '%d, %d, %d, -1, ', Faces(1,i), Faces(2,i), Faces(3,i) );
        end
        fprintf ( DstFID, '%d, %d, %d, -1 ]\n', Faces(1,lf), Faces(2,lf), Faces(3,lf) );
        fprintf ( DstFID, '    }\n' );
        fprintf ( DstFID, '}\n' );
      end
 
      fprintf ( DstFID, '\n' );
      fprintf ( DstFID, '#Output Finished: %s\n', datestr ( now ) );

      % Close
      DstRes = fclose ( DstFID );
      if DstRes ~= 0;
        error ( 'MATLAB:Ply2Iv:Io', 'Error (%d) closing file ''%s''!', DstRes, DstPlyFileName );
      end
    else
      error ( 'MATLAB:Ply2Iv:Io', 'Unable to open iv ''%s'' file for writting!', DstPlyFileName );      
    end

    % Close
    SrcRes = fclose ( SrcFID );
    if SrcRes ~= 0;
      error ( 'MATLAB:Ply2Iv:Io', 'Error (%d) closing file ''%s''!', SrcRes, SrcPlyFileName );
    end
  else
    error ( 'MATLAB:Ply2Iv:Io', 'Unable to open ply ''%s'' file for reading!', SrcPlyFileName );
  end

  Res = SrcRes + DstRes;
end
