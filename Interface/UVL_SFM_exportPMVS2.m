%% User Options

OptionLevel = 1; % image decimation level
OptionCsize = 1; % pixel cell size after reconstruction
OptionThreshold = 0.75; % patch reconstruction threshold
OptionWsize = 7; % window size for computing photometric consistency score
OptionMinImageNum = 3; % minimum number of views for the 3D point
OptionCPU = 2; % number of CPU to be used
OptionUseVisData = 1; % wether to use vis.dat
OptionSequence = 0; % how many images before and after should be used

%% Creating directories

if ~exist('.\PMVS2', 'dir')
    mkdir('PMVS2')
end
if ~exist('.\PMVS2\visualize', 'dir')
    mkdir('.\PMVS2\visualize')
end
if ~exist('.\PMVS2\txt', 'dir')
    mkdir('.\PMVS2\txt')
end

if ~exist('.\PMVS2\models', 'dir')
    mkdir('.\PMVS2\models')
end


TotalViews = numel(View);

%% Exporting Data

for CurrView = 1 : TotalViews
    fprintf('Frame: %d/%d\n', CurrView, TotalViews);
    % exporting projection matrix
    ProjMatrix = K * View(CurrView).Pose(1:3, :);
    CameraFileName = sprintf('.\\PMVS2\\txt\\%08d.txt', CurrView - 1);
    CameraFile = fopen(CameraFileName, 'wt');
    fprintf(CameraFile, 'CONTOUR\n');
    for I = 1 : 3
        fprintf(CameraFile, '%f ', ProjMatrix(I, :));
        fprintf(CameraFile, '\r');
    end
    fclose(CameraFile);
    % exporting image
    Image = UVL_SFM_getInputStream(InputDataInfo, CurrView);
    ImageFileName = sprintf('.\\PMVS2\\visualize\\%08d.jpg', CurrView - 1);
    imwrite(Image, ImageFileName, 'jpg');
end

%% Writing the overlap matrix

OverlapMatrix = single(zeros(TotalViews, TotalViews));
for PType = 1 : numel(P3D)
    TotalPoints = numel(P3D(PType).Status);
    for CurrP3D = 1 : TotalPoints
        if P3D(PType).Status(CurrP3D) == 3
            PViews = P3D(PType).ViewIndex(P3D(PType).DescIndex{CurrP3D});
            for I = 1 : numel(PViews)
                OverlapMatrix(PViews(I), PViews) = 1;
            end
        end
    end
end
OverlapMatrix = OverlapMatrix .* (1 - eye(TotalViews));
OverlappingImgs = find(sum(OverlapMatrix, 2));

VisDatFile = fopen('.\\PMVS2\\vis.dat', 'wt');
fprintf(VisDatFile, 'VISDATA\n');
fprintf(VisDatFile, '%d\n', numel(OverlappingImgs));

for CurrView = OverlappingImgs'
    DataRow = find(OverlapMatrix(CurrView, :));
    OverlapImages = numel(DataRow);
    OutputData = [CurrView-1 OverlapImages DataRow-1];
    fprintf(VisDatFile, '%d ', OutputData);
    fprintf(VisDatFile, '\n');
end

fclose(VisDatFile);

%% Writing option file

OptionFile = fopen('.\\PMVS2\\option.txt', 'wt');

fprintf(OptionFile, 'level %d\n', OptionLevel);
fprintf(OptionFile, 'csize %d\n', OptionCsize);
fprintf(OptionFile, 'threshold %.01f\n', OptionThreshold);
fprintf(OptionFile, 'wsize %d\n', OptionWsize);
fprintf(OptionFile, 'minImageNum %d\n', OptionMinImageNum);
fprintf(OptionFile, 'CPU %d\n', OptionCPU);
fprintf(OptionFile, 'useVisData %d\n', OptionUseVisData);
if OptionSequence
    fprintf(OptionFile, 'sequence %d\n', OptionSequence);
end
fprintf(OptionFile, 'timages -1 0 %d\n', TotalViews);
fprintf(OptionFile, 'oimages 0\n');

fclose(OptionFile);


    