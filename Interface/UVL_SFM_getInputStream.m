function Frame = UVL_SFM_getInputStream(InputDataInfo, FrameIndex) 
if nargin < 2
    error('MATLAB:UVL_SFM_getInputStream', 'Invalid syntax. Check help');
end
if FrameIndex < 1 || FrameIndex > InputDataInfo.TotalFrames
    error('MATLAB:UVL_SFM_getInputStream', 'Frame index out of range');
end    
switch lower(InputDataInfo.Type)
    case 'sequence'
        Frame = imread([InputDataInfo.DirName '/' InputDataInfo.Data(FrameIndex).name]);
    case 'datfile'
        InputDataInfo.FileHandle = fopen(InputDataInfo.DatFileName, 'rb');
        if InputDataInfo.FileHandle == -1
            error('MATLAB:UVL_SFM_getInputStream', 'Stream IO Error');
        end
        Frame = data_read(InputDataInfo.FileHandle, FrameIndex);
        fclose(InputDataInfo.FileHandle);
    otherwise
        error('MATLAB:UVL_SFM_getInputStream', 'Invalid syntax. Check help');
end