% converts avi movies to sequence files
% Usage: UVL_SFM_importIMGS(InputDir, InputFileType, OutputDir, FrameSequence, Deinterlace, Rectify)
%        [start step stop] - frame reading 
%        Deinterlace - bool, true to deinterlace the frames
%        Rectify - bool, true to rectify the frames

function UVL_SFM_importIMGS(InputDir, InputFileType, OutputDir, FrameSequence, Deinterlace, Rectify)

if nargin < 3
    fprintf('Not enough input parameters. Use: UVL_SFM_importIMGS')
    return
end
InputData = dir([InputDir '\*.' InputFileType]);
TotalFrames = numel(InputData);

if ~exist('FrameSequence', 'var')
    Sequence = 1 : TotalFrames;
elseif numel(FrameSequence) == 1   %% only 1 parameter - assuming frame step
    Sequence = 1 : FrameSequence : TotalFrames;
elseif numel(FrameSequence) == 2
    Sequence = FrameSequence(1) : min([TotalFrames FrameSequence(2)]);
elseif numel(FrameSequence) == 3
    Sequence = FrameSequence(1) : FrameSequence(1) : min([TotalFrames FrameSequence(3)]);
else
    fprintf('Invalid format on FrameSequence. Use: help UVL_SFM_importIMGS')
    return
end    

if ~exist('Deinterlace', 'var')
    Deinterlace = false;
end

if ~exist('Rectify', 'var')
    Rectify = false;
end
fprintf('Aditional options: ')
if Deinterlace
    fprintf('[deinterlacing] ')
end
if Rectify
    fprintf('[rectifying] ')
end
fprintf('\n');

Frame = imread([InputDir '\' InputData(1).name]);

FrameHeight = size(Frame, 1);
FrameWidth = size(Frame, 2);
Is16Bit = max(max(max(Frame))) > 2^8;

if length(Sequence)<1
    error('0 frames extracted. Check the prameters')
end
if Rectify
    fprintf('Reading radial distortion params [radial_dist.mat]...\n')
    if ~exist('radial_dist.mat', 'file')
        error('Radial distortion parameter file not found.')
    end
    load radial_dist
    fprintf('Computing rectification LUT...\n');
    if Deinterlace 
        RectLUT = computeRectLUT(eye(3), fc, cc, kc, alpha_c, KK_new, FrameWidth / 2, FrameHeight / 2);
    else
        RectLUT = computeRectLUT(eye(3), fc, cc, kc, alpha_c, KK_new, FrameWidth, FrameHeight);
    end        
end
for FrameIndex = Sequence
    fprintf('[%d/%d] > Reading ', FrameIndex, numel(Sequence));
    Frame = imread([InputDir '\' InputData(FrameIndex).name]);
    Frame = double(Frame);
    if Deinterlace
        fprintf('> Deinterlacing ')
        Frame = deinterlace(Frame);
    end
    if Rectify
        fprintf('> Rectifying ')
        Frame = applyRectLUT(Frame, RectLUT);       
    end
    if Is16Bit
        imwrite(uint16(Frame), [OutputDir '\v' InputData(FrameIndex).name]);
    else
        imwrite(uint8(Frame), [OutputDir '\v' InputData(FrameIndex).name]);
    end
    fprintf('> Done.\n')
end

function imgd = deinterlace(img)
imgd = (img(1:2:end, 1:2:end, :) + img(1:2:end, 2:2:end, :)) / 2;
