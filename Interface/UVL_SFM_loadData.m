%% load default parameters
UVL_SFM_defaultParameters
%% try load specific parameters for sequence
if exist('UVL_SFM_sequenceParameters.m', 'file')
    UVL_SFM_sequenceParameters
else
    error ('MATLAB:UVL_SFM_loadData', 'No sequence specific parameters found.');
end
load last
