function [Frame, Features] = UVL_SFM_loadCache(FrameIndex, Param, CachePath, Verbose )
    
if ~exist('CachePath', 'var'), CachePath = './Cache'; end
if ~exist('Verbose', 'var'), Verbose = true ; end


%% load the image if exist
ImageFileName = sprintf('%s/I%06dP%d%d%d%d%d', CachePath, FrameIndex, Param.Processing.SingleChannel, Param.Processing.NonUniformLightCorrection, Param.Processing.GaussianSize, Param.Processing.Equalization, Param.Processing.ResizeCoeff);
ImageFileNameExt = [ImageFileName '.png'];
if exist(ImageFileNameExt, 'file')
    Frame = im2double(imread(ImageFileNameExt));
else
    Frame = [];
end

if nargout > 1
    for count = 1:numel(Param.Detection.Type)
        %% Load Features if exist
        Type = Param.Detection.Type(count);
        TypeName = Param.Detection.Detectors{Type};
        FeaturesFileName = sprintf('%sF%d%d%d%d', ImageFileName, Type, Param.Detection.(TypeName).MaxNumber, Param.Detection.(TypeName).RadiusNonMaximal, Param.Detection.(TypeName).Border);
        if Type == 6, %MSER Specific parameters
            FeaturesFileName = sprintf('%s%d%d', FeaturesFileName, Param.Detection.MSER.MinimumRegionSize, Param.Detection.MSER.MinimumMargin);
        end
        FeaturesFileNameExt = [FeaturesFileName '.mat'];
        Features(count).DetectionCacheFile = FeaturesFileNameExt;
        if exist(FeaturesFileNameExt, 'file')
            load(FeaturesFileNameExt)
            Features(count).Points = CacheData.Points;
            Features(count).PointType = CacheData.PointType;
            if Verbose, UVL_SFM_printOut(sprintf(' %s [Loaded]: %d,', TypeName, size(Features(count).Points, 2))); end
        else
            Features(count).Points = [];
            Features(count).PointType = [];
        end
        Features(count).PointTypeName = TypeName;
        if nargout == 2;
%             Features = [];
            %% check input parameters
            if numel(Param.Description.Type) ~= 1 && numel(Param.Description.Type) ~= numel(Param.Detection.Type)
                error ('MATLAB:UVL_SFM_LoadFeaturesCache', 'No valid pairs of detector - descriptor');
            elseif numel(Param.Description.Type) == 1 && numel(Param.Detection.Type) ~= 1,
                Param.Description.Type = ones(1, numel(Param.Detection.Type)) .* Param.Description.Type;
            end
            
            %% Load Descriptions if exist
            Type = Param.Description.Type(count);
            TypeName = Param.Description.Descriptors{Type};
            DescriptionsFileNameExt = sprintf('%sD%d.mat', FeaturesFileName, Type);
            Features(count).DescriptionCacheFile = DescriptionsFileNameExt;
            if exist(DescriptionsFileNameExt, 'file')
                load(DescriptionsFileNameExt)
                Features(count).Descriptors = CacheData.Descriptors;
                Features(count).DescriptorType = CacheData.DescriptorType;
            else
                Features(count).Descriptors = [];
                Features(count).DescriptorType = [];
            end
            Features(count).DescriptorTypeName = TypeName;
        end
    end
%     for count = 1:numel(Param.Detection.Type)
%         %% Load Features if exist
%         Type = Param.Detection.Type(count);
%         TypeName = Param.Detection.Detectors{Type};
%         FeaturesFileName = sprintf('%sF%d%d%d%d', ImageFileName, Type, Param.Detection.(TypeName).MaxNumber, Param.Detection.(TypeName).RadiusNonMaximal, Param.Detection.(TypeName).Border);
%         if Type == 6, %MSER Specific parameters
%             FeaturesFileName = sprintf('%s%d%d', FeaturesFileName, Param.Detection.MSER.MinimumRegionSize, Param.Detection.MSER.MinimumMargin);
%         end
%         FeaturesFileNameExt = [FeaturesFileName '.mat'];
%         
%         Features(count).DetectionCacheFile = FeaturesFileNameExt;
%         if exist(FeaturesFileNameExt, 'file')
%             load(FeaturesFileNameExt)
%             Features(count).Points = CacheData.Points;
%             Features(count).PointType = CacheData.PointType;
%             UVL_SFM_printOut(sprintf(' %s [Loaded]: %d,', TypeName, size(Features(count).Points, 2)));
%         else
%             Features(count).Points = [];
%             Features(count).PointType = [];
%         end
%         Features(count).PointTypeName = TypeName;
%         
%         %% Load Descriptions if exist
%         Type = Param.Description.Type(count);
%         TypeName = Param.Description.Descriptors{Type};
%         DescriptionsFileNameExt = sprintf('%sD%d.mat', FeaturesFileName, Type);
%         Features(count).DescriptionCacheFile = DescriptionsFileNameExt;
%         if exist(DescriptionsFileNameExt, 'file')
%             load(DescriptionsFileNameExt)
%             Features(count).Descriptors = CacheData.Descriptors;
%             Features(count).DescriptorType = CacheData.DescriptorType;
%         else
%             Features(count).Descriptors = [];
%             Features(count).DescriptorType = [];
%         end
%         Features(count).DescriptorTypeName = TypeName;
%     end
end
