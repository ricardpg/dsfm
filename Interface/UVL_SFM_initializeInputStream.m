function [InputDataInfo TotalFrames] = UVL_SFM_initializeInputStream(StreamType, varargin) 
if ~nargin
    error('MATLAB:UVL_SFM_initializeInputStream', 'Invalid syntax. Check help');
end
switch lower(StreamType)
    case 'sequence'
        if numel(varargin) ~= 2
            error('MATLAB:UVL_SFM_initializeInputStream', 'Invalid syntax, use: UVL_SFM_initializeInputStream("sequence", directory, file_extension)');
        end
        InputDataInfo.Type = 'sequence';
        InputDataInfo.DirName = varargin{1};
        InputDataInfo.FileExt = varargin{2};
        InputDataInfo.Data = dir([InputDataInfo.DirName '/*.' InputDataInfo.FileExt]);
        TotalFrames = numel(InputDataInfo.Data);
        if TotalFrames == 0
            error('MATLAB:UVL_SFM_initializeInputStream', 'Input Stream Error');
        end
    case 'datfile'
        if numel(varargin) ~= 1
            error('MATLAB:UVL_SFM_initializeInputStream', 'Invalid syntax, use: UVL_SFM_initializeInputStream("datfile", dat_file)');
        end
        InputDataInfo.Type = 'datfile';
        InputDataInfo.DatFileName = varargin{1};
        InputDataInfo.FileHandle = fopen(InputDataInfo.DatFileName, 'rb');
        if InputDataInfo.FileHandle == -1
            error('MATLAB:UVL_SFM_initializeInputStream', 'Input Stream Error');
        end
        TotalFrames = data_read(InputDataInfo.FileHandle);
    otherwise
        error('MATLAB:UVL_SFM_initializeInputStream', 'Invalid syntax. Check help');
end
InputDataInfo.TotalFrames = TotalFrames;
