function Features = UVL_SFM_loadFeaturesCache(FrameIndex, Param)
%% check input parameters
if size(Param.Description.Type, 2) ~= 1 && size(Param.Description.Type, 2) ~= size(Param.Detection.Type, 2)
    error ('MATLAB:UVL_SFM_LoadFeaturesCache', 'No valid pairs of detector - descriptor');
elseif size(Param.Description.Type, 2) == 1 && size(Param.Detection.Type, 2),
    Param.Description.Type = ones(1,size(Param.Detection.Type, 2)) .* Param.Description.Type;
end
%% parse preprocessing params
PreprocessingParamFields = fieldnames(Param.Processing, '-full');
PreprocessingParamsString = 'P';
for currField = 1:numel(PreprocessingParamFields)
    PreprocessingParamsString = sprintf('%s%d', PreprocessingParamsString, getfield(Param.Processing, PreprocessingParamFields{currField}));
end

%% parse data structures
DetectorTypes = fieldnames(Param.Detection, '-full');
DescriptorTypes = fieldnames(Param.Description, '-full');
for count = 1:numel(Param.Detection.Type)
   % cache files for features
    DetectionCacheFile = sprintf('Cache\\I%06d%sF%d', FrameIndex, PreprocessingParamsString, Param.Detection.Type(count));
    DetectorParameters = getfield(Param.Detection, DetectorTypes{Param.Detection.Type(count)+1});
    if isstruct(DetectorParameters)
        DetectorParameterFields = fieldnames(DetectorParameters, '-full');
    else
        DetectorParameterFields = [];
    end
    for currField = 1: numel(DetectorParameterFields)
        FieldValue = getfield(DetectorParameters, DetectorParameterFields{currField});
        DetectionCacheFile = sprintf('%s%d',DetectionCacheFile, FieldValue);
    end
    DetectionCacheFileExt = sprintf('%s.mat',DetectionCacheFile);
    Features(count).DetectionCacheFile = DetectionCacheFileExt;
    if exist(DetectionCacheFileExt, 'file')
        load(DetectionCacheFileExt)
        Features(count).Points = CacheData.Points;
        Features(count).PointType = CacheData.PointType;
        UVL_SFM_printOut(sprintf(' %s [Loaded]: %d,', DetectorTypes{Param.Detection.Type(count)+1}, size(Features(count).Points,2)));
    else
        Features(count).Points = [];
        Features(count).PointType = [];
    end
    % cache files for feature descriptors
    DescriptionCacheFile = sprintf('%sD%d.mat', DetectionCacheFile, Param.Description.Type(count));
    Features(count).DescriptionCacheFile = DescriptionCacheFile;
    if exist(DescriptionCacheFile, 'file')
        load(DescriptionCacheFile)
        Features(count).Descriptors = CacheData.Descriptors;
        Features(count).DescriptorType = CacheData.DescriptorType;
%        UVL_SFM_printOut(sprintf(' %s [Loaded]: %d,', DetectorTypes{Param.Detection.Type(count)+1}, size(Features(count).Points,2)));
    else
        Features(count).Descriptors = [];
        Features(count).DescriptorType = [];
    end

end        