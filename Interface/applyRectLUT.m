function Irec = applyRectLUT(I, rectLUT)

[height, width, depth] = size(I);

Irec = 255*ones(height,width,depth);
Irec_channel = 255*ones(height,width);

for channel = 1:depth
    Ichannel = I(:,:,channel);
    Irec_channel(rectLUT.ind_new) = rectLUT.a1 .* Ichannel(rectLUT.ind_lu) + rectLUT.a2 .* Ichannel(rectLUT.ind_ru) + ...
        rectLUT.a3 .* Ichannel(rectLUT.ind_ld) + rectLUT.a4 .* Ichannel(rectLUT.ind_rd);
    Irec(:,:,channel) = Irec_channel;
end

