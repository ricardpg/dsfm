% converts images to seq dat file
% Usage: UVL_SFM_convertImgs2Dat(root,type, start,step,stop,interlaced,rectify,blur)
%        root, type - input files
%        start, step, stop- frame reading (use stop = 0 to read untill end)
%        interlaced - bool, whether the video is interlaced

function UVL_SFM_convertImgs2Dat(root,type,start,step,stop,dat_file,interlaced,rectify,blur)

if nargin < 7
    error('Not enough input parameters. Use: help UVL_SFM_convertImgs2Dat')
end
feat = 0;
fprintf('Aditional options: ')
if interlaced
    fprintf('[deinterlacing] ')
    feat = 1;
end
if rectify
    fprintf('[rectifying] ')
    feat = 1;
end
if blur
    fprintf('[motion blur] ')
    feat = 1;
end
if ~feat 
    fprintf('[none]')
end
fprintf('\n')

%gauss = fspecial('gaussian', 3 , 1);


image_pos = start:step:stop;
if length(image_pos)<1
    error('0 frames. Check the prameters')
end
img_name = sprintf('%s%04d.%s', root, image_pos(1), type);
im = imread(img_name);
[height width depth] = size(im);
if interlaced
    fid = init_data_file(dat_file, width/2, height/2, depth);
else
    fid = init_data_file(dat_file, width, height, depth);
end

if rectify
    fprintf('Reading radial distortion params [radial_dist.mat]...\n')
    load radial_dist
    fprintf('Computing rectification LUT...\n');
    if interlaced 
        rectLUT = computeRectLUT(eye(3),fc,cc,kc,alpha_c,KK_new,width/2,height/2);
    else
        rectLUT = computeRectLUT(eye(3),fc,cc,kc,alpha_c,KK_new,width,height);
    end        
end


for index = 1:length(image_pos)
%    curr_pos = image_pos(index);
    img_name = sprintf('%s%04d.%s', root, image_pos(index), type);
    img = double(imread(img_name));
    fprintf('[%d/%d - fr. %s] > Reading ', index, length(image_pos), img_name);
    if interlaced
        fprintf('> Deinterlacing ')
        img = deinterlace(img);
    end
    if rectify
        fprintf('> Rectifying ')
%        img = rectRGB(double(img),eye(3),fc,cc,kc,alpha_c,KK_new);
        img = applyRectLUT(img,rectLUT);
    end
%     if blur
%         fprintf('> Blur scoring ')
%         imgGray = rgb2gray(img/255);
%         imgGauss = conv2(imgGray, gauss, 'same');
% %        images(index).blurring = mean(mean(abs(imgGray-imgGauss)));
%    end
    data_write(fid, uint8(img));
    fprintf('> Done.\n')
end
fclose(fid)
function imgd = deinterlace(img)
%    imgd = (img(1:2:end, 1:2:end, :) + img(1:2:end, 2:2:end, :)) / 2;
%    imgd = (img(1:2:end, 1:2:end, :) + img(1:2:end, 2:2:end, :)) / 2;
    imgd = imresize(img,.5, 'bilinear');
