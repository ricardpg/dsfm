%% cleanup
close all
clear

%% turn off anoying warnings
warning off MATLAB:divideByZero
warning off MATLAB:singularMatrix
warning off MATLAB:nearlySingularMatrix
warning off MATLAB:illConditionedMatrix

%% load default parameters
UVL_SFM_defaultParameters

%% try load specific parameters for sequence
if exist('UVL_SFM_sequenceParameters.m', 'file')
    UVL_SFM_sequenceParameters
else
    warning('MATLAB:UVL_SFM_run', 'No sequence specific parameters found.');
end

%% open logfile if needed
if exist('LOGFILE_', 'var')
    LOGFILE_ = fopen('runSFM.log', 'a');
end
Now = clock();
UVL_SFM_printOut(sprintf('++++++++++++++++++++++ START_SESSION ++++++++++++++++++++++++++\n'));
UVL_SFM_printOut(sprintf('at: %d-%d-%d, %d:%02d:%02.0f\n', Now(1), Now(2), Now(3), Now(4), Now(5),  Now(6)));

%% check and create cache dir
% WARNING: You must not have another folder named Cache in matlab's search
%          path, as it may conflict with this one!
if ~exist('.\Cache', 'dir')
    mkdir('Cache')
end

%% load initialization data or resume
if exist('last.mat', 'file') && RESUME_ && ~RESTART_
    load last
elseif exist('initial_model.mat', 'file') && ~RESTART_
    load initial_model
end

%% initialize variables
UVL_SFM_printOut(sprintf('Total number of frames: %d\n', NbrFrames));
if ~exist('P3D', 'var')
    P3D = UVL_SFM_allocateDB(Param.General, Param.Detection, Param.Description);
    View = [];
    View(1).Pose = eye(4);
    View(1).PoseComputed = true;
    DBStatistics.DBItems = [];
    DBStatistics.P3DItems = [];    
    ExecutionTimes = zeros(4, NbrFrames);
end

%% Front End Initialization
if FRONTEND_
    FigImage = figure;
    FigWorld = figure;
    FigMatch = figure;
    FigStats = figure;
    UVL_SFM_frontEndFigurePropreties('read');
end

%% model initialization
% check if there is 3D information available
No3DInfo = numel(View) < 2;
if No3DInfo
    UVL_SFM_printOut(sprintf('\n[Initializing model]\n'));
end
%% initialize misc variables
CurFrame = 1;
JustGot3DInfo = 0;
if ~Param.CameraPose.CheckAttSmothness
    Param.CameraPose.AttSmothnessDelta = 0;
end


while No3DInfo
    tic
    UVL_SFM_printOut(sprintf('--------Frame %d--------\n',CurFrame));
    
    UVL_SFM_printOut(sprintf('\tExtracting features  >>'));
    [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
    if isempty(ICur),
        ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
        ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
    end
    Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
    Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc));
    PrevTime = toc;  
    
    UVL_SFM_printOut(sprintf('\tMatching Features    >>'));
    Features = UVL_SFM_match(P3D, Features, Param.Matching);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc;  
    
    % if not the 1st view, try to compute the motion and check for outliers
    if CurFrame >= Param.General.MinimumViewsForInitialization
        % attempts to compute camera motion
        UVL_SFM_printOut(sprintf('\tCamera Motion        >> '));
        [CurrentCameraMotion P3D Features] = UVL_SFM_computeCameraMotion(P3D, Features, K, Param.Motion);
        % if motion was successfuly computed
        if numel(CurrentCameraMotion)
            No3DInfo = false;
            View(CurFrame).Pose = CurrentCameraMotion;
            View(CurFrame).PoseComputed = true;
            View(1).IsInitial = true;
            View(CurFrame).IsInitial = true;
            % now that we have a starting 3D model, let's find the previous camera positions
            for BaseIndex = 2:(CurFrame-1)
                UVL_SFM_printOut(sprintf('\tRegistering camera   >>'));
                [View(BaseIndex).Pose P3D] = UVL_SFM_getCameraPoseRecursive(P3D, BaseIndex, K, Param.CameraPose);
                if isempty(View(BaseIndex).Pose),
                    View(BaseIndex).PoseComputed = false;
                    error('camera pose not found (should erase the points of this view)');
                else
                    View(BaseIndex).PoseComputed = true;
                    View(BaseIndex).IsInitial = true;
                end
                UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
                PrevTime = toc;
            end
            DBStatistics = UVL_SFM_frontEndDBStatistics(P3D, DBStatistics);
        end
    end
    % update database
    UVL_SFM_printOut(sprintf('\tUpdating Database    >>'));
    P3D = UVL_SFM_updateDatabase(P3D, Features, CurFrame, Param.General);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    CurFrame = CurFrame + 1;
    JustGot3DInfo = 1;
    UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc));
    if Param.Geometrical.Enable
        P3D = UVL_SFM_updateCovariance(P3D, View, K);
    end
end

if JustGot3DInfo
    tic
    UVL_SFM_printOut(sprintf('--------SBA--------\n'));
    [P3D View Update] = UVL_SFM_runSBA(P3D, View, K, 0);
    if Param.Geometrical.Enable
        P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
    end    
    UVL_SFM_printOut(sprintf('\tCheck bad points     >>')); 
    P3D = UVL_SFM_checkBpError(P3D, View, K, 2, 2.5);
    UVL_SFM_printOut(sprintf('\n'));
    UVL_SFM_printOut(sprintf('\tNew 3D verteces      >>')); 
    [P3D Update] = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, Param.VertexPosition);
    if Param.Geometrical.Enable
        P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
    end    
    UVL_SFM_printOut(sprintf('\n'));
    UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc));  
    save initial_model P3D View DBStatistics
end


%% direct camera registration
UVL_SFM_printOut(sprintf('\n[Switching to direct registration]\n'));
for CurFrame = numel(View) + 1 : NbrFrames
    tic
    UVL_SFM_printOut(sprintf('--------Frame %d--------\n',CurFrame));
    
    UVL_SFM_printOut(sprintf('\tExtracting features  >>'));
    [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
    if isempty(ICur),
        ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
        ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
    end
    Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
    Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc));
    PrevTime = toc;
    
    UVL_SFM_printOut(sprintf('\tMatching Features    >>'));
%    Features = UVL_SFM_match(P3D, Features, Param.Matching, max([1 CurFrame-20]):CurFrame);
    Features = UVL_SFM_match(P3D, Features, Param.Matching);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    ExecutionTimes(1, CurFrame) =  sum([P3D(:).Status]>0);
    ExecutionTimes(2, CurFrame) =  toc-PrevTime;
    PrevTime = toc;  
    % get camera position
    UVL_SFM_printOut(sprintf('\tRegistering camera   >>'));
    [View(CurFrame).Pose Features] = UVL_SFM_getCameraPoseDirect(P3D, Features, K, Param.CameraPose);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    ExecutionTimes(3, CurFrame) =  toc-PrevTime;
    PrevTime = toc;
    if isempty(View(CurFrame).Pose),
        View(CurFrame).Pose = eye(4);
        View(CurFrame).PoseComputed = false;
%        Features = FeaturesTmp;
    else
        View(CurFrame).PoseComputed = true;
        View(CurFrame).IsInitial = false;
%        UVL_SFM_printOut(sprintf('\tRefining matches     >>'));
%        [View(CurFrame).Pose Features] = UVL_SFM_refineMatches(P3D, Features, View(CurFrame).Pose, K, Param.Frame, Param.Matching);
    end

    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc;
    
    % update database
    UVL_SFM_printOut(sprintf('\tUpdating Database    >>'));
    [P3D Update] = UVL_SFM_updateDatabase(P3D, Features, CurFrame, Param.General);
    if Param.Geometrical.Enable
        P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
    end    

    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    PrevTime = toc; 
    
    % get position of new vertexes
    UVL_SFM_printOut(sprintf('\tNew 3D verteces      >>'));
    [P3D Update] = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, Param.VertexPosition);

    if Param.Geometrical.Enable
        P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
    end        
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    ExecutionTimes(4, CurFrame) =  toc-PrevTime;
    PrevTime = toc;
    
    % cleanup forgotten features
    UVL_SFM_printOut(sprintf('\tCleaning up Database >>'));
    [P3D Update] = UVL_SFM_reMatchForgottenFeatures(P3D, View, K,  CurFrame, size(ICur,2), size(ICur,1), Param.Matching, Param.EraseFeatures);
%    WrongP3D = checkDB(P3D);   
        
    if Param.Geometrical.Enable
        P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
    end        
    
%    P3D = UVL_SFM_eraseForgottenFeatures(P3D, CurFrame, Param.EraseFeatures);
    UVL_SFM_printOut(sprintf(' [%.1f sec]\n', toc-PrevTime));
    
    UVL_SFM_printOut(sprintf('\tRemoving outliers    >>'));
    P3D = UVL_SFM_P3D_removeOutliers(P3D);
    UVL_SFM_printOut(sprintf('\n'));
    
    % some more front end
    if FRONTEND_
        UVL_SFM_frontEndShowMatchingFrame(Features, ICur, FigImage, Param.Processing.ResizeCoeff);
        DBStatistics=UVL_SFM_frontEndDBStatistics(P3D, DBStatistics, FigStats);
        UVL_SFM_frontEndShowWorldTopDown(P3D, View, FigWorld)
    else
        DBStatistics=UVL_SFM_frontEndDBStatistics(P3D, DBStatistics);
    end
    UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc));       

    % global alignment
    if ~mod(CurFrame, Param.General.BundleStep)
        UVL_SFM_printOut(sprintf('\tRemoving outliers    >>'));
        P3D = UVL_SFM_P3D_removeOutliers(P3D);
        UVL_SFM_printOut(sprintf('\n'));
        tic
        UVL_SFM_printOut(sprintf('--------SBA--------\n'));
        range_start = max([1 CurFrame-Param.General.BundleRange]);
        [P3D View Update] = UVL_SFM_runSBA(P3D, View, K, 0, range_start:CurFrame);
        if Param.Geometrical.Enable
            P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
        end        
        UVL_SFM_printOut(sprintf('\tCheck bad points     >>')); 
        P3D = UVL_SFM_checkBpError(P3D, View, K, 2.0, 2.5, range_start:CurFrame);
        UVL_SFM_printOut(sprintf('\n'));
        UVL_SFM_printOut(sprintf('\tNew 3D verteces      >>'));
        [P3D Update] = UVL_SFM_get3DVertexPositionFactorial(P3D, View, K, Param.VertexPosition);
        if Param.Geometrical.Enable
            P3D = UVL_SFM_updateCovariance(P3D, View, K, Update);
        end        
        UVL_SFM_printOut(sprintf('\n'));
%           %%% bundle the whole sequence
         UVL_SFM_printOut(sprintf('\tRemoving outliers    >>'));
         P3D = UVL_SFM_P3D_removeOutliers(P3D);
         UVL_SFM_printOut(sprintf('\n'));
        UVL_SFM_printOut(sprintf('\tStep time [%.1f sec]\n', toc)); 
    end
    % geometrical
    if Param.Geometrical.Enable
        if ((CurFrame / Param.Geometrical.FrameDec) == round(CurFrame / Param.Geometrical.FrameDec))
            P3D = UVL_SFM_selectGeometricalVertices(P3D, CurFrame-1, CurFrame, Param.Geometrical, InputDataInfo, 0);
        end
    end
    pause(0.001);
end
%[P3D View] = UVL_SFM_runSBA(P3D, View, K, 0);
save last P3D View DBStatistics

% close the log file
if exist('LOGFILE_', 'var')
    if ~isempty(LOGFILE_)
        fclose(LOGFILE_);
        LOGFILE_ = [];
    end
end
