Since Surf implementation in FIM is not currently working in 64 bits Windows system 
(uses a precompiled 32 bits library), an external executable is used to compute Surf detection 
and description.
However, and since FIM's function "SurfOrientationMx" also uses this library, there is no way to 
use SURF descriptor with another detector apart from surf itself...