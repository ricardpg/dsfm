/* 	return the squared correlation coeficient of each pixel of the search window 
	input image should UINT8
	input correlation window should be square
	input search window should be at least the same size than correlation window*/
	
#include <math.h>
#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double *out, *initout, xi, yi, xi2, yi2, xiyi, r2, dxi, dyi, *max, y1, y2, y3, y4, y5, m1, m2, det, cx, cy;
	int Ch, Sh, Sw, n, nbrx, nbry, x, xx, y, yy;
    unsigned char *SData, *CData;
	
	/* parameter checks */
	if ((nrhs != 2) || (nlhs != 2)) {
		mexErrMsgTxt("Usage: [corr maxCoor] = correl(SearchWindow,CorrelWindow)\n");
		return;
	}
	
	/* reading the parameters */
	Ch = mxGetM(prhs [1]);
	Sh = mxGetM(prhs [0]);
	Sw = mxGetN(prhs [0]);
	SData = (unsigned char*) mxGetPr(prhs [0]);
	CData = (unsigned char*) mxGetPr(prhs [1]);

	nbrx = Sw - Ch + 1;
	nbry = Sh - Ch + 1;
    n = Ch*Ch;
	
	/* require memory for return */
	plhs [0] = mxCreateDoubleMatrix(nbry, nbrx, mxREAL);
	out = (double *) mxGetPr(plhs [0]);
    initout = out;
    plhs [1] = mxCreateDoubleMatrix(1, 3, mxREAL);
	max = (double *) mxGetPr(plhs [1]);
	


    max[2] = -1;
    for(x = 0; x < nbrx; ++x)
	{
		for(y = 0; y < nbry; ++y)
		{
			xi = yi = xi2 = yi2 = xiyi = 0.0;
			for(xx = 0; xx < Ch; ++xx)
			{
				for(yy = 0; yy < Ch; ++yy)
				{
					dxi = CData[xx*Ch+yy];
					dyi = SData[(xx+x)*Sh+(yy+y)];
					xi += dxi;
					yi += dyi;
					xi2 += dxi * dxi;
					yi2 += dyi * dyi;
					xiyi += dxi * dyi;
				}
			}
			r2 = xiyi*n - xi*yi;
			r2 *= (r2 < 0 ? -r2 : r2);
            r2 /= ((xi2*n - xi*xi) * (yi2*n - yi*yi));
            
            if(r2>max[2])
            {
                max[2] = r2;
                max[0] = x;
                max[1] = y;
            }
            
   			*out = r2;
            out++;
		}
	}
        
    cx = cy = 0;
    if(max[0]>0 && max[1]>0 && max[0]<nbrx-1 && max[1]<nbry-1)
    {
        initout += (int)((max[0]-1)*nbry+(max[1]-1));
        y1 =   *initout   - max[2];
        y2 =   *initout   - max[2];
        y3 =   *initout   - max[2];
        y4 = -(*initout   - max[2]);
        y5 = -(*initout++ - max[2]);
        y1 +=  *initout   - max[2];
        y4 -=  *initout++ - max[2];
        y1 +=  *initout   - max[2];
        y2 -=  *initout   - max[2];
        y3 +=  *initout   - max[2];
        y4 +=  *initout   - max[2];
        y5 +=  *initout   - max[2];
        initout+= (nbry-2);
        y3 +=  *initout   - max[2];
        y5 -=  *initout   - max[2];
        initout+= 2;
        y3 +=  *initout   - max[2];
        y5 +=  *initout   - max[2];
        initout+= (nbry-2);
        y1 +=  *initout   - max[2];
        y2 -=  *initout   - max[2];
        y3 +=  *initout   - max[2];
        y4 -=  *initout   - max[2];
        y5 -=  *initout++ - max[2];
        y1 +=  *initout   - max[2];
        y4 +=  *initout++ - max[2];
        y1 +=  *initout   - max[2];
        y2 +=  *initout   - max[2];
        y3 +=  *initout   - max[2];
        y4 +=  *initout   - max[2];
        y5 +=  *initout++ - max[2];
        y2 /= 4;
        y4 /= 6;
        y5 /= 6;
        
        m1 =  0.6 * y1 - 0.4 * y3;
        m2 = -0.4 * y1 + 0.6 * y3;

        det = m1*m2 - y2*y2;

        cx = -(m2/det) * y4 + (y2/det) * y5;
        cy =  (y2/det) * y4 - (m1/det) * y5;
        if(cx<-0.5 || cx>0.5 || cy<-0.5 || cy>0.5)
            cx = cy = 0;
    }       
        
    max[1] += (cy + 1);
    max[0] += (cx + 1);
   	return;
}

