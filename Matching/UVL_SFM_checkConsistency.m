function [Features E] = UVL_SFM_checkConsistency(P3D, Features, K, base, motionParameters)
% check consistency of the data with other views

feat(numel(Features)).Indices = [];
P = [];
p = [];
for fe = 1 : numel(Features)
    for i = 1 : size(Features(fe).Pairs, 2)
        index = find([P3D(Features(fe).Pairs(2,i)).View(:).Index] == base);
        if numel(index)
            P = [P P3D(Features(fe).Pairs(2,i)).View(index).Pos(1:2)];
            p = [p Features(fe).Points(1:2, Features(fe).Pairs(1,i))];
            feat(fe).Indices = [feat(fe).Indices i];
        end
    end
end

[E, Inliers] = UVL_SFM_getEssentialMatrix([P; p], motionParameters, K);

for fe = 1 : numel(Features)
    InliersGroupIndices = Inliers(Inliers <= numel(feat(fe).Indices));
    Inliers = Inliers(Inliers > numel(feat(fe).Indices)) - numel(feat(fe).Indices);
    InliersGroupMask = true(1, size(Features(fe).Pairs,2));
    InliersGroupMask(feat(fe).Indices) = false;
    InliersGroupMask(feat(fe).Indices(InliersGroupIndices)) = true;
    Features(fe).Pairs = Features(fe).Pairs(:, InliersGroupMask);
end
