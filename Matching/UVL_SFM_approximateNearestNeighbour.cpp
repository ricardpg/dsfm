//Method 1 = nearest neighbor
//Method 2 = second nearest neighbor
//Method 3 = second nearest neighbor bidirectionnal

#include "mex.h"
#include <ANN/ANN.h>					        // ANN declarations


void mexFunction(int nout, mxArray *out[], int nin, const mxArray *in[])
{
	ANNpointArray		dataPts;				// data points
	ANNidxArray			nnIdx;					// near neighbor indices
	ANNdistArray		dists;					// near neighbor distances
	ANNkd_tree*			kdTree;					// search structure
    double              *PointsDB, *PointsImg, *DistArray;
    int                 TotMatch = 0, Dimension, NumPointsDB, NumPointsImg, Method;
    double              ErrorBounds, Thresh;



    if (nin != 5 || nout > 1) 
        mexErrMsgTxt("Usage: Pairs = UVL_SFM_approximateNearestNeighbour(Descriptor1, Descriptors2, Method, Threshold, ErrorBounds)");
    


    PointsImg = mxGetPr(in[0]);
    NumPointsImg = mxGetN(in[0]);
    PointsDB = mxGetPr(in[1]);
    NumPointsDB = mxGetN(in[1]);
    Dimension = mxGetM(in[1]);
    Method = *mxGetPr(in[2]);
    Thresh = *mxGetPr(in[3]);
    ErrorBounds = *mxGetPr(in[4]);
 
    if (NumPointsDB == 0 || NumPointsImg == 0)
    {
        out[0] = mxCreateDoubleMatrix(3, 0, mxREAL) ;
        return;
    }
    
	dataPts = new ANNpoint[NumPointsDB];
    for (int i = 0; i < NumPointsDB; i++, PointsDB += Dimension) {
		dataPts[i] = PointsDB;
	}
   	kdTree = new ANNkd_tree(dataPts, NumPointsDB, Dimension);					// build search structure



    if(Method == 1) //Method 1 = nearest neighbor
    {    
        out[0] = mxCreateDoubleMatrix(3, NumPointsImg, mxREAL) ;
        DistArray = mxGetPr(out[0]); 
        nnIdx = new ANNidx[1];						    // allocate near neighbor indices
    	dists = new ANNdist[1];						    // allocate near neighbor dists
        Thresh *= Thresh;
        for(int i = 0; i < NumPointsImg; ++i, PointsImg += Dimension)
        {

            kdTree->annkFRSearch(PointsImg, Thresh, 1, nnIdx, dists, ErrorBounds);	// search
            if(*dists <= Thresh)
            {
                ++TotMatch;
                *DistArray++ = i + 1.0;
                *DistArray++ = *nnIdx + 1.0;
                *DistArray++ = sqrt(*dists);
            }
        }
    }
    else if(Method == 2) //Method 2 = second nearest neighbor (ratio between squared distances)
    {
        out[0] = mxCreateDoubleMatrix(6, NumPointsImg, mxREAL) ;
        DistArray = mxGetPr(out[0]); 
       	nnIdx = new ANNidx[2];						    // allocate near neighbor indices
    	dists = new ANNdist[2];						    // allocate near neighbor dists
        for(int i = 0; i < NumPointsImg; ++i, PointsImg += Dimension)
        {
            kdTree->annkSearch(PointsImg, 2, nnIdx, dists, ErrorBounds);	// search
            if(dists[1] / dists[0] >= Thresh)
            {
                ++TotMatch;
                *DistArray++ = i + 1.0;
                *DistArray++ = nnIdx[0] + 1.0;
                *DistArray++ = dists[0];
                *DistArray++ = nnIdx[1] + 1.0;
                *DistArray++ = dists[1];
                *DistArray++ = dists[1] / dists[0];
            }
        }
    }
    else if(Method == 3) //Method 3 = second nearest neighbor bidirectionnal (ratio between squared distances)
    {
        out[0] = mxCreateDoubleMatrix(6, NumPointsImg, mxREAL) ;
        DistArray = mxGetPr(out[0]);  

        ANNpoint* imgPts;
        imgPts = new ANNpoint[NumPointsImg];
        for (int i = 0; i < NumPointsImg; i++, PointsImg += Dimension) {
            imgPts[i] = PointsImg;
        }

        ANNidx* nnIdx2;
        ANNdist* dists2;
        nnIdx = new ANNidx[2];						    // allocate near neighbor indices
        dists = new ANNdist[2];						    // allocate near neighbor dists
        nnIdx2 = new ANNidx[2];						    // allocate near neighbor indices
        dists2 = new ANNdist[2];	

        ANNkd_tree* kiTree;
        kiTree = new ANNkd_tree(imgPts, NumPointsImg, Dimension);

        for(int i = 0; i < NumPointsImg; ++i)
        {
            kdTree->annkSearch(imgPts[i], 2, nnIdx, dists, ErrorBounds);	// search
            if(dists[1] / dists[0] >= Thresh)
            {
                kiTree->annkSearch(dataPts[nnIdx[0]], 2, nnIdx2, dists2, ErrorBounds);	// search
                if(dists2[1] / dists2[0] >= Thresh & nnIdx2[0] == i)
                {
                    ++TotMatch;
                    *DistArray++ = i + 1.0;
                    *DistArray++ = nnIdx[0] + 1.0;
                    *DistArray++ = dists[0];
                    *DistArray++ = nnIdx[1] + 1.0;
                    *DistArray++ = dists[1];
                    *DistArray++ = dists[1] / dists[0];
                }
            }
        }
        delete [] nnIdx2;							// clean things up
        delete [] dists2;
        delete kiTree;
    }
    mxSetN(out[0], TotMatch);



    delete [] nnIdx;							// clean things up
    delete [] dists;
    delete kdTree;
	annClose();									// done with ANN
	return;
}
