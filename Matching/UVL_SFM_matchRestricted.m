function [Features, DB_Size, ElapsedTime] = UVL_SFM_matchRestricted(P3D, Features, Matching, FrameRange)

for i = 1 : size(Features,2),
    if ~numel(Features(i).Points), Features(i).Pairs = []; continue; end
    DBItem = find([P3D(:).PointType] == Features(i).PointType & [P3D(:).DescriptorType] == Features(i).DescriptorType);
    Features(i).DBIndex = DBItem;
    ValidItems = find(P3D(DBItem).Status);
    if exist('FrameRange', 'var')
        P3DInRangeMask = false(1, numel(P3D(DBItem).ViewIndex));
        for CurFrame = FrameRange
            P3DInRangeMask(P3D(DBItem).ViewIndex == CurFrame) = true;
        end
        P3DInRange = P3D(DBItem).ViewP3DIndex(P3DInRangeMask);
        P3DInRange = unique(P3DInRange);
        ValidItems = P3DInRange(P3D(DBItem).Status(P3DInRange) > 0);
    end
    if ~numel(ValidItems); Features(i).Pairs = []; continue; end
    FeaturesDB = P3D(DBItem).Desc(:, ValidItems);
    DB_Size = size(FeaturesDB, 2);
%% match local features with DB
    StartTime = toc;
    Thresh = Matching.(Features(i).DescriptorTypeName).DistanceThreshold;
    if Matching.ErrorBounds
        UVL_SFM_printOut(' [ANN]');
        Features(i).Pairs = UVL_SFM_approximateNearestNeighbour(Features(i).Descriptors, double(FeaturesDB), 1, Thresh, Matching.ErrorBounds);
    else
        UVL_SFM_printOut(' [NN]');
        Features(i).Pairs = UVL_SFM_nearestNeighbour(Features(i).Descriptors, double(FeaturesDB), Thresh);
    end
    Features(i).Pairs(2,:) = ValidItems(Features(i).Pairs(2,:));
    MaxMatches = size(Features(i).Pairs, 2) * Matching.MaxMatches;
    if size(Features(i).Pairs, 2) > MaxMatches,
        [B, IX] = sort(Features(i).Pairs(3,:), 2);
        Features(i).Pairs = Features(i).Pairs(:,IX(1:MaxMatches));
        [B, IX] = sort(Features(i).Pairs(1,:), 2);
        Features(i).Pairs = Features(i).Pairs(:,IX);
    end
    UVL_SFM_printOut(sprintf(' Type %d: %d,',Features(i).PointType, size(Features(i).Pairs,2)));
    StopTime = toc;
    ElapsedTime = StopTime - StartTime;
end