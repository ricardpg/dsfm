function [CameraPose Features] = UVL_SFM_refineMatches(P3D, Features, CameraPose, K, Width, Height, Matching)
%TODO : find with correl and recompute the descriptors.

    SBAP = [];
    SBAp = [];
    for i = 1 : size(Features,2),
        DBItem = find([P3D(:).PointType] == Features(i).PointType & [P3D(:).DescriptorType] == Features(i).DescriptorType);
        ValidDBMask = P3D(DBItem).Status == 3;
        ValidDBMask(Features(i).Pairs(2,:)) = false;
        ValidDB = find(ValidDBMask);

        if ~numel(ValidDB); continue; end
        DB3DPos = P3D(DBItem).Pos(:, ValidDB);
        DB3DPos(4,:) = 1;

        DB2DPos = (K * CameraPose(1:3,:)) * DB3DPos;
        DB2DPos(1,:) = DB2DPos(1,:) ./ DB2DPos(3,:);
        DB2DPos(2,:) = DB2DPos(2,:) ./ DB2DPos(3,:);
        DB2DPos(3,:) = 1;
        Valid3DPoints = DB2DPos(1,:) > 1 & ...
                        DB2DPos(1,:) < Width & ...
                        DB2DPos(2,:) > 1 & ...
                        DB2DPos(2,:) < Height;

        ValidDB = ValidDB(Valid3DPoints);
        DB2DPos = DB2DPos(:, Valid3DPoints);
        DB2DDesc = double(P3D(DBItem).Desc(:, ValidDB));

        ValidPointsMask = true(1, size(Features(i).Points, 2));
        ValidPointsMask(Features(i).Pairs(1,:)) = false;
        ValidPoints = find(ValidPointsMask);
        PointsDesc = Features(i).Descriptors(:, ValidPointsMask);
        PointsPos = Features(i).Points(1:2, ValidPointsMask);

    %% match local features with DB
        Thresh = Matching.(Features(i).DescriptorTypeName).DistanceThreshold * 1.5;
        Error = Matching.ErrorBounds;
        Pairs = UVL_SFM_approximateNearestNeighbour(PointsDesc, DB2DDesc, 1, Thresh, Error);

    %% Reject the outliers
        SquaredDistance = sum((DB2DPos(1:2, Pairs(2,:)) - PointsPos(1:2, Pairs(1,:))) .^ 2);
        ValidDistancesMask = SquaredDistance < 2.25; %1.5^2
        Pairs = Pairs(:, ValidDistancesMask);

    %% Update the pairs
        Pairs(1,:) = ValidPoints(Pairs(1,:));
        Pairs(2,:) = ValidDB(Pairs(2,:));
        Features(i).Pairs = [Features(i).Pairs Pairs];
        UVL_SFM_printOut(sprintf(' Type %d: %d,',Features(i).PointType, size(Pairs, 2)));

        Pairs3DMask = P3D(DBItem).Status(Features(i).Pairs(2,:)) == 3;
        Pairs3D = Features(i).Pairs(:, Pairs3DMask);
        SBAP = [SBAP P3D(DBItem).Pos(:, Pairs3D(2,:))];
        SBAp = [SBAp Features(i).Points(1:2, Pairs3D(1,:))];
    end
CameraPose = UVL_SFM_adjustCameraDirectSBA(SBAP, SBAp, CameraPose, K); % NL adjust of camera position
end