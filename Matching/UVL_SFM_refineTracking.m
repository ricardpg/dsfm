function P3D = UVL_SFM_refineTracking(P3D, View, ParamsCorr, Param)
    CorrRad = (ParamsCorr.CorrWindow - 1) / 2;
    SearchRad = (ParamsCorr.SearchWindow - 1) / 2;
    [XPatchR, YPatchR] = meshgrid(1 : ParamsCorr.CorrWindow + 2);
    for Group = 1 : numel(P3D)
        P3D(Group).ViewPatches = zeros(ParamsCorr.CorrWindow, ParamsCorr.CorrWindow, numel(P3D(Group).ViewIndex), 'uint8');
    end
    for Vv = 1 : numel(View)
        fprintf('\nImg: %d/%d ', Vv, numel(View));        
        Counter = 0;
        ICurr = im2uint8(UVL_SFM_loadCache(Vv, Param));
        for Group = 1 : numel(P3D)
            PointsInView = find(P3D(Group).ViewIndex == Vv);
            P3DInView = P3D(Group).ViewP3DIndex(PointsInView);
            Mask = P3D(Group).Status(P3DInView) == 3;
            PointsInView = PointsInView(Mask);
            P3DInView = P3DInView(Mask);
            for PointIdx = 1 : numel(P3DInView)
                LocalIndices = P3D(Group).DescIndex{P3DInView(PointIdx)};
                LocalViews = P3D(Group).ViewIndex(LocalIndices);
                if LocalViews(1) == Vv
                    PCoords = P3D(Group).ViewPos(:, PointsInView(PointIdx));
                else
                    CurrView = find(LocalViews == Vv);
                    BasePatch = P3D(Group).ViewPatches(:, :, LocalIndices(CurrView - 1));
                    PCurrR = round(P3D(Group).ViewPos(:, LocalIndices(CurrView)));
                    try
                        PatchCurr = ICurr(PCurrR(2) - CorrRad - SearchRad : PCurrR(2) + CorrRad + SearchRad, PCurrR(1) - CorrRad - SearchRad : PCurrR(1) + CorrRad + SearchRad);
                        [corr maxCorr] = correlWindow(PatchCurr, BasePatch);
                        if maxCorr(3) < ParamsCorr.MinCorr                            
                            P3D = eliminateView(P3D, Group, P3DInView(PointIdx), PointsInView(PointIdx), ParamsCorr.MinViews);
                            Counter = Counter + 1; 
                            continue
                        end
                        PCoords = PCurrR - SearchRad * ones(2, 1) + (maxCorr(1:2)' - 1);
                    catch
                        P3D = eliminateView(P3D, Group, P3DInView(PointIdx), PointsInView(PointIdx), ParamsCorr.MinViews);
%                        Counter = Counter + 1;
                        continue
                    end
                end
                try
                    Patch = getPatchInterpolation(ICurr, PCoords, XPatchR, YPatchR, CorrRad);
                catch
                    P3D = eliminateView(P3D, Group, P3DInView(PointIdx), PointsInView(PointIdx), ParamsCorr.MinViews);
%                    Counter = Counter + 1;                    
                    continue
                end 
                P3D(Group).ViewPatches(:, :, PointsInView(PointIdx)) = Patch;
                dist = norm(P3D(Group).ViewPos(:, PointsInView(PointIdx)) - PCoords);
                P3D(Group).ViewPos(:, PointsInView(PointIdx)) = PCoords;
%                fprintf('%f ', dist)
            end
        end
        fprintf(' - %d removed \n', Counter);
    end

    function Patch = getPatchInterpolation(I, PCoords, XPatchR, YPatchR, PatchRadius)
        PCoordsR = round(PCoords);
        PatchBaseR = I(PCoordsR(2) - PatchRadius - 1 : PCoordsR(2) + PatchRadius + 1, PCoordsR(1) - PatchRadius - 1 : PCoordsR(1) + PatchRadius + 1);
        CoordDiff = PCoords - PCoordsR;
        XPatch = XPatchR(2:end-1, 2:end-1) + CoordDiff(1);
        YPatch = YPatchR(2:end-1, 2:end-1) + CoordDiff(2);
        Patch = uint8(interp2(XPatchR, YPatchR, double(PatchBaseR), XPatch, YPatch));
    end

    function P3D = eliminateView(P3D, Group, P3DIndex, ViewIndex, MinViews)
        if P3D(Group).DescIndex{P3DIndex} < MinViews + 1
            P3D(Group).Status(P3DIndex) = 0;
            P3D(Group).ViewIndex(P3D(Group).DescIndex{P3DIndex}) = 0;
        else
            P3D(Group).ViewIndex(ViewIndex) = 0;
            IdxMask = P3D(Group).DescIndex{P3DIndex} ~= ViewIndex;
            P3D(Group).DescIndex{P3DIndex} = P3D(Group).DescIndex{P3DIndex}(IdxMask);
        end
    end
end
