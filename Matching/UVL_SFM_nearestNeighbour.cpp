//Method 1 = nearest neighbor
//Method 2 = second nearest neighbor
//Method 3 = second nearest neighbor bidirectionnal

#include"mex.h"



void mexFunction(int nout, mxArray *out[], int nin, const mxArray *in[])
{
    int feat1items, feat2items, dimensions, ic1, ic2, ic3, dd, mindistindex1, mindistindex2, mindistindex3, mindistindex4, TotMatch=0;
    double *DistArray, *feat1array, *feat1arraybis, *feat2array, Method, Thresh, ff, dst, mindist1, mindist2, mindist3, mindist4;



    if (nin != 4 || nout > 1) 
        mexErrMsgTxt("Usage: Pairs = UVL_SFM_nearestNeighbour(Descriptor1, Descriptors2, Method, Threshold)");
  


    dimensions = mxGetM(in[0]);
    feat1items = mxGetN(in[0]);
    feat1array = mxGetPr(in[0]);
    feat2items = mxGetN(in[1]);
    feat2array = mxGetPr(in[1]);
    Method = *mxGetPr(in[2]);
    Thresh = *mxGetPr(in[3]);



	if(Method == 1) //Method 1 = nearest neighbor
    {    
		out[0] = mxCreateDoubleMatrix(3, feat1items, mxREAL) ;
		DistArray = mxGetPr(out[0]);  
		Thresh *= Thresh;
		for(ic1 = 0; ic1 < feat1items; ++ic1, feat1array += dimensions){
			mindist1 = Thresh;
			for(ic2 = 0; ic2 < feat2items; ++ic2, feat2array += dimensions){
				dst = 0.0;
				dd = 0;
				while(dd < dimensions && dst < mindist1)
				{
					ff = feat1array[dd] - feat2array[dd];
					dst += ff * ff;
					++dd;
				}
				if (dd == dimensions && dst < mindist1)
				{
					mindist1 = dst;
					mindistindex1 = ic2;
				}
			}
			if (mindist1 < Thresh)
			{
				++TotMatch;
				*DistArray++ = ic1 + 1.0;
				*DistArray++ = mindistindex1 + 1.0;
				*DistArray++ = mindist1;
			}
			feat2array -= feat2items*dimensions;
		}
	}
	else if (Method == 2) //Method 2 = second nearest neighbor (ratio between squared distances)
	{
		out[0] = mxCreateDoubleMatrix(3, feat1items, mxREAL) ;
		DistArray = mxGetPr(out[0]);  
		for(ic1 = 0; ic1 < feat1items; ++ic1, feat1array += dimensions){
			mindist1 = mindist2 = mxGetInf();
			for(ic2 = 0; ic2 < feat2items; ++ic2, feat2array += dimensions){
				dst = 0.0;
				for(dd = 0; dd < dimensions; ++dd)
				{
					ff = feat1array[dd] - feat2array[dd];
					dst += ff * ff;
				}
				if (dst < mindist1)
				{
					mindist2 = mindist1;
					mindistindex2 = mindistindex1;
					mindist1 = dst;
					mindistindex1 = ic2;
				}
				else if (dst < mindist2)
				{
					mindist2 = dst;
					mindistindex2 = ic2;
				}
			}
			if (mindist2 / mindist1 <= Thresh)
			{
				++TotMatch;
				*DistArray++ = ic1 + 1.0;
				*DistArray++ = mindistindex1 + 1.0;
				*DistArray++ = mindist1;
				*DistArray++ = mindistindex2 + 1.0;
				*DistArray++ = mindist2;
				*DistArray++ = mindist2 / mindist1;
			}
			feat2array -= feat2items*dimensions;
		}
	}
    else if(Method == 3) //Method 3 = second nearest neighbor bidirectionnal (ratio between squared distances)
	{
		out[0] = mxCreateDoubleMatrix(3, feat1items, mxREAL) ;
		DistArray = mxGetPr(out[0]);  
		feat1arraybis = feat1array;
		for(ic1 = 0; ic1 < feat1items; ++ic1, feat1array += dimensions){
			mindist1 = mindist2 = mxGetInf();
			for(ic2 = 0; ic2 < feat2items; ++ic2, feat2array += dimensions){
				dst = 0.0;
				for(dd = 0; dd < dimensions; ++dd)
				{
					ff = feat1array[dd] - feat2array[dd];
					dst += ff * ff;
				}
				if (dst < mindist1)
				{
					mindist2 = mindist1;
					mindistindex2 = mindistindex1;
					mindist1 = dst;
					mindistindex1 = ic2;
				}
				else if (dst < mindist2)
				{
					mindist2 = dst;
					mindistindex2 = ic2;
				}
			}
			feat2array -= feat2items*dimensions;

			if (mindist2 / mindist1 <= Thresh)
			{
				mindist3 = mindist4 = mxGetInf();
				for(ic3 = 0; ic3 < feat1items; ++ic3, feat1arraybis += dimensions)
				{
					dst = 0.0;
					for(dd = 0; dd < dimensions; ++dd)
					{
						ff = feat1array[dd] - feat2array[dd];
						dst += ff * ff;
					}
					if (dst < mindist3)
					{
						mindist4 = mindist3;
						mindistindex4 = mindistindex3;
						mindist3 = dst;
						mindistindex3 = ic3;
					}
					else if (dst < mindist4)
					{
						mindist4 = dst;
						mindistindex4 = ic3;
					}
				}
				feat1arraybis -= feat1items*dimensions;

				if (mindist4 / mindist3 <= Thresh & mindistindex3 == ic1)
				{
					++TotMatch;
					*DistArray++ = ic1 + 1.0;
					*DistArray++ = mindistindex1 + 1.0;
					*DistArray++ = mindist1;
					*DistArray++ = mindistindex2 + 1.0;
					*DistArray++ = mindist2;
					*DistArray++ = mindist2 / mindist1;
				}
			}
		}
	}
    mxSetN(out[0], TotMatch);
}
  
  
  