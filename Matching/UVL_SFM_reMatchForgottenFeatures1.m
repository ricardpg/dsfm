function P3D = UVL_SFM_reMatchForgottenFeatures(P3D, Views, K,  current, Width, Height, Matching, EraseFeatures)

PointsRematched = 0;
PointsErased = 0;

for Group = 1 : numel(P3D)
    LocalIndicesToRemove = [];
    no3D = find(P3D(Group).Status == 2);
    for pp = no3D,
        LocalIndices = P3D(Group).DescIndex{pp};
        if P3D(Group).ViewIndex(LocalIndices(end)) < current - EraseFeatures.LastSeen;
            P3D(Group).Status(pp) = uint8(0);
            LocalIndicesToRemove = [LocalIndicesToRemove LocalIndices]; %don't remove it yet, we'll try to rematch them
%             P3D(Group).ViewIndex(LocalIndices) = uint16(0);
%             ErasedPoints = ErasedPoints + 1;
        end
    end
    
    ViewIndices = P3D(Group).ViewIndex(LocalIndicesToRemove);
    ValidPoints3DMask = P3D(Group).Status == 3;
    Points3DPos = P3D(Group).Pos(:, ValidPoints3DMask);
    Points3DPos(4,:) = 1;
 
    for ViewIndex = 1 : current - EraseFeatures.LastSeen - 1,
        ViewLocalIndex = LocalIndicesToRemove(ViewIndices == ViewIndex);
        if numel(ViewLocalIndex),
            Points2DPos = P3D(Group).ViewPos(1:2, ViewLocalIndex);
            Points2DDesc = double(P3D(Group).ViewDesc(:, ViewLocalIndex)) ./ 2^15;
            
            ValidPoints3D = find(ValidPoints3DMask);
            Points3DProj = (K * Views(ViewIndex).Pose(1:3,:)) * Points3DPos;
            Points3DProj(1,:) = Points3DProj(1,:) ./ Points3DProj(3,:);
            Points3DProj(2,:) = Points3DProj(2,:) ./ Points3DProj(3,:);
            Points3DProj(3,:) = 1;
            ValidPoints =   Points3DProj(1,:) > 1 & ...
                            Points3DProj(1,:) < Width & ...
                            Points3DProj(2,:) > 1 & ...
                            Points3DProj(2,:) < Height;
            ValidPoints3D = ValidPoints3D(ValidPoints);
            Points3DProj = Points3DProj(:, ValidPoints);
            Points3DDesc = double(P3D(Group).Desc(:, ValidPoints3D));
            
        %% match forgoten feature with 3D points
            Thresh = Matching.(Matching.Descriptors{P3D(Group).DescriptorType}).DistanceThreshold * 1.5;
            Error = Matching.ErrorBounds;
            Pairs = UVL_SFM_approximateNearestNeighbour(Points2DDesc, Points3DDesc, 1, Thresh, Error);
            if numel(Pairs)
            %% Reject the outliers
                SquaredDistance = sum((Points3DProj(1:2, Pairs(2,:)) - Points2DPos(1:2, Pairs(1,:))) .^ 2);
                ValidDistancesMask = SquaredDistance < 2.25; %1.5^2
                Pairs = Pairs(:, ValidDistancesMask);

            %% Update the database with the pair the pairs
                for count = 1 : size(Pairs, 2)
                    Point3DItem = ValidPoints3D(Pairs(2,count));
                    Point2DItem = ViewLocalIndex(Pairs(1,count));
                    Points3DLocalIndices = P3D(Group).DescIndex{Point3DItem};
                    Points3DViewsIndices = P3D(Group).ViewIndex(Points3DLocalIndices);
                    HasAlreadyAPoint =  Points3DViewsIndices == ViewIndex;
                    if any(HasAlreadyAPoint),
%                        oldDist = sum((P3D(Group).ViewPos(1:2, Points3DLocalIndices(HasAlreadyAPoint)) - Points3DProj(1:2, Pairs(2,count))) .^ 2)
%                        newdist = sum((Points2DPos(1:2, Pairs(1,count)) - Points3DProj(1:2, Pairs(2,count))) .^ 2)
%                        warning('MATLAB:UVL_SFM_updateDatabase', 'Duplicate Database entries!'); % not added
                    else
                        Points3DViewsIndices(end+1) = ViewIndex;
                        [A B] = sort(Points3DViewsIndices);
                        Points3DLocalIndices(end+1) = Point2DItem;
                        Points3DLocalIndices = Points3DLocalIndices(B);
                        P3D(Group).DescIndex{Point3DItem} = Points3DLocalIndices; % insert at good position
                        P3D(Group).TotalFrames(:, Point3DItem) = numel(Points3DLocalIndices);
                        P3D(Group).Desc(:, Point3DItem) = (Points3DDesc(:,Pairs(2,count)) * numel(HasAlreadyAPoint) + Points2DDesc(:,Pairs(1,count))) ./ numel(Points3DLocalIndices);
                        P3D(Group).ViewP3DIndex(Point2DItem) = Point3DItem;
                    end
                end
                ViewLocalIndex(Pairs(1,:)) = [];
            end
            P3D(Group).ViewIndex(ViewLocalIndex) = 0;
            PointsRematched = PointsRematched + size(Pairs,2);
            PointsErased = PointsErased + numel(ViewLocalIndex);

        end
    end
end

UVL_SFM_printOut(sprintf(' removed %d feat; remached %d feat.', PointsErased, PointsRematched));