for PType = 1 : numel(P3D)
    FrameData(PType).FrameSimilarity = zeros(numel(View));
    FrameData(PType).Indexing = [];
end


for CurFrame = 1:numel(View)
%             [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
%             if isempty(ICur),
%                 ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
%                 ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
%             end
%            Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
%           Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
   [Features P3DIndex] = UVL_SFM_getFeaturesFromDB(P3D, CurFrame);
    for PType = 1 : numel(P3D)
        FeatImg = Features(PType).Descriptors;
        if VisualVocabularyParams.ScaleData
            FeatImg = UVL_SFM_scaleFeatures(FeatImg, MinVect, MaxVect);
        end
        FrameIndex = UVL_SFM_indexFrameLDA(FeatImg, VisualVocabularyData(PType), VisualVocabularyData(PType).LDATransform, VisualVocabularyData(PType).Normalization, VisualVocabularyParams, P3DIndex);
  %            FrameIndex = UVL_SFM_indexFrameLDAKNN(FeatImg, VisualVocabularyData(PType).Clusters, VisualVocabularyData(PType).LDATransform, VisualVocabularyParams);

        FrameData(PType).Indexing = [FrameData(PType).Indexing FrameIndex];
        for MatchingFrame = 1 : CurFrame - 1
            %            FrameDataCurr = UVL_SFM_freqInvDocWeighting(FrameData(PType).Indexing);
            FrameDataCurr = FrameData(PType).Indexing;
            FrameIndex = FrameDataCurr(:, CurFrame) / norm(FrameDataCurr(:, CurFrame));
            MatchingFrameIndex = FrameDataCurr(:, MatchingFrame) / norm(FrameDataCurr(:, MatchingFrame));
            Similarity = FrameIndex' * MatchingFrameIndex;
            Similarity = Similarity - min(Similarity);
            Similarity = Similarity / max(Similarity);
            FrameData(PType).FrameSimilarity(MatchingFrame, CurFrame) = FrameIndex' * MatchingFrameIndex;
        end
    end
    if ~isempty(MatchingFrame)
        PlotData = [];
        for PType = 1 : numel(P3D)
            PlotData = [PlotData FrameData(PType).FrameSimilarity(1:MatchingFrame, CurFrame)];
        end
        %            PlotData = [PlotData Ratios(1 : MatchingFrame, CurFrame)];
        figure(FigSim), hold off, plot(PlotData), legend(FeatureType)
    end
end
