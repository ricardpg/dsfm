 clear cluster_mask
global clusterCACHE climg height width
visualVocabularyConfig
%climg = figure;
%figh = figure;
%%%%%%%%%%%%%%%%%%%%%%
%% initialization
Itmp = UVL_SFM_getInputStream(InputDataInfo, 1); %%% get frame size
[height, width, depth] = size(Itmp);
index_frames_all = [];
%% PCA initialization
if ~exist('Eigen', 'var')
    for PType = 1 : numel(P3D)
        if VisualVocabularyParams.UseEigen
            ValidPoints = P3D(PType).Status == 3;
            LocalIndices = [P3D(PType).DescIndex{ValidPoints}];
            Descriptors = double(P3D(PType).ViewDesc(:, LocalIndices)) / 2^15;
            [Eigen(PType).Vectors Eigen(PType).Variance] = eigenDecomposition(Descriptors);
        else
            DescriptorDimensions = size(P3D(PType).ViewDesc, 1);
            Eigen(PType).Vectors = eye(DescriptorDimensions);
            Eigen(PType).Variance = ones(DescriptorDimensions, 1);
        end
    end
    clear Descriptors;
end

%% check if feature radius is already computed if not, compute
if ~isfield(P3D(PType), 'FeatureRadius')
    P3D(1).FeatureRadius = [];
end
for PType = 1 : numel(P3D)
    if isempty(P3D(PType).FeatureRadius)
        P3D(PType).FeatureRadius = zeros(1, numel(P3D(PType).Status));
        P3D(PType) = UVL_SFM_computeFeatureRadius(P3D(PType), Eigen(PType), VisualVocabularyParams);
    end
end


%% loop
for PType = 1 : numel(P3D)
    VisualVocabularyData(PType).Clusters = [];
    VisualVocabularyData(PType).FrameIndices = [];
end
FrameRangeStart = 1;
for FrameRangeLimit = VisualVocabularyParams.InitialFrames : VisualVocabularyParams.FrameStep : numel(View)
    FrameRange = [FrameRangeStart FrameRangeLimit];
    fprintf('\nFrames %d-%d\n', FrameRange(1), FrameRange(2));

clusters_mser = [];
frame_indices_mser = [];
frame_indices_full = [];
clusterCACHE = int8([]);
range1 = 1;
for i = initial_views:step_view:numel(View)
    frame_range = [range1 i];
    fprintf('\nFrames %d-%d\n', frame_range(1), frame_range(2));
    free_ = true(1, numel(P3D));
    mask = false(1, numel(P3D));
    for pp = 1:numel(P3D)
        if numel(intersect([P3D(pp).View(:).Index], frame_range(1):frame_range(2)))
            mask(pp) = true;
        end
    end
    P3Dcurrent = P3D(mask);
    free_(mask) = false;
    [features_mser_full frame_indices_mser_new radius_feat_mser] = UVL_SFM_visualVocabularyGetFeatures(P3Dcurrent, View, frame_range, 5);

    frame_indices_mser = [frame_indices_mser zeros(size(frame_indices_mser,1), size(frame_indices_mser_new,2));
        zeros(size(frame_indices_mser_new,1), size(frame_indices_mser,2)) frame_indices_mser_new];
    clusters_b4 = size(frame_indices_mser, 1);
    mapping_matrix = eye(clusters_b4);
    
    features_mser_full = eigvect_mser' * features_mser_full;
    features_mser_db = features_mser_full(end-dimensions+1:end, :);
    variance_mser_dim = variance_mser((64-dimensions+1):end);
    [clusters_mser frame_indices_mser mapping_matrix] = UVL_SFM_visualVocabularyIterativeClustering(features_mser_db, variance_mser_dim, frame_indices_mser, clusters_mser, radius_feat_mser, ratios, mapping_matrix, features_mser_full);
    P3D = P3D(free_);
    range1 = i + 1;
    if numel(index_frames_all)
        index_frames_all(clusters_b4, size(index_frames_all,2)) = 0;
        index_frames_all = (index_frames_all' * mapping_matrix)';
    end
    for frame_no = frame_range(1):frame_range(2)
        fprintf('Frame %d...', frame_no)
        cache_file = sprintf('Features\\features_%05d.mat',frame_no);
        if(exist(cache_file,'file'))
            fprintf('Reading cache...')
            load(cache_file)
        else
            current_frame = data_read(ffid, frame_no);
            features_mser = extractFeatures(current_frame, 'mser');
            fprintf('Caching...')
            save(cache_file, 'features_mser')
        end
        fprintf('%d features ', size(features_mser,2));
        features_mser = features_mser(15:end,:);
        features_mser_orig = features_mser;
        features_mser = eigvect_mser' * features_mser;
%        features_mser = features_mser((128-dimensions+1):128, :);
        frame_indexing = index_one_frame5(features_mser, clusters_mser, variance_mser);%, cluster_matching_th);
        fprintf(' (%d in)\n', sum(frame_indexing))
        index_frames_all = [index_frames_all frame_indexing]; % merge word counts for all frames
        true_clusters = ones(size(index_frames_all));
        weighted = freq_inv_doc_weighting1(index_frames_all, true_clusters);  % compute inverse freq weighting
        similarity = zeros(1,size(index_frames_all,2)-1);
        for i = 1:size(weighted,2)-1
            similarity(i) = weighted(:,i)' * weighted(:,end) / (norm(weighted(:,i)) * norm(weighted(:,end)));
        end
        if numel(similarity)
            sim_all(frame_no).sim = similarity;
            figure(figh), hold off, title(sprintf('Frame %d',frame_no)), plot(similarity, 'b-'),  %hold on, plot(similarity_synth, 'r-'), plot(similarity_hybrid, 'g-')%hold on, plot(similarity_kmeans, 'r-'), hold on,
        end
    end
end
for i = 1:numel(sim_all)
    map(1:numel(sim_all(i).sim),i) = sim_all(i).sim/(max(sim_all(i).sim)+eps);
end

