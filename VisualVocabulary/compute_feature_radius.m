function P3D = UVL_SFM_computeFeatureRadius(P3D, Params, Eigen)
ValidIndices = find(P3D.Status == 3);
for Point = ValidIndices
    LocalIndices = P3D.DescIndex{Point};
    Descriptors = double(P3D.ViewDesc(:, LocalIndices)) / 2^15;
    Centroid = double(P3D.Desc(:, Point));
    Distances = std_euclidian(Centroid, Descriptors, Eigen.Variance);
    switch lower(Params.RadiusType)
        case 'mean'
            P3D.FeatureRadius(Point) = mean(Distances) * Param.RadiusFactor;
        case 'max'
            P3D.FeatureRadius(Point) = max(Distances) * Param.RadiusFactor;
        otherwise
            error('Unknown radius method')
    end
end
