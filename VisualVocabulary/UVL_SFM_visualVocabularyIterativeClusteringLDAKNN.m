function VisualVocabularyData = UVL_SFM_visualVocabularyIterativeClusteringLDAKNN(VisualVocabularyData, Params)
Clusters = VisualVocabularyData.Clusters;
FrameIndices = VisualVocabularyData.FrameIndices';
IndexMapping = VisualVocabularyData.IndexMapping;
TotalDimensions = size(Clusters(1).Centroid, 1);
TotalElements = sum([Clusters(:).Elements]);
GlobalCentroid = zeros(TotalDimensions, 1);
%% compute global mean
for Cc = 1 : numel(Clusters)
    GlobalCentroid = GlobalCentroid + Clusters(Cc).Centroid * Clusters(Cc).Elements;
end
GlobalCentroid = GlobalCentroid / TotalElements;
Pass = 1;
ScoreTh = 0;
while true
    %% compute Sw and Sb
    TotalClusters = numel(Clusters);
    Sb = zeros(TotalDimensions);
    Sw = zeros(TotalDimensions);
    for Cc = 1 : numel(Clusters)
        Sw = Sw + Clusters(Cc).Cov;
        ClusterCentroidVariance = Clusters(Cc).Centroid - GlobalCentroid;
        Sb = Sb + Clusters(Cc).Elements * ClusterCentroidVariance * ClusterCentroidVariance';
    end
    Sb = Sb / TotalElements;
    Sw = Sw / TotalElements;
    %St = Sb + Sw;
    InitialEnergy = trace(Sb) / trace(Sw);
%    [EigenVect, EigenVal] = eig(Sb, St, 'qz');
    SwInv = inv(Sw);
    SbR = Sb ^.5;
    Crit = SbR * SwInv * SbR;
    [EigenVect, EigenVal] = eig(Crit);
    [dmy, EigIdx] = sort(diag(EigenVal), 'descend');
    EigIdx = EigIdx(1 : Params.LDADims);
    G = EigenVect(:, EigIdx)';
    
    if ~numel(G)
        G = eye(64);
    end 
    
    ClusterCentroids = [Clusters(:).Centroid];
    ClusterCentroids = G * ClusterCentroids;
    Norms = sum(ClusterCentroids.^2);
    ClusterCentroids = ClusterCentroids ./ repmat(Norms, size(ClusterCentroids, 1), 1);
    for Cc = 1 : numel(Clusters)
        Clusters(Cc).CentroidLDA = ClusterCentroids(:, Cc);
        Clusters(Cc).InvCovLDA = pinv(G * Clusters(Cc).Cov * G');
    end
    KNeighbors = UVL_SFM_AKNN(ClusterCentroids, ClusterCentroids, Params.KNN, Params.ClusteringErrorBounds);
    Pairs = zeros(3, numel(Clusters));
    for Cc = 1 : numel(Clusters)
        for Nn = 1 : Params.KNN
            Cn = KNeighbors(Nn*2, Cc);
            k1 = Clusters(Cc).CentroidLDA' * Clusters(Cn).InvCovLDA * Clusters(Cc).CentroidLDA + Clusters(Cn).CentroidLDA' * Clusters(Cn).InvCovLDA * Clusters(Cn).CentroidLDA - 2 * Clusters(Cc).CentroidLDA' * Clusters(Cn).InvCovLDA * Clusters(Cn).CentroidLDA;
            k2 = Clusters(Cc).CentroidLDA' * Clusters(Cc).InvCovLDA * Clusters(Cc).CentroidLDA + Clusters(Cn).CentroidLDA' * Clusters(Cc).InvCovLDA * Clusters(Cn).CentroidLDA - 2 * Clusters(Cc).CentroidLDA' * Clusters(Cc).InvCovLDA * Clusters(Cn).CentroidLDA;
            alpha = (sqrt(k1 * k2) - k2) / (k1 - k2);
            rij = k1 * alpha^2;
            KNeighbors(Nn * 2 + 1, Cc) = rij;
        end
        [MinVal, MinIdx] = min(KNeighbors(3:2:end, Cc));
        Pairs(1, Cc) = Cc;
        Pairs(2, Cc) = KNeighbors(MinIdx*2, Cc);
        Pairs(3, Cc) = MinVal;
    end
    %% compute cluster distances
%    Pairs = UVL_SFM_ANNDistances(ClusterCentroids, 10000, Params.ClusteringErrorBounds);
    [Dists, PairsIdx] = sort(Pairs(3, :));
    Pairs = Pairs(:, PairsIdx);
    %    Cursors = ones(1, TotalClusters, 'uint8');
    Available = true(1, TotalClusters);
    %    Cursor = 1;
    Counter = 0;
    Cutout = true(1, TotalClusters);
    fprintf('%d. (%d)', Pass, TotalClusters)
    Cursor = 1;
    for PairCounter = 1 : size(Pairs, 2)
        Counter = Counter + 1;
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        Cursor = Cursor + 1;
        C1 = Cc1;
        C2 = Cc2;
        % calculate new cluster
        NewElements = (Clusters(C1).Elements + Clusters(C2).Elements);
        NewCentroid = (Clusters(C1).Centroid * Clusters(C1).Elements + Clusters(C2).Centroid * Clusters(C2).Elements) / NewElements;
        CovNew = Clusters(C1).Cov * (Clusters(C1).Elements - 1) / (NewElements - 1) + ...
            Clusters(C2).Cov * (Clusters(C2).Elements - 1) / (NewElements - 1) + ...
            (Clusters(C1).Elements * Clusters(C2).Elements) / (NewElements * (NewElements - 1)) * ...
            ((Clusters(C1).Centroid - Clusters(C2).Centroid) * (Clusters(C1).Centroid - Clusters(C2).Centroid)');
        % calculate new Sw, Sb
        SwHat = Sw * TotalElements;
        SwHat = SwHat - Clusters(C1).Cov - Clusters(C2).Cov + CovNew;
        SwHat = SwHat / TotalElements;

        ClusterCentroidVarianceC1 = Clusters(C1).Centroid - GlobalCentroid;
        ClusterCentroidVarianceC2 = Clusters(C2).Centroid - GlobalCentroid;
        ClusterCentroidVarianceCNew = NewCentroid - GlobalCentroid;

        ClusterCentroidCovC1 = ClusterCentroidVarianceC1 * ClusterCentroidVarianceC1';
        ClusterCentroidCovC2 = ClusterCentroidVarianceC2 * ClusterCentroidVarianceC2';
        ClusterCentroidCovCNew = ClusterCentroidVarianceCNew * ClusterCentroidVarianceCNew';

        SbHat = Sb * TotalElements;
        SbHat = SbHat - Clusters(C1).Elements * ClusterCentroidCovC1 - Clusters(C2).Elements * ClusterCentroidCovC2;
        SbHat = SbHat + NewElements * ClusterCentroidCovCNew;
        SbHat = SbHat / TotalElements;
        ProjectedEnergy = trace(SbHat) / trace(SwHat);
        EnergyGain = ProjectedEnergy / InitialEnergy;
        Pairs(3, PairCounter) = EnergyGain;
    end
    %    Elements = [Clusters(Pairs(1,:)).Elements] + [Clusters(Pairs(2,:)).Elements];
    %    [SortedElements, PairIdx] = sort(Elements);
    %    Pairs = Pairs(:, PairIdx);
    %    MeanElements = mean(Elements);
    %    ScoreElements = MeanElements ./ SortedElements;
    %    Pairs(3, :) = Pairs(3, :) .* ScoreElements;
    for PairCounter = 1 : size(Pairs, 2)
        if sum(Cutout) <= Params.TargetItems
            break
        end
        if Pairs(3, PairCounter) < ScoreTh
            continue
        end
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        C1 = min([Cc1 Cc2]);
        C2 = max([Cc1 Cc2]);
        if ~Available(C1) || ~Available(C2) %|| (Clusters(C1).Elements + Clusters(C2).Elements) > 300
            continue
        end
        List = [C1 C2];
        % calculate new cluster
        NewElements = (Clusters(C1).Elements + Clusters(C2).Elements);
        NewCentroid = (Clusters(C1).Centroid * Clusters(C1).Elements + Clusters(C2).Centroid * Clusters(C2).Elements) / NewElements;
        CovNew = Clusters(C1).Cov * (Clusters(C1).Elements - 1) / (NewElements - 1) + ...
            Clusters(C2).Cov * (Clusters(C2).Elements - 1) / (NewElements - 1) + ...
            (Clusters(C1).Elements * Clusters(C2).Elements) / (NewElements * (NewElements - 1)) * ...
            ((Clusters(C1).Centroid - Clusters(C2).Centroid) * (Clusters(C1).Centroid - Clusters(C2).Centroid)');
        FrameIndices(:, C1) = FrameIndices(:, C1) + FrameIndices(:, C2);
        Cutout(C2) = false;
        Clusters(C1).Centroid = NewCentroid;
        Clusters(C1).Cov = CovNew;
        Clusters(C1).FeatureIndex = [Clusters(List).FeatureIndex];
        Clusters(C1).Elements = sum([Clusters(List).Elements]);
        IndexMapping(:, C1) = IndexMapping(:, C1) + IndexMapping(:, C2);
        Available(List) = false;
    end

    Clusters = Clusters(Cutout);
    IndexMapping = IndexMapping(:, Cutout);
    FrameIndices = FrameIndices(:, Cutout);
    %    fprintf('\n');
    if (numel(Cutout) - sum(Cutout) < 10)
        %        ScoreTh = ScoreTh - .1;
        %        fprintf('\nTo: %.01f ', ScoreTh)
        break
    end
    if numel(Clusters) <= Params.TargetItems
        break
    end
    Pass = Pass + 1;
end
Sb = zeros(TotalDimensions);
Sw = zeros(TotalDimensions);
for Cc = 1 : numel(Clusters)
    Sw = Sw + Clusters(Cc).Cov;
    ClusterCentroidVariance = Clusters(Cc).Centroid - GlobalCentroid;
    Sb = Sb + Clusters(Cc).Elements * ClusterCentroidVariance * ClusterCentroidVariance';
end
Sb = Sb / TotalElements;
Sw = Sw / TotalElements;
%St = Sb + Sw;
SwInv = inv(Sw);
SbR = Sb ^.5;
Crit = SbR * SwInv * SbR;
%Crit = SbR * pinv(St) * SbR;
[EigenVect, EigenVal] = eig(Crit);
%[EigenVect, EigenVal] = eig(Sb, St, 'qz');
[dmy, EigIdx] = sort(diag(EigenVal), 'descend');
EigIdx = EigIdx(1 : Params.LDADims);
%EigIdx = EigIdx(1 : Params.LDADims);
G = EigenVect(:, EigIdx)';

if ~numel(G)
    G = eye(64);
end

ClusterCentroids = [Clusters(:).Centroid];
ClusterCentroids = G * ClusterCentroids;
Norms = sum(ClusterCentroids.^2);
ClusterCentroids = ClusterCentroids ./ repmat(Norms, size(ClusterCentroids, 1), 1);
VisualVocabularyData.ClustersLDA = ClusterCentroids;
VisualVocabularyData.Clusters = Clusters;
VisualVocabularyData.FrameIndices = FrameIndices';
VisualVocabularyData.IndexMapping = IndexMapping;
VisualVocabularyData.LDATransform = G;

for Cc = 1 : numel(VisualVocabularyData.Clusters)
    VisualVocabularyData.Clusters(Cc).CentroidLDA = ClusterCentroids(:, Cc);
    VisualVocabularyData.Clusters(Cc).InvCovLDA = pinv(G * Clusters(Cc).Cov * G');
end
