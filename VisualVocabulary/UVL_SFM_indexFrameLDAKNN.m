function FrameIndex = UVL_SFM_indexFrameLDAKNN(Features, Clusters, LDATransform, Params)
Thresh = 100000;
%% matching
if ~numel(Clusters)
    FrameIndex = [];
    return
end
if ~numel(Features)
    FrameIndex = [];
    return
end
FeaturesLDA = LDATransform * Features;
Norms = sum(FeaturesLDA.^2);
FeaturesLDA = FeaturesLDA ./ repmat(Norms, size(FeaturesLDA, 1), 1);



%Pairs = UVL_SFM_approximateNearestNeighbour(double(FeaturesLDA), double(Clusters), 1, Thresh, Params.IndexingErrorBounds);
KNeighbors = UVL_SFM_AKNN(double(FeaturesLDA), double([Clusters(:).CentroidLDA]), Params.KNN, Params.IndexingErrorBounds);
Pairs = zeros(3, size(FeaturesLDA, 2));
for Ff = 1 : size(FeaturesLDA, 2)
    for Nn = 1 : Params.KNN
        Cc = KNeighbors(Nn*2, Ff);
        Diff = FeaturesLDA(:, Ff) - Clusters(Cc).CentroidLDA;
        MahDist = sqrt(Diff' * Clusters(Cc).InvCovLDA * Diff);  
        KNeighbors(Nn * 2 + 1, Ff) = MahDist;
    end
    [MinVal, MinIdx] = min(KNeighbors(3:2:end, Ff));
    Pairs(1, Ff) = Ff;
    Pairs(2, Ff) = KNeighbors(MinIdx*2, Ff);
    Pairs(3, Ff) = MinVal;
end

%Pairs = UVL_SFM_nearestNeighbour(double(Features), double(Clusters), Thresh);
%OutsideCluster = Pairs(3, :) > Limit;
fprintf('%f\n', sum(Pairs(3,:)));
OutsideCluster = Pairs(3, :) > inf;
Pairs(2, OutsideCluster) = 0;
FrameIndex = histc(Pairs(2,:), 0:size(Clusters,2))';
FrameIndex = FrameIndex(2:end);


