visualVocabularyConfig
labelFeatureTypes
%climg = figure;
FigSim = figure;
%%%%%%%%%%%%%%%%%%%%%%
if ~exist('Ratios', 'var')
    load ratios
end
%% initialization
% Itmp = UVL_SFM_getInputStream(InputDataInfo, 1); %%% get frame size
% [height, width, depth] = size(Itmp);
% index_frames_all = [];
for PType = 1 : numel(P3D) %#ok<NODEF>
    FrameData(PType).Indexing = [];
end
%% PCA initialization
if ~exist('Eigen', 'var')
    for PType = 1 : numel(P3D) %#ok<NODEF>
        if VisualVocabularyParams.UseEigen
            ValidPoints = P3D(PType).Status == 3;
            LocalIndices = [P3D(PType).DescIndex{ValidPoints}];
            Descriptors = double(P3D(PType).ViewDesc(:, LocalIndices)) / 2^15;
            [Eigen(PType).Vectors Eigen(PType).Variance] = eigenDecomposition(Descriptors);
            Eigen(PType).VarianceMatr = sqrt(diag(1 ./ Eigen(PType).Variance));
        else
            DescriptorDimensions = size(P3D(PType).ViewDesc, 1);
            Eigen(PType).Vectors = eye(DescriptorDimensions);
            Eigen(PType).Variance = ones(DescriptorDimensions, 1);
            Eigen(PType).VarianceMatr = sqrt(diag(1 ./ Eigen(PType).Variance));
        end
    end
    clear Descriptors;
end

%% check if feature radius is already computed if not, compute
if ~isfield(P3D(PType), 'FeatureRadius')
    P3D(1).FeatureRadius = [];
end
for PType = 1 : numel(P3D)
    if isempty(P3D(PType).FeatureRadius)
        P3D(PType).FeatureRadius = zeros(1, numel(P3D(PType).Status));
        P3D(PType) = UVL_SFM_computeFeatureRadius(P3D(PType), Eigen(PType), VisualVocabularyParams);
    end
end


%% loop
for PType = 1 : numel(P3D)
    VisualVocabularyData(PType).Clusters = [];
    VisualVocabularyData(PType).FrameIndices = [];
    FrameData(PType).FrameSimilarity = zeros(1000, 1000);
end

FrameRangeStart = 1;
for FrameRangeLimit = VisualVocabularyParams.InitialFrames : VisualVocabularyParams.FrameStep : numel(View)
    FrameRange = FrameRangeStart:FrameRangeLimit;
    fprintf('\nFrames %d-%d\n', FrameRange(1), FrameRange(end));
    for PType = 1 : numel(P3D)
        fprintf('\n----------------------------------\n');
        ClustersBefore = numel(VisualVocabularyData(PType).Clusters);
        VisualVocabularyData(PType) = UVL_SFM_visualVocabularyGetFeatures(VisualVocabularyData(PType), P3D(PType), Eigen(PType), FrameRange);
        ClustersAfter = numel(VisualVocabularyData(PType).Clusters);
        NewClusters = ClustersAfter - ClustersBefore;
        VisualVocabularyData(PType).IndexMapping = [eye(ClustersBefore) zeros(ClustersBefore, NewClusters)];
        VisualVocabularyData(PType) = UVL_SFM_visualVocabularyIterativeClusteringRanking(VisualVocabularyData(PType), Eigen(PType), Ratios, VisualVocabularyParams);
        if ~isempty(FrameData(PType).Indexing)
            FrameData(PType).Indexing = (FrameData(PType).Indexing' * VisualVocabularyData(PType).IndexMapping)';
        end
    end


    for CurFrame = []%FrameRange
        [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
        if isempty(ICur),
            ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
            ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
        end
        Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection);
        Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description);
        %    test
        for PType = 1 : numel(P3D)
            FeatImg = Features(PType).Descriptors;
            FeatImg = Eigen(PType).Vectors' * FeatImg; %% PCA descomposition
            FeatImg = (FeatImg' * Eigen(PType).VarianceMatr)';  %% normalize by variance
            ClusterCentroids = [VisualVocabularyData(PType).Clusters(:).Centroid];
            ClusterCentroids = (ClusterCentroids' * Eigen(PType).VarianceMatr)';
            ClusterRadius = [VisualVocabularyData(PType).Clusters(:).Radius];
            FrameIndex = UVL_SFM_indexFrame(FeatImg, ClusterCentroids, ClusterRadius, VisualVocabularyParams);
            FrameData(PType).Indexing = [FrameData(PType).Indexing FrameIndex];
            for MatchingFrame = 1 : CurFrame - 1
                %            FrameDataCurr = UVL_SFM_freqInvDocWeighting(FrameData(PType).Indexing);
                FrameDataCurr = FrameData(PType).Indexing;
                FrameIndex = FrameDataCurr(:, CurFrame) / norm(FrameDataCurr(:, CurFrame));
                MatchingFrameIndex = FrameDataCurr(:, MatchingFrame) / norm(FrameDataCurr(:, MatchingFrame));
                FrameData(PType).FrameSimilarity(MatchingFrame, CurFrame) = FrameIndex' * MatchingFrameIndex;
            end
        end
        if ~isempty(MatchingFrame)
            PlotData = [];
            for PType = 1 : numel(P3D)
                PlotData = [PlotData FrameData(PType).FrameSimilarity(1:MatchingFrame, CurFrame)];
            end
            PlotData = [PlotData Ratios(1 : MatchingFrame, CurFrame)];
            figure(FigSim), hold off, plot(PlotData), legend(FeatureType)
        end
    end
    FrameRangeStart = FrameRangeLimit + 1;
end

