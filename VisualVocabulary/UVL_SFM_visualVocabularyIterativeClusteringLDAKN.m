function VisualVocabularyData = UVL_SFM_visualVocabularyIterativeClusteringLDAKN(VisualVocabularyData, P3D, Params)
Clusters = VisualVocabularyData.Clusters;
FrameIndices = VisualVocabularyData.FrameIndices';
%IndexMapping = VisualVocabularyData.IndexMapping;
TotalDimensions = size(Clusters(1).Centroid, 1);
TotalElements = sum([Clusters(:).Elements]);
GlobalCentroid = zeros(TotalDimensions, 1);
%% compute global mean
for Cc = 1 : numel(Clusters)
    GlobalCentroid = GlobalCentroid + Clusters(Cc).Centroid * Clusters(Cc).Elements;
end
GlobalCentroid = GlobalCentroid / TotalElements;
Pass = 1;
ScoreTh = 0;
while true
    %% compute Sw and Sb
    TotalClusters = numel(Clusters);
    Sb = zeros(TotalDimensions);
    Sw = zeros(TotalDimensions);
    for Cc = 1 : numel(Clusters)
        Sw = Sw + Clusters(Cc).Elements / TotalElements * Clusters(Cc).Cov;
        ClusterCentroidVariance = Clusters(Cc).Centroid - GlobalCentroid;
        Sb = Sb + Clusters(Cc).Elements / TotalElements * ClusterCentroidVariance * ClusterCentroidVariance';
    end
    SwInv = inv(Sw);
    SbR = Sb ^.5;
    Crit = SbR * SwInv * SbR;
    [EigenVect, EigenVal] = eig(Crit);
    [dmy, EigIdx] = sort(diag(EigenVal), 'descend');
    EigIdx = EigIdx(1 : Params.LDADims);
    G = EigenVect(:, EigIdx)';
    
    if ~numel(G)
        G = eye(64);
    end 
    
    ClusterCentroids = [Clusters(:).Centroid];
    ClusterCentroids = G * ClusterCentroids;
    
    %% compute cluster distances
    KNeighbors = UVL_SFM_AKNN(ClusterCentroids, ClusterCentroids, Params.KNN, Params.ClusteringErrorBounds);
    KNeighbors = KNeighbors([1 4:end], :);
    Pairs = zeros(3, size(KNeighbors, 2));
    for PairCount = 1 : size(KNeighbors, 2)
        Cluster1 = KNeighbors(1, PairCount);
        Cluster2 = KNeighbors(2, PairCount);
        Neighbourhood = [KNeighbors([1 2:2:end], Cluster1); KNeighbors([1 2:2:end], Cluster2)];
        Neighbourhood = unique(Neighbourhood)';
        NeighElements = sum([Clusters(Neighbourhood).Elements]);
        NeighCentroid = zeros(TotalDimensions, 1);
        for Cc = Neighbourhood
            NeighCentroid = NeighCentroid + Clusters(Cc).Centroid * Clusters(Cc).Elements;
        end
        NeighCentroid = NeighCentroid / NeighElements;
        SbNI = zeros(TotalDimensions);
        SwNI = zeros(TotalDimensions);
        for Cc = 1 : numel(Neighbourhood)
            SwNI = SwNI + Clusters(Cc).Elements / NeighElements * Clusters(Cc).Cov;
            ClusterCentroidVariance = Clusters(Cc).Centroid - NeighCentroid;
            SbNI = SbNI + Clusters(Cc).Elements / NeighElements * ClusterCentroidVariance * ClusterCentroidVariance';
        end
        InitialEnergy = trace(SbNI) / trace(SwNI);
        LocalIndices = [Clusters(Cluster1).Indices Clusters(Cluster2).Indices];
        Features = P3D.ViewDesc(:, LocalIndices);
        Features = double(Features) * 2 ^ -15;
        [CovNew NewCentroid] = covData(Features);
        SwNE = SwNI - Clusters(Cluster1).Elements / NeighElements * Clusters(Cluster1).Cov - Clusters(Cluster2).Elements / NeighElements * Clusters(Cluster2).Cov +...
            (Clusters(Cluster1).Elements + Clusters(Cluster2).Elements) / NeighElements * CovNew;
        Cluster1Variance = Clusters(Cluster1).Centroid - NeighCentroid;
        Cluster2Variance = Clusters(Cluster2).Centroid - NeighCentroid;
        ClusterNewVariance = NewCentroid - NeighCentroid;
        SbNE = SbNI - Clusters(Cluster1).Elements / NeighElements * Cluster1Variance * Cluster1Variance' - Clusters(Cluster2).Elements / NeighElements * Cluster2Variance * Cluster2Variance'+...
            (Clusters(Cluster1).Elements + Clusters(Cluster2).Elements) / NeighElements * ClusterNewVariance * ClusterNewVariance';
        EstimatedEnergy = trace(SbNE) / trace(SwNE);
        Pairs(1:2, PairCount) = [Cluster1 Cluster2]';
        Pairs(3, PairCount) = EstimatedEnergy / InitialEnergy;
    end
    
    
    
    
%    Candidates = Scores >= 1;
%    Ratios = KNeighbors(5,:) ./ KNeighbors(3,:);
%    Candidates = Ratios > 1.2;
    
%    Pairs = KNeighbors(1:3, Candidates);
    [SortedDist, PairIdx] = sort(Pairs(3,:), 'descend');
%    Elements = [Clusters(Pairs(1,:)).Elements] + [Clusters(Pairs(2,:)).Elements];
%    [SortedElements, PairIdx] = sort(Elements);
    Pairs = Pairs(:, PairIdx);
    Pairs = Pairs(:, 1:1500);

    Cutout = true(1, TotalClusters);
    Available = true(1, TotalClusters);
    fprintf('%d. (%d)', Pass, TotalClusters)
    Cursor = 1;    
    
    for PairCounter = 1 : size(Pairs, 2)
        if sum(Cutout) <= Params.TargetItems
            break
        end
        if Pairs(3, PairCounter) < ScoreTh
            continue
        end
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        C1 = min([Cc1 Cc2]);
        C2 = max([Cc1 Cc2]);
        if ~Available(C1) || ~Available(C2) %|| (Clusters(C1).Elements + Clusters(C2).Elements) > 300
            continue
        end
        List = [C1 C2];
        LocalIndices = [Clusters(C1).Indices Clusters(C2).Indices];
        Features = P3D(1).ViewDesc(:, LocalIndices);
        Features = double(Features) * 2 ^ -15;
%        Features = UVL_SFM_scaleFeatures(Features, MinVect, MaxVect);
        [CovNew NewCentroid] = covData(Features);
        FrameIndices(:, C1) = FrameIndices(:, C1) + FrameIndices(:, C2);
        Cutout(C2) = false;
        Clusters(C1).Centroid = NewCentroid;
        Clusters(C1).Cov = CovNew;
        Clusters(C1).FeatureIndex = [Clusters(List).FeatureIndex];
        Clusters(C1).Elements = sum([Clusters(List).Elements]);
        Clusters(C1).Indices = [Clusters(List).Indices];        
%        IndexMapping(:, C1) = IndexMapping(:, C1) + IndexMapping(:, C2);
        Available(List) = false;
    end

    Clusters = Clusters(Cutout);
%    IndexMapping = IndexMapping(:, Cutout);
    FrameIndices = FrameIndices(:, Cutout);
    %    fprintf('\n');
    if (numel(Cutout) - sum(Cutout) < 5)
        %        ScoreTh = ScoreTh - .1;
        %        fprintf('\nTo: %.01f ', ScoreTh)
        break
    end
    if numel(Clusters) <= Params.TargetItems
        break
    end
    Pass = Pass + 1;
    break
end
Sb = zeros(TotalDimensions);
Sw = zeros(TotalDimensions);
for Cc = 1 : numel(Clusters)
    Sw = Sw + Clusters(Cc).Cov;
    ClusterCentroidVariance = Clusters(Cc).Centroid - GlobalCentroid;
    Sb = Sb + Clusters(Cc).Elements * ClusterCentroidVariance * ClusterCentroidVariance';
end
Sb = Sb / TotalElements;
Sw = Sw / TotalElements;
St = Sb + Sw;
SwInv = inv(Sw);
SbR = Sb ^.5;
Crit = SbR * SwInv * SbR;
%Crit = SbR * pinv(St) * SbR;
[EigenVect, EigenVal] = eig(Crit);
%[EigenVect, EigenVal] = eig(Sb, St, 'qz');
[dmy, EigIdx] = sort(diag(EigenVal), 'descend');
EigIdx = EigIdx(1 : Params.LDADims);
%EigIdx = EigIdx(1 : Params.LDADims);
G = EigenVect(:, EigIdx)';

if ~numel(G)
    G = eye(64);
end

ClusterCentroids = [Clusters(:).Centroid];
ClusterCentroids = G * ClusterCentroids;
StDev = std(ClusterCentroids, [], 2);
StDev = ones(size(StDev));
ClusterCentroids = ClusterCentroids ./ repmat(StDev, 1, size(ClusterCentroids,2));
VisualVocabularyData.ClustersLDA = ClusterCentroids;
VisualVocabularyData.Normalization = StDev;
%Norms = sum(ClusterCentroids.^2);
%VisualVocabularyData.ClustersLDA = ClusterCentroids ./ repmat(Norms, size(ClusterCentroids, 1), 1);
VisualVocabularyData.Clusters = Clusters;
VisualVocabularyData.FrameIndices = FrameIndices';
%VisualVocabularyData.IndexMapping = IndexMapping;
VisualVocabularyData.LDATransform = G;

