function [Cov Mean] = covData(Data, MinCov)
Mean = mean(Data, 2);
Measurements = size(Data, 2);
Data = Data - repmat(Mean, 1, Measurements);
Cov = Data * Data';
Cov = Cov / Measurements;
%Cov = Cov * 10;
if exist('MinCov', 'var')
    if norm(Cov) < norm(MinCov)
        Cov = MinCov;
    end
end