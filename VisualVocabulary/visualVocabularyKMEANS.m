visualVocabularyConfig
labelFeatureTypes
%climg = figure;
FigSim = figure;
%%%%%%%%%%%%%%%%%%%%%%
if ~exist('Ratios', 'var')
    load ratios
end

ClNo = 3500;

%% PCA initialization
if ~exist('Eigen', 'var')
    for PType = 1 : numel(P3D) %#ok<NODEF>
        if VisualVocabularyParams.UseEigen
            ValidPoints = P3D(PType).Status == 3;
            LocalIndices = [P3D(PType).DescIndex{ValidPoints}];
            Descriptors = double(P3D(PType).ViewDesc(:, LocalIndices)) / 2^15;
            [Eigen(PType).Vectors Eigen(PType).Variance] = eigenDecomposition(Descriptors);
            Eigen(PType).VarianceMatr = sqrt(diag(1 ./ Eigen(PType).Variance));
        else
            DescriptorDimensions = size(P3D(PType).ViewDesc, 1);
            Eigen(PType).Vectors = eye(DescriptorDimensions);
            Eigen(PType).Variance = ones(DescriptorDimensions, 1);
            Eigen(PType).VarianceMatr = sqrt(diag(1 ./ Eigen(PType).Variance));
        end
    end
    clear Descriptors;
end
%% check if feature radius is already computed if not, compute
if ~isfield(P3D(PType), 'FeatureRadius')
    P3D(1).FeatureRadius = [];
end
for PType = 1 : numel(P3D)
    if isempty(P3D(PType).FeatureRadius)
        P3D(PType).FeatureRadius = zeros(1, numel(P3D(PType).Status));
        P3D(PType) = UVL_SFM_computeFeatureRadius(P3D(PType), Eigen(PType), VisualVocabularyParams);
    end
end

for PType = 1 : numel(P3D)
    VisualVocabularyData(PType).Clusters = [];
    VisualVocabularyData(PType).FrameIndices = [];
    FrameData(PType).FrameSimilarity = zeros(1000, 1000);
end

for PType = 1 : numel(P3D)
    fprintf('\n----------------------------------\n');
    VisualVocabularyData(PType) = UVL_SFM_visualVocabularyGetFeatures(VisualVocabularyData(PType), P3D(PType), Eigen(PType), 1:numel(View));    
    CX = vgg_kmeans([VisualVocabularyData(PType).Clusters(:).Centroid], ClNo);
    VisualVocabularyData(PType).Clusters = VisualVocabularyData(PType).Clusters(1:ClNo);
    for C = 1 : ClNo
        VisualVocabularyData(PType).Clusters(C).Centroid = CX(:, C);
    end
end

