for PType = 1 : numel(P3D)
    FeatureType{PType} = [Param.Detection.Detectors{P3D(PType).PointType} ' - ' Param.Description.Descriptors{P3D(PType).DescriptorType}];
end
FeatureType{PType + 1} = 'Ground Truth';