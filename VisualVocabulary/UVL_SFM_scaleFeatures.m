function Features = UVL_SFM_scaleFeatures(Features, MinVect, MaxVect)
DataItems = size(Features, 2);
MinMatrix = repmat(MinVect, 1, DataItems);
MaxMatrix = repmat(MaxVect, 1, DataItems);
Features = 2 * (Features - MinMatrix) ./ (MaxMatrix - MinMatrix) - 1;
