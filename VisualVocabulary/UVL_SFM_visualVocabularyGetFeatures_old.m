function VisualVocabularyData = UVL_SFM_visualVocabularyGetFeatures(VisualVocabularyData, P3D, Eigen, FrameRange)
ClusterIndexing = zeros(1, FrameRange(end)); %% allocate frame indexing for one cluster
FrameIndexingCursor = size(VisualVocabularyData.FrameIndices, 1) + 1;
VisualVocabularyData.FrameIndices(size(VisualVocabularyData.FrameIndices, 1) + 1000, FrameRange(end)) = 0; %% extend frame indexing
CurrentSize = size(VisualVocabularyData.FrameIndices, 1);
Rule1 = P3D.TotalFrames >= 4;
Rule2 = P3D.Status == 3;
Sequence = find(Rule1 & Rule2);

ClusterMapping = zeros(2, 1);
for Index = 1 : numel(VisualVocabularyData.Clusters)
    ClusterMapping = [ClusterMapping [VisualVocabularyData.Clusters(Index).FeatureIndex; Index * ones(size(VisualVocabularyData.Clusters(Index).FeatureIndex))]];
end
    
for Point = Sequence
    LocalIndices = P3D.DescIndex{Point};
    Frames = P3D.ViewIndex(LocalIndices);
    ViewInP3D = ismembc2(double(FrameRange), double(Frames));
    ViewInRange = logical(ViewInP3D);
    FramesInRange = ViewInP3D(ViewInRange);    
    FramesInRange = Frames(FramesInRange);
    if numel(FramesInRange)
        ClusterIndexing(:) = 0;
        ToCluster = ClusterMapping(1, :) == Point;
        ToCluster = ClusterMapping(2, ToCluster);
        if numel(ToCluster) %% old feature, just update frameindex
            ClusterIndexing(FramesInRange) = 1;
            VisualVocabularyData.FrameIndices(ToCluster, :) = VisualVocabularyData.FrameIndices(ToCluster, :) + ClusterIndexing;
        else  %% new feature, creat new cluster
            FramesSoFar = Frames <= FrameRange(end);
            ClusterIndexing(Frames(FramesSoFar)) = 1;
            NewIndex = numel(VisualVocabularyData.Clusters) + 1;
            VisualVocabularyData.Clusters(NewIndex).FeatureIndex = Point;
            VisualVocabularyData.Clusters(NewIndex).Centroid = Eigen.Vectors' * double(P3D.Desc(:, Point));
            VisualVocabularyData.Clusters(NewIndex).Features = VisualVocabularyData.Clusters(NewIndex).Centroid;
            VisualVocabularyData.Clusters(NewIndex).Radius = P3D.FeatureRadius(Point);
            VisualVocabularyData.Clusters(NewIndex).Elements = 1;
            VisualVocabularyData.FrameIndices(FrameIndexingCursor, :) = ClusterIndexing;
            FrameIndexingCursor = FrameIndexingCursor + 1;
            if FrameIndexingCursor > CurrentSize
                VisualVocabularyData.FrameIndices(size(VisualVocabularyData.FrameIndices, 1) + 1000, size(VisualVocabularyData.FrameIndices, 2)) = 0; %% extend frame indexing
                CurrentSize = size(VisualVocabularyData.FrameIndices, 1);
            end                
        end
    end
end
VisualVocabularyData.FrameIndices = VisualVocabularyData.FrameIndices(1 : (FrameIndexingCursor - 1), :);
