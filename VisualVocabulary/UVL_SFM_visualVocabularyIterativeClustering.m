function VisualVocabularyData = UVL_SFM_visualVocabularyIterativeClustering(VisualVocabularyData, Eigen, Ratios, Params)
Clusters = VisualVocabularyData.Clusters;
FrameIndices = VisualVocabularyData.FrameIndices';
IndexMapping = VisualVocabularyData.IndexMapping;
TotalDimensions = size(Clusters(1).Centroid, 1);
DimensionMask = false(TotalDimensions, 1);
DimensionMask((TotalDimensions - Params.EigenDimensions + 1) : end) = true;
Pass = 1;
while true
    ClusterCentroids = [Clusters(:).Centroid];
    ClusterRadius = [Clusters(:).Radius];
    [MuRadius, SigmaRadius] = normfit(ClusterRadius);
    ClusterCentroids = ClusterCentroids(DimensionMask, :);
    ClusterDistances = std_euclidian_rad(ClusterCentroids, Eigen.Variance(DimensionMask), [Clusters(:).Radius]);
    ClusterDistances = ClusterDistances + ClusterDistances';
    % get the pairs
    [SortedByCol, IndexByCol] = sort(ClusterDistances);
    TotalClusters = numel(Clusters);
    Pairs = [1 : TotalClusters;
        IndexByCol(2, :)];
    % get the probability density
    ClusterDistancesRed = SortedByCol(2, :); %% fast
    [SortedCols, IndexCols] = sort(ClusterDistancesRed);
    Pairs = Pairs(:, IndexCols);
    Available = true(1, TotalClusters);
    Cutout = true(1, TotalClusters);
    Cursor = 1;
    Counter = 0;
    tic;
    %    LastPhotometricScore = 0;
    %    LastIndexingScore = 0;
    fprintf('%d. (%d)', Pass, TotalClusters)
    for PairCounter = 1 : size(Pairs, 2)
        Counter = Counter + 1;
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        Cursor = Cursor + 1;
        if ~Available(Cc1) || ~Available(Cc2)
            continue
        end
        C1 = min([Cc1 Cc2]);
        C2 = max([Cc1 Cc2]);
        List = [C1 C2];
        Features = [Clusters(List).Features];
        [Centroid Radius] = UVL_SFM_createCluster(Features, Eigen.Variance, DimensionMask, Params);

        % indexing based
        NewClusterIndices = FrameIndices(:, C1) + FrameIndices(:, C2);
        ActiveFrames = NewClusterIndices > 0;
        NewClusterIndices = NewClusterIndices(ActiveFrames, :);
        ClustersIndices = [FrameIndices(ActiveFrames, C1) FrameIndices(ActiveFrames, C2)];

        ActiveFrameRatios = Ratios(ActiveFrames, ActiveFrames);

        [Energy EnergyArray] = UVL_SFM_clusterIndexingEnergy(ClustersIndices', ActiveFrameRatios); %#ok<NASGU>
        [EnergyNew EnergyArrayNew] = UVL_SFM_clusterIndexingEnergy(NewClusterIndices', ActiveFrameRatios); %#ok<NASGU>
        IndexingScore = EnergyNew / Energy;
        IndexingScore = 1;
        GeneralScore = IndexingScore * exp( ( MuRadius - Radius ) / SigmaRadius );
        if GeneralScore < 1.0
            continue
        end
        FrameIndices(:, C1) = FrameIndices(:, C1) + FrameIndices(:, C2);
        Cutout(C2) = false;
        Clusters(C1).Features = [Clusters(List).Features];
        Clusters(C1).Centroid = Centroid;
        Clusters(C1).Radius = Radius;
        Clusters(C1).FeatureIndex = [Clusters(List).FeatureIndex];
        Clusters(C1).Elements = sum([Clusters(List).Elements]);
        IndexMapping(:, C1) = IndexMapping(:, C1) + IndexMapping(:, C2);
        Available(List) = false;
    end
    Clusters = Clusters(Cutout);
    IndexMapping = IndexMapping(:, Cutout);
    FrameIndices = FrameIndices(:, Cutout);
    %    fprintf('\n');
    if (numel(Cutout)-sum(Cutout)<2)
        break
    end
    if numel(Clusters) < Params.TargetItems
        break
    end
    Pass = Pass + 1;
end
VisualVocabularyData.Clusters = Clusters;
VisualVocabularyData.FrameIndices = FrameIndices';
VisualVocabularyData.IndexMapping = IndexMapping;