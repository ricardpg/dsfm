function [Features P3DIndex] = UVL_SFM_getFeaturesFromDB(P3D, ViewIndex)
for PType = 1 : numel(P3D)
    PointsInView = P3D(PType).ViewIndex == ViewIndex;
    P3DIndex = P3D.ViewP3DIndex(PointsInView);
%    Features(PType).Descriptors = double(P3D(PType).ViewDesc(:, PointsInView)) * 2 ^ -15;
    Features(PType).Descriptors = double(P3D(PType).Desc(:,P3DIndex));
end