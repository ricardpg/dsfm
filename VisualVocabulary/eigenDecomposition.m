function [EigenVect, Variance] = eigenDecomposition(Features)
covDB = cov(Features')';
[EigenVect, EigenVal] = eig(covDB);
Features = EigenVect' * Features;
Variance = diag(cov(Features')');
