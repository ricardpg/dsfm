function [Centroid Radius] = UVL_SFM_createCluster(Features, Variance, DimensionMask, Params)
Centroid = mean(Features,2);
Distances = std_euclidian(Centroid(DimensionMask), Features(DimensionMask, :), Variance(DimensionMask));

switch lower(Params.CentroidType)
    case 'median'
        MedianPos = find(Distances == min(Distances));
        MedianPos = MedianPos(1);
        Centroid = Features(:, MedianPos);
        Distances = std_euclidian(Centroid(DimensionMask), Features(DimensionMask, :), Variance(DimensionMask));
    case 'mean'
        %% nothing to do actually
    otherwise
        error('Unknown radius method')
end

switch lower(Params.RadiusType)
    case 'mean'
        Radius = mean(Distances);
    case 'max'
        Radius = max(Distances);
    case 'normal'
        [MuDistances, SigmaDistances] = normfit(Distances);
        Radius =  MuDistances + 2 * SigmaDistances;
    case 'hausdorff'
        Distances = std_euclidian(Features(DimensionMask, :), Features(DimensionMask, :), Variance(DimensionMask));
        Radius = max(max(Distances)) / 2;
    otherwise
        error('Unknown radius method')
end