function DocumentIndexing = UVL_SFM_freqInvDocWeighting(DocumentIndexing)
[NoWords, NoFrames] = size(DocumentIndexing);
ActiveItems = DocumentIndexing > 0;
ActiveWords = repmat(sum(ActiveItems, 2), [1 NoFrames]);
ActiveFrames = repmat(sum(ActiveItems, 1), [NoWords 1]);
TotalFrames = NoFrames * ones(NoWords, NoFrames);
DocumentIndexing = DocumentIndexing ./ ActiveWords .* log( TotalFrames ./ ActiveFrames);