function FrameIndex = UVL_SFM_indexFrame(Features, Clusters, ClusterRadius, Params)
Thresh = 100000;
%% matching
if ~numel(Clusters)
    FrameIndex = [];
    return
end
if ~numel(Features)
    FrameIndex = [];
    return
end
TotalDimensions = size(Features, 1);
DimensionMask = false(TotalDimensions, 1);
DimensionMask((TotalDimensions - Params.EigenDimensionsIndexing + 1) : end) = true;
Features = Features(DimensionMask, :);
Clusters = Clusters(DimensionMask, :);

Pairs = UVL_SFM_approximateNearestNeighbour(double(Features), double(Clusters), 1, Thresh, Params.IndexingErrorBounds);
%Pairs = UVL_SFM_nearestNeighbour(double(Features), double(Clusters), Thresh);
%Limit = ClusterRadius(Pairs(2, :));
%OutsideCluster = Pairs(3, :) > Limit;
OutsideCluster = Pairs(3, :) > inf;
Pairs(2, OutsideCluster) = 0;
FrameIndex = histc(Pairs(2,:), 0:size(Clusters,2))';
FrameIndex = FrameIndex(2:end);


