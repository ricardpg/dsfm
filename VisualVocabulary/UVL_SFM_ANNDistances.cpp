//Method 1 = nearest neighbor
//Method 1 = second nearest neighbor
//Method 1 = second nearest neighbor bidirectionnal

#include "mex.h"
#include <ANN/ANN.h>					// ANN declarations


void mexFunction(int nout, mxArray *out[], int nin, const mxArray *in[])
{
    ANNpointArray		dataPts;				// data points
    ANNidxArray			nnIdx;					// near neighbor indices
    ANNdistArray		dists;					// near neighbor distances
    ANNkd_tree*			kdTree;					// search structure
    double              *PointsDB, *PointsImg, *DistArray;
    int                 TotMatch = 0, Dimension, NumPointsDB, NumPointsImg, Method;
    double              ErrorBounds, Thresh;
    
    if (nin != 3)
        mexErrMsgTxt("Usage: Pairs = UVL_SFM_approximateNearestNeighbour(Descriptors, Threshold, ErrorBounds)");
    else if (nout != 1)
        mexErrMsgTxt("Too many output arguments.");
    
    PointsImg = mxGetPr(in[0]);
    NumPointsImg = mxGetN(in[0]);
    PointsDB = mxGetPr(in[0]);
    NumPointsDB = mxGetN(in[0]);
    Dimension = mxGetM(in[0]);
    Thresh = *mxGetPr(in[1]);
    ErrorBounds = *mxGetPr(in[2]);
    
    
    
    dataPts = new ANNpoint[NumPointsDB];
    for (int i = 0; i < NumPointsDB; i++, PointsDB += Dimension) {
        dataPts[i] = PointsDB;
    }
    kdTree = new ANNkd_tree(dataPts, NumPointsDB, Dimension);					// build search structure
    
    
    out[0] = mxCreateDoubleMatrix(3, NumPointsImg, mxREAL);
    DistArray = mxGetPr(out[0]);
    nnIdx = new ANNidx[2];						    // allocate near neighbor indices
    dists = new ANNdist[2];						// allocate near neighbor dists
    for(int i = 0; i < NumPointsImg; ++i, PointsImg += Dimension)
    {
        kdTree->annkSearch(PointsImg, 2, nnIdx, dists, ErrorBounds);	// search
        if(dists[1] <= Thresh)
        {
            ++TotMatch;
            *DistArray++ = i + 1.0;
            *DistArray++ = nnIdx[1] + 1.0;
            *DistArray++ = dists[1];
        }
    }
    
    mxSetN(out[0], TotMatch);
    
    delete [] nnIdx;							// clean things up
    delete [] dists;
    delete kdTree;
    annClose();									// done with ANN
    
    return;
}
