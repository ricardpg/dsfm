%VisualVocabulary.RadiusType = 'mean';
%VisualVocabularyParams.RadiusType = 'max';
%VisualVocabularyParams.RadiusType = 'normal';
VisualVocabularyParams.RadiusType = 'hausdorff';
VisualVocabularyParams.CentroidType = 'mean';
%VisualVocabularyParams.CentroidType = 'median';

VisualVocabularyParams.ScaleData = false;
VisualVocabularyParams.UseEigen = 0;
VisualVocabularyParams.EigenDimensions = 64;
VisualVocabularyParams.EigenDimensionsIndexing = 64;
VisualVocabularyParams.InitialFrames = 5;
VisualVocabularyParams.FrameStep = 5;
VisualVocabularyParams.RadiusFactor = 1;
VisualVocabularyParams.MaximumRadius = .7;
VisualVocabularyParams.TargetItems = 0;
VisualVocabularyParams.LDADims = 50;
VisualVocabularyParams.KNN = 50;


VisualVocabularyParams.IndexingErrorBounds = 0;
VisualVocabularyParams.ClusteringErrorBounds = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%

Param.Detection.Hessian.MaxNumber = 2000;                     % max number of features to detect;  
Param.Detection.Hessian.RadiusNonMaximal = 3;                 % radius for non-maximal suppression for detector
Param.Detection.Hessian.Border = 5;                          % outer border
                          % outer border

Param.Detection.SURF.MaxNumber = 2000;                        % max number of features to detect;  
Param.Detection.SURF.RadiusNonMaximal = 3;                    % radius for non-maximal suppression for detector
Param.Detection.SURF.Border = 5;                             % outer border


%Param.Detection.MSER.MaxNumber = 2000;                        % max number of features to detect;  
%Param.Detection.MSER.RadiusNonMaximal = 1;                    % radius for non-maximal suppression for detector
%Param.Detection.MSER.Border = 1;                             % outer border
%Param.Detection.MSER.MinimumRegionSize = 1;
%Param.Detection.MSER.MinimumMargin = 0; % not used now
