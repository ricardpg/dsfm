function [VisualVocabularyData EigenVect EigenVal] = UVL_SFM_visualVocabularyGetTransformation(VisualVocabularyData)
Clusters = VisualVocabularyData.Clusters;
TotalDimensions = size(Clusters(1).Centroid, 1);
A = zeros(TotalDimensions);
B = zeros(TotalDimensions);
ClusterSeq = 1:numel(Clusters);
for CurCluster = ClusterSeq
    fprintf('%d\n', CurCluster);
    ClusterMask = ClusterSeq ~= CurCluster;
    AllOtherFeat = [Clusters(ClusterMask).Features];
    for CurFeat = 1 : Clusters(CurCluster).Elements
        FeatureMask = (1:Clusters(CurCluster).Elements) ~= CurFeat;
        CurFeature = Clusters(CurCluster).Features(:, CurFeat);
        Diff = repmat(CurFeature, 1, Clusters(CurCluster).Elements-1) - Clusters(CurCluster).Features(:, FeatureMask);
        A = A + Diff * Diff';
        Diff = repmat(CurFeature, 1, size(AllOtherFeat, 2)) - AllOtherFeat;
        B = B + Diff * Diff';
    end
end
[EigenVect, EigenVal] = eig(A, B, 'qz');

                 
        

