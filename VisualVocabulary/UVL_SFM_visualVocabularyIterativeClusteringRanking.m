function VisualVocabularyData = UVL_SFM_visualVocabularyIterativeClusteringRanking(VisualVocabularyData, Eigen, Ratios, Params)
Clusters = VisualVocabularyData.Clusters;
FrameIndices = VisualVocabularyData.FrameIndices';
IndexMapping = VisualVocabularyData.IndexMapping;
TotalDimensions = size(Clusters(1).Centroid, 1);
DimensionMask = false(TotalDimensions, 1);
DimensionMask((TotalDimensions - Params.EigenDimensions + 1) : end) = true;
Pass = 1;
ScoreTh = 1;
while true
    TotalClusters = numel(Clusters);
    ClusterCentroids = [Clusters(:).Centroid];
    ClusterRadius = [Clusters(:).Radius];
    [MuRadius, SigmaRadius] = normfit(ClusterRadius);
    ClusterCentroids = ClusterCentroids(DimensionMask, :);
    Pairs = UVL_SFM_ANNDistances(ClusterCentroids, 10000, Params.ClusteringErrorBounds);    
    Scores = zeros(20, TotalClusters, 'single');
    Ranks = zeros(20, TotalClusters, 'uint16');
    Cursors = ones(1, TotalClusters, 'uint8');
    % get the probability density
    Available = true(1, TotalClusters);
    Cutout = true(1, TotalClusters);
    Cursor = 1;
    Counter = 0;
    tic;
    %    LastPhotometricScore = 0;
    %    LastIndexingScore = 0;
    fprintf('%d. (%d)', Pass, TotalClusters)
    for PairCounter = 1 : size(Pairs, 2)
        Counter = Counter + 1;
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        Cursor = Cursor + 1;
        if ~Available(Cc1) || ~Available(Cc2)
            continue
        end
        C1 = min([Cc1 Cc2]);
        C2 = max([Cc1 Cc2]);
        if numel(find(Ranks(:, C1) == C2))
            continue
        end
        List = [C1 C2];
        Features = [Clusters(List).Features];
        [Centroid Radius] = UVL_SFM_createCluster(Features, Eigen.Variance, DimensionMask, Params);

        % indexing based
        NewClusterIndices = FrameIndices(:, C1) + FrameIndices(:, C2);
        ActiveFrames = NewClusterIndices > 0;
        NewClusterIndices = NewClusterIndices(ActiveFrames, :);
        ClustersIndices = [FrameIndices(ActiveFrames, C1) FrameIndices(ActiveFrames, C2)];

        ActiveFrameRatios = Ratios(ActiveFrames, ActiveFrames);

        [Energy EnergyArray] = UVL_SFM_clusterIndexingEnergy(ClustersIndices', ActiveFrameRatios); %#ok<NASGU>
        [EnergyNew EnergyArrayNew] = UVL_SFM_clusterIndexingEnergy(NewClusterIndices', ActiveFrameRatios); %#ok<NASGU>
        IndexingScore = EnergyNew / Energy;
%        IndexingScore = 1;
        GeneralScore = sqrt(IndexingScore * exp( ( MuRadius - Radius ) / SigmaRadius ));
        if GeneralScore < ScoreTh
            continue
        end
        Scores(Cursors(C1), C1) = GeneralScore;
        Ranks(Cursors(C1), C1) = C2;
        Cursors(C1) = Cursors(C1) + 1;
        
    end
    while true
        MaxScore = max(max(Scores));
        if MaxScore == 0
            break
        end
        [ClusterCursor C1] = find(Scores == MaxScore);
        if numel(C1) > 1
            C1 = C1(1);
            ClusterCursor = ClusterCursor(1);
        end
        Scores(ClusterCursor, C1) = 0;
        C2 = Ranks(ClusterCursor, C1);
        if ~Available(C1) || ~Available(C2)
            continue
        end
        List = [C1 C2];
        Features = [Clusters(List).Features];
        [Centroid Radius] = UVL_SFM_createCluster(Features, Eigen.Variance, DimensionMask, Params);
        FrameIndices(:, C1) = FrameIndices(:, C1) + FrameIndices(:, C2);
        Cutout(C2) = false;
        Clusters(C1).Features = [Clusters(List).Features];
        Clusters(C1).Centroid = Centroid;
        Clusters(C1).Radius = Radius;
        Clusters(C1).FeatureIndex = [Clusters(List).FeatureIndex];
        Clusters(C1).Elements = sum([Clusters(List).Elements]);
        IndexMapping(:, C1) = IndexMapping(:, C1) + IndexMapping(:, C2);
        Available(List) = false;
        if sum(Cutout) <= Params.TargetItems
            break
        end
    end
    
    Clusters = Clusters(Cutout);
    IndexMapping = IndexMapping(:, Cutout);
    FrameIndices = FrameIndices(:, Cutout);
    %    fprintf('\n');
    if (numel(Cutout)-sum(Cutout) < 10)
        ScoreTh = ScoreTh - .1;
        fprintf('\nTo: %.01f ', ScoreTh)
    end
    if numel(Clusters) <= Params.TargetItems
        break
    end
    Pass = Pass + 1;
end
VisualVocabularyData.Clusters = Clusters;
VisualVocabularyData.FrameIndices = FrameIndices';
VisualVocabularyData.IndexMapping = IndexMapping;