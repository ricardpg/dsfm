visualVocabularyConfig
labelFeatureTypes
%climg = figure;
FigSim = figure;
warning off MATLAB:legend:IgnoringExtraEntries
%%%%%%%%%%%%%%%%%%%%%%
if ~exist('Ratios', 'var')
%    load ratios
end
%% initialization
% Itmp = UVL_SFM_getInputStream(InputDataInfo, 1); %%% get frame size
% [height, width, depth] = size(Itmp);
% index_frames_all = [];
for PType = 1 : numel(P3D) %#ok<NODEF>
    FrameData(PType).Indexing = [];
end
%% PCA initialization
if ~exist('Eigen', 'var')
    for PType = 1 : numel(P3D) %#ok<NODEF>
        if VisualVocabularyParams.UseEigen
            ValidPoints = P3D(PType).Status == 3;
            LocalIndices = [P3D(PType).DescIndex{ValidPoints}];
            Descriptors = double(P3D(PType).ViewDesc(:, LocalIndices)) / 2^15;
            [Eigen(PType).Vectors Eigen(PType).Variance] = eigenDecomposition(Descriptors);
            Eigen(PType).VarianceMatr = sqrt(diag(1 ./ Eigen(PType).Variance));
        else
            DescriptorDimensions = size(P3D(PType).ViewDesc, 1);
            Eigen(PType).Vectors = eye(DescriptorDimensions);
            Eigen(PType).Variance = ones(DescriptorDimensions, 1);
            Eigen(PType).VarianceMatr = sqrt(diag(1 ./ Eigen(PType).Variance));
        end
    end
    clear Descriptors;
end


%% loop
for PType = 1 : numel(P3D)
    VisualVocabularyData(PType).Clusters = [];
    VisualVocabularyData(PType).ClustersLDA = [];
    VisualVocabularyData(PType).FrameIndices = [];
    VisualVocabularyData(PType).LDATransform = [];
    VisualVocabularyData(PType).Normalization = [];
    FrameData(PType).FrameSimilarity = zeros(numel(View));
    FrameData(PType).Indexing = [];
end
counter = 0;
FrameRangeStart = 1;
for FrameRangeLimit = VisualVocabularyParams.InitialFrames : VisualVocabularyParams.FrameStep : numel(View)
    FrameRange = FrameRangeStart:FrameRangeLimit;
    fprintf('\nFrames %d-%d\n', FrameRange(1), FrameRange(end));
    for PType = 1 : numel(P3D)
        fprintf('\n----------------------------------\n');
        ClustersBefore = numel(VisualVocabularyData(PType).Clusters);
        VisualVocabularyData(PType) = UVL_SFM_visualVocabularyGetFeaturesLDA(VisualVocabularyData(PType), P3D(PType), FrameRange);
        ClustersAfter = numel(VisualVocabularyData(PType).Clusters);
        NewClusters = ClustersAfter - ClustersBefore;
        %VisualVocabularyData(PType).IndexMapping = [eye(ClustersBefore) zeros(ClustersBefore, NewClusters)];
        VisualVocabularyData(PType) = UVL_SFM_visualVocabularyIterativeClusteringLDAKNF(VisualVocabularyData(PType), P3D(PType), VisualVocabularyParams);
        if ~isempty(FrameData(PType).Indexing)
%            FrameData(PType).Indexing = (FrameData(PType).Indexing' * VisualVocabularyData(PType).IndexMapping)';
        end
    end


    for CurFrame = []%FrameRange
        [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
        if isempty(ICur),
            ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
            ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
        end
        Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection, Param.Processing.ResizeCoeff);
        Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description, Param.Processing.ResizeCoeff);
%        [Features P3DIndex] = UVL_SFM_getFeaturesFromDB(P3D, CurFrame);
        for PType = 1 : numel(P3D)
            FeatImg = Features(PType).Descriptors;
            FeatImg = UVL_SFM_scaleFeatures(FeatImg, MinVect, MaxVect);
            FrameIndex = UVL_SFM_indexFrameLDA(FeatImg, VisualVocabularyData(PType).ClustersLDA, VisualVocabularyData(PType).LDATransform, VisualVocabularyParams);
%            FrameIndex = UVL_SFM_indexFrameLDAKNN(FeatImg, VisualVocabularyData(PType).Clusters, VisualVocabularyData(PType).LDATransform, VisualVocabularyParams);
            
            FrameData(PType).Indexing = [FrameData(PType).Indexing FrameIndex];
            for MatchingFrame = 1 : CurFrame - 1
                %            FrameDataCurr = UVL_SFM_freqInvDocWeighting(FrameData(PType).Indexing);
                FrameDataCurr = FrameData(PType).Indexing;
                FrameIndex = FrameDataCurr(:, CurFrame) / norm(FrameDataCurr(:, CurFrame));
                MatchingFrameIndex = FrameDataCurr(:, MatchingFrame) / norm(FrameDataCurr(:, MatchingFrame));
                Similarity = FrameIndex' * MatchingFrameIndex;
                Similarity = Similarity - min(Similarity);
                Similarity = Similarity / max(Similarity);
                FrameData(PType).FrameSimilarity(MatchingFrame, CurFrame) = FrameIndex' * MatchingFrameIndex;
            end
        end
        if ~isempty(MatchingFrame)
            PlotData = [];
            for PType = 1 : numel(P3D)
                PlotData = [PlotData FrameData(PType).FrameSimilarity(1:MatchingFrame, CurFrame)];
            end
%            PlotData = [PlotData Ratios(1 : MatchingFrame, CurFrame)];
            figure(FigSim), hold off, plot(PlotData), legend(FeatureType)
        end
    end
    FrameRangeStart = FrameRangeLimit + 1;
    counter = counter + 1;
end

