function [frame_indexing frame_indexing_matr] = UVL_SFM_indexFrameFull(features, clusters, variance)
%% matching
if ~numel(clusters)
    frame_indexing = [];
    return
end

frame_indexing = zeros(numel(clusters),1);
if ~numel(features)
    return
end
ths = [clusters(:).Radius]';
distances = std_euclidian([clusters(:).Centroid], features, variance);
min_dist_vect = min(distances);
min_dist_matr = repmat(min_dist_vect, [numel(clusters) 1]);
th_matr = repmat(ths, [1 size(features,2)]);
%min_dist_matr = min_dist_matr .* (min_dist_matr<=th);
%min_dist_matr = min_dist_matr .* (min_dist_matr<=th_matr);
frame_indexing_matr = distances == min_dist_matr;
frame_indexing = sum(frame_indexing_matr,2);
