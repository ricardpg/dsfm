#include"mex.h"
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<assert.h>
#include<matrix.h>
#define eps 0.000000000001


/* convolve and transopose */

void
mexFunction(int nout, mxArray *out[],
int nin, const mxArray *in[])
{
    enum {ratios_par=0, frame_indices_par};
    enum {energy_par=0, energy_array_par, ttmp};
    
  /* ------------------------------------------------------------------
   **                                                Check the arguments
   ** --------------------------------------------------------------- */
    if (nin != 2) {
        mexErrMsgTxt("Exactly 2 input arguments required.");
    } else if (nout > 3) {
        mexErrMsgTxt("Too many output arguments.");
    }
    // get dimensions
    int total_clusters = mxGetM(in[frame_indices_par]);
    int total_frames = mxGetN(in[frame_indices_par]);
    // read pointers
    double* ratios = mxGetPr(in[ratios_par]);
    double* frame_indices = mxGetPr(in[frame_indices_par]);
    // create matrices
    long int entries = total_frames * (total_frames-1)/2;
    out[energy_par] = mxCreateDoubleMatrix(1, 1, mxREAL);
    out[energy_array_par] = mxCreateDoubleMatrix(3, entries, mxREAL);
    out[ttmp] = mxCreateDoubleMatrix(total_clusters, total_frames, mxREAL);
    double* energy_array = mxGetPr(out[energy_array_par]);
    double* energy = mxGetPr(out[energy_par]);
    double* frame_indices_norm = mxGetPr(out[ttmp]);
    // normalize the indexing vectors
    for(int ic = 0; ic < total_frames; ic++){
        double sum = 0;
		long int icc = ic * total_clusters;
		for(int ir = 0; ir < total_clusters; ir++){
			double fiv = frame_indices[icc + ir];
            sum +=  fiv * fiv;
		}
        sum = 1/sqrt(sum);
        for(int ir = 0; ir < total_clusters; ir++){
			long int iccir = icc + ir;
            frame_indices_norm[iccir] =  frame_indices[iccir] * sum;
        }
    }
    // calculate indexing energy
    double numerator = 0;
	double denominator = 0;
    unsigned int cursor = 0;
    for(int f1 = 0; f1 < (total_frames - 1); f1++){
        long int f1d = f1 * total_clusters;
        for(int f2 = f1 + 1; f2 < total_frames; f2++){
            long int f2d = f2 * total_clusters;
            double ratio = ratios[f2 * total_frames + f1];
            double top = 0;
            double bot = 0;
            for(int cc = 0;cc < total_clusters; cc++){
                top += (double)(ratio) * frame_indices_norm[f1d + cc] * frame_indices_norm[f2d + cc];
                bot += (double)(1 - ratio) * frame_indices_norm[f1d + cc] * frame_indices_norm[f2d + cc];
                energy_array[cursor] = top;
                energy_array[cursor + 1] = bot;
				energy_array[cursor + 2] = ratio;
            }
            numerator += top;
            denominator += bot;
            cursor += 3;
        }
    }
    energy[0] = numerator / (denominator + eps);
     
}
