#include "mex.h"
#include <ANN/ANN.h>					// ANN declarations


void mexFunction(int nout, mxArray *out[], int nin, const mxArray *in[])
{
    ANNpointArray		dataPts;				// data points
    ANNidxArray			nnIdx;					// near neighbor indices
    ANNdistArray		dists;					// near neighbor distances
    ANNkd_tree*			kdTree;					// search structure
    double              *PointsDB, *PointsImg, *DistArray;
    int                 Dimension, NumPointsDB, NumPointsImg, K;
    double              ErrorBounds;
    
    if (nin != 4)
        mexErrMsgTxt("Usage: Pairs = UVL_SFM_AKNN(Descriptor1, Descriptors2, KNN, ErrorBounds)");
    else if (nout != 1)
        mexErrMsgTxt("Too many output arguments.");
    
    PointsImg = mxGetPr(in[0]);
    NumPointsImg = mxGetN(in[0]);
    PointsDB = mxGetPr(in[1]);
    NumPointsDB = mxGetN(in[1]);
    Dimension = mxGetM(in[1]);
    K = *mxGetPr(in[2]);
    ErrorBounds = *mxGetPr(in[3]);
    
    
    
    dataPts = new ANNpoint[NumPointsDB];
    for (int i = 0; i < NumPointsDB; i++, PointsDB += Dimension) {
        dataPts[i] = PointsDB;
    }
    kdTree = new ANNkd_tree(dataPts, NumPointsDB, Dimension);					// build search structure
    
    
    
    out[0] = mxCreateDoubleMatrix(K * 2 + 1, NumPointsImg, mxREAL) ;
    DistArray = mxGetPr(out[0]);
    nnIdx = new ANNidx[K];						    // allocate near neighbor indices
    dists = new ANNdist[K];						// allocate near neighbor dists
    for(int i = 0; i < NumPointsImg; ++i, PointsImg += Dimension)
    {
        
        kdTree->annkSearch(PointsImg, K, nnIdx, dists, ErrorBounds);	// search
        *DistArray++ = i + 1.0;
        for(int PairCount = 0; PairCount < K; PairCount++)
        {
            *DistArray++ = nnIdx[PairCount] + 1.0;
            *DistArray++ = sqrt(dists[PairCount]);
        }
    }
    delete [] nnIdx;							// clean things up
    delete [] dists;
    delete kdTree;
    annClose();									// done with ANN
    
    return;
}
