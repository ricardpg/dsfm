function Energy = getEnergy(FrameIndices, Ratios)
Frames = size(FrameIndices, 2);
Clusters = size(FrameIndices, 1);
Numerator = 0;
Denominator = 0;
for I1 = 1 : Frames - 1
    for I2 = I1+1 : Frames
        for C = 1 : Clusters
            Numerator = Numerator + Ratios(I1, I2) * FrameIndices(C, I1) * FrameIndices(C, I2);
            Denominator = Denominator + (1 - Ratios(I1, I2)) * FrameIndices(C, I1) * FrameIndices(C, I2);
        end
    end
end
Energy = Numerator / (Denominator + eps);
            