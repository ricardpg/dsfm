function FrameIndex = UVL_SFM_indexFrameLDA(Features, Vocabulary, LDATransform, Normalization, Params, P3DIndex)
Thresh = 100000;
%% matching
Clusters = Vocabulary.ClustersLDA;
if ~numel(Clusters)
    FrameIndex = [];
    return
end
if ~numel(Features)
    FrameIndex = [];
    return
end
FeaturesLDA = LDATransform * Features;
%Norms = sum(FeaturesLDA.^2);
%FeaturesLDA = FeaturesLDA ./ repmat(Norms, size(FeaturesLDA, 1), 1);
FeaturesLDAN = FeaturesLDA ./ repmat(Normalization, 1, size(FeaturesLDA, 2));



%Pairs = UVL_SFM_approximateNearestNeighbour(double(FeaturesLDAN), double(Clusters), 1, Thresh, Params.IndexingErrorBounds);
%Pairs = UVL_SFM_nearestNeighbour(double(Features), double(Clusters), Thresh);
%OutsideCluster = Pairs(3, :) > Limit;
KNeighbors = UVL_SFM_AKNN(double(FeaturesLDAN), double(Clusters), 30, Params.ClusteringErrorBounds);
Match = zeros(1, size(FeaturesLDAN, 2));
for i = 1 : numel(Match)

    Match(i) = sum(ismembc2(double(Vocabulary.Clusters(KNeighbors(2,i)).FeatureIndex), double(P3DIndex(KNeighbors(1,i)))));

end
fprintf('%f\n', sum(Pairs(3,:)));
OutsideCluster = Pairs(3, :) > inf;
Pairs(2, OutsideCluster) = 0;
FrameIndex = histc(Pairs(2,:), 0:size(Clusters,2))';
FrameIndex = FrameIndex(2:end);


