%clear all;

%%%%%%%%%%%%%%%%%%%%%% Generating data

X = [];
Y = [];
FeaturesImg = [];
for i = 1 : 300%numel(VisualVocabularyData.Clusters)
    NoFeatures = size(VisualVocabularyData.Clusters(i).Features, 2);
    X = [X VisualVocabularyData.Clusters(i).Features];
    Y = [Y; i * ones(NoFeatures, 1)];
    FeaturesImg = [FeaturesImg VisualVocabularyData.Clusters(i).Features(:, 1)];
end

K=Kmatrix_Gauss(X,1);
[T,Z]=KLFDA(K,Y,30);

%%%%%%%%%%%%%%%%%%%%%% Computing LFDA solution
%[T,Z]=LFDA(X',Y,64, 'orthonormalized');


ClusterCentroids = [VisualVocabularyData.Clusters(:).Centroid];
ClusterCentroidsLDA = T' * ClusterCentroids;
FrameIndex = UVL_SFM_indexFrameLDA(FeatImg, ClusterCentroidsLDA, T', VisualVocabularyParams);

figure, imagesc(FrameIndex)