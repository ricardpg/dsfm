FigSim = figure;

FrameRange = 1:1000;


for PType = 1 : numel(P3D) %#ok<NODEF>
    FrameData(PType).Indexing = [];
    FrameData(PType).FrameSimilarity = zeros(numel(FrameRange));
end
for CurFrame = FrameRange
    fprintf('\n----------- %d -----------\n', CurFrame)
    [ICur Features] = UVL_SFM_loadCache(CurFrame, Param);
    if isempty(ICur),
        ICur = UVL_SFM_getInputStream(InputDataInfo, CurFrame);
        ICur = UVL_SFM_preprocessImage(ICur, CurFrame, Param.Processing);
    end
   Features = UVL_SFM_detectFeatures(ICur, Features, Param.Detection);
   Features = UVL_SFM_calculateDescriptors(ICur, Features, Param.Description);
%    test
    figure(FigSim), hold off
    for PType = 1 : numel(P3D)
        FeatImg = Features(PType).Descriptors;
        FeatImg = Eigen(PType).Vectors' * FeatImg; %% PCA descomposition
        FT = FeatImg;
        FeatImg = (FeatImg' * Eigen(PType).VarianceMatr)';  %% normalize by variance
        ClusterCentroids = [VisualVocabularyData(PType).Clusters(:).Centroid];
        ClusterCentroids = (ClusterCentroids' * Eigen(PType).VarianceMatr)';
        ClusterRadius = [VisualVocabularyData(PType).Clusters(:).Radius];
        FrameIndex = UVL_SFM_indexFrame(FeatImg, ClusterCentroids, ClusterRadius, VisualVocabularyParams);
%        FrameIndexFull  = UVL_SFM_indexFrameFull(FT, VisualVocabularyData(PType).Clusters, Eigen(PType).Variance);
        FrameData(PType).Indexing = [FrameData(PType).Indexing FrameIndex];
        for MatchingFrame = 1 : CurFrame - 1
%            FrameDataCurr = UVL_SFM_freqInvDocWeighting(FrameData(PType).Indexing);
            FrameDataCurr = FrameData(PType).Indexing;
            FrameIndex = FrameDataCurr(:, CurFrame) / norm(FrameDataCurr(:, CurFrame));
            MatchingFrameIndex = FrameDataCurr(:, MatchingFrame) / norm(FrameDataCurr(:, MatchingFrame));
            FrameData(PType).FrameSimilarity(MatchingFrame, CurFrame) = FrameIndex' * MatchingFrameIndex;
        end
    end
    if ~isempty(MatchingFrame)
        PlotData = [];
        for PType = 1 : numel(P3D)
            PlotData = [PlotData FrameData(PType).FrameSimilarity(1:MatchingFrame, CurFrame)];
        end
        PlotData = [PlotData Ratios(1 : MatchingFrame, CurFrame)];
        figure(FigSim), hold off, plot(PlotData), legend(FeatureType)
    end
end