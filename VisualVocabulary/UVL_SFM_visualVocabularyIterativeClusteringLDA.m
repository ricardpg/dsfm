function VisualVocabularyData = UVL_SFM_visualVocabularyIterativeClusteringLDA(VisualVocabularyData, P3D, MinVect, MaxVect, Params)
Clusters = VisualVocabularyData.Clusters;
FrameIndices = VisualVocabularyData.FrameIndices';
IndexMapping = VisualVocabularyData.IndexMapping;
TotalDimensions = size(Clusters(1).Centroid, 1);
TotalElements = sum([Clusters(:).Elements]);
GlobalCentroid = zeros(TotalDimensions, 1);
%% compute global mean
for Cc = 1 : numel(Clusters)
    GlobalCentroid = GlobalCentroid + Clusters(Cc).Centroid * Clusters(Cc).Elements;
end
GlobalCentroid = GlobalCentroid / TotalElements;
Pass = 1;
ScoreTh = .9;
while true
    %% compute Sw and Sb
    TotalClusters = numel(Clusters);
    Sb = zeros(TotalDimensions);
    Sw = zeros(TotalDimensions);
    for Cc = 1 : numel(Clusters)
        Sw = Sw + Clusters(Cc).Elements / TotalElements * Clusters(Cc).Cov;
        ClusterCentroidVariance = Clusters(Cc).Centroid - GlobalCentroid;
        Sb = Sb + Clusters(Cc).Elements / TotalElements * ClusterCentroidVariance * ClusterCentroidVariance';
    end
%    Sb = Sb / TotalElements;
%    Sw = Sw / TotalElements;
%    St = Sb + Sw;
    InitialEnergy = trace(Sb) / trace(Sw);
%    [EigenVect, EigenVal] = eig(Sb, St, 'qz');
    SwInv = inv(Sw);
    SbR = Sb ^.5;
    Crit = SbR * SwInv * SbR;
    [EigenVect, EigenVal] = eig(Crit);
    [dmy, EigIdx] = sort(diag(EigenVal), 'descend');
    EigIdx = EigIdx(1 : Params.LDADims);
    G = EigenVect(:, EigIdx)';
    
    if ~numel(G)
        G = eye(64);
    end 
    
    ClusterCentroids = [Clusters(:).Centroid];
    ClusterCentroids = G * ClusterCentroids;
    Norms = sum(ClusterCentroids.^2);
    ClusterCentroids = ClusterCentroids ./ repmat(Norms, size(ClusterCentroids, 1), 1);
    
    %% compute cluster distances
    Pairs = UVL_SFM_ANNDistances(ClusterCentroids, 10000, Params.ClusteringErrorBounds);
    [Dists, PairsIdx] = sort(Pairs(3, :));
    Pairs = Pairs(:, PairsIdx);
    %    Cursors = ones(1, TotalClusters, 'uint8');
    Available = true(1, TotalClusters);
    %    Cursor = 1;
    Counter = 0;
    Cutout = true(1, TotalClusters);
    fprintf('%d. (%d)', Pass, TotalClusters)
    Cursor = 1;
    for PairCounter = 1 : size(Pairs, 2)
        Counter = Counter + 1;
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        Cursor = Cursor + 1;
        C1 = Cc1;
        C2 = Cc2;
        % calculate new cluster
        NewElements = (Clusters(C1).Elements + Clusters(C2).Elements);
%         NewCentroid = (Clusters(C1).Centroid * Clusters(C1).Elements + Clusters(C2).Centroid * Clusters(C2).Elements) / NewElements;
%         CovNew = Clusters(C1).Cov * (Clusters(C1).Elements - 1) / (NewElements - 1) + ...
%             Clusters(C2).Cov * (Clusters(C2).Elements - 1) / (NewElements - 1) + ...
%             (Clusters(C1).Elements * Clusters(C2).Elements) / (NewElements * (NewElements - 1)) * ...
%             ((Clusters(C1).Centroid - Clusters(C2).Centroid) * (Clusters(C1).Centroid - Clusters(C2).Centroid)');
        LocalIndices = [Clusters(C1).Indices Clusters(C2).Indices];
        Features = P3D(1).ViewDesc(:, LocalIndices);
        Features = double(Features) * 2 ^ -15;
        Features = UVL_SFM_scaleFeatures(Features, MinVect, MaxVect);
        [CovNew NewCentroid] = covData(Features);
        
        % calculate new Sw, Sb
%        SwHat = Sw * TotalElements;
        SwHat = Sw - Clusters(C1).Elements / TotalElements * Clusters(C1).Cov - Clusters(C2).Elements / TotalElements * Clusters(C2).Cov + ...
            NewElements / TotalElements * CovNew;
%        SwHat = SwHat / TotalElements;

        ClusterCentroidVarianceC1 = Clusters(C1).Centroid - GlobalCentroid;
        ClusterCentroidVarianceC2 = Clusters(C2).Centroid - GlobalCentroid;
        ClusterCentroidVarianceCNew = NewCentroid - GlobalCentroid;

        ClusterCentroidCovC1 = ClusterCentroidVarianceC1 * ClusterCentroidVarianceC1';
        ClusterCentroidCovC2 = ClusterCentroidVarianceC2 * ClusterCentroidVarianceC2';
        ClusterCentroidCovCNew = ClusterCentroidVarianceCNew * ClusterCentroidVarianceCNew';

%        SbHat = Sb * TotalElements;
        SbHat = Sb - Clusters(C1).Elements / TotalElements * ClusterCentroidCovC1 - ...
            Clusters(C2).Elements / TotalElements * ClusterCentroidCovC2;
        SbHat = SbHat + NewElements / TotalElements * ClusterCentroidCovCNew;
%        SbHat = SbHat / TotalElements;
        ProjectedEnergy = trace(SbHat) / trace(SwHat);
        EnergyGain = ProjectedEnergy / InitialEnergy;
        Pairs(3, PairCounter) = EnergyGain;
    end
    Elements = [Clusters(Pairs(1,:)).Elements] + [Clusters(Pairs(2,:)).Elements];
    [SortedElements, PairIdx] = sort(Elements);
    Pairs = Pairs(:, PairIdx);
    %    MeanElements = mean(Elements);
    %    ScoreElements = MeanElements ./ SortedElements;
    %    Pairs(3, :) = Pairs(3, :) .* ScoreElements;
    for PairCounter = 1 : size(Pairs, 2)
        if sum(Cutout) <= Params.TargetItems
            break
        end
        if Pairs(3, PairCounter) < ScoreTh
            continue
        end
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        C1 = min([Cc1 Cc2]);
        C2 = max([Cc1 Cc2]);
        if ~Available(C1) || ~Available(C2) %|| (Clusters(C1).Elements + Clusters(C2).Elements) > 300
            continue
        end
        List = [C1 C2];
        % calculate new cluster
%        NewElements = (Clusters(C1).Elements + Clusters(C2).Elements);
%         NewCentroid = (Clusters(C1).Centroid * Clusters(C1).Elements + Clusters(C2).Centroid * Clusters(C2).Elements) / NewElements;
%         CovNew = Clusters(C1).Cov * (Clusters(C1).Elements - 1) / (NewElements - 1) + ...
%             Clusters(C2).Cov * (Clusters(C2).Elements - 1) / (NewElements - 1) + ...
%             (Clusters(C1).Elements * Clusters(C2).Elements) / (NewElements * (NewElements - 1)) * ...
%             ((Clusters(C1).Centroid - Clusters(C2).Centroid) * (Clusters(C1).Centroid - Clusters(C2).Centroid)');
        LocalIndices = [Clusters(C1).Indices Clusters(C2).Indices];
        Features = P3D(1).ViewDesc(:, LocalIndices);
        Features = double(Features) * 2 ^ -15;
        Features = UVL_SFM_scaleFeatures(Features, MinVect, MaxVect);
        [CovNew NewCentroid] = covData(Features);
        FrameIndices(:, C1) = FrameIndices(:, C1) + FrameIndices(:, C2);
        Cutout(C2) = false;
        Clusters(C1).Centroid = NewCentroid;
        Clusters(C1).Cov = CovNew;
        Clusters(C1).FeatureIndex = [Clusters(List).FeatureIndex];
        Clusters(C1).Elements = sum([Clusters(List).Elements]);
        Clusters(C1).Indices = [Clusters(List).Indices];        
        IndexMapping(:, C1) = IndexMapping(:, C1) + IndexMapping(:, C2);
        Available(List) = false;
    end

    Clusters = Clusters(Cutout);
    IndexMapping = IndexMapping(:, Cutout);
    FrameIndices = FrameIndices(:, Cutout);
    %    fprintf('\n');
    if (numel(Cutout) - sum(Cutout) < 5)
        %        ScoreTh = ScoreTh - .1;
        %        fprintf('\nTo: %.01f ', ScoreTh)
        break
    end
    if numel(Clusters) <= Params.TargetItems
        break
    end
    Pass = Pass + 1;
end
Sb = zeros(TotalDimensions);
Sw = zeros(TotalDimensions);
for Cc = 1 : numel(Clusters)
    Sw = Sw + Clusters(Cc).Cov;
    ClusterCentroidVariance = Clusters(Cc).Centroid - GlobalCentroid;
    Sb = Sb + Clusters(Cc).Elements * ClusterCentroidVariance * ClusterCentroidVariance';
end
Sb = Sb / TotalElements;
Sw = Sw / TotalElements;
St = Sb + Sw;
SwInv = inv(Sw);
SbR = Sb ^.5;
Crit = SbR * SwInv * SbR;
%Crit = SbR * pinv(St) * SbR;
[EigenVect, EigenVal] = eig(Crit);
%[EigenVect, EigenVal] = eig(Sb, St, 'qz');
[dmy, EigIdx] = sort(diag(EigenVal), 'descend');
EigIdx = EigIdx(1 : Params.LDADims);
%EigIdx = EigIdx(1 : Params.LDADims);
G = EigenVect(:, EigIdx)';

if ~numel(G)
    G = eye(64);
end

ClusterCentroids = [Clusters(:).Centroid];
ClusterCentroids = G * ClusterCentroids;
Norms = sum(ClusterCentroids.^2);
VisualVocabularyData.ClustersLDA = ClusterCentroids ./ repmat(Norms, size(ClusterCentroids, 1), 1);
VisualVocabularyData.Clusters = Clusters;
VisualVocabularyData.FrameIndices = FrameIndices';
VisualVocabularyData.IndexMapping = IndexMapping;
VisualVocabularyData.LDATransform = G;
