function VisualVocabularyData = UVL_SFM_visualVocabularyIterativeClustering(VisualVocabularyData, Eigen, Ratios, Params)
Clusters = VisualVocabularyData.Clusters;
FrameIndices = VisualVocabularyData.FrameIndices';
IndexMapping = VisualVocabularyData.IndexMapping;
TotalDimensions = size(Clusters(1).Centroid, 1);
DimensionMask = false(TotalDimensions, 1);
DimensionMask((TotalDimensions - Params.EigenDimensions + 1) : end) = true;
Pass = 1;
while true
    ClusterCentroids = [Clusters(:).Centroid];
    ClusterCentroids = ClusterCentroids(DimensionMask, :);
    ClusterDistances = std_euclidian_rad(ClusterCentroids, Eigen.Variance(DimensionMask), [Clusters(:).Radius]);
%    ClusterDistances(ClusterDistances == 0) = nan;
    ClusterDistances = ClusterDistances + ClusterDistances';
    % get the pairs
    [SortedByCol, IndexByCol] = sort(ClusterDistances);
    TotalClusters = numel(Clusters);
    Pairs = [1 : TotalClusters;
             IndexByCol(2, :)];
    % get the probability density
    ClusterDistancesRed = SortedByCol(2:10, :); %% fast
    ClusterDistancesRed = sum(ClusterDistancesRed);
    [SortedCols, IndexCols] = sort(ClusterDistancesRed);
    % reorder pairs by density
    Pairs = Pairs(:, IndexCols);
%    [SortedAbs, IndexAbs] = sort(SortedCols(2,:), 'descend');
%    Pairs = [IndexAbs; IndexCols(1, IndexAbs)];
%    Pairs = Pairs(:,~isnan(SortedAbs));
    Available = true(1, TotalClusters);
    Cutout = true(1, TotalClusters);
    Cursor = 1;
    Counter = 0;
%    Txxt = [];
    tic;
    LastPhotometricScore = 0;
    LastIndexingScore = 0;
    fprintf('%d. (%d)', Pass, TotalClusters)
    for PairCounter = 1 : size(Pairs, 2)
        Elapsed = toc;
%        EraseString = sprintf('fprintf(''%s'')', repmat('\b',[1 numel(Txxt)]));
%        eval(EraseString);
%        Txxt = sprintf('Iterations: %d/%d, clusters: %d, merged: %d, last_ph_score: %f, last_idx_score: %f, elapsed: %d', Counter, size(Pairs,2), numel(Cutout), numel(Cutout) - sum(Cutout), LastPhotometricScore, LastIndexingScore, floor(Elapsed));
%        fprintf('%s', Txxt);
        Counter = Counter + 1;
        Cc1 = Pairs(1, PairCounter);
        Cc2 = Pairs(2, PairCounter);
        Cursor = Cursor + 1;
        if ~Available(Cc1) || ~Available(Cc2)
            continue
        end
        C1 = min([Cc1 Cc2]);
        C2 = max([Cc1 Cc2]);
%        if elapsed > bored_counter
%            break
%        end
        List = [C1 C2];
%        TotalElements = sum([Clusters(list).Elements]);
        Features = [Clusters(List).Features];
        [Centroid Radius] = UVL_SFM_createCluster(Features, Eigen.Variance, DimensionMask, Params);     
        Radius1 = Clusters(C1).Radius;
        Radius2 = Clusters(C2).Radius;
        
        if Radius > Params.MaximumRadius
            continue
        end
        PhotometricScore = max([Radius1 Radius2]) / Radius;
        if PhotometricScore >= 1
%            GeneralScore = PhotometricScore;
            GeneralScore = 1;
            IndexingScore = 0;
        else
            % indexing based
            NewClusterIndices = FrameIndices(:, C1) + FrameIndices(:, C2);
            ActiveFrames = NewClusterIndices > 0;
            NewClusterIndices = NewClusterIndices(ActiveFrames, :);
            ClustersIndices = [FrameIndices(ActiveFrames, C1) FrameIndices(ActiveFrames, C2)];
            
            ActiveFrameRatios = Ratios(ActiveFrames, ActiveFrames);
                
            [Energy EnergyArray] = UVL_SFM_clusterIndexingEnergy(ClustersIndices', ActiveFrameRatios);
            [EnergyNew EnergyArrayNew] = UVL_SFM_clusterIndexingEnergy(NewClusterIndices', ActiveFrameRatios);
%            GainArray = EnergyArray - EnergyArrayNew;
%            IndexingScore = sum(GainArray(1,:)) / sum(GainArray(2,:));
%            IndexingScore = 0;
            IndexingScore = EnergyNew / Energy;
            if IndexingScore > .9
                IndexingScore = 1;
            end
            GeneralScore = IndexingScore;
        end
        if GeneralScore < 1.0
            continue
        end
        LastPhotometricScore = PhotometricScore;
        LastIndexingScore = IndexingScore;
%       if IndexingScore == 0  %%% Indexing not computed
        FrameIndices(:, C1) = FrameIndices(:, C1) + FrameIndices(:, C2);
%        FrameIndices(:, C2) = 0;
%        end
        Cutout(C2) = false;
        Clusters(C1).Features = [Clusters(List).Features];
        Clusters(C1).Centroid = Centroid;
        Clusters(C1).Radius = Radius;
        Clusters(C1).FeatureIndex = [Clusters(List).FeatureIndex];
        Clusters(C1).Elements = sum([Clusters(List).Elements]);
        IndexMapping(:, C1) = IndexMapping(:, C1) + IndexMapping(:, C2);
        Available(List) = false;
        tic;
    end
    Clusters = Clusters(Cutout);
    IndexMapping = IndexMapping(:, Cutout);
    FrameIndices = FrameIndices(:, Cutout);
%    fprintf('\n');
    if (numel(Cutout)-sum(Cutout)<2)
        break
    end
    if numel(Clusters) < Params.TargetItems
        break
    end     
    Pass = Pass + 1;
end
VisualVocabularyData.Clusters = Clusters;
VisualVocabularyData.FrameIndices = FrameIndices';
VisualVocabularyData.IndexMapping = IndexMapping;