#include"mex.h"
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<assert.h>
#include<matrix.h>
#define EPS 0.000000000001

void mexFunction(int nout, mxArray *out[], int nin, const mxArray *in[])
{
    enum {FrameIndicesPar = 0, RatiosPar};
    enum {EnergyPar = 0, EnergyArrayPar};
    
  /* ------------------------------------------------------------------
   **                                                Check the arguments
   ** --------------------------------------------------------------- */
    if (nin != 2) {
        mexErrMsgTxt("Exactly 2 input arguments required.");
    } else if (nout > 2) {
        mexErrMsgTxt("Too many output arguments.");
    }
    // get dimensions
    unsigned int TotalClusters = mxGetM(in[FrameIndicesPar]);
    unsigned int TotalFrames = mxGetN(in[FrameIndicesPar]);
    // read pointers
    double* Ratios = mxGetPr(in[RatiosPar]);
    double* FrameIndices = mxGetPr(in[FrameIndicesPar]);
    // create matrices
    long int Entries = TotalFrames * (TotalFrames - 1) / 2;
    out[EnergyPar] = mxCreateDoubleMatrix(1, 1, mxREAL);
    out[EnergyArrayPar] = mxCreateDoubleMatrix(3, Entries, mxREAL);
    double* EnergyArray = mxGetPr(out[EnergyArrayPar]);
    double* Energy = mxGetPr(out[EnergyPar]);

    // calculate indexing energy
    double Numerator = 0;
	double Denominator = 0;
    unsigned int Cursor = 0;
    for(int F1 = 0; F1 < (TotalFrames - 1); F1++){
        long int F1d = F1 * TotalClusters;
        for(int F2 = F1 + 1; F2 < TotalFrames; F2++){
            long int F2d = F2 * TotalClusters;
            double Ratio = Ratios[F2 * TotalFrames + F1];
            double Top = 0;
            double Bot = 0;
            for(int Cc = 0; Cc < TotalClusters; Cc++){
                Top += (double)(Ratio) * FrameIndices[F1d + Cc] * FrameIndices[F2d + Cc];
                Bot += (double)(1 - Ratio) * FrameIndices[F1d + Cc] * FrameIndices[F2d + Cc];
                EnergyArray[Cursor] = Top;
                EnergyArray[Cursor + 1] = Bot;
				EnergyArray[Cursor + 2] = Ratio;
            }
            Numerator += Top;
            Denominator += Bot;
            Cursor += 3;
        }
    }
    Energy[0] = Numerator / (Denominator + EPS);
     
}
