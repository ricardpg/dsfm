function P3D = UVL_SFM_computeFeatureRadius(P3D, Eigen, Params)
ValidIndices = find(P3D.Status == 3);
TotalDimensions = size(P3D.Desc, 1);
DimensionMask = false(TotalDimensions, 1);
DimensionMask((TotalDimensions - Params.EigenDimensions + 1) : end) = true;
for Point = ValidIndices
    LocalIndices = P3D.DescIndex{Point};
    Descriptors = double(P3D.ViewDesc(:, LocalIndices)) / 2^15;
    Centroid = double(P3D.Desc(:, Point));
    Distances = std_euclidian(Centroid(DimensionMask), Descriptors(DimensionMask, :), Eigen.Variance(DimensionMask));
    switch lower(Params.RadiusType)
        case 'mean'
            P3D.FeatureRadius(Point) = mean(Distances) * Params.RadiusFactor;
        case 'max'
            P3D.FeatureRadius(Point) = max(Distances) * Params.RadiusFactor;
        case 'normal'
            [MuDistances, SigmaDistances] = normfit(Distances);
            P3D.FeatureRadius(Point) =  MuDistances + 2 * SigmaDistances;
        case 'hausdorff'
            Distances = std_euclidian(Descriptors(DimensionMask, :), Descriptors(DimensionMask, :), Eigen.Variance(DimensionMask));
            P3D.FeatureRadius(Point) = max(max(Distances)) / 2;
        otherwise
            error('Unknown radius method')
    end
end
