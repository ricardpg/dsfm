#include"mex.h"
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<assert.h>

/* convolve and transopose */

void
mexFunction(int nout, mxArray *out[], 
            int nin, const mxArray *in[])
{
  int feat1items, dimensions, ic1, ic2, dd;
  double* feat1array;
  double* variance;
  double* distarray;
  double* radius;
  double* InvVar;
  double dst;
  enum {feat1=0,var, rad} ;
  enum {dist=0, InvVarPar} ;
 

  /* ------------------------------------------------------------------
  **                                                Check the arguments
  ** --------------------------------------------------------------- */ 
  if (nin != 3) {
    mexErrMsgTxt("Exactly three input arguments required.");
  } else if (nout > 2) {
    mexErrMsgTxt("Too many output arguments.");
  }
  
  dimensions = mxGetM(in[feat1]);
  feat1items = mxGetN(in[feat1]);
  feat1array = mxGetPr(in[feat1]);
  variance = mxGetPr(in[var]);
  radius = mxGetPr(in[rad]);
  out[dist] = mxCreateDoubleMatrix(feat1items, feat1items, mxREAL) ;
  out[InvVarPar] = mxCreateDoubleMatrix(dimensions, 1, mxREAL) ;
  distarray = mxGetPr(out[dist]);  
  InvVar = mxGetPr(out[InvVarPar]);
  
//  double *InvVar = new double(dimensions);
  
  for(dd=0; dd < dimensions; dd++)
    InvVar[dd] = 1 / variance[dd];
    
  for(ic1 = 0; ic1 < feat1items - 1; ic1++){
	  int ic1dim = ic1 * dimensions;
      for(ic2 = ic1 + 1; ic2 < feat1items; ic2++){
		  int ic2dim = ic2 * dimensions;
          dst = 0;
          for(dd=0; dd<dimensions; dd++){
             dst += ((feat1array[ic1dim+dd] - feat1array[ic2dim+dd]) * (feat1array[ic1dim+dd] - feat1array[ic2dim+dd])) * InvVar[dd];
          }
//          distarray[ic2*feat1items+ic1] = (radius[ic1]+radius[ic2]) / sqrt(dst);
          distarray[ic2*feat1items+ic1] = sqrt(dst);
      }
 
  }
}
