function Dirs = UVL_SFM_getDirRecursive(Dirs)
%BasePath = pwd;
%Directory = [BasePath '\' Dirs{end}];
Directory = Dirs{end};
fprintf('Entering %s\n', Directory);
cd(Dirs{end})
Files = dir;
for FileIndex = 1 : numel(Files)
    if Files(FileIndex).isdir && (Files(FileIndex).name(1) ~= '.')
        Dirs{end + 1} = [Directory '\' Files(FileIndex).name];
        Dirs = UVL_SFM_getDirRecursive(Dirs);
    end
end
cd ..