% Set the path
path([cd '/0_3rdParty/Quaternions'], path)
path([cd '/3D'], path)
path([cd '/BundleAdjustment'], path)
path([cd '/Feature Extraction'], path)
path([cd '/FrontEnd'], path)
path([cd '/Interface'], path)
path([cd '/Matching'], path)
path([cd '/Misc'], path)
path([cd '/Matching'], path)
path([cd '/Mosaicing'], path)
path([cd '/Motion'], path)
path([cd '/0_3rdParty/msba'], path)
path(cd, path);
savepath



% Detect Matlab Version
MatVer = version('-release');
Major = str2double(MatVer(1:2)); 
if Major <= 13; % 13 for 6.5 or smaller and 20 for others
    error('MATLAB:UVL_SFM_initialize', 'This script is only for Matlab > 6.5.');
end



% Detect Platform
Arch = computer;
if strcmpi ( Arch, 'pcwin' ); Windows = 1; Nbit = 32;
elseif strcmpi ( Arch, 'pcwin64' ); Windows = 1; Nbit = 64;
else Windows = 0;
end



fprintf('\nCompiling the quaternions functions\n');
cd 0_3rdParty/Quaternions/
fprintf('\tCompiling quat2rotmatMx.cpp\n');
Cmd = 'mex -I. quat2rotmatMx.cpp quaternions.cpp';
fprintf(['\t' Cmd '\n']);
eval ( Cmd );
fprintf('\tCompiling rotmat2quatMx.cpp\n');
Cmd = 'mex -I. rotmat2quatMx.cpp quaternions.cpp';
fprintf(['\t' Cmd '\n']);
eval ( Cmd );
cd ../..



fprintf('\nCompiling the Monocular Sparse Bundle Adjustment\n');
if ~(exist('0_3rdParty/sba/sba.lib', 'file') | exist('0_3rdParty/sba/libsba.a', 'file')),
    fprintf('You should have first compiled the SBA library !\n');
    fprintf('Open a command window with the environmental variables of your compiler,\n');
    fprintf('enter into the folder: DSFM/0_3rdParty/sba/\n');
    fprintf('and execute:\n');
    fprintf('    nmake /f Makefile.vc32    (For Windows 32 Bit Users)\n');
    fprintf('    nmake /f Makefile.vc64    (For Windows 64 Bit Users)\n');
    fprintf('    make                      (For Linux Users)\n');
else
    cd 0_3rdParty/msba/
    SbaFolder = '../sba';
    SbaDemoFolder = [SbaFolder '/demo'];
    LibraryPath = [' -L' SbaFolder];
    if Windows;
        LapackFolder = '../clapack/';
        if Nbit == 32;
            LapackFolder = [ LapackFolder 'win32' ];
        elseif Nbit == 64;
            LapackFolder = [ LapackFolder 'x64' ];
        end
        LibraryPath = [ LibraryPath ' -L' LapackFolder ];
        lLapackLibrary = ' -lclapack -lblas -llibF77 -llibI77';
    else
        lLapackLibrary = ' -llapack';
    end
    fprintf('\tCompiling msbaiMx.cpp\n');
    Cmd = ['mex -I' SbaFolder ' -I' SbaDemoFolder LibraryPath ' -lsba ' lLapackLibrary ' msbaiMx.cpp msbaChecks.cpp errorMx.cpp bin/msba.cpp ' SbaDemoFolder '/imgproj.c'];
    fprintf(['\t' Cmd '\n']);
    eval ( Cmd );
    cd ../..
end


fprintf('\nCompiling the Approximate Nearest Neighbor\n');
if ~(exist('0_3rdParty/ANN/ANN.lib', 'file') | exist('0_3rdParty/ANN/libANN.a', 'file')),
    fprintf('You should have first compiled the ANN library !\n');
    fprintf('Open a command window with the environmental variables of your compiler,\n');
    fprintf('enter into the folder: DSFM/0_3rdParty/ANN/\n');
    fprintf('and execute:\n');
    fprintf('    nmake /f Makefile.vc32    (For Windows 32 Bit Users)\n');
    fprintf('    nmake /f Makefile.vc64    (For Windows 64 Bit Users)\n');
    fprintf('    make linux-g++            (For Other Users)\n');
else
    cd Matching/
    ANNFolder = '../0_3rdParty/ANN';
    fprintf('\tCompiling UVL_SFM_approximateNearestNeighbour.cpp\n');
    Cmd = ['mex -I' ANNFolder ' -L' ANNFolder ' -lANN UVL_SFM_approximateNearestNeighbour.cpp'];
    fprintf(['\t' Cmd '\n']);
    eval ( Cmd );
    cd ..
end



fprintf('\nCompiling DSFM functions\n');
cd Misc/
fprintf('\tCompiling UVL_SFM_EuclidianDistances.cpp\n');
Cmd = 'mex UVL_SFM_EuclidianDistances.cpp';
fprintf(['\t' Cmd '\n']);
eval ( Cmd );
cd ..
cd Matching/
fprintf('\tCompiling UVL_SFM_nearestNeighbour.cpp\n');
Cmd = 'mex UVL_SFM_nearestNeighbour.cpp';
fprintf(['\t' Cmd '\n']);
eval ( Cmd );
cd ..
cd Motion/
fprintf('\tCompiling UVL_SFM_computeResidualsHomography.cpp\n');
Cmd = 'mex UVL_SFM_computeResidualsHomography.cpp';
fprintf(['\t' Cmd '\n']);
eval ( Cmd );
cd ..
cd Mosaicing/
fprintf('\tCompiling index_triangles.cpp\n');
Cmd = 'mex index_triangles.cpp';
fprintf(['\t' Cmd '\n']);
eval ( Cmd );
cd ..
clear

