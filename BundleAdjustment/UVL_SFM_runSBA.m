function [P3D View Update] = UVL_SFM_runSBA(P3D, View, K, optimization_type, Range)


if ~exist('optimization_type', 'var'), optimization_type = 1; end
if ~exist('Range', 'var'), Range = 1:numel(View); end

Range = Range([View(Range).PoseComputed]);
Cameras = zeros(7, numel(Range));
for camIndex = 1:numel(Range)
    Cameras(1:4,camIndex) = rotmat2quatMx(View(Range(camIndex)).Pose(1:3, 1:3));
    Cameras(5:7,camIndex) = View(Range(camIndex)).Pose(1:3, 4);
end

PointIndices = [];
NumP3D = 0;
PIndices = [];
Points = cell(0,0);
Update = [];

for PType = 1 : numel(P3D)
    PIndicesGroup = false(1, numel(P3D(PType).Status));
    PSequence = find(P3D(PType).Status == 3); %% has 3D position
    for Point = PSequence
        LocalIndices = P3D(PType).DescIndex{Point};
        ViewInP3D = ismembc2(Range, double(P3D(PType).ViewIndex(LocalIndices)));
        ViewInRange = find(ViewInP3D);
        ViewInP3D = ViewInP3D(ViewInRange);
        if numel(ViewInP3D) >= 2
            %Update = [Update [PType PSequence(Point)]'];
            NumP3D = NumP3D + 1;
            PIndicesGroup(Point) = true;
            Points{1, NumP3D} = P3D(PType).Pos(:, Point);
            PointCell = zeros(3, numel(ViewInP3D));
            for vv = 1:numel(ViewInP3D)
                LocalIndex = P3D(PType).DescIndex{Point}(ViewInP3D(vv));
                PointCell(:, vv) = [ViewInRange(vv); P3D(PType).ViewPos(:, LocalIndex)];
            end
            Points{2, NumP3D} = PointCell;
        end
    end
    PointIndices = [PointIndices PIndicesGroup];
end

Type = int8(optimization_type); % int8 [0:BA_MOTSTRUCT], 1:BA_MOT, 2:BA_STRUCT, 3:BA_MOT_MOTSTRUCT
AnalyticJac = true;             % bool [true]
InitialMu = 1e-3;               % double [1e-03]
TermThresh1 = 1e-12;            % double [1e-012]
TermThresh2 = 1e-12;            % double [1e-012]
MeanReprojecError = 1e-12;      % double [1e-012]
Expert = true;                  % bool [true]
if Range(1) == 1,
    NumConstFrames = 1;             % double [1]
else
    NumConstFrames = 2;             % double [1]
end
MaxIterations = 100;            % double [100]
tic;
[CamerasSBA, PSBA, Errors] = msbaiMx(K, Cameras, Points, Type, AnalyticJac, ...
                                     InitialMu, TermThresh1, TermThresh2, MeanReprojecError,...
                                     Expert, NumConstFrames, MaxIterations );
toc;
UVL_SFM_printOut(sprintf('\tSBA: Final Error %f (Init %f)\n', Errors(1), Errors(2)));

for vv = 1:numel(Range)
    View(Range(vv)).Pose = [quat2rotmatMx(CamerasSBA(1:4, vv)) CamerasSBA(5:7, vv); 0 0 0 1];
end
for PType = 1 : numel(P3D)
    GroupElements = numel(P3D(PType).Status);
    PIndicesGroup = logical(PointIndices(1:GroupElements));
    PointIndices = PointIndices(GroupElements+1:end);
    ActiveElements = sum(PIndicesGroup);
    P3D(PType).Pos(:, PIndicesGroup) = PSBA(:, 1:ActiveElements);
    PSBA = PSBA(:, ActiveElements + 1 : end);
end

fprintf( '\n' ) ;

