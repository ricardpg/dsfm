function cameraPoseSBA = UVL_SFM_adjustCameraDirectSBA(P, p, InitialView, K)

Cameras = [rotmat2quatMx(InitialView(1:3, 1:3)); InitialView(1:3, 4)];

Points = cell(2, size(P,2));
for pp = 1:size(P,2)
    Points{1, pp} = P(:,pp)';
    Points{2, pp} = [1; p(1:2,pp)];
end

Type = int8(1);                 % int8 [0:BA_MOTSTRUCT], 1:BA_MOT, 2:BA_STRUCT, 3:BA_MOT_MOTSTRUCT
AnalyticJac = true;             % bool [true]
InitialMu = 1e-3;               % double [1e-03]
TermThresh1 = 1e-12;            % double [1e-012]
TermThresh2 = 1e-12;            % double [1e-012]
MeanReprojecError = 1e-12;      % double [1e-012]
Expert = true;                  % bool [true]
NumConstFrames = 0;             % double [1]
MaxIterations = 100;            % double [100]
[CamerasSBA, PSBA, Errors] = msbaiMx(K, Cameras, Points, Type, AnalyticJac, ...
                             InitialMu, TermThresh1, TermThresh2, MeanReprojecError,...
                             Expert, NumConstFrames, MaxIterations );

%UVL_SFM_printOut(sprintf(' Final Error %f (Init %f)', Errors(1), Errors(2)));
                         
cameraPoseSBA = [quat2rotmatMx(CamerasSBA(1:4)) CamerasSBA(5:7); 0 0 0 1];








% %% write the instrinsic parameters file
% camCalibF = fopen('camCalib1.txt', 'w');
% fprintf(camCalibF, sprintf('%f\t%f\t%f\r\n%f\t%f\t%f\r\n%f\t%f\t%f\r\n',K(1,1),K(1,2),K(1,3),K(2,1),K(2,2),K(2,3),K(3,1),K(3,2),K(3,3)));
% fclose(camCalibF);
% 
% %% write the camera postion file
% camF = fopen('camMotion1.txt', 'w');
% R = InitialView(1:3, 1:3);
% Q = rotmat2quatMx(R);
% t = InitialView(1:3, 4);
% fprintf(camF, sprintf('%f\t%f\t%f\t%f\t%f\t%f\t%f\r\n', Q(1),Q(2),Q(3),Q(4),t(1),t(2),t(3)));
% fclose(camF);
% 
% %% write the points position file
% pntsF = fopen('pnts1.txt', 'w');
% for pp = 1:size(P,2)
% 	fprintf(pntsF, sprintf('%f\t%f\t%f\t%d',P(1,pp),P(2,pp),P(3,pp), 1));
%     fprintf(pntsF, sprintf('\t%d\t%f\t%f', 0, p(1,pp), p(2,pp)));
%     fprintf(pntsF, sprintf('\r\n'));
% end
% fclose(pntsF);
% %% run the SBA code
% BinaryFilePath = which('eucsbademo.exe');
% if isempty(BinaryFilePath)
%     error('MATLAB:adjustCameraPose', 'eucsbademo.exe not found in the path of matlab');
% end
% [status, result] = system(['"' BinaryFilePath '"' ' camMotion1.txt pnts1.txt camCalib1.txt 1 1 1 0']);
% %% recover the camera positions
% CameraTrajSBA = dlmread('motionParams.txt', ' ');
% R = quat2rotmatMx(CameraTrajSBA(1, 1:4));
% cameraPoseSBA = [R, CameraTrajSBA(1, 5:7)'];