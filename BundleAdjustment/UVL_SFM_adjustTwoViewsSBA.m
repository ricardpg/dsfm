function [PSBA Views_SBA FinalError] = UVL_SFM_adjustTwoViewsSBA(P, p, Views, K)

Cameras = zeros(7, numel(Views));
for camIndex = 1:numel(Views)
    camPose = Views{camIndex};
    R = camPose(1:3, 1:3);
    Q = rotmat2quatMx(camPose(1:3, 1:3));
    if any(isnan(Q)) % MODIF_HERE check why is this happening
        Q = rotmat2quatMx(rodrigues(rodrigues(R)));
    end
    Cameras(1:4,camIndex) = Q;
    Cameras(5:7,camIndex) = camPose(1:3, 4);
end;

Points = cell(2, size(P,2));
for pp = 1:size(P,2)
    Points{1, pp} = P(:,pp)';
    PointCell = zeros(3, numel(Views));
    for vv = 1:numel(Views)
        PointCell(:, vv) = [vv; p(vv*2-1:vv*2,pp)];
    end
    Points{2, pp} = PointCell;
end

Type = int8(0);                 % int8 [0:BA_MOTSTRUCT], 1:BA_MOT, 2:BA_STRUCT, 3:BA_MOT_MOTSTRUCT
AnalyticJac = true;             % bool [true]
InitialMu = 1e-3;               % double [1e-03]
TermThresh1 = 1e-12;            % double [1e-012]
TermThresh2 = 1e-12;            % double [1e-012]
MeanReprojecError = 1e-12;      % double [1e-012]
Expert = true;                  % bool [true]
NumConstFrames = 1;             % double [1]
MaxIterations = 100;            % double [100]
[CamerasSBA, PSBA, Errors] = msbaiMx(K, Cameras, Points, Type, AnalyticJac, ...
                             InitialMu, TermThresh1, TermThresh2, MeanReprojecError,...
                             Expert, NumConstFrames, MaxIterations );

UVL_SFM_printOut(sprintf('\tAdjust 2 views SBA: Final Error %f (Init %f)\n', Errors(1), Errors(2)));

FinalError = Errors(1);

Views_SBA = cell(1, numel(Views));
for camIndex = 1 : numel(Views)
    Views_SBA{camIndex} = [quat2rotmatMx(CamerasSBA(1:4, camIndex)) CamerasSBA(5:7, camIndex); 0 0 0 1];
end




% %fprintf('\tSBA Wrapping...')
% 
% %% write the instrinsic parameters file
% camCalibF = fopen('camCalib1.txt', 'w');
% fprintf(camCalibF, sprintf('%f\t%f\t%f\r\n%f\t%f\t%f\r\n%f\t%f\t%f\r\n',K(1,1),K(1,2),K(1,3),K(2,1),K(2,2),K(2,3),K(3,1),K(3,2),K(3,3)));
% fclose(camCalibF);
% %% write the camera postion file
% camF = fopen('camMotion1.txt', 'w');
% for camIndex = 1:size(Views,2)
%     cam_pose = Views{camIndex};
%     R = cam_pose(1:3, 1:3);
%     t = cam_pose(1:3, 4);
%     Q = rotmat2quatMx(R);
%     % MODIF_HERE check why is this happening
%     if any(isnan(Q))
%         Q = rotmat2quatMx(rodrigues(rodrigues(R)));
%     end
%         
%     fprintf(camF, sprintf('%f\t%f\t%f\t%f\t%f\t%f\t%f\r\n', Q(1),Q(2),Q(3),Q(4),t(1),t(2),t(3)));
% end;
% fclose(camF);
% %% write the points position file
% pntsF = fopen('pnts1.txt', 'w');
% for pp = 1:size(P,2)
%     fprintf(pntsF, sprintf('%f\t%f\t%f\t%d', P(1,pp),P(2, pp),P(3, pp), camIndex));
%     for vv = 1:size(Views,2)
%         fprintf(pntsF, sprintf('\t%d\t%f\t%f', vv - 1, p((vv-1)*2+1, pp), p((vv-1)*2+2, pp)));
%     end
%     fprintf(pntsF, sprintf('\r\n'));
% end
% fclose(pntsF);
% %% run the SBA code
% %fprintf(' Running...')
% BinaryFilePath = which('eucsbademo.exe');
% if isempty(BinaryFilePath)
%     error('MATLAB:adjust_two_views_SBA', 'eucsbademo.exe not found in the path of matlab');
% end
% %[method expert analytical_jacobian fixed_poses fixed_poses]
% [status, result] = system(['"' BinaryFilePath '"' ' camMotion1.txt pnts1.txt camCalib1.txt 0 1 1 1']);
% errs = sscanf(result, '%f %f');
% %fprintf(' Final Error %f (Init %f)\n', errs(1), errs(2));
% 
% %% recover the camera positions
% CamerasSBA = dlmread('motionParams.txt', ' ');
% Views_SBA = cell(1, numel(Views));
% for camIndex = 1 : numel(Views)
%     R = quat2rotmatMx(CamerasSBA(camIndex, 1:4));
%     t = CamerasSBA(camIndex, 5:7)';
%     Views_SBA{camIndex} = [R t; 0 0 0 1];
% end
% %% recover the 3D points
% PSBA = dlmread('structParams.txt', ' ');
% PSBA = PSBA';

            
        


