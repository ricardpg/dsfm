function P3D = UVL_SFM_adjust3DPointsSBA(P3D, View, ActiveViews, ActivePoints, K)

view_mapping = nan(1,numel(View));
view_mapping(ActiveViews) = 1 : numel(ActiveViews);

Cameras = zeros(7, numel(ActiveViews));
for camIndex = 1:numel(ActiveViews)
    Cameras(1:4,camIndex) = rotmat2quatMx(View(ActiveViews(camIndex)).Pose(1:3, 1:3));
    Cameras(5:7,camIndex) = View(ActiveViews(camIndex)).Pose(1:3, 4);
end

Points = cell(2,numel(ActivePoints));
for pp = 1 : numel(ActivePoints),
    P3DIndex = ActivePoints(pp);
    LocalIndex = P3D.DescIndex{P3DIndex};
    Points{1, pp} = P3D.Pos(:, P3DIndex)';
    PointCell = zeros(3, numel(LocalIndex));
    for vv = 1:numel(LocalIndex)
        PointCell(:, vv) = [view_mapping(P3D.ViewIndex(LocalIndex(vv))); P3D.ViewPos(:, LocalIndex(vv))];
    end
    Points{2, pp} = PointCell;
end

Type = int8(2);                 % int8 [0:BA_MOTSTRUCT], 1:BA_MOT, 2:BA_STRUCT, 3:BA_MOT_MOTSTRUCT
AnalyticJac = true;             % bool [true]
InitialMu = 1e-3;               % double [1e-03]
TermThresh1 = 1e-12;            % double [1e-012]
TermThresh2 = 1e-12;            % double [1e-012]
MeanReprojecError = 1e-12;      % double [1e-012]
Expert = true;                  % bool [true]
NumConstFrames = 1;             % double [1]
MaxIterations = 100;            % double [100]
[CamerasSBA, PSBA, Errors] = msbaiMx(K, Cameras, Points, Type, AnalyticJac, ...
                             InitialMu, TermThresh1, TermThresh2, MeanReprojecError,...
                             Expert, NumConstFrames, MaxIterations );

%UVL_SFM_printOut(sprintf(' Final Error %f (Init %f)', Errors(1), Errors(2)));

P3D.Pos(:, ActivePoints) = PSBA;            

      





















% view_mapping = nan(1,numel(View));
% view_mapping(ActiveViews) = 0 : (numel(ActiveViews) - 1);
% 
% %% write the instrinsic parameters file
% camCalibF = fopen('camCalib1.txt', 'w');
% fprintf(camCalibF, sprintf('%f\t%f\t%f\r\n%f\t%f\t%f\r\n%f\t%f\t%f\r\n',K(1,1),K(1,2),K(1,3),K(2,1),K(2,2),K(2,3),K(3,1),K(3,2),K(3,3)));
% fclose(camCalibF);
% %% write the camera postion file
% camF = fopen('camMotion1.txt', 'w');
% for camIndex = 1:numel(ActiveViews)
%     CamPose = View(ActiveViews(camIndex)).Pose;
%     R = CamPose(1:3, 1:3); %%% why is it the inverse rotation????
%     Q = rotmat2quatMx(R);
%     t = CamPose(1:3, 4);
%     fprintf(camF, sprintf('%f\t%f\t%f\t%f\t%f\t%f\t%f\r\n', Q(1),Q(2),Q(3),Q(4),t(1),t(2),t(3)));
% end;
% fclose(camF);
% %% write the points position file
% pntsF = fopen('pnts1.txt', 'w');
% for pp = 1:numel(P3D)
%     fprintf(pntsF, sprintf('%f\t%f\t%f\t%d',P3D(pp).Pos(1),P3D(pp).Pos(2),P3D(pp).Pos(3), numel(P3D(pp).View)));
%     for vv = 1:numel(P3D(pp).View)
%         p = P3D(pp).View(vv).Pos(1:2);
%         fprintf(pntsF, sprintf('\t%d\t%f\t%f', view_mapping(P3D(pp).View(vv).Index), p(1), p(2)));
%     end
%     fprintf(pntsF, sprintf('\r\n'));
% end
% fclose(pntsF);
% %% run the SBA code
% BinaryFilePath = which('eucsbademo.exe');
% if isempty(BinaryFilePath)
%     error('MATLAB:adjust3Dpoints', 'eucsbademo.exe not found in the path of matlab');
% end
% [status,result] = system(['"' BinaryFilePath '"' ' camMotion1.txt pnts1.txt camCalib1.txt 2']);
% %% recover the 3D points
% Points3DSBA = dlmread('structParams.txt', ' ');
% for pp = 1:numel(P3D)
%     P3D(pp).Pos = Points3DSBA(pp, :)';
% end
        


