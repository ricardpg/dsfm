/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Quaternion / Rotation Matrix generic routines.

  File          : quaternions.cpp
  Date          : 23/02/2007 - 01/10/2007

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cmath>

#include "quaternions.h"

void normquat ( double q[4] )
{
  double N = sqrt ( q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3] );

  q[0] /= N;
  q[1] /= N;
  q[2] /= N;
  q[3] /= N;
}

void normquat ( const double qs[4], double qd[4] )
{
  qd[0] = qs[0];
  qd[1] = qs[1];
  qd[2] = qs[2];
  qd[3] = qs[3];

  normquat ( qd );
}

void quat2rotmat ( const double q[4], double R[9] )
{
  // Salamin '79

  //     |  q02+qx2-qy2-qz2         -q0z+qxy          q0y+qxz |   | r11 r12 r13 |
  // R = |          q0z+qxy  q02-qx2+qy2-qz2         -q0x+qyz | = | r21 r22 r23 |
  //     |         -q0y+qxz          q0x+qyz  q02-qx2-qy2+qz2 |   | r31 r32 r33 |
  //
  double q0, qx, qy, qz;
  double q02, qx2, qy2, qz2;
  double q0x, q0y, q0z, qxy, qxz, qyz;
  double N, N2;

  // Get the quaternion
  q0 = q02 = q[0];
  qx = qx2 = q[1];
  qy = qy2 = q[2];
  qz = qz2 = q[3];

  // Compute squares
  q02 *= q02;
  qx2 *= qx2;
  qy2 *= qy2;
  qz2 *= qz2;

  // Normalize                            
  N = N2 = sqrt ( q02 + qx2 + qy2 + qz2 );
  N2 *= N2;

  q0 /= N;
  qx /= N;
  qy /= N;
  qz /= N;
  q02 /= N2;
  qx2 /= N2;
  qy2 /= N2;
  qz2 /= N2;

  // Terms for non diagonals
  q0x = 2.0 * q0 * qx;
  q0y = 2.0 * q0 * qy;
  q0z = 2.0 * q0 * qz;
  qxy = 2.0 * qx * qy;
  qxz = 2.0 * qx * qz;
  qyz = 2.0 * qy * qz;

  // Compute the rotation matrix
  R[_R11_] =  q02+qx2-qy2-qz2;
  R[_R12_] = -q0z+qxy;
  R[_R13_] =  q0y+qxz;
  R[_R21_] =  q0z+qxy;
  R[_R22_] =  q02-qx2+qy2-qz2;
  R[_R23_] = -q0x+qyz;
  R[_R31_] = -q0y+qxz;
  R[_R32_] =  q0x+qyz;
  R[_R33_] =  q02-qx2-qy2+qz2;
}

void rotmat2quat ( const double R[9], double q[4] )
{
/*
  tr=trace(R);
  w=sqrt((tr+1)/4);
  x=(R(3,2)-R(2,3))/4/w;
  y=(R(1,3)-R(3,1))/4/w;
  z=(R(2,1)-R(1,2))/4/w;
  q=[w x y z];
*/

  double Trace = R[_R11_] + R[_R22_] + R[_R33_];
  double w = sqrt ( ( Trace + 1.0 ) / 4.0 );

  q[0] = w;
  q[1] = ( R[_R32_] - R[_R23_] ) / ( w * 4.0 );
  q[2] = ( R[_R13_] - R[_R31_] ) / ( w * 4.0 );
  q[3] = ( R[_R21_] - R[_R12_] ) / ( w * 4.0 );
}

/*
void rotmat2quat ( const double R[9], double q[4] )
{
  // Salamin '79

  double R11, R12, R13, R21, R22, R23, R31, R32, R33;

  // Rotation Matrix
  R11 = R[_R11_]; R12 = R[_R12_]; R13 = R[_R13_];
  R21 = R[_R21_]; R22 = R[_R22_]; R23 = R[_R23_];
  R31 = R[_R31_]; R32 = R[_R32_]; R33 = R[_R33_];

  q[0] = ( 1.0 + R11 + R22 + R33 ) / 4.0;
  q[1] = ( 1.0 + R11 - R22 - R33 ) / 4.0;
  q[2] = ( 1.0 - R11 + R22 - R33 ) / 4.0;
  q[3] = ( 1.0 - R11 - R22 + R33 ) / 4.0;

  // Compute the maximum qi
  double MaxVal = q[0];
  int Index = 0;
  int i = 1;
  while ( MaxVal < ( 1.0 / 4.0 ) && i < 4 )
  {
    if ( q[i] > MaxVal )
    {
      MaxVal = q[i];
      Index = i;
    }

    i++;
  }

  double qi = sqrt ( MaxVal );
                                              
  switch ( Index )
  {
    case 0:
      q[0] = qi;
      q[1] = ( ( R32 - R23 ) / 4.0 ) / qi;
      q[2] = ( ( R13 - R31 ) / 4.0 ) / qi;
      q[3] = ( ( R21 - R12 ) / 4.0 ) / qi;
      break;

    case 1:
      q[0] = ( ( R32 - R23 ) / 4.0 ) / qi;
      q[1] = qi;
      q[2] = ( ( R12 + R21 ) / 4.0 ) / qi;
      q[3] = ( ( R13 + R31 ) / 4.0 ) / qi;
      break;

    case 2:
      q[0] = ( ( R13 - R31 ) / 4.0 ) / qi;
      q[1] = ( ( R12 + R21 ) / 4.0 ) / qi;
      q[2] = qi;
      q[3] = ( ( R23 + R32 ) / 4.0 ) / qi;
      break;

    case 3:
      q[0] = ( ( R21 - R12 ) / 4.0 ) / qi;
      q[1] = ( ( R13 + R31 ) / 4.0 ) / qi;
      q[2] = ( ( R23 + R32 ) / 4.0 ) / qi;
      q[3] = qi;
      break;

    default:
      break;
  }
}
*/

void quatprod ( const double q1[4], const double q2[4], double qd[4] )
{
  double q01, qx1, qy1, qz1, q02, qx2, qy2, qz2;

  q01 = q1[0]; qx1 = q1[1]; qy1 = q1[2]; qz1 = q1[3];
  q02 = q2[0]; qx2 = q2[1]; qy2 = q2[2]; qz2 = q2[3];

  qd[0] = q01 * q02 - qx1 * qx2 - qy1 * qy2 - qz1 * qz2;
  qd[1] = q01 * qx2 + qx1 * q02 + qy1 * qz2 - qz1 * qy2;
  qd[2] = q01 * qy2 - qx1 * qz2 + qy1 * q02 + qz1 * qx2;
  qd[3] = q01 * qz2 + qx1 * qy2 - qy1 * qx2 + qz1 * q02;
}
