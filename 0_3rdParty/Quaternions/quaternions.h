/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Quaternion / Rotation Matrix generic routines.

  File          : quaternions.h
  Date          : 23/02/2007 - 01/10/2007

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2006 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#ifndef __QUATERNIONS_H__
#define __QUATERNIONS_H__

/*
// Rotation Matrices Stored in Rows
#define _R11_ 0
#define _R12_ 1
#define _R13_ 2
#define _R21_ 3        //     | 0 1 2 |
#define _R22_ 4        // R = | 3 4 5 |
#define _R23_ 5        //     | 6 7 8 |
#define _R31_ 6
#define _R32_ 7
#define _R33_ 8
*/

// Rotation Matrices Stored in Columns (Matlab Convention)
#define _R11_ 0
#define _R12_ 3
#define _R13_ 6
#define _R21_ 1        //     | 0 3 6 |
#define _R22_ 4        // R = | 1 4 7 |
#define _R23_ 7        //     | 2 5 8 |
#define _R31_ 2
#define _R32_ 5
#define _R33_ 8

//! Normalize source/destination quaternion q.
void normquat ( double q[4] );

//! Normalize the source quaternion qs and write the result to qd.
void normquat ( const double qs[4], double qd[4] );

//! Convert quaternion q to a 3x3 Rotation Matrix R.
void quat2rotmat ( const double q[4], double R[9] );

//! Convert the 3x3 Rotation Matrix R to the quaternion q.
void rotmat2quat ( const double R[9], double q[4] );

//! Quaternion product of q1, q2 giving the result to qd.
void quatprod ( const double q1[4], const double q2[4], double qd[4] );

#endif
