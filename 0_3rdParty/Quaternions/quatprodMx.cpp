/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Quaternion / Product routine.

  File          : quatprodMx.cpp
  Date          : 01/10/2007 - 01/10/2007

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>            // Matlab Mex Functions

#include <quaternions.h>

#define ERROR_HEADER  "MATLAB:quatprodMx:Input\n"

// Identify the input parameters by it's index
enum { Q1 = 0, Q2 };
// Identify the output parameters by it's index
enum { Q = 0 };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 1 || nrhs != 2 )
    mexErrMsgTxt ( ERROR_HEADER "Usage: q = quatprodMx ( q1, q2 )" );

  // Get input lenghts
  int r1 = mxGetM ( prhs[Q1] );
  int c1 = mxGetN ( prhs[Q1] );  
  int r2 = mxGetM ( prhs[Q2] );
  int c2 = mxGetN ( prhs[Q2] );  

  if ( ( r1 != 1 || c1 != 4 ) && ( r1 != 4 || c1 != 1 ) ||
       ( r2 != 1 || c2 != 4 ) && ( r2 != 4 || c2 != 1 ) )
    mexErrMsgTxt ( ERROR_HEADER "Input vectors must be 1x4 or 4x1 quaternions!" );

  // Create Output Quaternion
  plhs[Q] = mxCreateDoubleMatrix ( 4, 1, mxREAL );

  // Get the pointers
  double *q1 = (double *)mxGetPr ( prhs[Q1] );
  double *q2 = (double *)mxGetPr ( prhs[Q2] );
  double *q = (double *)mxGetPr ( plhs[Q] );

  // Compute
  quatprod ( q1, q2, q );
}
