%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert Angles G, B and A to a Rotation Matrix R.
%
%  File          : FixedXYZ2RotMat.m
%  Date          : 08/11/2007 - 08/11/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - See RotMat2MobileRPY to use other conventions.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  FixedXYZ2RotMat Composition of 3 Rotation Angles G, B and A to obtain a
%                  3x3 Rotation Matrix.
%                  The used convention is Fixed X-Y-Z in Craig (pg 47):
%                       Rxyz(G, B, A) = Rz(A) * Ry(B) * Rx(G)
%                  So, first there is a Gamma rotation wrt. the X axis, then a Beta
%                  rotation wrt. the Y axis and, finally, an Alpha rotation wrt.
%                  the Z axis allways defined in the FIXED axis reference frame.
%
%                  This convention IS the convention in the Craig, pag. 47!
%
%     Input Parameters:
%      G, B, A: 3D Rigid Motion angles.
%
%     Output Parameters:
%      R: 3x3 Rotation Matrix.
%

function R = FixedXYZ2RotMat ( G, B, A )

%  >> G = sym ( 'G', 'real' );
%  >> B = sym ( 'B', 'real' );
%  >> A = sym ( 'A', 'real' );
%  >> Rx = [1 0 0 ; 0 cos(G) -sin(G); 0 sin(G) cos(G)];
%  >> Ry = [ cos(B) 0 sin(B); 0 1 0 ; -sin(B) 0 cos(B)];
%  >> Rz = [ cos(A) -sin(A) 0 ; sin(A) cos(A) 0; 0 0 1];
%  >> Rxyz = Rz * Ry * Rx
%
%   Rxyz =
%     [                       cos(A)*cos(B), -sin(A)*cos(G)+cos(A)*sin(B)*sin(G),  sin(A)*sin(G)+cos(A)*sin(B)*cos(G)]
%     [                       sin(A)*cos(B),  cos(A)*cos(G)+sin(A)*sin(B)*sin(G), -cos(A)*sin(G)+sin(A)*sin(B)*cos(G)]
%     [                             -sin(B),                       cos(B)*sin(G),                       cos(B)*cos(G)]

  R = RotZ ( A ) * RotY ( B ) * RotX ( G );
end
