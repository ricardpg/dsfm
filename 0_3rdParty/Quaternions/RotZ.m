function R = RotZ ( a )
  % Compute a Rotation Matrix around Z Axis of a Rads

  R = [  cos(a), -sin(a),       0;
         sin(a),  cos(a),       0;
              0,       0,       1 ];
end
