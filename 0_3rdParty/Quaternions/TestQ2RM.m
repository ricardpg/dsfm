n = 1000;

A=zeros(3,3,n);
AA=zeros(3,3,n);
qa=zeros(4,n);

Epsilon = 1e-8;
I = eye ( 3 );
for i = 1:n;
  NotOk = true;
  while NotOk;
    R = rand ( 3, 3 );
    An = R*(R'*R)^-0.5;

    % Check Det => +1
    NotOk = det ( An ) < 0;
  end
  
  c(1) = norm ( An(:,1) );
  c(2) = norm ( An(:,2) );
  c(3) = norm ( An(:,3) );
  c(4) = norm ( An(1,:) );
  c(5) = norm ( An(2,:) );
  c(6) = norm ( An(3,:) );

  cc = abs(c) - 1;
  ccc = cc > Epsilon;
  
  I1 = An * An';
  I2 = An' * An;
  
  i1 = norm ( I - I1, 'fro' );
  i2 = norm ( I - I2, 'fro' );
  
  if rank ( An ) ~= 3 || any ( ccc ) || i1 > Epsilon || i2 > Epsilon 
    s = sprintf ( 'Matriu %d no SO(3)!', i );
    warning ( 'MATLAB:TestQ2RM:Input', s );
%    An
  end

  qan = rotmat2quatMx ( An );
  AAn = quat2rotmatMx ( qan );

%  qan = rm2q ( An );
%  AAn = q2rm ( qan );
%  qan = R2quaternion ( An );
%  AAn = quaternion2R ( qan );

  qa(:,i) = qan;
  A(:,:,i) = An;
  AA(:,:,i) = AAn;
end

Ns = zeros ( 1, n );
for i = 1:n;
  Ns(i) = norm ( A(:,:,i) - AA(:,:,i), 'fro' );
end

find ( Ns > Epsilon )
