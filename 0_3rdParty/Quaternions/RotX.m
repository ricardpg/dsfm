function R = RotX ( a )
  % Compute a Rotation Matrix around X Axis of a Rads

  R = [ 1,       0,       0;
        0,  cos(a), -sin(a);
        0,  sin(a),  cos(a) ];
end
