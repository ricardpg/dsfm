%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Generate Matlab Mex Files.
%
%  File          : Compile.m
%  Date          : 23/02/2007 - 23/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Detect Platform
Arch = computer;
Arch = Arch(1:5);
if strcmpi ( Arch, 'pcwin' ); Windows = 1; else Windows = 0; end

QuaternionsFolder = '.';

disp ( sprintf ( 'Compiling quat2rotmatMx.cpp' ) );
Cmd = ['mex -I' QuaternionsFolder ' quat2rotmatMx.cpp ' QuaternionsFolder '/quaternions.cpp'];
disp ( sprintf ( ['\n' Cmd '\n' ] ) );
eval ( Cmd );

disp ( sprintf ( 'Compiling rotmat2quatMx.cpp' ) );
Cmd = ['mex -I' QuaternionsFolder ' rotmat2quatMx.cpp ' QuaternionsFolder '/quaternions.cpp'];
disp ( sprintf ( ['\n' Cmd '\n' ] ) );
eval ( Cmd );
