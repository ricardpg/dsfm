%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Conversion from Rotation Matrix to Unit Quaternion.
%
%  File          : rotmat2quad.m
%  Date          : 20/02/2007 - 23/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  rotmat2quad Convert a 3x3 Rotation Matrix R into a 1x4 Unit Quaternion
%              expresed as ( q0, qx, qy, qz )
%
%      R = rotmat2quad ( q )
%
%     Input Parameters:
%      R: 3x3 Rotation Matrix.
%
%     Output Parameters:
%      q: 1x4 Unit Quaternion vector in the form ( q0, qx, qy, zq ).
%
%  Extracted from: Byung-Uk Lee, Stereo Matching of Skull LandMarks, Ph.D. 
%                  Thesis, Standford. Univ., Standford, CA, 1991.
%

function q = rotmat2quat ( R )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check Size
  if all ( size ( R ) == [3 3] );
    q = rotmat2quatMx ( R );
  else
    error ( 'MATLAB:rotmat2quat:Input', ...
            'Input matrix must be a 3x3 rotation matrix!' );
  end
end
