/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Monocular Bundle Adjustement Wrapper for Matlab.

  File          : msbaiMx.cpp
  Date          : 16/01/2007 - 12/10/2007

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <ctime>            // STL: Time functions
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>

#include <mex.h>            // Matlab Mex Functions

#include <sba.h>            // Sparse Bundle Adjustment
#include <eucsbademo.h>

#include "bin/msba.h"       // Monocular Sparse Bundle Adjustment
#include "errorMx.h"        // To Print out Errors
#include "msbaChecks.h"     // Structure Checkings

#define CLOCKS_PER_MSEC (CLOCKS_PER_SEC/1000.0)
#define MSBA_MAX_REPROJ_ERROR 0.1  // Threshold to refine motion+structure after structure

// Common string to some output messages
#define ERROR_HEADER       "MATLAB:msbaiMx:Input\n"
#define CELL_CONTENT_MSG1  "{ (X,Y,Z) * 1 Time; (Frame Number, x_2D, y_2D) * T Times }!"

// Identify the input parameters by it's index
enum { CALIBRATION=0, CAMERAS, POINTS, TYPE, ANALYTICJAC, INITIALMU, TERMTHRESH1,
       TERMTHRESH2, MEANREPROJECERROR, EXPERT, NUMCONSTFRAMES, MAXITERATIONS };
// Identify the output parameters by it's index
enum { CAMERASR=0, POINTSR, ERRORS };


void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Constants
  const int pnp = 3;           // 3D Point number of elements (X, Y, Z)
  const int cnp = 7;           // Pose number of elements (q1, q2, q3, q4, X, Y, Z)
  const int mnp = 2;           // 2D Point number of elements (x, y)
  const int Verbose = 0;       // Debugging Output wanted?

  // Parameter checks
  if ( nlhs != 3 || nrhs != 12 )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [CamerasR, PointsR, Errors] = msbaiMx ( Calibration, Cameras, Points, Type, AnalyticJac,\n"
                   "                                               InitialMu, TermThresh1, TermThresh2, MeanReprojecError,\n"
                   "                                               Expert, NumConstFrames, MaxIterations )" );

  // Test the Camera Poses Input parameter
  if ( mxGetNumberOfDimensions ( prhs[CAMERAS] ) != 2 || !mxIsDouble ( prhs[CAMERAS] ) )
    mexErrMsgTxt ( ERROR_HEADER "Camera poses must be a 7xN double matrix!" );

  int nframes = mxGetN ( prhs[CAMERAS] );  // Number of cameras

  // Motion Parameters must be 7: q1, q2, q3, q4, X, Y, Z. Number of cameras must be >= 0
  if ( mxGetM ( prhs[CAMERAS] ) != cnp || nframes <= 0 )
    mexErrMsgTxt ( ERROR_HEADER "Camera poses must be a 7xN double matrix!" );

  // Test the Calibration Matrix input parameter
  if ( mxGetNumberOfDimensions ( prhs[CALIBRATION] ) != 2 || !mxIsDouble ( prhs[CALIBRATION] ) )
    mexErrMsgTxt ( ERROR_HEADER "Calibration matrix must be a 3x3 double matrix!" );

  // Calibration Matrix must be a 3x3 matrix.
  if ( mxGetM ( prhs[CALIBRATION] ) != 3 || mxGetN ( prhs[CALIBRATION] ) != 3 )
    mexErrMsgTxt ( ERROR_HEADER "Calibration matrix must be a 3x3 double matrix!" );

  double ical[9];           // Intrinsic Calibration Parameters

  // The data must be passed by rows for the minimization.
  // In matlab is stored by columns => Transpose
  double *IntPar = (double *)mxGetPr ( prhs[CALIBRATION] );
  ical[0] = *IntPar++;  ical[3] = *IntPar++;  ical[6] = *IntPar++;
  ical[1] = *IntPar++;  ical[4] = *IntPar++;  ical[7] = *IntPar++;
  ical[2] = *IntPar++;  ical[5] = *IntPar++;  ical[8] = *IntPar++;

  static char *HowToName[] = { "BA_MOTSTRUCT", "BA_MOT", "BA_STRUCT", "BA_MOT_MOTSTRUCT" };
  bool Expert, AnalyticJac;   // Expert mode
  char HowTo;                 // Select the minimization type
  int MaxIterations;          // Maximum number of iterations when optimizing
  int NumConstFrames;         // Number of constant camera poses (they will not be optimized)

  // Retrieve the Howto
  if ( mxGetNumberOfElements ( prhs[TYPE] ) != 1 || !mxIsInt8 ( prhs[TYPE] ) )
    mexErrMsgTxt ( ERROR_HEADER "Type must be a 8Bit integer scalar value!" );
  else        
    HowTo = *(unsigned char *)mxGetPr ( prhs[TYPE] );

  // Retrieve the expert mode
  if ( mxGetNumberOfElements ( prhs[EXPERT] ) != 1 || !mxIsLogical ( prhs[EXPERT] ) )
    mexErrMsgTxt ( ERROR_HEADER "Expert must be a logical scalar value!" );
  else        
    Expert = *(bool *)mxGetPr ( prhs[EXPERT] );

  // Retrieve the jacobian type
  if ( mxGetNumberOfElements ( prhs[ANALYTICJAC] ) != 1 || !mxIsLogical ( prhs[ANALYTICJAC] ) )
    mexErrMsgTxt ( ERROR_HEADER "AnalyticJac must be a logical scalar value!" );
  else        
    AnalyticJac = *(bool *)mxGetPr ( prhs[ANALYTICJAC] );

  // Retrieve the maximum number of iterations
  if ( mxGetNumberOfElements ( prhs[MAXITERATIONS] ) != 1 || !mxIsDouble ( prhs[MAXITERATIONS] ) )
    mexErrMsgTxt ( ERROR_HEADER "MaxIterations must be a scalar value!" );
  else        
    MaxIterations = static_cast<int> ( *(double *)mxGetPr ( prhs[MAXITERATIONS] ) );

  // Retrieve the maximum number of constant frames
  if ( mxGetNumberOfElements ( prhs[NUMCONSTFRAMES] ) != 1 || !mxIsDouble ( prhs[NUMCONSTFRAMES] ) )
    mexErrMsgTxt ( ERROR_HEADER "NumConstFrames must be a scalar value!" );
  else        
    NumConstFrames = static_cast<int> ( *(double *)mxGetPr ( prhs[NUMCONSTFRAMES] ) );
  
  double opts[SBA_OPTSSZ];  // Options

  // Retrieve the minimization options
  if ( mxGetNumberOfElements ( prhs[INITIALMU] ) != 1 || !mxIsDouble ( prhs[INITIALMU] ) )
    mexErrMsgTxt ( ERROR_HEADER "InitialMu must be a double scalar value!" );
  else        
    opts[0] = *(double *)mxGetPr ( prhs[INITIALMU] );

  if ( mxGetNumberOfElements ( prhs[TERMTHRESH1] ) != 1 || !mxIsDouble ( prhs[TERMTHRESH1] ) )
    mexErrMsgTxt ( ERROR_HEADER "TermThresh1 must be a double scalar value!" );
  else        
    opts[1] = *(double *)mxGetPr ( prhs[TERMTHRESH1] );

  if ( mxGetNumberOfElements ( prhs[TERMTHRESH2] ) != 1 || !mxIsDouble ( prhs[TERMTHRESH2] ) )
    mexErrMsgTxt ( ERROR_HEADER "TermThresh2 must be a double scalar value!" );
  else        
    opts[2] = *(double *)mxGetPr ( prhs[TERMTHRESH2] );

  if ( mxGetNumberOfElements ( prhs[MEANREPROJECERROR] ) != 1 || !mxIsDouble ( prhs[MEANREPROJECERROR] ) )
    mexErrMsgTxt ( ERROR_HEADER "MeanReprojecError must be a double scalar value!" );
  else        
    //opts[3] = 0.05 * numprojs;   // To force termination if the average reprojection error drops below 0.05
    opts[3] = *(double *)mxGetPr ( prhs[MEANREPROJECERROR] );

  // Number of 3D points
  int numpts3D = mxGetN ( prhs[POINTS] );

  // Compute the number of 3D points
  if ( mxGetM ( prhs[POINTS] ) != 2  || numpts3D <= 0 || !mxIsCell ( prhs[POINTS] ) )
    mexErrMsgTxt ( ERROR_HEADER "Points must be a cell-array containing on each column:\n"
                   CELL_CONTENT_MSG1 );
  
  mxArray *CurCell;      // Pointer to the current Cell
  int Rows, Columns;     // Current number of rows and columns
  double *Pd;            // Pointer to a double array
  char ErrorMsg[200];    // To print out error messages. Should be enought
  double *imgpts;        // Store the set of 2D points
  int nobs;              // Number of observations
  int i, j, k;

  // Check the cell array content before allocating memory
  checkPoints ( ERROR_HEADER, CELL_CONTENT_MSG1, prhs[POINTS], numpts3D, nframes );

  // Create the motion structure with the camera poses
  double *motstruct;      // The motion structure: (q1 q2 q3 q4 X, Y, Z), (X, Y, Z)
  char *vmask;            // The frame mask: (nframes x numpts3D) to define
                          // in which frames a 3D point is imaged

  double *PtMotstruct, *Ptimgpts;  // To scan data structures

  // Allocate space for camera poses and the set of 3D points (Variables to optimize)
  if ( ( motstruct = new double[nframes*cnp+numpts3D*pnp] ) == NULL )
    mexErrMsgTxt ( ERROR_HEADER "Not enought memory to store camera positions and 3D point structure!" );

  // Pointer to the source data
  double *Cameras = (double *)mxGetPr ( prhs[CAMERAS] );

  // To free memory later
  PtMotstruct = motstruct;
  
  // Fill the motion structure first with the camera poses
  for ( i = 0; i < nframes*cnp; i++ ) *PtMotstruct++ = *Cameras++;
  
  // Compute the number of 2D projections
  int numprojs = calcNum2DProj ( prhs[POINTS], numpts3D );

  // Allocate for the 2D set of points (Constraints)
  if ( ( imgpts = new double[numprojs*mnp] ) == NULL )
  {
    delete []motstruct;
    mexErrMsgTxt ( ERROR_HEADER "Not enought memory to store the 2D points structure!" );
  }

  // Allocate the 2D mask ( nframes x numpts3D )
  if ( ( vmask = new char[nframes*numpts3D] ) == NULL )
  {
    delete []motstruct;
    delete []imgpts;
    mexErrMsgTxt ( ERROR_HEADER "Not enought memory to store 3D Points vs Camera mask structure!" );
  }
  
  // Reset the mask
  for ( i = 0; i < nframes*numpts3D; i++ )
    vmask[i] = 0;

  // Count number of observations
  nobs = 0;

  // To free memory later
  Ptimgpts = imgpts;
  // Fill the 3D and 2D points.
  for ( i = 0; i < numpts3D; i++ )
  {
    // Read X, Y, Z
    CurCell = mxGetCell ( prhs[POINTS], i * 2 + 0 );
    Rows = mxGetM ( CurCell );
    Columns = mxGetN ( CurCell );
      
    Pd = (double *)mxGetPr ( CurCell );
    // Copy to the motion structure
    *PtMotstruct++ = *Pd++;
    *PtMotstruct++ = *Pd++;
    *PtMotstruct++ = *Pd++;

    // Read N, x, y
    CurCell = mxGetCell ( prhs[POINTS], i * 2 + 1 );
    Rows = mxGetM ( CurCell );
    Columns = mxGetN ( CurCell );

    // Current Cell Pointer
    Pd = (double *)mxGetPr ( CurCell );

    for ( j = 0; j < Columns; j++ )
    {
      // Get N
      k = static_cast<int> ( *Pd++ ) - 1;

      // Copy x, y
      *Ptimgpts++ = *Pd++;
      *Ptimgpts++ = *Pd++;

      // Unmask
      vmask[i*nframes+k] = 1;
      nobs++;
    }
  }
   
  double *motstruct_copy;       // Pointer to store a temporal copy used in 2-Step minimization
  double info[SBA_INFOSZ];      // To store minimization returning values
  double phi;                   // Store resulting error in 2-Step minimization
  int n;                        // Store minimization status
  clock_t start_time, end_time; // To compute the time

  // Number of variables
  int nvars = 0;

  switch ( HowTo )
  {
    case BA_MOTSTRUCT: 
    case BA_MOT_MOTSTRUCT: nvars = nframes * cnp + numpts3D * pnp; break;
    case BA_MOT:           nvars = nframes * cnp; break;
    case BA_STRUCT:        nvars = numpts3D * pnp; break;
    default: mexErrMsgTxt ( ERROR_HEADER "Unknown Bundle Adjustement method!" ); break;
  }

  // Observations
  nobs *= mnp;

  if ( nobs >= nvars )
  {
    // set up globs structure
    globs.intrcalib = ical;
    globs.ptparams = NULL;
    globs.camparams = NULL;

    start_time = clock ( );
    switch ( HowTo )
    {
      case BA_MOTSTRUCT: // BA for motion & structure
        if ( Expert )
          n = sba_motstr_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, mnp,
                                    img_projsRTS_x, (AnalyticJac) ? img_projsRTS_jac_x : NULL, (void *)(&globs),
                                    MaxIterations, Verbose, opts, info );
        else
          n = sba_motstr_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, mnp,
                                  img_projRTS, (AnalyticJac)? img_projRTS_jac : NULL, (void *)(&globs),
                                  MaxIterations, Verbose, opts, info );
      break;

      case BA_MOT: // BA for motion only
        globs.ptparams = motstruct + nframes * cnp;
        
        if ( Expert )
          n = sba_mot_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, mnp,
                                 img_projsRT_x, (AnalyticJac)? img_projsRT_jac_x : NULL, (void *)(&globs),
                                 MaxIterations, Verbose, opts, info );
        else
          n = sba_mot_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, mnp,
                               img_projRT, (AnalyticJac)? img_projRT_jac : NULL, (void *)(&globs),
                               MaxIterations, Verbose, opts, info );
      break;

      case BA_STRUCT: // BA for structure only
        globs.camparams = motstruct;
        if ( Expert )
          n = sba_str_levmar_x ( numpts3D, nframes, vmask, motstruct+nframes*cnp, pnp, imgpts, mnp,
                                 img_projsS_x, (AnalyticJac)? img_projsS_jac_x : NULL, (void *)(&globs),
                                 MaxIterations, Verbose, opts, info );
        else
          n = sba_str_levmar ( numpts3D, nframes, vmask, motstruct+nframes*cnp, pnp, imgpts, mnp,
                               img_projS, (AnalyticJac)? img_projS_jac : NULL, (void *)(&globs),
                               MaxIterations, Verbose, opts, info );
      break;

      case BA_MOT_MOTSTRUCT: // BA for motion only; if error too large, then BA for motion & structure
        if ( ( motstruct_copy = new double[nvars] ) == NULL )
        {
          delete []motstruct;
          delete []imgpts;
          delete []vmask;
          mexErrMsgTxt ( ERROR_HEADER "Memory allocation to copy motstruct failed!" );
        }

        memcpy ( motstruct_copy, motstruct, nvars * sizeof ( double ) ); // Save starting point for later use
        globs.ptparams = motstruct + nframes * cnp;
//        nvars = nframes * cnp;

        if ( Expert )
          n = sba_mot_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, mnp,
                                 img_projsRT_x, (AnalyticJac)? img_projsRT_jac_x : NULL, (void *)(&globs),
                                 MaxIterations, Verbose, opts, info );
        else
          n = sba_mot_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, imgpts, mnp,
                               img_projRT, (AnalyticJac)? img_projRT_jac : NULL, (void *)(&globs),
                               MaxIterations, Verbose, opts, info );

//        if ( ( phi = info[1] / numprojs ) > SBA_MAX_REPROJ_ERROR )
        if ( ( phi = info[1] / numprojs ) > MSBA_MAX_REPROJ_ERROR )
        {
          printfmexWarnMsgTxt ( ERROR_HEADER "Refining structure (motion only error %g)...\n", phi );

          memcpy ( motstruct, motstruct_copy, nvars * sizeof ( double ) ); // Reset starting point

          if ( Expert )
            n = sba_motstr_levmar_x ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, mnp,
                                      img_projsRTS_x, (AnalyticJac)? img_projsRTS_jac_x : NULL, (void *)(&globs),
                                      MaxIterations, Verbose, opts, info );
          else
            n = sba_motstr_levmar ( numpts3D, nframes, NumConstFrames, vmask, motstruct, cnp, pnp, imgpts, mnp,
                                    img_projRTS, (AnalyticJac)? img_projRTS_jac : NULL, (void *)(&globs),
                                    MaxIterations, Verbose, opts, info );
        }
        else
          printfmexWarnMsgTxt ( ERROR_HEADER "Only refined structure. Motion+structure was not done because: (phi = info[1] / numprojs = %g  >  SBA_MAX_REPROJ_ERROR %g)...\n",
                                phi, SBA_MAX_REPROJ_ERROR );

        delete []motstruct_copy;
      break;

      default:
        mexErrMsgTxt ( ERROR_HEADER "Unknown Bundle Adjustement method!" );
        break;
    }
    end_time = clock ( );

    if ( Verbose )
    {
      fprintf ( stderr, "SBA using %d 3D pts, %d frames and %d image projections, %d variables\n", numpts3D, nframes, numprojs, nvars);
      fprintf ( stderr, "\nMethod %s, %s driver, %s jacobian\n\n", HowToName[HowTo], Expert? "expert" : "simple", AnalyticJac ? "analytic" : "approximate");
      fprintf ( stderr, "SBA returned %d in %g iter, reason %g, error %g [initial %g], %d/%d func/fjac evals, %d lin. systems\n", n, info[5], info[6], info[1]/numprojs, info[0]/numprojs, (int)info[7], (int)info[8], (int)info[9]);
      fprintf ( stderr, "Elapsed time: %.2lf seconds, %.2lf msecs\n", ((double) (end_time - start_time)) / CLOCKS_PER_SEC,
                                                                      ((double) (end_time - start_time)) / CLOCKS_PER_MSEC);
    }
  }
  else
    printfmexWarnMsgTxt ( ERROR_HEADER "Cannot solve a problem with fewer measurements [%d] than unknowns [%d]!\n", nobs, nvars );

  // To free structure later
  PtMotstruct = motstruct;

  // Copy to the resulting matrices
  plhs[CAMERASR] = mxCreateDoubleMatrix ( cnp, nframes, mxREAL );
  Pd = (double *)mxGetPr ( plhs[CAMERASR] );
  for ( i = 0; i < nframes*cnp; i++ )
    *Pd++ = *PtMotstruct++;

  plhs[POINTSR] = mxCreateDoubleMatrix ( pnp, numpts3D, mxREAL );
  Pd = (double *)mxGetPr ( plhs[POINTSR] );
  for ( i = 0; i < numpts3D*pnp; i++ )
    *Pd++ = *PtMotstruct++;
  
  plhs[ERRORS] = mxCreateDoubleMatrix ( 1, 2, mxREAL );
  Pd = (double *)mxGetPr ( plhs[ERRORS] );
  *Pd++ = info[1]/numprojs;
  *Pd++ = info[0]/numprojs;

  // Release memory
  delete []motstruct;
  delete []imgpts;
  delete []vmask;

  return;
}
