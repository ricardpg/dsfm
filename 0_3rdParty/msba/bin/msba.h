#ifndef __MSBA_H__
#define __MSBA_H__

/* pointers to additional data, used for computed image projections and their jacobians */
extern struct globs_ {
  double *intrcalib; /* intrinsic callibration matrix in row-major storage */
        
  double *ptparams; /* needed only when bundle adjusting for camera parameters only */
  double *camparams; /* needed only when bundle adjusting for structure parameters only */
} globs;

void img_projRTS(int j, int i, double *aj, double *bi, double *xij, void *adata);
void img_projRTS_jac(int j, int i, double *aj, double *bi, double *Aij, double *Bij, void *adata);
void img_projRT(int j, int i, double *aj, double *xij, void *adata);
void img_projRT_jac(int j, int i, double *aj, double *Aij, void *adata);
void img_projS(int j, int i, double *bi, double *xij, void *adata);
void img_projS_jac(int j, int i, double *bi, double *Bij, void *adata);
void img_projsRTS_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata);
void img_projsRTS_jac_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *jac, void *adata);
void img_projsRT_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata);
void img_projsRT_jac_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *jac, void *adata);
void img_projsS_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *hx, void *adata);
void img_projsS_jac_x(double *p, struct sba_crsm *idxij, int *rcidxs, int *rcsubs, double *jac, void *adata);

#endif
