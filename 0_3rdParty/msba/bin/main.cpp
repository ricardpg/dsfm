/* Euclidean bundle adjustment demo using the sba package */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <sba.h>        // Sparse Bundle Adjustement

#ifdef __cplusplus
extern "C" {
#endif
  #include <eucsbademo.h>
#ifdef __cplusplus
}
#endif

#include "fileio.h"
#include "msba.h"       // Definitions

#define CLOCKS_PER_MSEC (CLOCKS_PER_SEC/1000.0)


/* Driver for sba_xxx_levmar */
void msba_driver ( char *camsfname, char *ptsfname, char *calibfname, char *finalcamera, char *final3dpoints, char *strnconstframes, char *strmaxiterations, int howto )
{
  double *motstruct, *motstruct_copy, *imgpts;
  double ical[9]; // intrinsic calibration params
  char *vmask;
  double opts[SBA_OPTSSZ], info[SBA_INFOSZ], phi;
//  int howto, 
  int expert, analyticjac, n, verbose=1;
  int nframes, numpts3D, numprojs, nvars;
  const int cnp=7, pnp=3, mnp=2;

  static char *howtoname[]={"BA_MOTSTRUCT", "BA_MOT", "BA_STRUCT", "BA_MOT_MOTSTRUCT"};

  clock_t start_time, end_time;

  int nconstframes = atoi ( strnconstframes );
  int MaxIterations = atoi ( strmaxiterations );

  readInitialSBAEstimate(camsfname, ptsfname, &nframes, &numpts3D, &numprojs, &motstruct, &imgpts, &vmask);

  //printSBAData(motstruct, nframes, numpts3D, imgpts, numprojs, vmask);

  /* set up globs structure */
  readCalibParams(calibfname, ical); 
  globs.intrcalib=ical;
  globs.ptparams=NULL;
  globs.camparams=NULL;

  /* call sparse LM routine */
  opts[0]=SBA_INIT_MU; opts[1]=SBA_TERMINATION_THRESH; opts[2]=SBA_TERMINATION_THRESH;
  opts[3]=SBA_TERMINATION_THRESH;
  //opts[3]=0.05*numprojs; // uncomment to force termination if the average reprojection error drops below 0.05

  /* Notice the various BA options demonstrated below */

  /* minimize motion & structure, motion only, o
   * motion and possibly motion & structure in a 2nd pass?
   */
  //howto=BA_MOTSTRUCT;
  //howto=BA_MOT;
  //howto=BA_STRUCT;
  //howto=BA_MOT_MOTSTRUCT;

  /* simple or expert drivers? */
  //expert=0;
  expert=1;

  /* analytic or approximate jacobian? */
  //analyticjac=0;
  analyticjac=1;

  start_time=clock();
  switch(howto){
    case BA_MOTSTRUCT: /* BA for motion & structure */
      nvars=nframes*cnp+numpts3D*pnp;
      if(expert)
        n=sba_motstr_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, mnp,
                            img_projsRTS_x, (analyticjac)? img_projsRTS_jac_x : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_motstr_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, mnp,
                            img_projRTS, (analyticjac)? img_projRTS_jac : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
    break;

    case BA_MOT: /* BA for motion only */
      globs.ptparams=motstruct+nframes*cnp;
      nvars=nframes*cnp;
      if(expert)
        n=sba_mot_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, mnp,
                          img_projsRT_x, (analyticjac)? img_projsRT_jac_x : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_mot_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, mnp,
                          img_projRT, (analyticjac)? img_projRT_jac : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
    break;

    case BA_STRUCT: /* BA for structure only */
      globs.camparams=motstruct;
      nvars=numpts3D*pnp;
      if(expert)
        n=sba_str_levmar_x(numpts3D, nframes, vmask, motstruct+nframes*cnp, pnp, imgpts, mnp,
                          img_projsS_x, (analyticjac)? img_projsS_jac_x : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_str_levmar(numpts3D, nframes, vmask, motstruct+nframes*cnp, pnp, imgpts, mnp,
                          img_projS, (analyticjac)? img_projS_jac : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
    break;

    case BA_MOT_MOTSTRUCT: /* BA for motion only; if error too large, then BA for motion & structure */
      if((motstruct_copy=(double *)malloc((nframes*cnp + numpts3D*pnp)*sizeof(double)))==NULL){
        fprintf(stderr, "memory allocation failed in readInitialSBAEstimate()\n");
        exit(1);
      }

      memcpy(motstruct_copy, motstruct, (nframes*cnp + numpts3D*pnp)*sizeof(double)); // save starting point for later use
      globs.ptparams=motstruct+nframes*cnp;
      nvars=nframes*cnp;

      if(expert)
        n=sba_mot_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, mnp,
                          img_projsRT_x, (analyticjac)? img_projsRT_jac_x : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
      else
        n=sba_mot_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, imgpts, mnp,
                        img_projRT, (analyticjac)? img_projRT_jac : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);

      if((phi=info[1]/numprojs)>SBA_MAX_REPROJ_ERROR){
        fflush(stdout); fprintf(stdout, "Refining structure (motion only error %g)...\n", phi); fflush(stdout);
        memcpy(motstruct, motstruct_copy, (nframes*cnp + numpts3D*pnp)*sizeof(double)); // reset starting point

        if(expert)
          n=sba_motstr_levmar_x(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, mnp,
                                img_projsRTS_x, (analyticjac)? img_projsRTS_jac_x : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
        else
          n=sba_motstr_levmar(numpts3D, nframes, nconstframes, vmask, motstruct, cnp, pnp, imgpts, mnp,
                              img_projRTS, (analyticjac)? img_projRTS_jac : NULL, (void *)(&globs), MaxIterations, verbose, opts, info);
      }
      free(motstruct_copy);

    break;

    default:
      fprintf(stderr, "unknown BA method \"%d\" in msba_driver()!\n", howto);
      exit(1);
  }
  end_time=clock();
  

  fflush(stdout);
  fprintf(stdout, "SBA using %d 3D pts, %d frames and %d image projections, %d variables\n", numpts3D, nframes, numprojs, nvars);
  fprintf(stdout, "\nMethod %s, %s driver, %s jacobian\n\n", howtoname[howto],
                  expert? "expert" : "simple",
                  analyticjac? "analytic" : "approximate");
  fprintf(stdout, "SBA returned %d in %g iter, reason %g, error %g [initial %g], %d/%d func/fjac evals, %d lin. systems\n", n, info[5], info[6], info[1]/numprojs, info[0]/numprojs, (int)info[7], (int)info[8], (int)info[9]);
  fprintf(stdout, "Elapsed time: %.2g seconds, %.2g msecs\n", ((double) (end_time - start_time)) / CLOCKS_PER_SEC,
                  ((double) (end_time - start_time)) / CLOCKS_PER_MSEC);
  fflush(stdout);

  /* refined motion and structure are now in motstruct */
  printSBAData ( motstruct, nframes, numpts3D, imgpts, numprojs, vmask );

  saveResults ( finalcamera, final3dpoints, nframes, numpts3D, motstruct );

  /* just in case... */
  globs.intrcalib=NULL;

  free(motstruct);
  free(imgpts);
  free(vmask);
}


int main(int argc, char *argv[])
{
  int Type;

  if ( argc != 8 && argc != 9 )
  {
    fprintf(stderr, "Usage:\n%s <Cameras Filename> <Points Filename> <Intrinsic Calibration Filename> <Final Cameras Filename> <Final 3D Filename> <Num. Const Frames> <Max. Iterations> [Type]\n", argv[0]);
    fprintf(stderr, "\nType must be: BA_MOTSTRUCT (default), BA_MOT, BA_STRUCT, BA_MOT_MOTSTRUCT\n" );

    exit ( -1 );
  }

  if ( argc == 8 )
    Type = BA_MOTSTRUCT;
  else
  {
    if ( !strcmp ( argv[7], "BA_MOTSTRUCT" ) ) Type = BA_MOTSTRUCT;
    if ( !strcmp ( argv[7], "BA_MOT" ) ) Type = BA_MOT;
    if ( !strcmp ( argv[7], "BA_STRUCT" ) ) Type = BA_STRUCT;
    if ( !strcmp ( argv[7], "BA_MOT_MOTSTRUCT" ) ) Type = BA_MOT_MOTSTRUCT;
  }

  msba_driver(argv[1], argv[2], argv[3], argv[4], argv[5], argv[6], argv[7], Type );

  return 0;
}
