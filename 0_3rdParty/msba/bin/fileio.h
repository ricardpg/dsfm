#ifndef __FILEIO_H__
#define __FILEIO_H__

#include <stdio.h>

bool saveResults ( const char *finalcamera, const char *final3dpoints, const int NumFrames, const int Num3DPoints, double *motstruct );

#endif
