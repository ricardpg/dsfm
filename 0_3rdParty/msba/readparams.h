/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Monocular Bundle Adjustement Wrapper for Matlab.

  File          : readparams.h
  Date          : 16/01/2007 - 13/02/2007

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2006-2007 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#ifndef __READPARAMS_H__
#define __READPARAMS_H__

/* Read the data from text files taking into account the error returning */

bool readInitialSBAEstimate ( char *camsfname, char *ptsfname, int *ncams, int *n3Dpts, int *n2Dprojs,
                              double **motstruct, double **imgpts, char **vmask );

bool readCalibParams ( char *fname, double ical[9] );
void printSBAData ( double *motstruct, int ncams, int n3Dpts, double *imgpts, int n2Dprojs, char *vmask );

#endif
