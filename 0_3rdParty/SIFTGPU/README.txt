A GPU implementation of David Lowe's Scale Invariant Feature Transform compiled on graphics cards 
by Changchang wu (http://cs.unc.edu/~ccwu) and interfaced with matlab by Adam Chapman (http://www.mace.manchester.ac.uk)

Matlab programs are inside the folder 

%\SIFTPGU\TestWin\src

A tutorial is provided in a word document titled 'SIFTGPU Matlab Tutorial'.






1. SIFT 

	SiftGPU is an implementation of SIFT for GPU. It does pyramid construction, keypoint 
	detection and descriptor generation on GPU. Not only does SiftGPU process pixels/features 
	paralelly with GPU, this implementation also reduces readback time by generating compact
	feature list with GPU algorithms

	SiftGPU borrows a lot from Andrea Vedaldi's sift++ . Many parameters of sift++ ( for 
	example, number of octaves,number of DOG levels, edge threshold, etc) are available 
	in SiftGPU. Shader programs are dynamically generated according to those parameters. 


2. Hardware 

	The entire functionality works fully only on hardware that supports cg profile fp40/vp40 
	or higher, for example,  nvidia 7900, 8800. If your GPU does not support fp40/vp40, 
	orientation computation of SIFT will be simplified, edge elimination will be ignored, 
	and descriptor will be ignored. 

	There are also some GPU parameters to play with. You can try to tune them to get the 
	best performance for you GPU. 


3. Environment

	Both VC6 workspace and VS2005 solution are provided: VC\SiftGPU.dsw and VC\SIftGPU.sln.
	Make sure you set necessary arguments when running the binaries in VC. Please use 
	Release build in VC to get better performance. 

	Linux makefile is now also provided.

4. Dependencies
	
	SiftGPU uses the CG (1.5 or 2.0), DevIl Image library, GLEW and GLUT.	

5. Notes
	By default, (0, 0) is the corner of top-left pixel instead of the center of that. Use 
	parameter "-loweo" to get the same coordinate system as Lowe's.

	SiftGPU need to allocate textures for storage initially or when any image that cannot
	fit in the allocated storage. It would be very effecient if you pre-resize all images 
	to a same dimension, and process them with one SiftGPU instance because memeory is 
	being resued. 

	When processing image sequence with varing sizes, you can try use the maximum size
	to allocate storage for all the images , or you can try multiple pyramids

	Loading some images (.e.g jpg) may take a lot of time on decompressing, 
	Please use binary pgm file instead of jpg to get better speed.

	SiftGPU may get slightly different result on different GPUs due to the difference in 
	floating point precision.  

	SiftGPU might be slow if your graphic card does not have enough memory. This is because 
	virtual memory is automatically used when physical memory is running out.

6. Helps 

	Use -help to get parameter information. Check /doc/manual.doc for samples and explanations. 
	In the vc workspace, there is a project called SimpleSIF that gives an example of simple 
	SiftGPU usage. There are more examples of different ways of using SiftGPU in manual.doc. 


	Check /doc/manual.doc for help on the viewer. 

