////////////////////////////////////////////////////////////////////////////
//	File:		SimpleSIFT.cpp
//	Author:		Changchang Wu
//	Description : A simple example shows how to use SiftGPU 
//
//
//
//	Copyright (c) 2007 University of North Carolina at Chapel Hill
//	All Rights Reserved
//
//	Permission to use, copy, modify and distribute this software and its
//	documentation for educational, research and non-profit purposes, without
//	fee, and without a written agreement is hereby granted, provided that the
//	above copyright notice and the following paragraph appear in all copies.
//	
//	The University of North Carolina at Chapel Hill make no representations
//	about the suitability of this software for any purpose. It is provided
//	'as is' without express or implied warranty. 
//
//	Please send BUG REPORTS to ccwu@cs.unc.edu
//
////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <vector>
using namespace std;

//SiftGPU library dynamically
#define SIFTGPU_DLL_DYNAMIC

#ifdef _WIN32

	#ifdef SIFTGPU_DLL_DYNAMIC
		#include <windows.h>
	#else
		//dll import definition for win32
		#define SIFTGPU_DLL
		#ifdef _DEBUG
			#pragma comment(lib, "../lib/siftgpu_d.lib")
		#else
			#pragma comment(lib, "../lib/siftgpu.lib")
		#endif
	#endif
#else
	#ifdef SIFTGPU_DLL_DYNAMIC
		#include <dlfcn.h>
	#endif
#endif

#include "../../SiftGPU/src/SiftGPU.h"


int main()
{
#ifdef SIFTGPU_DLL_DYNAMIC

	SiftGPU* (*pCreateNewSiftGPU)(int) = NULL;
	#ifdef _WIN32
		#ifdef _DEBUG
			HMODULE  hsiftgpu = LoadLibrary("siftgpu_d.dll");
		#else
			HMODULE  hsiftgpu = LoadLibrary("siftgpu.dll");
		#endif
		if(hsiftgpu == NULL) return 0;
		pCreateNewSiftGPU = (SiftGPU* (*) (int)) GetProcAddress(hsiftgpu, "CreateNewSiftGPU");
		if(pCreateNewSiftGPU == NULL) return 0;
	#else
		//
		void * hsiftgpu = dlopen("libsiftgpu.so", RTLD_LAZY);
		if(hsiftgpu == NULL) return 0;
		pCreateNewSiftGPU =  (SiftGPU* (*) (int)) dlsym(hsiftgpu, "CreateNewSiftGPU");
	#endif
	SiftGPU &sift = *pCreateNewSiftGPU(1);
#else
	SiftGPU sift;
#endif
	vector<float > descriptors;
	vector<SiftGPU::SiftKeypoint> keys;	
	int num;


	//process parameters
	char * argv[] = {"-m", "-s", "-v", "1"};
	//-m,    up to 2 orientations for each feature
	//-s     enable subpixel subscale
	//-v 1   will invoke calling SiftGPU::SetVerbose(1),(only print out # feature and overal time)
	//sift.EnableOutput(); SiftGPU now downloads result to CPU by default

	int argc = sizeof(argv)/sizeof(char*);
	sift.ParseParam(argc, argv);

	//create an OpenGL context for computation
	if(sift.CreateContextGL() != SiftGPU::SIFTGPU_FULL_SUPPORTED) return 0;
	//run sift on a set of images

	//example 1 , save result to file
	//the format is the same as Lowe's since version 236
	//each feature is Y, X, Scale, Orientation, Descriptor
	if(sift.RunSIFT("../data/800-1.jpg"))
	{
		sift.SaveSIFT("../data/800-1.sift");
	}

	//example 2, get feature vector
	if(sift.RunSIFT("../data/800-2.jpg"))
	{
		//get feature count
		num = sift.GetFeatureNum();

		//allocate memory
		keys.resize(num);	descriptors.resize(128*num);

		//read back a feature vector
		//faster than reading and writing files
		//if you dont need keys/descriptors, just put a NULL here
		sift.GetFeatureVector(&keys[0], &descriptors[0]);
		//this can be used to write your own binary sift file, which is faster
	}


#ifdef SIFTGPU_DLL_DYNAMIC
			delete &sift;
		#ifdef _WIN32
			FreeLibrary(hsiftgpu);
		#else
			dlclose(hsiftgpu);// linux
		#endif
#endif

	return 1;
}