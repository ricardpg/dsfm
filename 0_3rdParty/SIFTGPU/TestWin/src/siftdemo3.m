%% compile mysift, and load the images
mex -c mysift3.cpp
mex mysift3.cpp

% the name of your image should be entered in the line below
imname = 'Blue2.jpg';
im=imread(imname);
imgray = rgb2gray(im);

%% Try the grayscale version
[descriptors keys]=mysift3({'e', '10'}, imgray);

x=keys(1,:);
y=keys(2,:);
scale=keys(3,:);
ori=keys(4,:);

figure; imshow(imgray), hold on

plot(x,y,'ro');

hold off

%% Try the RGB data
[descriptors keys]=mysift3({'e', '10'}, im);

x=keys(1,:);
y=keys(2,:);
scale=keys(3,:);
ori=keys(4,:);

figure; imshow(im), hold on

plot(x,y,'ro');

hold off