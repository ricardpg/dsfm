#include <stdio.h>
#include <stdlib.h>
#include <vector>
using namespace std;

int main()
{
//save result. The format is same as Lowe�s since version 236
//each features is saved as (y, x, scale, orientation, descriptor)
	if(sift.RunSIFT("../data/1.jpg") )  //SiftGPU use DEVIL library to load images
	sift.SaveSIFT("../data/1.sift"); 
//save this ANSCII format of file might be slow,
//you can get the feature vector as follows and store it as binary yourself

	// second imag, get feature vector
	sift.RunSIFT("../data/2.jpg");
	 
//get feature count
	int num = sift.GetFeatureNum();

	//allocate memory
vector<float > descriptors;	vector<SiftGPU::SiftKeypoint> keys;	
keys.resize(num);	descriptors.resize(128*num);

	//read back keypoints and normalized descritpros
	//specify NULL if you don�t need keypionts or descriptors
	sift.GetFeatureVector(&keys[0], &descriptors[0]);
}