%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.es
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.es
%
%  Module        : Projective Homography Estimation.
%
%  File          : CalcHomoPrj.m
%  Date          : 29/07/2006 - 10/09/2006
%
%  Compiler      : MATLAB
%  Libraries     : -
%
%  Notes         : - File writen in ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2006 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CALCHOMOPRJ Estimate using Least-Squares the planar homography H that
%     specifies the warping between the set of 2D points P and the set of
%     2D points M.
%
%      H = CalcHomoPrj ( P, M )
%
%     Input Parameters:
%      P: A 2xK set of 2D points.
%      M: A 2xK set of 2D points.
%      Svd: Optional flag that if it is 1, Singular value decomposition is
%           used
%
%     Output Parameters:
%      H: A 3x3 planar Homography matrix that estimates the planar movement
%         from the P set of points to the M set of points.
%

function H = CalcHomoPrj ( P, M, Svd )

  % Check Input Output Arguments
  if ( nargin == 2 || nargin == 3 ) && nargout == 1

    % Compute according to the Projective Motion Model

    % |l�x'|   | a b c |   | x |
    % |l�y'| = | d e f | � | y |
    % | l  |   | g h 1 |   | 1 |

    % l�x' = a�x + b�y + c
    % l�y' = d�x + e�y + f
    % l    = g�x + h�y + 1

    % x' = a�x + b�y + c                 - x'�x�g - x'�y�h
    % y' =               + d�x + e�y + f - y'�x�g - y'�y�h

    % 4 Pairs point-matching (x,y) - (x',y') are needed at least

    %  |x1'|   | x1  y1  1  0   0   0  -x1'�x1  -x1'�y1 |
    %  |y1'|   | 0   0   0  x1  y1  1  -y1'�x1  -y1'�y1 |
    %  |x2'|   | x2  y2  1  0   0   0  -x2'�x2  -x2'�y2 |   | a |
    %  |y2'|   | 0   0   0  x2  y2  1  -y2'�x2  -y2'�y2 |   | b |
    %  |x3'|   | x3  y3  1  0   0   0  -x3'�x3  -x3'�y3 |   | c |
    %  |y3'| = | 0   0   0  x3  y3  1  -y3'�x3  -y3'�y3 | * | d |
    %  |x4'|   | x4  y4  1  0   0   0  -x4'�x4  -x4'�y4 |   | e |
    %  |y4'|   | 0   0   0  x4  y4  1  -y4'�x4  -y4'�y4 |   | f |
    %   ...                      ...                        | g |
    %   ...                      ...                        | h |
    %   ...                      ...
    %  |xn'|   | xn  yn  1  0   0   0  -xn'�xn  -xn'�yn |
    %  |yn'|   | 0   0   0  xn  yn  1  -yn'�xn  -yn'�yn |
    %

    % Compute Sizes of Input Data
    [pr pc] = size ( P );
    [mr nc] = size ( M );

    % Test the Input Matrices
    if ( pr == mr ) && ( pc == nc ) && ( mr == 2 )
      % Compute if SVD is wanted
      DoSvd = 0;
      if nargin == 3; if Svd ~= 0; DoSvd = 1; end; end;

      % Compute the b Vector: Matching vector
      b = M(:);

      % Compute Sub-Matrices that will allow to compute A
      z  = zeros ( 1, nc );
      o  = ones ( 1, nc );
      x2 = [P(1,:); P(1,:)];
      y2 = [P(2,:); P(2,:)];

      Xi  = [P(1,:); z];
      Yi  = [P(2,:); z];
      Os  = [o; z];
      Zxi = [z; P(1,:)];
      Zyi = [z; P(2,:)];
      Zs  = [z; o];
      Mx  = [M(:) .* x2(:)];
      My  = [M(:) .* y2(:)];

      % Compute the A Matrix
      A = [ Xi(:) Yi(:) Os(:) Zxi(:) Zyi(:) Zs(:) -Mx(:) -My(:) ];

      if DoSvd ~= 0
        % Solve using Singular Value Decomposition
        % [ U w V ] = svd ( Un )
        %
        % Un = U � w � V'
        %
        % SVD decomposition
        [U w V] = svd ( A, 'econ' );

        % Solve the system
        x = V * diag ( 1 ./ ( diag ( w ) + eps ) ) * U' * b;
      else
        % Solve the system using Least-Squares
        %
        %  A�x = b
        %  At�A�x = At�b
        %  x = (At�A)^-1 � At � b
        x = A \ b;
      end

      % Reshape to the Homography
      H = [ x(1) x(2) x(3);
            x(4) x(5) x(6);
            x(7) x(8) 1 ];
    else
      error ( 'MATLAB:CalcHomoPrj:Input', 'Matrix dimensions do not agree!' );
    end
  else
    error ( 'MATLAB:CalcHomoPrj:InputOutput', 'Usage: H = CalcHomoPrj ( P, M )' );
  end
end
