function edges = extract_dm_edges_sigma(dm, geometricalParameters)
sigma = geometricalParameters.InitialDerivativeSigma;
dm_sum = double(zeros(size(dm)));
dmg = zeros(size(dm));
for i = 1:geometricalParameters.SigmaSteps
    sigma = sigma * 1.4;
    g = fspecial('gaussian',max(1,fix(6*sigma)), sigma);
    dmg = dmg + conv2(dm, g, 'same');
%    dm_sum = dm_sum+dm_sd;
end
if geometricalParameters.SigmaSteps
    dmg = dmg/i;
    edges = secondDerivative(dmg, 2, 4);
else
    edges = secondDerivative(dm, 2, 4);
end
%edges(edges<1) = 0;

