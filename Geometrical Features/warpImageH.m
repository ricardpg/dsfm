function imgout = warpImageH(imgin, H)
[height width depth] = size(imgin);
T = maketform('projective',H');
[imgw,XDATA,YDATA] = imtransform(imgin,T,'bilinear', 'XYScale', 1);
origx = round(XDATA(1));
origy = round(YDATA(1));

imgout = imgw;
[heightw widthw d] = size(imgw);
if origx < 0
    imgout = [imgout(:,(abs(origx)+1):end,:) 0*ones(heightw,abs(origx),depth)];
end
if origx > 0
    imgout = [0*ones(heightw,origx,depth) imgout(:, 1:(widthw-origx), :)];
end
if origy < 0
    imgout = [imgout((abs(origy)+1):end,:,:); 0*ones(abs(origy),widthw, depth)];
end
if origy > 0
    imgout = [0*ones(origy,widthw,depth); imgout(1:(heightw-origy),:,:)];
end
if widthw<width
    imgout = [imgout 0*ones(heightw, width-widthw, depth)];
end
if widthw>width
    imgout = imgout(:, 1:width, :);
end
if heightw<height
    imgout = [imgout; 0*ones(height-heightw, width, depth)];
end
if heightw>height
    imgout = imgout(1:height, :, :);
end
%figure, imshow(imgw)
%figure, imshow(imgout)
    



