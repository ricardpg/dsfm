function I2=warp_flow_2D(I1,u_x,u_y,interp)

% function I2=warp_flow_2D(I1,u_x,u_y,interp)
% This function does the reverse warping for a given two dimensional
% optical flow. 
%I1 = float(I1);
I2=zeros(size(I1));
[M,N,K]=size(I2);
[x,y]=meshgrid(1:N,1:M);
outmat=zeros(M,N);

for j=1:K,
    inmat=I1(:,:,j);
    if (interp==0)
        xp=round(x+u_x);
        yp=round(y+u_y);
        index=find( (xp<=N) & (xp>=1) & (yp<=M) & (yp>=1) );
        outmat(y(index)+M*(x(index)-1))=inmat(yp(index)+M*(xp(index)-1));
    else
        xp=x+u_x+0.001;
        yp=y+u_y+0.001;
        xpl=floor(xp);
        ypl=floor(yp);  
        xpu=ceil(xp);
        ypu=ceil(yp);
        index=find( (xpu<=N) & (xpu>=1) & (ypu<=M) & (ypu>=1) & (xpl<=N) & (xpl>=1) & (ypl<=M) & (ypl >=1) );
        
        % fit an affine intensity map
        outmat(y(index)+M*(x(index)-1))=  inmat(ypl(index)+M*(xpl(index)-1))+ ...
            (xp(index)-xpl(index))./(xpu(index)-xpl(index)).*...
            (inmat(ypl(index)+M*(xpu(index)-1))-inmat(ypl(index)+M*(xpl(index)-1)))+ ...
            (yp(index)-ypl(index))./(ypu(index)-ypl(index)).* ...
            (inmat(ypu(index)+M*(xpl(index)-1))-inmat(ypl(index)+M*(xpl(index)-1)))+ ...
            (xp(index)-xpl(index))./(xpu(index)-xpl(index)).*(yp(index)-ypl(index))./(ypu(index)-ypl(index)).* ...
            (inmat(ypu(index)+M*(xpu(index)-1))-inmat(ypl(index)+M*(xpu(index)-1))-inmat(ypu(index)+M*(xpl(index)-1))+inmat(ypl(index)+M*(xpl(index)-1))); 
    end
    I2(:,:,j)=outmat;
end
I2(find(isnan(I2)))=0;
