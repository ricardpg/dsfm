function [second] = secondDerivative(depth_map, sigma1, sigma2)
[fx,fy]=gaussgradient(depth_map,sigma1);
%first = derivativeSign(fx) .* derivativeSign(fy) .* sqrt(fx.^2+fy.^2);
first = sqrt(fx.^2+fy.^2);
[sx,sy]=gaussgradient(first,sigma2);
%second = derivativeSign(sx) .* derivativeSign(sy) .* sqrt(sx.^2+sy.^2);
second = sqrt(sx.^2+sy.^2);

function sign = derivativeSign(derivative)
sign = (derivative>=0) - (derivative<0);