function P3D = is_geometrical(P3D, view1, view2, ffid, offset)
global width height cfg_file
init_data = read_cfg_data(cfg_file, '[Geometrical Parameters]');
eval(init_data);

if ~exist('offset', 'var')
    offset = 0;
end
feat = [];
P3D_in_views = [];
desc1 = [];
desc2 = [];
%figure, hold on
for pp = 1:numel(P3D)
    if P3D(pp).status == 1
        pos1 = find([P3D(pp).view(:).index]==view1);
        pos2 = find([P3D(pp).view(:).index]==view2);
        if numel(pos1) && numel(pos2)
            feat = [feat [P3D(pp).view(pos1).pos(1:2); P3D(pp).view(pos2).pos(1:2)]];
%            plot(P3D(pp).view(pos1).pos(1), P3D(pp).view(pos1).pos(2), 'bx')
%            plot(P3D(pp).view(pos2).pos(1), P3D(pp).view(pos2).pos(2), 'rx')
%            plot([P3D(pp).view(pos1).pos(1) P3D(pp).view(pos2).pos(1)], [P3D(pp).view(pos1).pos(2) P3D(pp).view(pos2).pos(2)], 'g-');
%            text(P3D(pp).view(pos1).pos(1), P3D(pp).view(pos1).pos(2), num2str(pp));
            P3D_in_views = [P3D_in_views pp];
            desc1 = [desc1 P3D(pp).view(pos1).desc];
            desc2 = [desc2 P3D(pp).view(pos2).desc];
        end
    end
end
H = CalcHomoPrj ( feat(1:2,:), feat(3:4,:), 1 );
im1 = data_read(ffid, view1+offset);
im2 = data_read(ffid, view2+offset);

im1w = warpImageH(im1, H);
overlapMask = warpImageH(255*ones(height, width), H);
%    mask = (mask(:,:,1)==255) & (mask(:,:,2)==0) & (mask(:,:,3)==0);
se = strel('disk',6);
overlapMask = imerode(overlapMask,se);
[ur,Cu,vr,Cv,um1,um2,um3]=optical_flow_plane(double(im2),double(im1w),7,5,3,5,'color_3', overlapMask);
residual_flow = sqrt(ur.^2 + vr.^2);
dist = std_euclidian(desc1, desc2, ones(size(desc1,1),1));
dist = 1 ./ diag(dist);
%figure, imshow(im2), hold on, plot(feat(3,:), feat(4,:), 'rx')
%figure, imagesc(residual_flow)
[combined_features, geom_feat] = get_geometrical_features_direct(residual_flow, feat(3:4,:), dist, overlapMask, geometricalParameters);
%figure, imagesc(im2), hold on, plot(geom_feat(:,1), geom_feat(:,2), 'rx')
for i = 1:size(combined_features,1)
    current_feature = combined_features(i,6);
    P3D(P3D_in_views(current_feature)).is_geometrical = true;
end