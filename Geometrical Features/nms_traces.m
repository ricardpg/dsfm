% use: [coordy, coordx, valors] =  nms(I,N,nms_disk,[threshold])
function [coordy, coordx, valors] =  nms_traces(I,N,nms_disk,threshold)
if (nargin<4)
    threshold = -1e1000;
end
[rows,cols] = size(I);
nms_size = size(nms_disk,1);
nms_radius = (nms_size-1)/2;
iters = rows*cols;
if (N > iters)
    K=I; valor=[];
    disp('La mida de valors buscats (N) es mes gran que la mida de la matriu');
else
    J=I;
    %nms_mask = zeros(2*nms_size+1, 2*nms_size+1);
    coordx=[];
    coordy=[];
    valors=[];
    old_progress = 0;
%    fprintf('   ');
    for i=1:N
%        progress = floor(i/N*100);
%        if(progress-old_progress>=1)
%            fprintf('\b\b\b%02d%s', progress, '%');
%            old_progress=progress;
%        end;

        valor = max(max(J));
        [y, x] = find(J==valor);
        x = x(1);
        y = y(1);
%        plot(x(1),y(1), 'rx')
        % check for out of bounds in applying the non-minimal suppresion
        % mask
        jx_min = max(1,x-nms_radius);
        jx_max = min(size(J,2),x+nms_radius);
        jy_min = max(1,y-nms_radius);
        jy_max = min(size(J,1),y+nms_radius);
        ttmp = zeros(size(I));
        ttmp(jy_min:jy_max,jx_min:jx_max) = J(jy_min:jy_max,jx_min:jx_max)>0;
        lbls = bwlabel(ttmp,8);
        curr_lbl = lbls(y,x);
        J(lbls==curr_lbl) = 0;
        % apply the nms mask
        
%        J(jy_min:jy_max,jx_min:jx_max) = nms_disk(wy_min:wy_max, wx_min:wx_max).*J(jy_min:jy_max,jx_min:jx_max);
        
        coordx = [coordx;x];
        coordy = [coordy;y];
        valors = [valors;valor];
        if valor < threshold
            break
        end
    end
end
good_values = find(valors >= threshold);
coordx = coordx(good_values);
coordy = coordy(good_values);
valors = valors(good_values);
t = 1;


