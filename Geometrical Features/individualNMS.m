function J = individualNMS(J,x,y,nms_disk)

nms_size = size(nms_disk,1);
nms_radius = (nms_size-1)/2;
wx_min = max(1,nms_radius-x+2);
wx_max = min(nms_size, nms_size-nms_radius-x+size(J,2));
wy_min = max(1, nms_radius-y+2);
wy_max = min(nms_size, nms_size-nms_radius-y+size(J,1));
% apply the nms mask
jx_min = max(1,x-nms_radius);
jx_max = min(size(J,2),x+nms_radius);
jy_min = max(1,y-nms_radius);
jy_max = min(size(J,1),y+nms_radius);
J(jy_min:jy_max,jx_min:jx_max) = nms_disk(wy_min:wy_max, wx_min:wx_max).*J(jy_min:jy_max,jx_min:jx_max);