function edges = extract_dm_edges(dm, geometricalParameters)
sigma = geometricalParameters.InitialDerivativeSigma;
dm_sum = double(zeros(size(dm)));
for i = 1:geometricalParameters.SigmaSteps
    sigma = sigma * 2;
    dm_sd = secondDerivative(dm, sigma, 1);
    dm_sum = dm_sum+dm_sd;
end
%edges = equalize(dm_sum);
edges = dm_sum / i;