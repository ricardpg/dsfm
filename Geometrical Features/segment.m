function mat = segment(mat,wsiz,thresh,pixNum);
%Syntax : mask = segment(image,window_size,threshold,pix_number);
%Binary regional segmentation.
%Input should be gray level.
%Threshold should be z score
%pix_num = is the object size to eliminate (removes speckle noise)
%I = double(imread('rice.png'));
%mask = segment(I,100,0.5,10);
%imagesc(I),figure,imagesc(mask)
[xs,ys] = size(mat);
avr = mat2avr(mat,wsiz);
STD = sqrt(mat2avr(mat.^2,wsiz)-avr.^2);
mat = (mat - avr)./STD;
mat = mat > thresh;
mat = bwareaopen(mat,pixNum,4);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mat = mat2avr(mat,num)
%syntax : avr = mat2avr(mat,num)
%finds the 2*num+1 pixel square window average for each scalar in the matrix
[xs,ys] = size(mat);
mat = [mat(:,num+1:-1:2,:,:) mat mat(:,ys-1:-1:ys-num,:,:)];
mat = [mat(num+1:-1:2,:,:,:); mat; mat(xs-1:-1:xs-num,:,:)];
mask = zeros(size(mat));
mask(1:2*num+1,1:2*num+1,:,:) = 1;
mat = real(ifft2(fft2(mat).*fft2(mask)))./[(2*num+1).^2];
mat = mat(2*num+1:end,2*num+1:end,:,:);
return