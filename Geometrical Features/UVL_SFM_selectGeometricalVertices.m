function P3D = UVL_SFM_selectGeometricalVertices(P3D, BaseView, CurrentView, ParamGeometrical, InputDataInfo, Display)
if ~exist('Display','var')
    Display = 0;
end
Corresp = [];
%figure, hold on
Range = [BaseView CurrentView];
for PType = 1 : numel(P3D)
    CurrentPMask = false(1, numel(P3D(PType).Status));
    BasePMask = false(1, numel(P3D(PType).Status));
    CurrentViewMask = P3D(PType).ViewIndex == CurrentView;
    BaseViewMask = P3D(PType).ViewIndex == BaseView;
    CurrentPMask(P3D(PType).ViewP3DIndex(CurrentViewMask)) = true;
    BasePMask(P3D(PType).ViewP3DIndex(BaseViewMask)) = true;
    InViews = find(CurrentPMask & BasePMask);
    for Point = InViews
        LocalIndices = P3D(PType).DescIndex{Point};
        ViewInP3D = ismembc2(Range, double(P3D(PType).ViewIndex(LocalIndices)));
        ViewInRange = find(ViewInP3D);
        ViewInP3D = ViewInP3D(ViewInRange);
        LocalIndexBase = P3D(PType).DescIndex{Point}(ViewInP3D(1));
        LocalIndexCurrent = P3D(PType).DescIndex{Point}(ViewInP3D(2));
        Corresp = [Corresp [P3D(PType).ViewPos(:, LocalIndexBase); P3D(PType).ViewPos(:, LocalIndexCurrent)]];
    end
end
Corresp = Corresp ./ ParamGeometrical.Subsampling;
if size(Corresp, 2) < 4
    Homography = eye(3);
else
    Homography = double(CalcHomoPrj ( Corresp(1:2,:), Corresp(3:4,:), 1 ));
end
ImBase = imresize(double(UVL_SFM_getInputStream(InputDataInfo, BaseView)), 1/ParamGeometrical.Subsampling);
ImCurrent = imresize(double(UVL_SFM_getInputStream(InputDataInfo, CurrentView)), 1/ParamGeometrical.Subsampling);
ImBaseWarped = warpImageH(ImBase, Homography);
[ImHeight, ImWidth, ImDepth] = size(ImBase);
OverlapMask = warpImageH(255*ones(ImHeight, ImWidth), Homography);
%    mask = (mask(:,:,1)==255) & (mask(:,:,2)==0) & (mask(:,:,3)==0);
SE = strel('disk',6);
OverlapMask = imerode(OverlapMask, SE);

[ur, Cu, vr, Cv]=optical_flow_plane( ImCurrent,ImBaseWarped, ...
                                     ParamGeometrical.WindowSize, ...
                                     ParamGeometrical.NumLevels, ...
                                     ParamGeometrical.MField, ...
                                     ParamGeometrical.Weight, ...
                                     ParamGeometrical.Method, OverlapMask ) ;
DepthMap = sqrt(ur.^2 + vr.^2);
GeometricFeatures = UVL_SFM_getGeometricalFeatures(DepthMap, OverlapMask, ParamGeometrical, Display);
GeometricFeatures = GeometricFeatures' * ParamGeometrical.Subsampling;
if Display
    figure, imagesc( DepthMap ) ; title( 'Approximated Depth Map' ) ;
    figure, imshow(ImCurrent/255), hold on, plot(GeometricFeatures(1,:), GeometricFeatures(2,:), 'kx')
end
%% fetch photometrical features
PhotometricFeatures = [];
Scores = [];
Indices = [];
for PType = 1 : numel(P3D)
    CurrentPMask = false(1, numel(P3D(PType).Status));
    CurrentViewMask = P3D(PType).ViewIndex == CurrentView;
    ViewPos = P3D(PType).ViewPos(:, CurrentViewMask);
    CurrentPMask(P3D(PType).ViewP3DIndex(CurrentViewMask)) = true;
    StatusMaskView = P3D(PType).Status(CurrentPMask) == 3;
    ViewPos = ViewPos(:, StatusMaskView);
    StatusMaskView = P3D(PType).Status == 3;
    P3DMask = StatusMaskView & CurrentPMask;
    PhotometricFeatures = [PhotometricFeatures ViewPos];
    Scores = [Scores P3D(PType).Cov(P3DMask)];
    TmpIndices = find(P3DMask);
    TmpGroup = ones(size(TmpIndices)) * PType;
    Indices = [Indices [TmpGroup; TmpIndices]];
end
%% combine features
Scores = double(normalizeData(1 ./ Scores));
Scores = repmat(Scores, [size(GeometricFeatures, 2) 1]);
Distances = std_euclidian(double(GeometricFeatures), double(PhotometricFeatures), [1 1]');
CandidateScores = Scores .* cos(pi/2 * Distances / ParamGeometrical.MaximumDistance);
ResolvedGeometric = false(1, size(GeometricFeatures, 2));
TakenPhotometric = false(1, size(PhotometricFeatures, 2));
while true
    MaxScore = max(max(CandidateScores));
    if MaxScore <= 0
        break;
    end 
    [BestG, BestP] = find(CandidateScores == MaxScore);
    TakenPhotometric(BestP) = true;
    CandidateScores(BestG, :) = 0;
    CandidateScores(:, BestP) = 0;
end
%% write back to P3D

Indices = Indices(:, TakenPhotometric);
fprintf('\t%d geometric features\n', size(Indices, 2));
for PType = 1:numel(P3D)
    GroupMask = Indices(1, :) == PType;
    P3DinGroup = Indices(2, GroupMask);
    P3D(PType).IsGeometrical(P3DinGroup) = 1;
end
    
    




