#include<math.h>
#include"mex.h"

double getNormal(int, int, unsigned char*, int, int);

void mexFunction (int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ){

        double *normals_image;
        int l, n, i, j, k=0, step, x, y;
        unsigned char *image;
        int width, height;

        /* parameter checks */
        if ((nrhs < 1) || (nlhs < 1)) {
                mexErrMsgTxt ("Usage: normals = lineNormals(image, width, height\n\n");
                return;
        }

        /* reading the parameters */
        width = mxGetN (prhs [0]);
        height = mxGetM (prhs [0]);
//        printf("Width %d, height %d \n",width,height);
        image = (unsigned char*) mxGetPr (prhs [0]);
//        width = mxGetScalar(prhs[1]);
//        height = mxGetScalar(prhs[2]);

        /* require memory for return */
        plhs [0] = mxCreateDoubleMatrix (height, width, mxREAL);
        normals_image = (double *) mxGetPr (plhs [0]);
        for(y=0;y<height;y++){
            for(x=0;x<width;x++){
                if(image[x*height+y]>0){
                    normals_image[x*height+y] = getNormal(x, y, image, width, height);
                }
                else
                {
                    normals_image[x*height+y] = 0.0;
                }
//                normals_image[y*width+x] = y;
//                normals_image[y*width+x] = 1.0;
            }
        }
}



double getNormal(int x, int y, unsigned char *im, int width, int height) 
{

// Get thetha angle from the thinned line structures instead of direction image.
  double u=0,v=0, aux;
  double angle;
//  PointEl p;
  int kern[4], i, j;

for (i=0;i<4;i++)  kern[i]=0;

  for (i=-1; i<=1; i++) 
    for (j=-1; j<=1; j++) {
//        if ((!im->outLimit(i+el->x,j+el->y,1)) && (im->data(i+el->x,j+el->y)>0)) {
        if(((y+i)>=0) & ((y+i)<height) & ((x+j)>=0) & ((x+j)<width) & im[(x+j)*height+y+i]){
//        if ((!im->outLimit(i+el->x,j+el->y,1)) && (im->data(i+el->x,j+el->y)>0)) {
//	    if (((i==0)&&(j==-1)) || ((i==0)&&(j==+1))) kern[0]++;
//	    if (((i==-1)&&(j==1)) || ((i==1)&&(j==-1))) kern[1]++;
//	    if (((i==-1)&&(j==0)) || ((i==1)&&(j==0))) kern[2]++;
//	    if (((i==-1)&&(j==-1)) || ((i==1)&&(j==1))) kern[3]++;
	    if (((j==0)&&(i==-1)) || ((j==0)&&(i==+1))) kern[0]++;
	    if (((j==-1)&&(i==1)) || ((j==1)&&(i==-1))) kern[1]++;
	    if (((j==-1)&&(i==0)) || ((j==1)&&(i==0))) kern[2]++;
	    if (((j==-1)&&(i==-1)) || ((j==1)&&(i==1))) kern[3]++;        
	}
  }
  //for (int i=0;i<4;i++)  cout << kern[i] << endl;
  //u=kern[0]*0+kern[2]*1+kern[3]*1;
  //v=kern[0]*1+kern[2]*0+kern[3]*1;
  //if (u==0) {
     //u+=kern[1]*(-1);
     //v+=kern[1]*1;
  //} else {
     //u+=kern[1]*1;
     //v+=kern[1]*(-1);
  //}
  // optimum version below (rob)

  u=kern[2]+kern[3];
  v=kern[0]+kern[3];
  if (u==0) {
     u+=-kern[1];
     v+=kern[1];
  } else {
     u+=kern[1];
     v+=-kern[1];
  }
  aux = u;
  u=-v;
  v=aux;
//  if (u!=0) angle = atan2(v,u);
//  else angle = 3.141592654/2.0; 
  if (u!=0) angle = atan(v/u);
  else angle = 3.141592654/2.0; 
  //angle = atan2(v,u);
  //if (angle>3.14) angle= angle - 3.141592654;
  return angle;
  // cout << "( "<< el->x << "," << el->y <<  ") angle " << angle*180.0/3.141592654<<endl;
}