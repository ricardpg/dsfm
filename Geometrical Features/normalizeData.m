function data = normalizeData(data)
data = data - min(min(data));
data = data / max(max(data));
