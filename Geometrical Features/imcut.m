function y=imcut(x,Margin);

y=zeros(size(x));

[M,N]=size(x);

y(1+Margin:M-Margin,1+Margin:N-Margin)=x(1+Margin:M-Margin,1+Margin:N-Margin);
