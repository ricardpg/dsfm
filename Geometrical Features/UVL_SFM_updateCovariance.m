function P3D = UVL_SFM_updateCovariance(P3D, View, K, Update)
Xsigma = 0.5;
%% update for all vertices
if ~exist('Update', 'var')
    Update = [];
    for PP = 1:numel(P3D)
        Pts = find(P3D(PP).Status == 3);
        Group = ones(1, numel(Pts)) * PP;
        Update = [Update [Group; Pts]];
    end
end
            
 for PP = 1:size(Update, 2)
    PType = Update(1, PP);
    PIndex = Update(2, PP);
    V = []; P = [];
    LocalIndices = P3D(PType).DescIndex{PIndex};
    for pp = 1 : numel(LocalIndices)
        V(pp).projmat = [K zeros(3,1)] * View(P3D(PType).ViewIndex(LocalIndices(pp))).Pose;
        P(pp).u = double(P3D(PType).ViewPos(1, LocalIndices(pp)));
        P(pp).v = double(P3D(PType).ViewPos(2, LocalIndices(pp)));
    end
    [dmy, PCov] = UVL_SFM_vertexCovariance(V, P, Xsigma, P3D(PType).Pos(:, PIndex));
    P3D(PType).Cov(PIndex) = single(max(diag(PCov)));
 end
