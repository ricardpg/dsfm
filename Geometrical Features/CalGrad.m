function [Ix,Iy]=CalGrad(I1,I2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Ix1,Iy1]=grad(I1);
[Ix2,Iy2]=grad(I2);
Ix=(Ix1+Ix2)/2;
Iy=(Iy1+Iy2)/2;
