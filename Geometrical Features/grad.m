function [Ex,Ey]=grad(img);

[M,N,K]=size(img);

% h=fspecial('gaussian',3,0.5);
% I1=my_filter2(h,I1);

Ax=[-1 0 1 ;
    -2 0 2 ;
    -1 0 1 ]/8;

Ay=[-1 -2 -1 ;
    0  0  0 ;
    1  2  1 ]/8;

for j=1:K,
    I1=img(:,:,j);
    
    tmpx=xcorr2(I1,Ax);
    tmpy=xcorr2(I1,Ay);
    
    tmpx=tmpx(2:M+1,2:N+1);
    tmpy=tmpy(2:M+1,2:N+1);
    
    Ex(:,:,j)=imcut(tmpx,2);
    Ey(:,:,j)=imcut(tmpy,2);
end
