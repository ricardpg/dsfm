function y=my_medfilt2(x1,wind);


[M,N,K]=size(x1);
y=zeros(size(x1));

r=wind(1);
c=wind(2);

r=(r-1)/2;
c=(c-1)/2;

for j=1:K,
    x=x1(:,:,j);
    
    for i=1:r
        xup(i,:)=x(1,:);
        xdown(i,:)=x(M,:);
    end
    
    if r,
        x=[ xup ; x ; xdown ];
    end
    
    for i=1:c
        xleft(:,i)=x(:,1);
        xright(:,i)=x(:,N);
    end
    
    if c,
        x=[ xleft x xright ];
    end
    
    y1=medfilt2(x,wind);
    
    y(:,:,j)=y1(r+1:M+r,c+1:N+c);
end
