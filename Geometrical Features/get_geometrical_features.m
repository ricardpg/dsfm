function [features geometricFeatures] = get_geometrical_features_direct(depth_map, photometricFeatures, matchScores, overlapMask, geometricalParameters)
global DEBUG_

if DEBUG_
    figure, imagesc(depth_map)
end

features = [];
kernel = [1 1 1;   %point classification kernel
          1 0 1;
          1 1 1];

node_kernel = [1  2 1;
               2 32 2;
               1  2 1];

%% edge extraction
edges = extract_dm_edges_sigma(depth_map, geometricalParameters);
%mat = segment(edges,25,.2,2000);
mat = segment(edges,geometricalParameters.SegmentationWindowSize,geometricalParameters.SegmentationThreshold,...
    geometricalParameters.SegmentationObjectElements);
se1 = strel('disk',3);
se2 = strel('disk',1);
matop = imclose(mat, se2);
matcl = imopen(matop,se1);
edge_trace = bwmorph(matcl,'thin',Inf);
edge_trace = edge_trace .* overlapMask;

% erase small objects
edge_labels = bwlabel(edge_trace);
for count = 1: max(max(edge_labels))
    curr_object = edge_labels == count;
    if sum(sum(curr_object))<geometricalParameters.MinimumEdgeElements
        edge_trace = xor(edge_trace, curr_object > 0);
    end
end
if DEBUG_
    figure, imagesc(edges)
    figure, imshow(edge_trace)
end

%% geometric feature extraction
disk = makeDisk(geometricalParameters.RegionSuppresion);
%%% label the objects
labels = bwlabel(edge_trace, 8);
if DEBUG_
    figure, imagesc(labels);
end
global edge_trace_shadow
%edge_trace_shadow = edge_trace;
%se = strel('disk',1);
curvatures = zeros(size(edge_trace));
normals = lineNormals(edge_trace);
line_endsx = [];
line_endsy = [];
line_juncx = [];
line_juncy = [];
shadow_image = ones(size(edge_trace));
edge_trace_shadow = edge_trace;
for count = 1:max(max(labels))
%    fprintf('Tracing object %d...\n',count)
    current_object = labels == count;
    %    [y,x] = find(labels == count);
    % classify points
    classified = conv2(double(current_object), kernel, 'same');    
    nodes = (classified > 2) & current_object; %% branching points
    %new_points = 1;
%    [y_nn, x_nn] = find(nodes); %% line ends
    
    nodes_labeled = bwlabel(nodes); %% label the branch nodes
    nodes_power = conv2(double(nodes), node_kernel, 'same');  % find the best node point
    %    figure, imagesc(nodes_power)
    x_node = []; y_node = [];
    %%% remove nodes
    current_object_no_nodes = current_object;
    for index = 1: max(max(nodes_labeled))
        %%% find the best branch node
%        node_branches = 0;
        curr_node_label = double(nodes_labeled == index);
        curr_node_power =  curr_node_label .* nodes_power;
        [ytmp xtmp] = find(curr_node_power == max(max(curr_node_power)));
        xtmp = xtmp(1); ytmp = ytmp(1);
        x_node = [x_node; xtmp]; y_node = [y_node; ytmp];
        current_object_no_nodes(ytmp, xtmp) = 0;
        %%% erase radius around nodes
        shadow_image = individualNMS(shadow_image,xtmp,ytmp,disk);
    end

    [y_end, x_end] = find((classified == 1) & current_object & shadow_image); %% line ends
    %%% erase radius around line ends
    for curr_end = 1:numel(x_end);
        shadow_image = individualNMS(shadow_image,x_end(curr_end),y_end(curr_end),disk);
    end
    %%% add it all to global list
    line_endsx = [line_endsx; x_end];
    line_endsy = [line_endsy; y_end];    
    line_juncx = [line_juncx; x_node]; 
    line_juncy = [line_juncy; y_node];
    
   
    %%% remove small branches --- > function remove_branches
    %%% create point to point distance LUT
    [ypix, xpix] = find(current_object_no_nodes); %% list of all pixels in the object
    total_pixels = length(xpix);
    distance_LUT = NaN(total_pixels,total_pixels);
    for curr_pix = 1:total_pixels
        for rel_pix = (curr_pix+1):total_pixels
            distance = sqrt((xpix(curr_pix)-xpix(rel_pix))^2+(ypix(curr_pix)-ypix(rel_pix))^2);
            distance_LUT(curr_pix, rel_pix) = distance;
            distance_LUT(rel_pix, curr_pix) = distance;
        end
    end
    %%% compute point curvatures
    for main_pixel_index = 1:total_pixels
        main_pix_x = xpix(main_pixel_index);
        main_pix_y = ypix(main_pixel_index);
        distances_tmp = distance_LUT;
        curvature_sum = 0;
        curvature_vect = [];
        neighbours = 0;
        %% follow both line directions
        for direction = 1:2
            curr_pixel_index = main_pixel_index;
            for i = 1:geometricalParameters.CurvatureSmoothing
                curr_dist_vect = distances_tmp(curr_pixel_index, :);
                candidate_neighbour_index = find(curr_dist_vect == min(curr_dist_vect));
                if numel(candidate_neighbour_index)
                    candidate_neighbour_index = candidate_neighbour_index(1);
                    candidate_distance = curr_dist_vect(candidate_neighbour_index);
                    %%% setting distances
                    distances_tmp(curr_pixel_index, candidate_neighbour_index) = NaN;
                    distances_tmp(candidate_neighbour_index, curr_pixel_index) = NaN;
                    if candidate_distance <= (sqrt(2) + eps) %%% immediate neighbour
                        neighbours = neighbours + 1;
                        curr_pixel_index = candidate_neighbour_index;
                        curr_pixel_x = xpix(candidate_neighbour_index);
                        curr_pixel_y = ypix(candidate_neighbour_index);
%                        curvature = exp(-distance_LUT(curr_pixel_index, main_pixel_index)^2)*...
%                            (1-abs(cos(normals(main_pix_y, main_pix_x)-normals(curr_pixel_y,curr_pixel_x))));
                        curvature = 1/distance_LUT(curr_pixel_index, main_pixel_index)*...
                            (1-abs(cos(normals(main_pix_y, main_pix_x)-normals(curr_pixel_y,curr_pixel_x))));
                        curvature_sum = curvature_sum + curvature;
                        curvature_vect = [curvature_vect curvature];
                    else
                        break
                    end
                else
                    break
                end
            end
        end
        % compute the mean and add it
        if neighbours > (geometricalParameters.CurvatureSmoothing*1.5)
            curvatures(main_pix_y, main_pix_x) = curvature_sum / neighbours;
        end
    end
end

%%% suppress regions around other features
curvatures_sup = curvatures .* shadow_image;
[curvy, curvx] =  nms_traces(curvatures_sup,600,disk,0.01);

%plot(curvx, curvy, 'rx');
%plot(line_endsx, line_endsy, 'yx');
%plot(line_juncx, line_juncy, 'gx');

geometricFeatures = [line_endsx line_endsy;
                     line_juncx line_juncy;
                     curvx      curvy];

%% combine geometric and photometric features
photometricFeatures = photometricFeatures'; 
featureTaken = zeros(1, size(photometricFeatures,1));
for count = 1:size(geometricFeatures,1)
    candidate_vector = [];
    x_feat = geometricFeatures(count,1);
    y_feat = geometricFeatures(count,2);
    for index = 1:size(photometricFeatures,1)
        if ~featureTaken(index)
            x_cand = photometricFeatures(index,1);
            y_cand = photometricFeatures(index,2);
            charact_score = matchScores(index);
            distance = sqrt((x_feat-x_cand)^2+(y_feat-y_cand)^2);
            if distance <= geometricalParameters.MaximumDistance
                score = charact_score * cos(-pi/2 * distance / geometricalParameters.MaximumDistance);
                candidate_vector = [candidate_vector; index score distance];
            end
        end
        
%    fprintf('Feature %d, candidate %d\n', count, index);
    end
    if numel(candidate_vector)
        best_candidate_pos = find(candidate_vector(:,2) == max(candidate_vector(:,2)));
        best_candidate_pos = best_candidate_pos(1);
%    if numel(best_candidate_pos)
        bestImgMatch = candidate_vector(best_candidate_pos,1);
        bestImgMatchScore = candidate_vector(best_candidate_pos,2);
        distance = candidate_vector(best_candidate_pos,3);
        features = [features; x_feat y_feat bestImgMatchScore photometricFeatures(bestImgMatch, 1:2) bestImgMatch distance];
        featureTaken(bestImgMatch) = 1;
    end      
end




