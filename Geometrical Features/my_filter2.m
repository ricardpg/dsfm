function y=my_filter2(h,x1);

[r,c]=size(h);
r=(r-1)/2;
c=(c-1)/2;
    
[M,N,K]=size(x1);
y=zeros(size(x1));

for j=1:K,
    x=x1(:,:,j);
    
    for i=1:r
        xup(i,:)=x(1,:);
        xdown(i,:)=x(M,:);
    end
    
    if exist('xup') & exist('xdown')
        x=[ xup ; x ; xdown ];
    end
    
    for i=1:c
        xleft(:,i)=x(:,1);
        xright(:,i)=x(:,N);
    end
    
    if exist('xleft') & exist('xright')
        x=[ xleft x xright ];
    end
    
    y(:,:,j)=filter2(h,x,'valid');
end
