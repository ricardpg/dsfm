function [u,Cu,v,Cv,um1,um2,um3]=UVL_SFM_opticalFlowPlane(img1,img2,ws,nlevels,mfield,weight,method, maskin)

h=1/256*[1 4 6 4 1; 4 16 24 16 4; 6 24 36 24 6; 4 16 24 16 4; 1 4 6 4 1];

param.ws=ws;
param.mfield=mfield;
param.weight=weight;
param.method=method;

image{1,1}=img1; img1=[];
image{1,2}=img2; img2=[];
mask{1,1}=maskin; 


for k=1:nlevels,
    image{k+1,1}=my_decimate(h,image{k,1});
    image{k+1,2}=my_decimate(h,image{k,2});
    mask{k+1,1}=my_decimate(h,mask{k,1});
end

sz=size(image{k+1,1});

u=zeros(sz(1:2));
Cu=ones(sz(1:2))+~mask{k+1,1}*1e+10;
v=zeros(sz(1:2));
Cv=ones(sz(1:2))+~mask{k+1,1}*1e+10;

for k=nlevels:-1:1,
%     disp(sprintf('level: %d',k))
    if k==nlevels,
        param.threshold=2;
    else
        param.threshold=5;
    end
    [u,v,Cu,Cv,um1,um2,um3]=Flow2D(image{k+1,1},image{k+1,2},2*u,2*v,Cu,Cv,param);

    sz=size(image{k,1});
    u=imresize(u,sz(1:2));
%     Cu=imresize(Cu,sz(1:2));
%     Cu(find(Cu==0))=median(Cu(find(Cu)));
    v=imresize(v,sz(1:2));
%     Cv=imresize(Cv,sz(1:2));
%     Cv(find(Cv==0))=median(Cv(find(Cv)));
    image{k,2}=warp_flow_2D(image{k,2},2*u,2*v,1);
end

% disp('level: 0')
param.threshold=5;
[u,v,Cu,Cv,um1,um2,um3]=Flow2D(image{1,1},image{1,2},2*u,2*v,Cu,Cv,param);

ws=5;
u=my_medfilt2(u,[ws ws]);
Cu=my_medfilt2(Cu,[ws ws]);
v=my_medfilt2(v,[ws ws]);
Cv=my_medfilt2(Cv,[ws ws]);
