function [u,v,Cu,Cv,um1,um2,um3]=Flow2D(I1,I2,u0,v0,Cu0,Cv0,param)

global FLAG;

ws=param.ws;
bound=param.threshold;
mfield=param.mfield;
method=param.method;

idx=find(I1==0 | I2==0);

% gradient calculation step
[Ix,Iy]=CalGrad(I1,I2);
Ix(idx)=0;
Iy(idx)=0;

% Calculate the new 'It' based on the previous estimates of flow u0 and v0
[It,mask]=CalIt(I1,I2, Ix,Iy, u0,v0);

% Set up the local averaging filters plus the gradient matrices
[Im,In,Ik]=size(I1);
if min(Im,In)<=ws+20,
    FLAG=1;
    h=[1];
else
    FLAG=0;
    if mod(ws,2)
        h=fspecial('average',[ws ws]);
    else
        h=zeros(ws+1);
        h(1:ws,1:ws)=fspecial('average',[ws ws]);
    end
end

% initial weight
wgt=CalWgt(Ix,Iy,Cu0,Cv0,ws);

% % 5/23/03, new idea: weighting the color channels based on the temporal
% % derivative
% for i=1:Ik,
%     ItIt1(:,:,i)=NormalEq(h,It(:,:,i),It(:,:,i),mask(:,:,i),ones(Im,In));
% end
% if size(ItIt1,1)==1
%     for i=1:Ik,
%         tmp(:,:,i)=ItIt1(:,:,i)*ones(Im,In);
%     end
%     ItIt1=tmp;
% end
% idx=find(ItIt1~=0);
% wgt(idx)=wgt(idx)./ItIt1(idx);

[N,U,ItIt]=GetNU(h,I1,Ix,Iy,It,wgt,mask);

% solving the equations
if Ik==1
    if strcmp(method,'gray_l')
        [u,Cu,v,Cv,um3]=lsqg(N,U,ItIt,mfield,ws,0,1);
    elseif strcmp(method,'gray_clg')
        [u,Cu,v,Cv,um3]=lsqg(N,U,ItIt,mfield,ws,10,50);
    end
    um1=[];
    um2=[];
else
    if strcmp(method,'color_3')
        [u,Cu,v,Cv,um1,um2,um3]=lsqc3(N,U,ItIt,mfield,ws);
    elseif strcmp(method,'color_2')
        [u,Cu,v,Cv,um1,um2,um3]=lsqc2(N,U,ItIt,mfield,ws);
    elseif strcmp(method,'color_1')
        [u,Cu,v,Cv,um1,um2,um3]=lsqc1(N,U,ItIt,mfield,ws);
    end
end

% bounding the solution
[u,v]=BoundSolution(u,u0,v,v0,bound,ws);

% % effect of old covariance on the new one
% Cu=my_medfilt2(Cu,[3 3]);
% Cu=(Cu0.*Cu);
% % Cu=Cu/max(Cu(:));
% Cv=my_medfilt2(Cv,[3 3]);
% Cv=(Cv0.*Cv);
% % Cv=Cv/max(Cv(:));

% u(idx)=0;
% Cu(idx)=1e20;
% v(idx)=0;
% Cv(idx)=1e20;
% if ~isempty(um1), um1(idx)=0; end
% if ~isempty(um2), um2(idx)=0; end
% if ~isempty(um3), um3(idx)=0; end


%%%%%%%%%%%%%%%%%%%
% functions section
%%%%%%%%%%%%%%%%%%%

function [u,Cu,v,Cv,um]=lsqg(nn,uu,ItIt,NoOfCoefficients,ws,alpha,maxiter)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if maxiter>1
    h_horn=[1 2 1;2 0 2;1 2 1]/12;
    u=zeros(size(ItIt));
    v=u;
    alpha2=alpha^2;
end

a2=nn(:,:,1); b2=nn(:,:,2); c2=nn(:,:,3);
              d2=nn(:,:,4); e2=nn(:,:,5);
                            f2=nn(:,:,6);

if maxiter>1
    a2=alpha2+a2;
    e2=alpha2+e2;
end

if NoOfCoefficients==3,
    % Calulate the determinant of the inversion
    detA=-a2.*d2.*f2+a2.*e2.^2+b2.^2.*f2-2*b2.*c2.*e2+c2.^2.*d2;
    detA(find(detA>=0 & detA<eps))=eps;
    detA(find(detA<0 & detA>-eps))=-eps;
    
    a1=(-d2.*f2+e2.^2)./detA;
    b1=(b2.*f2-c2.*e2)./detA;
    c1=-(b2.*e2-c2.*d2)./detA;
    d1=-(a2.*f2-c2.^2)./detA;
    e1=(a2.*e2-b2.*c2)./detA;
    f1=-(a2.*d2-b2.^2)./detA;
    
    % Calculate the flow and the multiplier field
    if maxiter==1
        u =a1.*uu(:,:,1)+b1.*uu(:,:,2)+c1.*uu(:,:,3);
        v =b1.*uu(:,:,1)+d1.*uu(:,:,2)+e1.*uu(:,:,3);
        um=c1.*uu(:,:,1)+e1.*uu(:,:,2)+f1.*uu(:,:,3);
    else
        error('you should modify this part')
        for niter=1:maxiter,
            ubar=my_filter2(h_horn,u);
            vbar=my_filter2(h_horn,v);
            
            u=a_1.*(-uu(:,:,1)+alpha2*ubar)+b_1.*(-uu(:,:,2)+alpha2*vbar)+c_1.*uu(:,:,3);
            v=d_1.*(-uu(:,:,1)+alpha2*ubar)+e_1.*(-uu(:,:,2)+alpha2*vbar)+f_1.*uu(:,:,3);
            um=g_1.*(-uu(:,:,1)+alpha2*ubar)+h_1.*(-uu(:,:,2)+alpha2*vbar)+i_1.*uu(:,:,3);
        end
    end
    
    t1= (u.*a2 + v.*b2 + um.*c2).*u +...
        (u.*b2 + v.*d2 + um.*e2).*v +...
        (u.*c2 + v.*e2 + um.*f2).*um;
    t2=-2*(u.*uu(:,:,1) + v.*uu(:,:,2) + um.*uu(:,:,3));
    t3=ItIt;
    
elseif NoOfCoefficients==2,
    % Calulate the determinant of the inversion
    detA=a2.*d2-b2.*b2;
    detA(find(detA>=0 & detA<eps))=eps;
    detA(find(detA<0 & detA>-eps))=-eps;
    
    a1=d2./detA;
    b1=-b2./detA;
    d1=a2./detA;
    
    % Calculate the flow and the multiplier field
    if maxiter==1
        u=a1.*uu(:,:,1)+b1.*uu(:,:,2);
        v=b1.*uu(:,:,1)+d1.*uu(:,:,2);
        um=[];
    else    
        error('you should modify this part')
        for niter=1:maxiter,
            ubar=my_filter2(h_horn,u);
            vbar=my_filter2(h_horn,v);
            
            u=a_1.*(-uu(:,:,1)+alpha2*ubar)+b_1.*(-uu(:,:,2)+alpha2*vbar);
            v=d_1.*(-uu(:,:,1)+alpha2*ubar)+e_1.*(-uu(:,:,2)+alpha2*vbar);
        end
    end
    
    t1= (u.*a2 + v.*b2).*u +...
        (u.*b2 + v.*d2).*v;
    t2=-2*(u.*uu(:,:,1) + v.*uu(:,:,2));
    t3=ItIt;
end

vf=(t1 + t2 + t3)/(ws*ws - NoOfCoefficients);
% disp(sprintf('min. of vf, a1, d1: (%f %f %f)',min(vf(:)),min(a1(:)),min(d1(:))))

temp=GetCondNumber(a1,b1,d1);
Cu=GetCov(temp,a1,vf);
Cv=GetCov(temp,d1,vf);


function [u,Cu,v,Cv,um1,um2,um3]=lsqc1(N,U,ItIt,mfield,ws)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% N=
% [ Ix1Ix1+Ix2Ix2+Ix3Ix3    Ix1Iy1+Ix2Iy2+Ix3Iy3  -I1Ix1 ]=[a b c]
% [                         Iy1Iy1+Iy2Iy2+Iy3Iy3  -I1Iy1 ]=[  d e]
% [                                                I1I1  ]=[    f]

a2=N(:,:,1); b2=N(:,:,2); c2=N(:,:,3);
d2=N(:,:,6); e2=N(:,:,7);
f2=N(:,:,10);

detA=-a2.*d2.*f2+a2.*e2.^2+b2.^2.*f2-2*b2.*c2.*e2+c2.^2.*d2;
tol=eps;
detA(find(detA>=0 & detA<tol))=tol;
detA(find(detA<0 & detA>-tol))=-tol;

% Normal matrix
a1=(-d2.*f2+e2.^2)./detA;
b1=(b2.*f2-c2.*e2)./detA;
c1=-(b2.*e2-c2.*d2)./detA;
d1=-(a2.*f2-c2.^2)./detA;
e1=(a2.*e2-b2.*c2)./detA;
f1=-(a2.*d2-b2.^2)./detA;

B1=U(:,:,1);
B2=U(:,:,2);
B3=U(:,:,3);

u  =a1.*B1+b1.*B2+c1.*B3;
v  =b1.*B1+d1.*B2+e1.*B3;
um1=c1.*B1+e1.*B2+f1.*B3;
um2=[];
um3=[];

t1= (u.*a2 + v.*b2 + um1.*c2).*u +...
    (u.*b2 + v.*d2 + um1.*e2).*v +...
    (u.*c2 + v.*e2 + um1.*f2).*um1;
t2=-2*(u.*B1 + v.*B2 + um1.*B3);
t3=ItIt;
vf=(t1+t2+t3)/(ws*ws-3);
%disp(sprintf('min. of vf, a1, d1: (%f %f %f)',min(vf(:)),min(a1(:)),min(d1(:))))

temp=GetCondNumber(a1,b1,d1);
Cu=GetCov(temp,a1,vf);
Cv=GetCov(temp,d1,vf);


function [u,Cu,v,Cv,um3,um1,um2]=lsqc2(N,U,ItIt,mfield,ws)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% N=
% [ Ix1Ix1+Ix2Ix2    Ix1Iy1+Ix2Iy2  -I1Ix1  -I2Ix2]=[a b c d]
% [                  Iy1Iy1+Iy2Iy2  -I1Iy1  -I2Iy2]=[  e f g]
% [                                  I1I1    0    ]=[    h 0]
% [                                          I2I2 ]=[      u]

a2=N(:,:,1); b2=N(:,:,2); c2=N(:,:,3); d2=N(:,:,4);
e2=N(:,:,5); f2=N(:,:,6); g2=N(:,:,7);
h2=N(:,:,8);
j2=N(:,:,9);

detA=-a2.*e2.*h2.*j2+a2.*f2.^2.*j2+a2.*g2.^2.*h2+b2.^2.*h2.*j2-2*b2.*f2.*c2.*j2-2*b2.*g2.*d2.*h2+e2.*c2.^2.*j2-c2.^2.*g2.^2+2*c2.*g2.*d2.*f2+...
    e2.*d2.^2.*h2-d2.^2.*f2.^2;
detA(find(detA>=0 & detA<eps))=eps;
detA(find(detA<0 & detA>-eps))=-eps;

% Normal matrix
a1=-(e2.*h2.*j2-f2.^2.*j2-g2.^2.*h2)./detA;
b1=(b2.*h2.*j2-f2.*c2.*j2-g2.*d2.*h2)./detA;
c1=-(b2.*f2.*j2-e2.*c2.*j2+c2.*g2.^2-g2.*d2.*f2)./detA;
d1=-(b2.*g2.*h2-e2.*d2.*h2-f2.*c2.*g2+d2.*f2.^2)./detA;
e1=-(a2.*h2.*j2-c2.^2.*j2-d2.^2.*h2)./detA;
f1=-(-a2.*f2.*j2+c2.*b2.*j2-c2.*d2.*g2+d2.^2.*f2)./detA;
g1=-(-a2.*g2.*h2+c2.^2.*g2+d2.*b2.*h2-d2.*c2.*f2)./detA;
h1=(-a2.*e2.*j2+a2.*g2.^2+b2.^2.*j2-2*b2.*d2.*g2+d2.^2.*e2)./detA;
i1=(-a2.*g2.*f2+b2.*c2.*g2+d2.*b2.*f2-d2.*c2.*e2)./detA;
j1=(-a2.*e2.*h2+a2.*f2.^2+b2.^2.*h2-2*b2.*c2.*f2+c2.^2.*e2)./detA;

B1=U(:,:,1);
B2=U(:,:,2);
B3=U(:,:,3);
B4=U(:,:,4);

u=+(a1.*B1+b1.*B2+c1.*B3+d1.*B4);
v=+(b1.*B1+e1.*B2+f1.*B3+g1.*B4);
um1=+(c1.*B1+f1.*B2+h1.*B3+i1.*B4);
um2=+(d1.*B1+g1.*B2+i1.*B3+j1.*B4);
um3=[];

t1=(u.*a2 + v.*b2 + um1.*c2 + um2.*d2).*u +...
    (u.*b2 + v.*e2 + um1.*f2 + um2.*g2).*v +...
    (u.*c2 + v.*f2 + um1.*h2 + um2.*0).*um1+...
    (u.*d2 + v.*g2 + um1.*0 + um2.*j2).*um2;
t2=-2*(u.*B1+v.*B2+um1.*B3+um2.*B4);
t3=ItIt; 
vf=(t1+t2+t3)/(2*ws*ws-4);
%disp(sprintf('min. of vf, a1, e1: (%f %f %f)',min(vf(:)),min(a1(:)),min(e1(:))))

temp=GetCondNumber(a1,b1,e1);
Cu=GetCov(temp,a1,vf);
Cv=GetCov(temp,e1,vf);


function [u,Cu,v,Cv,um1,um2,um3]=lsqc3(N,U,ItIt,mfield,ws)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ===============================================
% two cases can be examined:
% 1) all three channels have 'm' field: mfield=3;
% 2) first channel without 'm' field:   mfield=2;

if mfield==2,
    % N=
    % [ Ix1Ix1+Ix2Ix2+Ix3Ix3    Ix1Iy1+Ix2Iy2+Ix3Iy3  -I2Ix2  -I3Ix3]=[a b c d]
    % [                         Iy1Iy1+Iy2Iy2+Iy3Iy3  -I2Iy2  -I3Iy3]=[  e f g]
    % [                                                I2I2    0    ]=[    h 0]
    % [                                                        I3I3 ]=[      u]
    
    a2=N(:,:,1); b2=N(:,:,2); c2=N(:,:,4); d2=N(:,:,5);
                 e2=N(:,:,6); f2=N(:,:,8); g2=N(:,:,9);
                              h2=N(:,:,11);
                                           j2=N(:,:,12);
    
    detA=-a2.*e2.*h2.*j2+a2.*f2.^2.*j2+a2.*g2.^2.*h2+b2.^2.*h2.*j2-2*b2.*f2.*c2.*j2-2*b2.*g2.*d2.*h2+e2.*c2.^2.*j2-c2.^2.*g2.^2+2*c2.*g2.*d2.*f2+...
        e2.*d2.^2.*h2-d2.^2.*f2.^2;
    detA(find(detA>=0 & detA<eps))=eps;
    detA(find(detA<0 & detA>-eps))=-eps;
    
    % Normal matrix
    a1=-(e2.*h2.*j2-f2.^2.*j2-g2.^2.*h2)./detA;
    b1=(b2.*h2.*j2-f2.*c2.*j2-g2.*d2.*h2)./detA;
    c1=-(b2.*f2.*j2-e2.*c2.*j2+c2.*g2.^2-g2.*d2.*f2)./detA;
    d1=-(b2.*g2.*h2-e2.*d2.*h2-f2.*c2.*g2+d2.*f2.^2)./detA;
    e1=-(a2.*h2.*j2-c2.^2.*j2-d2.^2.*h2)./detA;
    f1=-(-a2.*f2.*j2+c2.*b2.*j2-c2.*d2.*g2+d2.^2.*f2)./detA;
    g1=-(-a2.*g2.*h2+c2.^2.*g2+d2.*b2.*h2-d2.*c2.*f2)./detA;
    h1=(-a2.*e2.*j2+a2.*g2.^2+b2.^2.*j2-2*b2.*d2.*g2+d2.^2.*e2)./detA;
    i1=(-a2.*g2.*f2+b2.*c2.*g2+d2.*b2.*f2-d2.*c2.*e2)./detA;
    j1=(-a2.*e2.*h2+a2.*f2.^2+b2.^2.*h2-2*b2.*c2.*f2+c2.^2.*e2)./detA;

    B1=U(:,:,1);
    B2=U(:,:,2);
    B3=U(:,:,4);
    B4=U(:,:,5);
    
    u=+(a1.*B1+b1.*B2+c1.*B3+d1.*B4);
    v=+(b1.*B1+e1.*B2+f1.*B3+g1.*B4);
    um1=[];
    um2=+(c1.*B1+f1.*B2+h1.*B3+i1.*B4);
    um3=+(d1.*B1+g1.*B2+i1.*B3+j1.*B4);
    
    t1=(u.*a2 + v.*b2 + um2.*c2 + um3.*d2).*u +...
       (u.*b2 + v.*e2 + um2.*f2 + um3.*g2).*v +...
       (u.*c2 + v.*f2 + um2.*h2 + um3.*0).*um2+...
       (u.*d2 + v.*g2 + um2.*0 + um3.*j2).*um3;
    t2=-2*(u.*B1+v.*B2+um2.*B3+um3.*B4);
    t3=ItIt; 
    vf=(t1+t2+t3)/(3*ws*ws-4);
%    disp(sprintf('min. of vf, a1, e1: (%f %f %f)',min(vf(:)),min(a1(:)),min(e1(:))))
    
    temp=GetCondNumber(a1,b1,e1);
    Cu=GetCov(temp,a1,vf);
    Cv=GetCov(temp,e1,vf);
else
    % N=
    % [ Ix1Ix1+Ix2Ix2+Ix3Ix3    Ix1Iy1+Ix2Iy2+Ix3Iy3  -I1Ix1 -I2Ix2  -I3Ix3]=[a b c d e]
    % [                         Iy1Iy1+Iy2Iy2+Iy3Iy3  -I1Iy1 -I2Iy2  -I3Iy3]=[  f g h u]
    % [                                                I1I1   0       0    ]=[    v 0 0]
    % [                                                       I2I2    0    ]=[      w 0]
    % [                                                               I3I3 ]=[        z]
    
    a2=N(:,:,1); b2=N(:,:,2); c2=N(:,:,3); d2=N(:,:,4); e2=N(:,:,5);
                 f2=N(:,:,6); g2=N(:,:,7); h2=N(:,:,8); i2=N(:,:,9);
                              j2=N(:,:,10);
                                           m2=N(:,:,11);
                                                        o2=N(:,:,12);
    
    detA=-a2.*m2.*o2.*g2.^2-a2.*j2.*o2.*h2.^2+a2.*j2.*m2.*f2.*o2-a2.*i2.^2.*m2.*j2+g2.^2.*m2.*e2.^2+g2.^2.*d2.^2.*o2-2*g2.*c2.*d2.*o2.*h2-...
        2*g2.*m2.*c2.*e2.*i2+2*g2.*m2.*b2.*c2.*o2+h2.^2.*c2.^2.*o2+h2.^2.*j2.*e2.^2-2*h2.*d2.*e2.*j2.*i2+2*h2.*j2.*b2.*d2.*o2-f2.*c2.^2.*m2.*o2-...
        j2.*m2.*f2.*e2.^2+2*i2.*m2.*j2.*b2.*e2-j2.*b2.^2.*m2.*o2+i2.^2.*m2.*c2.^2-f2.*j2.*d2.^2.*o2+i2.^2.*d2.^2.*j2;
    detA(find(detA>=0 & detA<eps))=eps;
    detA(find(detA<0 & detA>-eps))=-eps;
    
    % Normal matrix
    a1=(-m2.*o2.*g2.^2-j2.*o2.*h2.^2+j2.*m2.*f2.*o2-i2.^2.*m2.*j2)./detA;
    b1=-(-c2.*o2.*m2.*g2-j2.*e2.*m2.*i2+b2.*o2.*j2.*m2-d2.*o2.*h2.*j2)./detA;
    c1=(-g2.*d2.*o2.*h2-g2.*i2.*m2.*e2+g2.*b2.*m2.*o2+c2.*o2.*h2.^2-c2.*m2.*f2.*o2+i2.^2.*m2.*c2)./detA;
    d1=(d2.*o2.*g2.^2-o2.*c2.*h2.*g2+j2.*d2.*i2.^2-e2.*j2.*h2.*i2-j2.*f2.*o2.*d2+b2.*o2.*j2.*h2)./detA;
    e1=(m2.*e2.*g2.^2-c2.*i2.*m2.*g2+j2.*e2.*h2.^2-j2.*i2.*d2.*h2-j2.*m2.*f2.*e2+i2.*m2.*j2.*b2)./detA;
    f1=(j2.*o2.*m2.*a2-m2.*c2.^2.*o2-m2.*j2.*e2.^2-o2.*d2.^2.*j2)./detA;
    g1=-(m2.*o2.*g2.*a2-g2.*d2.^2.*o2-g2.*m2.*e2.^2+c2.*d2.*o2.*h2+m2.*c2.*e2.*i2-m2.*b2.*c2.*o2)./detA;
    h1=-(o2.*j2.*h2.*a2+c2.*d2.*o2.*g2+d2.*e2.*j2.*i2-j2.*b2.*d2.*o2-e2.^2.*j2.*h2-o2.*c2.^2.*h2)./detA;
    i1=-(i2.*m2.*j2.*a2+c2.*e2.*m2.*g2+d2.*e2.*j2.*h2-i2.*d2.^2.*j2-b2.*e2.*m2.*j2-i2.*m2.*c2.^2)./detA;
    j1=(-a2.*o2.*h2.^2-a2.*i2.^2.*m2+a2.*f2.*m2.*o2+e2.^2.*h2.^2+2*h2.*b2.*d2.*o2-2*h2.*d2.*e2.*i2+i2.^2.*d2.^2-b2.^2.*m2.*o2+2*i2.*m2.*b2.*e2-...
        f2.*m2.*e2.^2-f2.*d2.^2.*o2)./detA;
    k1=(g2.*h2.*o2.*a2+g2.*i2.*d2.*e2-g2.*b2.*d2.*o2-g2.*h2.*e2.^2-c2.*d2.*i2.^2+c2.*e2.*h2.*i2-b2.*c2.*h2.*o2+c2.*d2.*o2.*f2)./detA;
    l1=(g2.*i2.*m2.*a2+c2.*e2.*m2.*f2+g2.*h2.*d2.*e2-g2.*b2.*e2.*m2+c2.*d2.*h2.*i2-c2.*e2.*h2.^2-g2.*i2.*d2.^2-b2.*c2.*i2.*m2)./detA;
    m1=(-a2.*o2.*g2.^2-a2.*j2.*i2.^2+a2.*j2.*f2.*o2+e2.^2.*g2.^2-2*g2.*c2.*e2.*i2+2*g2.*o2.*b2.*c2+c2.^2.*i2.^2+2*j2.*b2.*e2.*i2-...
        j2.*b2.^2.*o2-j2.*f2.*e2.^2-f2.*c2.^2.*o2)./detA;
    n1=(j2.*h2.*i2.*a2+j2.*d2.*e2.*f2+g2.*h2.*c2.*e2+c2.*d2.*g2.*i2-g2.^2.*d2.*e2-c2.^2.*h2.*i2-j2.*b2.*d2.*i2-j2.*h2.*b2.*e2)./detA;
    o1=(-a2.*g2.^2.*m2-a2.*j2.*h2.^2+a2.*j2.*m2.*f2+g2.^2.*d2.^2-2*g2.*h2.*c2.*d2+2*g2.*b2.*c2.*m2+c2.^2.*h2.^2+2*j2.*h2.*b2.*d2-...
        f2.*c2.^2.*m2-j2.*b2.^2.*m2-f2.*j2.*d2.^2)./detA;


    B1=U(:,:,1);
    B2=U(:,:,2);
    B3=U(:,:,3);
    B4=U(:,:,4);
    B5=U(:,:,5);
    
    u  =a1.*B1+b1.*B2+c1.*B3+d1.*B4+e1.*B5;
    v  =b1.*B1+f1.*B2+g1.*B3+h1.*B4+i1.*B5;
    um1=c1.*B1+g1.*B2+j1.*B3+k1.*B4+l1.*B5;
    um2=d1.*B1+h1.*B2+k1.*B3+m1.*B4+n1.*B5;
    um3=e1.*B1+i1.*B2+l1.*B3+n1.*B4+o1.*B5;
    
    t1= (u.*a2+v.*b2+um1.*c2+um2.*d2+um3.*e2).*u+...
        (u.*b2+v.*f2+um1.*g2+um2.*h2+um3.*i2).*v+...
        (u.*c2+v.*g2+um1.*j2).*um1+...
        (u.*d2+v.*h2+um2.*m2).*um2+...
        (u.*e2+v.*i2+um3.*o2).*um3;
    t2=-2*(u.*B1+v.*B2+um1.*B3+um2.*B4+um3.*B5);
    t3=ItIt; 
    vf=(t1+t2+t3)/(3*ws*ws-5);
%    disp(sprintf('min. of vf, a1, e1: (%f %f %f)',min(vf(:)),min(a1(:)),min(f1(:))))
    if min(vf(:))<0
        disp('')
    end

    temp=GetCondNumber(a1,b1,f1);
    Cu=GetCov(temp,a1,vf);
    Cv=GetCov(temp,f1,vf);
end


function [u,v]=BoundSolution(u,u0,v,v0,bound,ws)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Bound the flow based on the previous estimation
u=(u>=(u0+bound)).*(u0+bound)+(u<(u0+bound)).*u;
u=(u<=(u0-bound)).*(u0-bound)+(u>(u0-bound)).*u;

v=(v>=(v0+bound)).*(v0+bound)+(v<(v0+bound)).*v;
v=(v<=(v0-bound)).*(v0-bound)+(v>(v0-bound)).*v;


% Get rid of the borders
if min(size(u))>2*ws
    u(:,1:(ws-1))=u(:,ws:(2*ws-2));
    u(:,(end-ws+1):end)=u(:,(end-2*ws+1):(end-ws));
    u(1:(ws-1),:)=u(ws:(2*ws-2),:);
    u((end-ws+1:end),:)=u((end-2*ws+1):(end-ws),:);
    
    v(:,1:(ws-1))=v(:,ws:(2*ws-2));
    v(:,(end-ws+1):end)=v(:,(end-2*ws+1):(end-ws));
    v(1:(ws-1),:)=v(ws:(2*ws-2),:);
    v((end-ws+1:end),:)=v((end-2*ws+1):(end-ws),:);
end

% Fill out the points that haven't been updated
u(find(isnan(u)))=u0(find(isnan(u)));
v(find(isnan(v)))=v0(find(isnan(v)));

if mod(ws,2)
    u=my_medfilt2(u,[ws ws]);
    v=my_medfilt2(v,[ws ws]);
else
    u=my_medfilt2(u,[ws+1 ws+1]);
    v=my_medfilt2(v,[ws+1 ws+1]);
end

% Get rid of the borders again
if min(size(u)) > 2*ws
    u(:,1:(ws-1))=u(:,ws:(2*ws-2));
    u(:,(end-ws+1):end)=u(:,(end-2*ws+1):(end-ws));
    u(1:(ws-1),:)=u(ws:(2*ws-2),:);
    u((end-ws+1:end),:)=u((end-2*ws+1):(end-ws),:);
    
    v(:,1:(ws-1))=v(:,ws:(2*ws-2));
    v(:,(end-ws+1):end)=v(:,(end-2*ws+1):(end-ws));
    v(1:(ws-1),:)=v(ws:(2*ws-2),:);
    v((end-ws+1:end),:)=v((end-2*ws+1):(end-ws),:);
end


function I3=NormalEq(h,I1,I2,mask,wgt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global FLAG;

% Mask out the moments of the image
% if size(h,1)==1 & size(h,2)==1
if FLAG==1
    I3=mean2(I1.*I2.*mask);
else
    I3=my_filter2(h,I1.*I2.*wgt);
    I3=I3.*mask;
end


function [It,mask]=CalIt(I1,I2, Ix,Iy, u0,v0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

It=I2-I1;
mask=zeros(size(I1));
for j=1:size(I1,3),
    mask(:,:,j)=(It(:,:,j)~=0) & (Ix(:,:,j)~=0) & (Iy(:,:,j)~=0);
    It(:,:,j)=It(:,:,j)-Ix(:,:,j).*u0-Iy(:,:,j).*v0;
end


function wgt=CalWgt(Ix,Iy,Cu,Cv,ws)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C=Cu+Cv;
C(find(abs(C)<=eps))=1/eps;
for j=1:size(Ix,3),
    wgt(:,:,j)=1./C;
end
wgt=ones(size(Ix));

% t=GetIntensityNumber(Ix,Iy,ws);
% t(find(t==0))=eps;
% wgt=wgt./t;

% for j=1:size(Ix,3),
%     wgt(:,:,j)=Ix(:,:,j).^2+Iy(:,:,j).^2;
% end


function [N,U,ItIt]=GetNU(h,I1,Ix,Iy,It,wgt,mask)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(Ix,3)==1,
    N(:,:,1)=NormalEq(h,Ix,Ix,mask,wgt);
    N(:,:,2)=NormalEq(h,Ix,Iy,mask,wgt);
    N(:,:,3)=-NormalEq(h,Ix,I1,mask,wgt);
    N(:,:,4)=NormalEq(h,Iy,Iy,mask,wgt);
    N(:,:,5)=-NormalEq(h,Iy,I1,mask,wgt);
    N(:,:,6)=NormalEq(h,I1,I1,mask,wgt);
    
    U(:,:,1)=-NormalEq(h,It,Ix,mask,wgt);
    U(:,:,2)=-NormalEq(h,It,Iy,mask,wgt);
    U(:,:,3)=NormalEq(h,It,I1,mask,wgt);
    
    ItIt=NormalEq(h,It,It,mask,wgt);
elseif size(Ix,3)==3,
    N(:,:,1)=...
        NormalEq(h,Ix(:,:,1),Ix(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,Ix(:,:,2),Ix(:,:,2),mask(:,:,2),wgt(:,:,2))+...
        NormalEq(h,Ix(:,:,3),Ix(:,:,3),mask(:,:,3),wgt(:,:,3));
    N(:,:,2)=...
        NormalEq(h,Ix(:,:,1),Iy(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,Ix(:,:,2),Iy(:,:,2),mask(:,:,2),wgt(:,:,2))+...
        NormalEq(h,Ix(:,:,3),Iy(:,:,3),mask(:,:,3),wgt(:,:,3));
    N(:,:,3)=-NormalEq(h,Ix(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    N(:,:,4)=-NormalEq(h,Ix(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,5)=-NormalEq(h,Ix(:,:,3),I1(:,:,3),mask(:,:,3),wgt(:,:,3));
    N(:,:,6)=...
        NormalEq(h,Iy(:,:,1),Iy(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,Iy(:,:,2),Iy(:,:,2),mask(:,:,2),wgt(:,:,2))+...
        NormalEq(h,Iy(:,:,3),Iy(:,:,3),mask(:,:,3),wgt(:,:,3));
    N(:,:,7)=-NormalEq(h,Iy(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    N(:,:,8)=-NormalEq(h,Iy(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,9)=-NormalEq(h,Iy(:,:,3),I1(:,:,3),mask(:,:,3),wgt(:,:,3));
    N(:,:,10)=NormalEq(h,I1(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    N(:,:,11)=NormalEq(h,I1(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,12)=NormalEq(h,I1(:,:,3),I1(:,:,3),mask(:,:,3),wgt(:,:,3));
    
    U(:,:,1)=-...
        NormalEq(h,It(:,:,1),Ix(:,:,1),mask(:,:,1),wgt(:,:,1))-...
        NormalEq(h,It(:,:,2),Ix(:,:,2),mask(:,:,2),wgt(:,:,2))-...
        NormalEq(h,It(:,:,3),Ix(:,:,3),mask(:,:,3),wgt(:,:,3));
    U(:,:,2)=-...
        NormalEq(h,It(:,:,1),Iy(:,:,1),mask(:,:,1),wgt(:,:,1))-...
        NormalEq(h,It(:,:,2),Iy(:,:,2),mask(:,:,2),wgt(:,:,2))-...
        NormalEq(h,It(:,:,3),Iy(:,:,3),mask(:,:,3),wgt(:,:,3));
    U(:,:,3)=NormalEq(h,It(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    U(:,:,4)=NormalEq(h,It(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    U(:,:,5)=NormalEq(h,It(:,:,3),I1(:,:,3),mask(:,:,3),wgt(:,:,3));
    
    ItIt=...
        NormalEq(h,It(:,:,1),It(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,It(:,:,2),It(:,:,2),mask(:,:,2),wgt(:,:,2))+...
        NormalEq(h,It(:,:,3),It(:,:,3),mask(:,:,3),wgt(:,:,3));
else
    N(:,:,1)=...
        NormalEq(h,Ix(:,:,1),Ix(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,Ix(:,:,2),Ix(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,2)=...
        NormalEq(h,Ix(:,:,1),Iy(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,Ix(:,:,2),Iy(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,3)=-NormalEq(h,Ix(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    N(:,:,4)=-NormalEq(h,Ix(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,5)=...
        NormalEq(h,Iy(:,:,1),Iy(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,Iy(:,:,2),Iy(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,6)=-NormalEq(h,Iy(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    N(:,:,7)=-NormalEq(h,Iy(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    N(:,:,8)=NormalEq(h,I1(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    N(:,:,9)=NormalEq(h,I1(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    
    U(:,:,1)=-...
        NormalEq(h,It(:,:,1),Ix(:,:,1),mask(:,:,1),wgt(:,:,1))-...
        NormalEq(h,It(:,:,2),Ix(:,:,2),mask(:,:,2),wgt(:,:,2));
    U(:,:,2)=-...
        NormalEq(h,It(:,:,1),Iy(:,:,1),mask(:,:,1),wgt(:,:,1))-...
        NormalEq(h,It(:,:,2),Iy(:,:,2),mask(:,:,2),wgt(:,:,2));
    U(:,:,3)=NormalEq(h,It(:,:,1),I1(:,:,1),mask(:,:,1),wgt(:,:,1));
    U(:,:,4)=NormalEq(h,It(:,:,2),I1(:,:,2),mask(:,:,2),wgt(:,:,2));
    
    ItIt=...
        NormalEq(h,It(:,:,1),It(:,:,1),mask(:,:,1),wgt(:,:,1))+...
        NormalEq(h,It(:,:,2),It(:,:,2),mask(:,:,2),wgt(:,:,2));
end


function C=GetCondNumber(a,b,c)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lambda1=a+c+sqrt((a+c).^2-4*(a.*c-b.*b));
lambda1(find(lambda1==0))=eps;
lambda2=a+c-sqrt((a+c).^2-4*(a.*c-b.*b));
lambda2(find(lambda2==0))=eps;
C=abs(lambda1./lambda2);
idx=find(C<1);
C(idx)=abs(lambda2(idx)./lambda1(idx));


function C=GetIntensityNumber(Ix,Iy,ws)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Iy(find(Iy==0))=eps;
C=my_filter2(ones(ws),abs(Ix./Iy));
% C=C/max(C(:));


function cov=GetCov(cond_number,a,vf)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cov=abs(a.*vf.*cond_number);%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stuff related to robust implementation, which I have to add later
% % computation of residual vectors
% r= ...
%     ExpandMatrix(h,Ix).*kron(u,h)+ ...
%     ExpandMatrix(h,Iy).*kron(v,h)- ...
%     ExpandMatrix(h,I1).*kron(um,h)+ ...
%     ExpandMatrix(h,It);
% if size(h,1)==1 & size(h,2)==1
%     rn=r;
% else
%     rn=r./kron(MyArith('std',h,r,size(Ix)),h);
% end
% 
% % determination of the new weights
% wgt=1./sqrt(1+rn.^2);
% 
% dum1=[];
% dum2=[];
%
% function I3=NormalEq(h,I1,I2,mask,wgt)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % Mask out the moments of the image
% if size(h,1)==1 & size(h,2)==1
%     I3=mean2(I1.*I2.*mask);
% else
%     [i,j]=size(I1);
%     [ih,jh]=size(h);
%     I1=ExpandMatrix(h,I1);
%     I2=ExpandMatrix(h,I2);
%     I3=I1.*I2.*wgt;
%     I3=im2col(I3,[ih jh],'distinct');
%     I3=sum(I3);
%     I3=reshape(I3,i,j);
% end
% 
% function rm=MyArith(action,h,r,siz)
% %%%%%%%%%%%%%%%%%%%%%%%%
% 
% rm=im2col(r,size(h),'distinct');
% eval(['rm=' action '(rm,1);'])
% rm=reshape(rm',siz);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
