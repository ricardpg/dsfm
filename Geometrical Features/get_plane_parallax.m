function [ur vr overlapMask] = get_plane_parallax(P3D, view1, view2, ffid, offset)
global width height
if ~exist('offset', 'var')
    offset = 0;
end
feat = [];
for pp = 1:numel(P3D)
    pos1 = find([P3D(pp).view(:).index]==view1);
    pos2 = find([P3D(pp).view(:).index]==view2);
    if numel(pos1) && numel(pos2)
        feat = [feat [P3D(pp).view(pos1).pos(1:2); P3D(pp).view(pos2).pos(1:2)]];
    end
end
H = CalcHomoPrj ( feat(1:2,:), feat(3:4,:), 1 );
im1 = data_read(ffid, view1+offset);
im2 = data_read(ffid, view2+offset);

im1w = warpImageH(im1, H);
overlapMask = warpImageH(255*ones(height, width), H);
%    mask = (mask(:,:,1)==255) & (mask(:,:,2)==0) & (mask(:,:,3)==0);
se = strel('disk',6);
overlapMask = imerode(overlapMask,se);
[ur,Cu,vr,Cv,um1,um2,um3]=optical_flow_plane(double(im2),double(im1w),7,5,3,5,'color_3', overlapMask);
figure, imagesc(sqrt(ur.^2+vr.^2));
figure, imagesc(im1w)
figure, imagesc(im2)