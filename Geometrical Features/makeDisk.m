function disk = makeDisk(radius)
disk = ones(radius*2+1);
center = [radius+1 radius+1];
for y = 1:(radius*2+1)
    for x = 1:(radius*2+1)
        distance = sqrt((center(1)-x)^2 + (center(2)-y)^2);
        if distance<=radius
            disk(y,x) = 0;
        end
    end
end