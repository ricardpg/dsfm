Im = function ImageRangeFixUint8(Im)
Im = double(Im);
MaxValue = max(max(max(Im)));
if MaxValue < 1.0
    Im = Im * 255
end
if MaxValue < 255.0
    Im = Im / 255;
end