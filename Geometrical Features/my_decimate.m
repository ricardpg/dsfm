function y=my_decimate(h,x)

[M,N,K]=size(x);
x=my_filter2(h,x);
for j=1:K,
    y(:,:,j)=x(1:2:M,1:2:N,j);
end
