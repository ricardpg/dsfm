DSFM Structure description
--------------------------

- PointType: Detection type.
- DescriptorType.

3D points related:
	- Status: Reconstruction status (3 == 3D recovered for that point)
	- IsGeometrical: ???.
	- IsInitial: Indicates if it is part of the initial model.
	- Cov: Covariance of each 3D point.
	- Pos: 3D Reconstructed point.
	- DescIndex: Correspondence between 3D and 2D indices.
	- TotalFrames: Number of frames from where the 3D point has been seen.
	- Desc: Descriptor of the 3D point.

2D points related:
	- ViewIndex: Frame where the P2D is located.
	- ViewP3DIndex: Corresponding point in 3D for a 2D point.
	- ViewPos: Pixel coordinates of the 2D points in the image.
	- ViewDesc: Descriptor of the 2D point.
	- ViewSaliency: ???.
