function [ Descriptors, Frames ] = UVL_SFM_surfDescriptorBuiltin ( InputImg, Frames ) %, doubleImageSize, upright, extended, indexSize )

% Construct the SurfFeatures structure
if size( Frames, 1 ) < 5,
  % Do not specify orientation, let the extractFeatures decide  
  SurfFeat = SURFPoints( Frames( 1:2, : )' ) ;  
else
  % SurfFeat = SURFPoints( Frames( 1:2, : )', 'Scale', Frames( 3, : ), 'Orientation', Frames( 4, : ), 'Metric', Frames( 5, : ) ) ;  
  SurfFeat = SURFPoints( Frames( 1:2, : )', 'Scale', Frames( 3, : ), 'Metric', Frames( 5, : ) ) ;  
end

% Compute the descriptors
[ Descriptors, VALID_POINTS ] = extractFeatures( InputImg, SurfFeat ) ;
Descriptors = double( Descriptors' ) ;

if nargout > 1,
  % Fill the other parameters for Frames
  Frames( 5, : ) = Frames( 3, : ) ; % Preserve the strength of the original detector
  Frames( 3, : ) = double( [ VALID_POINTS(:).Scale ] ) ; 
  Frames( 4, : ) = double( [ VALID_POINTS(:).Orientation ] ) ; 
end