function Descriptors = UVL_SFM_surfDescriptor ( InputImg, Frames, doubleImageSize, ...
                                        upright, extended, indexSize )

    % Test Frames Size: (x, y, Scale and Orientation)
    if size(Frames, 1) ~= 5
    error ( 'MATLAB:SurfDescriptor:Input', ...
            'Input Frames must have 5 columns: X, Y, Scale, Orientation and Strength (Goodness)!' );
    end
  
    imwrite(InputImg, '~tmp.pgm');

    %Frames = Frames([1 2 3 5 4],:);
    fid = fopen('~SURF_full_feat.txt', 'w');
    fprintf(fid, '%d\n', size(Frames,1));
    fprintf(fid, '%d\n', size(Frames,2));
    fprintf(fid, '%.5f %.5f %.5f %.5f %.5f\n', Frames);
    fclose(fid);

    options = [];
    if exist('doubleImageSize', 'var') && doubleImageSize,
        options = [options ' -d'];
    end
    if exist('upright', 'var') && upright,
        options = [options ' -u'];
    end
    if exist('extended', 'var') && extended,
        options = [options ' -e'];
    end
    if exist('indexSize', 'var'),
        options = [options ' -in ' num2str(indexSize)];
    end

    system(['"' which('surfWINDLLDemo.exe') '" -mode 3 -i ~tmp.pgm -p1 ~SURF_full_feat.txt -o ~SURF_desc.txt' options]);

    fid = fopen('~SURF_desc.txt', 'rt');
    descriptor_len = fscanf(fid,'%d',[1 1]);
    descriptor_num = fscanf(fid,'%d', [1 1]);
    Descriptors = fscanf(fid,'%f',[descriptor_len descriptor_num]);
    fclose(fid); 

    delete '~SURF_full_feat.txt'
    delete '~SURF_desc.txt'
    delete '~tmp.pgm'
end
