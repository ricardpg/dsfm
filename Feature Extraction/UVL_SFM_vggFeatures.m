function Frames = UVL_SFM_vggFeatures(InputImg, Mask, Type, MaxNumber, Threshold, Radius)

imwrite(InputImg, '~tmp.png');
system(['"' which('extract_features.exe') '" -' Type ' -i ~tmp.pgm -sift -o2 ~tmp.sift']);

descf = fopen('~tmp.sift', 'rt');
descriptor_len = fscanf(descf,'%d',[1 1]);
descriptor_num = fscanf(descf,'%d', [1 1]);
Frames = fscanf(descf,'%f',[descriptor_len+13 descriptor_num]);
fclose(descf); 

for i = 1:size(features,2)
    Frames(14:end,i) = Frames(14:end,i)/norm(Frames(14:end,i));
end

[h w] = size ( InputImg );
Frames = UVL_SFM_spreadFeatures(Frames, 13, h, w, Mask, Radius, MaxNumber, Threshold);

delete '~tmp.pgm';
delete '~tmp.sift';




