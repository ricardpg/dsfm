
function [ Frames, GoodIndex ] = UVL_SFM_spreadFeatures(Frames, CornNum, ImHeight, ...
                      ImWidth, Mask, Radius, MaxNumber, Threshold )

error ( nargchk ( 7, 8, nargin ) );
error ( nargoutchk ( 1, 2, nargout ) );

if nargin == 5; Threshold = 0; end
if isempty(Mask); Mask = true( ImHeight, ImWidth ); end
Mask(1:Radius, :) = false;
Mask(end+1-Radius:end, :) = false;
Mask(:, 1:Radius) = false;
Mask(:, end+1-Radius:end) = false;

% % preparation of circular mask for suppression
% if Radius > 0
%     fmask = ~ceil(fspecial('disk',Radius));
% else
%     fmask = 0;
% end

% sorting according to cornerness
[ignore,ind] = sort(abs(Frames(CornNum,:)));

% Take only the good points (non-maxima, threshold, MaxNumber and Mask)
GoodIndex = zeros(1,MaxNumber);

count = 0;
i = numel(ind);
while i>0 && count < MaxNumber && Frames(CornNum,ind(i)) >= Threshold
    x = round(Frames(1,ind(i)));
    y = round(Frames(2,ind(i)));
    try
    if Mask(y,x),
        count = count + 1;
        GoodIndex(count) = ind(i);
        Mask(y-Radius:y+Radius, x-Radius:x+Radius) = false;
%        Mask(y-Radius:y+Radius, x-Radius:x+Radius) .* fmask;
    end
    catch
      % Outside Mask's limits! WHY???
      disp('problem') ;
    end
    i=i-1;
end
GoodIndex = GoodIndex(1:count);
Frames = Frames(:,GoodIndex);

% -------------------------------------------------------------------------
