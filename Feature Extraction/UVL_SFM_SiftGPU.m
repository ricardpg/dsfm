
function [ Frames, Descriptors ] = UVL_SFM_SiftGPU( InputImg, Mask, MaxNumber, Threshold, Radius )

% Detect & describe features
[ Frames, Descriptors ] = SiftGPUMx( InputImg ) ;

Frames( 5, : ) = Frames( 3, : ) ; % Dummy Strength value, we do not know how to compute it...

% Change the type, since all coming functions expect doubles...
Frames = double( Frames ) ;
Descriptors = double( Descriptors ) ;

% Non-Maximal suppression
[ h, w ] = size ( InputImg ) ;
[ Frames, GoodIndexes ] = UVL_SFM_spreadFeatures( Frames, 5, h, w, Mask, Radius, MaxNumber, Threshold ) ;

% Remove non-used Descriptors
Descriptors = Descriptors( :, GoodIndexes ) ;