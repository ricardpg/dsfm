function Features = UVL_SFM_calculateDescriptors(I, Features, Description, ResizeCoefficient, Verbose )

if ~exist('Verbose', 'var'), Verbose = true ; end

if size(Description.Type, 2) ~= 1 && size(Description.Type, 2) ~= size(Features, 2),
    error ('MATLAB:UVL_SFM_calculateDescriptors', 'No valid pairs of detector - descriptor');
elseif size(Description.Type, 2) == 1 && size(Features, 2) ~= 1,
    Description.Type = ones(1,size(Features, 2)) .* Description.Type;
end

for i = 1 : size(Features,2)
    % check if field exists and is filled
    if i <= numel(Features)
        if Description.Type(i) == 10,
          % --> SIFTGPU
          % Save the descriptors directly into the cache, they should have been computed already
          Features(i).DescriptorType = Description.Type(i);
          Features(i).DescriptorTypeName = Description.Descriptors{Description.Type(i)};
          % check if DescriptionCacheFile was set
          if isfield(Features(i), 'DescriptionCacheFile') && ~isempty(Features(i).DescriptionCacheFile),
              CacheData.Descriptors = Features(i).Descriptors;
              CacheData.DescriptorType = Features(i).DescriptorType;
              CacheData.DescriptorTypeName = Features(i).DescriptorTypeName;
              save(Features(i).DescriptionCacheFile, 'CacheData');
          end
          continue
        end
      
        if isfield(Features(i), 'Descriptors') && numel(Features(i).Descriptors),
            continue % if filled, skip
        elseif isfield(Features(i), 'DescriptionCacheFile') && numel(Features(i).DescriptionCacheFile),
            if exist(Features(i).DescriptionCacheFile, 'file')
                load(Features(i).DescriptionCacheFile)
                Features(i).Descriptors = CacheData.Descriptors;
                Features(i).DescriptorType = CacheData.DescriptorType;
                if Verbose, UVL_SFM_printOut(sprintf(' [Loaded]')); end
                continue
            end
        end
    end
    
    

    if numel(Features(i).Points),
        Features(i).Points(1:2, :) = Features(i).Points(1:2, :) * ResizeCoefficient;
        if Description.Type(i) == 1, %SIFT
            if any (Features(i).PointType == [1 2 3 6]),
                Features(i).Points = CalculateOrientation(I, Features(i).Points, 'SIFT');
                Features(i).Descriptors = SiftDescriptor(I, Features(i).Points(1:6,:));
            elseif Features(i).PointType == 4,
                Features(i).Descriptors = SiftDescriptor(I, Features(i).Points(1:6,:));
            elseif Features(i).PointType == 5,
                error('MATLAB:UVL_SFM_calculateDescriptors', 'Invalid combination of a SURF detector and other descriptor');
            elseif any(Features(i).PointType == [7 8 9 10 11]),
                %sift is already calculated
                Features(i).Descriptors = Features(i).Points(14:end,:);
                Features(i).Points = Features(i).Points(1:13,:);
            end
        elseif any(Description.Type(i) == [2 3 4 5 6 7]), %SURF
            if any (Features(i).PointType == [1 2 3 6]),
                if verLessThan('matlab', '8.0'),
%                   Features(i).Points = CalculateOrientation(I, Features(i).Points, 'SURF');
                  Features(i).Points = UVL_SFM_CalculateOrientation(I, Features(i).Points, 'SURF');
                  switch Description.Type(i) %SurfDescriptor
                      case 2, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, false, false, 4);
                      case 3, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, true, false, 4);
                      case 4, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, false, true, 4);
                      case 5, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, true, true, 4);
                      case 6, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, false, false, 3);
                      case 7, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, true, false, 3);
                  end
                else
                  % NOTE: Using the builtin function, the original surf description parameters are ignored...
                  [ Features(i).Descriptors Features(i).Points ] = UVL_SFM_surfDescriptorBuiltin(I, Features(i).Points ) ;                                    
                end
            elseif Features(i).PointType == 4,
                error('MATLAB:UVL_SFM_calculateDescriptors', 'Invalid combination of a SIFT detector and other descriptor');
            elseif Features(i).PointType == 5,
              if verLessThan('matlab', '8.0'),
                switch Description.Type(i)
                    case 2, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, false, false, 4);
                    case 3, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, true, false, 4);
                    case 4, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, false, true, 4);
                    case 5, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, true, true, 4);
                    case 6, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, false, false, 3);
                    case 7, Features(i).Descriptors = UVL_SFM_surfDescriptor(I, Features(i).Points(1:5,:), false, true, false, 3);
                end
              else
                % NOTE: Using the builtin function, the original surf description parameters are ignored...
                  Features(i).Descriptors = UVL_SFM_surfDescriptorBuiltin(I, Features(i).Points(1:5,:) ) ;                                    
              end
                Features(i).DescriptorType = Description.Type(i);
            elseif any(Features(i).PointType == [7 8 9 10 11]),
                %sift is already calculated
                error('MATLAB:UVL_SFM_calculateDescriptors', 'SIFT descriptors already computed');
            end
        elseif Features(i).PointType == 8,
            %????
        elseif Features(i).PointType == 9,
            %????
        end
        Features(i).Points(1:2, :) = Features(i).Points(1:2, :) / ResizeCoefficient;
    end
    Features(i).DescriptorType = Description.Type(i);
    Features(i).DescriptorTypeName = Description.Descriptors{Description.Type(i)};
    % check if DescriptionCacheFile was set
    if isfield(Features(i), 'DescriptionCacheFile') && ~isempty(Features(i).DescriptionCacheFile),
        CacheData.Descriptors = Features(i).Descriptors;
        CacheData.DescriptorType = Features(i).DescriptorType;
        CacheData.DescriptorTypeName = Features(i).DescriptorTypeName;
        save(Features(i).DescriptionCacheFile, 'CacheData');
    end
end