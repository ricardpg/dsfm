function Features = UVL_SFM_detectFeatures(I, Features, Detection, ResizeCoefficient, Verbose)

if ~exist('Verbose', 'var'), Verbose = true ; end

UseProvidedMask = false;
if exist('Mask', 'var'), UseProvidedMask = true; end

[h w] = size(I);
for i=1:length(Detection.Type)
    % check if field exists and is filled
    if numel(Features) >= i
        if isfield(Features(i), 'Points')
            if numel(Features(i).Points)
                % if filled then skip
                continue
            end
        end
    end
    
    if ~UseProvidedMask,
        Border = Detection.(Detection.Detectors{Detection.Type(i)}).Border;
        Mask = false(h, w);
        Mask(Border+1 : h-Border-1, Border+1 : w-Border-1) = true;
    end
    
    switch Detection.Type(i)
        case 1, %Harris
            Features(i).Points = HarrisFeatures(I, Mask, 1, Detection.Harris.MaxNumber, 0, Detection.Harris.RadiusNonMaximal, 1);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' Har: %d,', size(Features(i).Points,2))); end
        case 2, %Laplacian
            Features(i).Points = LaplacianFeatures(I, Mask, 1, Detection.Laplacian.MaxNumber, 0, Detection.Laplacian.RadiusNonMaximal, 1);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' Lap: %d,', size(Features(i).Points,2))); end
        case 3, %Hessian
            Features(i).Points = HessianFeatures(I, Mask, 1, Detection.Hessian.MaxNumber, 0, Detection.Hessian.RadiusNonMaximal, 1);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' Hes: %d,', size(Features(i).Points,2))); end
        case 4, %SIFT
            Features(i).Points = SiftFeatures(I, Mask, Detection.SIFT.MaxNumber, 0, Detection.SIFT.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' SIFT: %d,', size(Features(i).Points,2))); end
        case 5, %SURF     %SurfFeatures
            if verLessThan('matlab', '8.0')
              % For MATLAB older than MATLAB 8.0, use the surf exe
              Features(i).Points = UVL_SFM_surfFeatures(I, Mask, Detection.SURF.MaxNumber, 0.0001, Detection.SURF.RadiusNonMaximal);
            else
              % In newer versions of MATLAB, use the builtin function
              % (already computes the descriptors)
              Features(i).Points = UVL_SFM_surfFeaturesBuiltin(I, Mask, Detection.SURF.MaxNumber, 0.0001, Detection.SURF.RadiusNonMaximal);
            end            
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' SURF: %d,', size(Features(i).Points,2))); end
        case 6, %MSER
            Features(i).Points = MserFeatures(I, Mask, Detection.MSER.MaxNumber, Detection.MSER.MinimumRegionSize, Detection.MSER.MinimumMargin, Detection.MSER.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' MSER: %d,', size(Features(i).Points,2))); end
        case 7, %VGG Harris-Laplace
            Features(i).Points = UVL_SFM_vggFeatures(I, Mask, 'harlap', Detection.HarLap.MaxNumber, 0, Detection.HarLap.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' HarLap: %d,', size(Features(i).Points,2))); end
        case 8, %VGG Hessian-Laplace
            Features(i).Points = UVL_SFM_vggFeatures(I, Mask, 'heslap', Detection.HesLap.MaxNumber, 0, Detection.HesLap.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' HesLap: %d,', size(Features(i).Points,2))); end
        case 9, %VGG Harris-Affine
            Features(i).Points = UVL_SFM_vggFeatures(I, Mask, 'haraff', Detection.HarAff.MaxNumber, 0, Detection.HarAff.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' HarAff: %d,', size(Features(i).Points,2))); end
        case 10, %Hessian-Affine
            Features(i).Points = UVL_SFM_vggFeatures(I, Mask, 'hesaff', Detection.HesAff.MaxNumber, 0, Detection.HesAff.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' HesAff: %d,', size(Features(i).Points,2))); end
        case 11, %Harris-Hessian-Laplace
            Features(i).Points = UVL_SFM_vggFeatures(I, Mask, 'harhes', Detection.HarHesLap.MaxNumber, 0, Detection.HarHesLap.RadiusNonMaximal);
            NanMask = sum(isnan(Features(i).Points));
            Features(i).Points = Features(i).Points(:, NanMask == 0);
            if Verbose, UVL_SFM_printOut(sprintf(' HarHesLap: %d,', size(Features(i).Points,2))); end
        case 12
            % Experimental: Detection and description is performed at the same time
            [ Features(i).Points, Features(i).Descriptors ] = UVL_SFM_SiftGPU(I, Mask, Detection.SIFTGPU.MaxNumber, 0, Detection.SIFTGPU.RadiusNonMaximal);
            if Verbose, UVL_SFM_printOut(sprintf(' SIFTGPU: %d,', size(Features(i).Points,2))); end
    end
    Features(i).Points(1:2, :) = Features(i).Points(1:2, :) / ResizeCoefficient;
    Features(i).PointType = Detection.Type(i);
    Features(i).PointTypeName = Detection.Detectors(Detection.Type(i));
    % check if DetectionCacheFile was set    
    if isfield(Features(i), 'DetectionCacheFile') && ~isempty(Features(i).DetectionCacheFile) && ~UseProvidedMask,
        CacheData.Points = Features(i).Points;
        CacheData.PointType = Features(i).PointType;
        CacheData.PointTypeName = Features(i).PointTypeName;
        save(Features(i).DetectionCacheFile, 'CacheData');
    end
end
