function Frames = UVL_SFM_surfFeatures( InputImg, Mask, MaxNumber, Threshold, Radius )

imwrite(InputImg, '~tmp.pgm');
Architecture = computer;
switch Architecture
    case {'PCWIN', 'PCWIN64'}
        system(['"' which('surfWINDLLDemo.exe') '" -mode 1 -i ~tmp.pgm -o ~SURF_features.txt -d -thres ' num2str(Threshold)]); 
        system(['"' which('surfWINDLLDemo.exe') '" -mode 2 -i ~tmp.pgm -p1 ~SURF_features.txt -o ~SURF_orientation.txt']);
    case 'GLNXA64'
        system(['"' which('surf.ln') '" -mode 1 -i ~tmp.pgm -o ~SURF_features.txt -d -thres ' num2str(Threshold)]); 
        system(['"' which('surf.ln') '" -mode 2 -i ~tmp.pgm -p1 ~SURF_features.txt -o ~SURF_orientation.txt']);
    otherwise
         error('MATLAB:UVL_SFM_surfFeatures', 'Unsupported PC Architecture (Yet:)');
end

descf = fopen('~SURF_features.txt', 'rt');
descriptor_len = fscanf(descf,'%d',[1 1]);
descriptor_num = fscanf(descf,'%d', [1 1]);
Frames = fscanf(descf,'%f',[descriptor_len descriptor_num]);
fclose(descf); 
Frames(5,:) = Frames(4,:);
descf = fopen('~SURF_orientation.txt', 'rt');
descriptor_len = fscanf(descf,'%d',[1 1]);
descriptor_num = fscanf(descf,'%d', [1 1]);
Frames(4,:) = fscanf(descf,'%f',[descriptor_len descriptor_num]);
fclose(descf); 

delete '~tmp.pgm';
delete '~SURF_features.txt';
delete '~SURF_orientation.txt';

[h w] = size ( InputImg );
Frames = UVL_SFM_spreadFeatures(Frames, 5, h, w, Mask, Radius, MaxNumber, Threshold);
