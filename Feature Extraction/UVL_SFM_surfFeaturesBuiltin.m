% function Frames = UVL_SFM_surfFeaturesBuiltin( InputImg, Mask, MaxNumber, Threshold, Radius )
%
% Description:Function to extract Surf points using matlab built-in function (requires matlab version > 2012b)
%
% INPUT:
%   (TODO)
%
% OUTPUT:
%    Frames: Matrix of features with assigned orientation, scale and in case
%            of SIFT descriptor, octave. Each column corresponds to one 
%            feature. X (row1), Y (row2), Scale (row3), Orientation (row4),
%            Goodness (row5) and only for 'SIFT' descriptor Octave (row6).
%

function Frames = UVL_SFM_surfFeaturesBuiltin( InputImg, Mask, MaxNumber, Threshold, Radius )

% Detect features
SurfFeat = detectSURFFeatures( InputImg, 'MetricThreshold', Threshold ) ;

xy = [SurfFeat(:).Location] ;
Frames( 1, : ) = double( xy( :, 1 ) ) ;
Frames( 2, : ) = double( xy( :, 2 ) ) ;
Frames( 3, : ) = double( [ SurfFeat(:).Scale ] ) ;
Frames( 4, : ) = double( [ SurfFeat(:).Orientation ] ) ; 
Frames( 5, : ) = double( [ SurfFeat(:).Metric ] ) ; % According to the documentation, the metric value is the Strength

% Non-Maximal suppression
[ h w ] = size ( InputImg ) ;
Frames = UVL_SFM_spreadFeatures( Frames, 5, h, w, Mask, Radius, MaxNumber, Threshold ) ;
