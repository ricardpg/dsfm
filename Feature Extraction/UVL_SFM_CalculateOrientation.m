%  CalculateOrientation ---------------------------------------------------
%
%  The function calculates orientation for the keypoints form detectors 
%  which do not provide orientation. The orientation is calculated using 
%  SIFT or SURF method depending on the specified and used afterwards 
%  Descriptor.
%
%  Author        : Jordi Ferrer Plana, Marina Kudinova, Ricard Campos
%  e-mail        : {jferrerp,kudinova,rcampos}@eia.udg.es
%  Working Group : Computer Vision and Robotics Group
%  Homepage      : http://vicorob.udg.es/
%  Project       : FIM
%  File          : CalculateOrientation.m
%  Date          : 19/06/2007 - 21/06/2007
%  Compiler      : MATLAB >= 7.0
%
%  ------------------------------------------------------------------------
%
%  U S A G E :
%
%  Frames = CalculateOrientation ( InputImg, FramesS, Descriptor );
%
%  I N P U T :
%
%  InputImg   - initial image, where the frames were detected.
%  FramesS    - matrix containing the list of detected features, each 
%               column corresponds to one feature. X (row1), Y (row2),
%               Goodness (row3).
%  Descriptor - type of the descriptor. Available: 'SIFT', 'SURF', 
%               'SURF-128', 'SURF-36', 'U-SURF', 'U-SURF-128', 'U-SURF-36'.
%
%  O U T P U T :
%
%  Frames - matrix of features with assigned orientation, scale and in case
%           of SIFT descriptor, octave. Each column corresponds to one 
%           feature. X (row1), Y (row2), Scale (row3), Orientation (row4),
%           Goodness (row5) and only for 'SIFT' descriptor Octave (row6).
%
% -------------------------------------------------------------------------

function Frames = CalculateOrientation ( InputImg, FramesS, Descriptor )

error ( nargchk ( 3, 3, nargin ) );
error ( nargoutchk ( 1, 1, nargout ) );

if isempty(FramesS); Frames = []; return; end

% Sort the keypoints by the Cornerness in ascending order
KPoints = sortrows ( FramesS', -3 );

% SIFT Frames -------------------------------------------------------------
%
%   X              ->  1 row
%   Y              ->  2 row
%   Scale          ->  3 row
%   Orientation    ->  4 row
%   Goodness       ->  5 row
%   Octave         ->  6 row

if strcmpi ( Descriptor, 'sift' ),
   
    % Compute:
    %   Scale, Orientation and Octave using the initial parameters of
    %   Lowe's Sift binary Implementation
    S = 3;
    SMin = -1;
    Sigma0 = 1.6 * 2 ^ ( 1 / S );
    Scale = 2 - 1 + SMin; % 2nd Img at DoG must be the first possible
    o = 2;
    % f = 2^(o-1+OMin);   % Factor to multiply the x, y, s ( == 1 )

    % Provide the Position at Second Octave and Scale 2 (and the index)
    oframes = [ KPoints(:,1), KPoints(:,2), ...
                repmat(Scale,size(KPoints,1),1), (1:size(KPoints,1))' ]';

    % Simulate the second image in an Octave of the Scale-Space
    %  Scale 0 ( 2 - 1 - SMin ) => 2on position in the octave
    % At least, in de DoG space, keypoints are at 2nd scale
    I(:,:,2) = InputImg;

    % Compute the Orientations
    ooframes = siftormx ( oframes, I, S, SMin, Sigma0 );
    
    % Remove duplicates and only keep the best orientation
    [Dumy, Idx] = unique ( ooframes ( 5, : ), 'first' );
    ooframes = ooframes ( :, Idx );

    % Compute the Goodness for duplicated frames
    Goodness = KPoints ( ooframes ( 5, : ), 3 );

    % Create the new frame vector: X, Y, Scale, Orientation, Octave
    Frames(1:4,:) = ooframes(1:4,:);               % x, y, --, Orientation
    Frames(3,:) = Sigma0 * 2.^(ooframes(3,:)/S);   % Scale
    Frames(5,:) = Goodness;                        % Goodness
    Frames(6,:) = o;                               % Octave

    
% SURF Frames -------------------------------------------------------------
%
%   X              ->  1 row
%   Y              ->  2 row
%   Scale          ->  3 row
%   Orientation    ->  4 row
%   Goodness       ->  5 row
    
else

%     if ~isempty(findstr('surf',lower(char(Descriptor))))
    
    Frames(1,:) = KPoints(:,1);  % X
    Frames(2,:) = KPoints(:,2);  % Y

    % Simulate Scale of the Original Image    
    Frames(3,:) = 1.2;
    
    % Calculation of SURF orientation
    if strcmp( computer, 'PCWIN64' ),    
      fid = fopen('~SURF_nonOriented_feat.txt', 'w');
      fprintf(fid, '%d\n', size(Frames,1));
      fprintf(fid, '%d\n', size(Frames,2));
      fprintf(fid, '%.5f %.5f %.5f %.5f %.5f\n', Frames);
      fclose(fid);

      imwrite(InputImg, '~tmp.pgm');
      Architecture = computer;
      switch Architecture
          case {'PCWIN', 'PCWIN64'}            
              system(['"' which('surfWINDLLDemo.exe') '" -mode 2 -i ~tmp.pgm -p1 ~SURF_nonOriented_feat.txt -o ~SURF_orientation.txt']);
          case 'GLNXA64'
              system(['"' which('surf.ln') '" -mode 2 -i ~tmp.pgm -p1 ~SURF_nonOriented_feat.txt -o ~SURF_orientation.txt']);
          otherwise
               error('MATLAB:UVL_SFM_surfFeatures', 'Unsupported PC Architecture (Yet:)');
      end
      descf = fopen('~SURF_orientation.txt', 'rt');
      descriptor_len = fscanf(descf,'%d',[1 1]);
      descriptor_num = fscanf(descf,'%d', [1 1]);
      Frames(4,:) = fscanf(descf,'%f',[descriptor_len descriptor_num]);
      fclose(descf); 

      delete '~SURF_nonOriented_feat.txt';
      delete '~SURF_orientation.txt';
    else
      Frames(4,:) = SurfOrientationMx ( InputImg, Frames(1:3,:) );
    end
    Frames(5,:) = KPoints(:,3);  % Goodness
    
% else
%     error ( 'MATLAB:DetectFeatures:Input', ...
%           [ 'Invalid type of descriptor! \n',...
%             'Available: SIFT, SURF, SURF-128, SURF-36,\n',...
%             '           U-SURF, U-SURF-128, U-SURF-36' ] );
%     Frames = [];
end

% -------------------------------------------------------------------------
