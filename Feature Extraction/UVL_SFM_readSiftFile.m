function data = UVL_SFM_readSiftFile(sift_file)
descf = fopen(sift_file, 'rt');
if descf == -1
    data = [];
    return;
end
descriptor_len = fscanf(descf,'%d',[1 1]);
descriptor_num = fscanf(descf,'%d', [1 1]);
data = fscanf(descf,'%f',[descriptor_len+13 descriptor_num]);
%data = data';
fclose(descf); 
    