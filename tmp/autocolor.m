%    AUTOCOLOR
%    --> This code adjusts the hue and saturation both.Since both these
%        constitute the color of an image,thus the overall color level
%        is optimally adjusted.

function autocolor
warning off
%--------------------------------------------------------------------------
[FileName,PathName] = uigetfile('*.jpg','Select any image');
if PathName~=0
    y= [PathName,FileName];
    img=imread(y);
    [m1 n1 r1]=size(img);
    if r1~=3
        error('AUTOCOLOR IS ONLY FOR COLORED IMAGES');
    else
        %------------------------------------------------------------------
        a= rgb2ntsc(img);
        %------------------------------------------------------------------
        my_limit=0.1;
        mean_adjustment=my_limit-mean(mean(a(:,:,2)));
        %------------------------------------------------------------------
        if mean_adjustment~=0
            for i=1:m1
                for j=1:n1
                    a(i,j,2)=a(i,j,2)+mean_adjustment;
                    if a(i,j,2)>1               a(i,j,2)=1;
                    elseif(a(i,j,2)<-1)         a(i,j,2)=-1;
                    end
                end
            end
        end
        %------------------------------------------------------------------
        my_limit=-0.02;
        mean_adjustment=my_limit-mean(mean(a(:,:,3)));
        %------------------------------------------------------------------
        if mean_adjustment~=0
            for i=1:m1
                for j=1:n1
                    a(i,j,3)=a(i,j,3)+mean_adjustment;
                    if a (i,j,3)>1                    a(i,j,3)=1;
                    elseif a(i,j,3)<-1                a(i,j,3)=-1;
                    end
                end
            end
        end
        %------------------------------------------------------------------
        image=ntsc2rgb(a);
        image=image.*255;
        output_image=uint8(image);
        %------------------------------------------------------------------
        [FileName,PathName] = uiputfile('*.jpg','Select any name for this enhanced image');
        if PathName~=0
            y= [PathName,FileName];
            imwrite(output_image,y);
        end
        figure,imshow(img),title('ORIGINAL IMAGE');
        figure,imshow(output_image),title('AUTO COLORED IMAGE');
        %------------------------------------------------------------------
    end
end 