border = 500;
im = imread('DSC_0142.JPG');
[height width depth] = size(im);
imc = imcrop(im, [border border width - 2 * border height - 2 * border]);
figure, imshow(imc), hold on,
img = rgb2gray(imc);
[y, x] = find(img == min(min(img)));
y = y(1);
x = x(1);
plot(x, y, 'rx'), hold on
lowIn = double(squeeze([im(y, x, :)]))' / 255;

[y, x] = find(img == max(max(img)));
y = y(1);
x = x(1);
plot(x, y, 'rx')
highIn = double(squeeze([im(y, x, :)]))' / 255;


%lowIn = [70 114 74]/255; % Original RGB for black
%highIn = [149 209 161]/255; % Original RGB for white (taken form sample on white tile)

lowOut = [5 5 5]/255; % Intended RGB for black
highOut = [250 250 250]/255; % Intended RGB for white

I1norm = double(im)/255;
I1norm = imadjust(I1norm,[lowIn; highIn],[lowOut; highOut]);
I1norm = uint8(I1norm*255);
figure, imshow(I1norm)